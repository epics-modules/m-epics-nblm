# sudo -E make install LIBVERSION=2.0.0
# launch IOC:
# iocsh -r nblm,2.0.0 -c "requireSnippet(nblm.cmd,'PREFIX=IFC1410_nBLM,DEVICE=PROTO')"

require nblm, 2.0.0

epicsEnvSet(AREA,               "$(AREA=CEA)") 
epicsEnvSet(DEVICE,             "PBI-nBLM")
epicsEnvSet(P_SY4527,           "SY4527")                   # service name of HV/LV
epicsEnvSet(LV_SLOT,            "10")
epicsEnvSet(SLOW,               "SLOW")
epicsEnvSet(FAST,               "FAST")


# same config on all channels
dbLoadRecords("hv_test_all_ch.db", "AREA=${AREA}")

iocInit
