# quick save and restore in/from a file
# file format:
# <pv_value11> <pv_name1>
# <pv_value12> <pv_name2>
#   ...

import os       # check if file exists
import sys      # shell arg
import time     # get date for file name
import epics    # epics channel acces (caget(), caput())

from save_and_restore_like_user_config import * # user config: PV list

# import my epics functions
sys.path.insert(0, m_epics_PATH + "m-epics-nblm/misc")
from epicsFunctions import * # secure caget and caput (blocant function with read back)

# constants
SAVE    = "save"
RESTORE = "restore"

def main():
    # file name
    print "File name:", fileName, "\n"

    # check arg and choose beetwen save and restore
    # print 'sys.argv: ', sys.argv
    if len(sys.argv) > 1:
        if sys.argv[1] == SAVE:
            # save is asked
            save    = True
            restore = False
        elif sys.argv[1] == RESTORE:
            # restore is asked
            save    = False
            restore = True
        else:
            # unknown argument
            exit("FAILED: unknown argument")
    else:
        exit("FAILED: no argument given. use: python <script_name>.py <save_or_restore> (save_or_restore: save or restore)")

    # print list of PV to save and restore
    print "List of PV to save and restore:"
    for i in range(len(PVlistTosaveAndRestore)):
        print "\t-", PVlistTosaveAndRestore[i]
    print "" # for more convenient prompt

    # save procedure
    if save == True:
        print "Save actual config ..."
        # list to save
        listToSave = []

        # get PV values
        for i in range(len(PVlistTosaveAndRestore)):
            # pv name
            pvName = PVlistTosaveAndRestore[i]
            # connect PV
            myPV = connectPV(pvName)
            # get value from PV
            pvValue = myCaget(myPV)
            print pvValue, "\t<-", pvName
            # save pv name and value
            listToSave.append([PVlistTosaveAndRestore[i], pvValue])

        # output file name
        # date        = time.strftime("%Y_%m_%d_%H_%M_%S")
        # date        = "2018_11_13_12_19_14"
        # fileName    = "config/nblm_confilg_" + date + ".txt"

        # put data in file
        file = open(fileName, "w")
        for i in range(len(listToSave)):
            file.write(str(listToSave[i][1]) + "\t" + listToSave[i][0] + "\n")
        file.close()

        print "... actual config saved"

    # restore procedure
    if restore == True:
        print "Restore config ..."

        # check config exist
        if os.path.isfile(fileName) == False:
            exit("FAILED: " + fileName + " does not exit")
        with open(fileName) as rawDataFile :
            for line in rawDataFile : # for each line in the file
                # split line in words
                lineSplitted    = line.split()
                # fetch value and pv name from line
                pvValue         = float(lineSplitted[0])
                pvName          = lineSplitted[1]
                print pvValue, "->", pvName
                # connect to PV
                myPV = connectPV(pvName) 
                # push data to PVs
                myCaput(myPV, pvValue, myPV) 

        print "... config restored"

if __name__ == "__main__":
    main()
