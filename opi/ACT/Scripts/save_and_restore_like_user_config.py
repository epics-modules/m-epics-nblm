
# m-epics-path
m_epics_PATH = "/home/vnadot/dev/"          # my computer, testing purpose
m_epics_PATH = "/home/iocuser/devspace/"    # CT on MTCA # temp

# file name (input for save, output for restore)
fileName =m_epics_PATH + "m-epics-nblm/opi/Scripts/config/nblm_config.txt" # my computer, testing purpose

# defines PV MACROs
PREFIX          = "IFC1410_nBLM" 
DEVICE          = "PROTO"         
DEVICE_TUNING  = "PBI-nBLM"       
CHANNEL         = "CH1"             # CH0: File reader, CH1: ADC ch0, ...

PVlistTosaveAndRestore =    [   # neutron detection configuration
                                PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFCHARGEMPV",
                                PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFPEDESTAL",
                                PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFEVENTDETECTIONTHRESHOLD",
                                PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFNEUTRONAMPLITUDEMIN",
                                PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFNEUTRONTOTMIN",
                                PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFTOTPILEUPSTART",
                                PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFTOTPILEUPLIMIT",
                                PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFNBOFDATAFORMPV",
                                PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFNBOFWINDOWSFORPEDESTAL",

                                # scope
                                PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "TRIGGERVALUESCOPE",
                                PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "SCOPEPOSTTRIGGER",
                                # SAVE AND RESTRE bug for lists (driver)
                                PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "TRIGGERSELECTIONSCOPELIST",
                                PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "TRIGGERSELECTIONSCOPE1",
                                PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "TRIGGERSELECTIONSCOPE2",
                                # ADC select
                                PREFIX + ':' + DEVICE + "-NBLM_" + "CHANNELSCOPE1",

                                # histo limits
                                # low limits
                                PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "histoChargeMPVlowLimit",
                                PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "histoTOTMPVlowLimit",
                                PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "histoPEDESTALlowLimit",
                                PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "histoRiseTimeMPVlowLimit",
                                PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "histoAmplitudeMPVlowLimit",
                                # up limits
                                PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "histoChargeMPVupLimit",
                                PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "histoTOTMPVupLimit",
                                PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "histoPEDESTALupLimit",
                                PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "histoRiseTimeMPVupLimit",
                                PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "histoAmplitudeMPVupLimit",

                                # pulse processing trigger
                                PREFIX + ':' + DEVICE + "-NBLM_" + "TIMINGSRC",
                                PREFIX + ':' + DEVICE + "-NBLM_" + "SOFTFREQ",

                                # TO ADD
                                # IFC1410_nBLM:PROTO-NBLM_SOFTFREQORPERIOD
                                # IFC1410_nBLM:PROTO-NBLM_SOFTPERIOD
                                # IFC1410_nBLM:PROTO-NBLM_SOFTFREQ
                            ]



