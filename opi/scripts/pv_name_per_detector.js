importPackage(Packages.org.csstudio.opibuilder.scriptUtil); 


function pad(num, size) {
        var s = "000000000" + num;
        return s.substr(s.length-size);
}

var prefix = PVUtil.getDouble(pvs[1]); // Prefix
var nblmNumber = PVUtil.getDouble(pvs[2]); // nblm number
var type = PVUtil.getString(pvs[3]); // slow or fast
var signal = PVUtil.getString(pvs[0]); // signal PV


var DEVICE_STRUCTURE = ":PBI-nBLM-";
// prefix
if(prefix == 0 )
        var prefix = "MEBT-01" + DEVICE_STRUCTURE
else if(prefix == 1 )
        var prefix = "DTL-01" + DEVICE_STRUCTURE
else if(prefix == 2 )
        var prefix = "DTL-02" + DEVICE_STRUCTURE
else if(prefix == 3 )
        var prefix = "SPK-01" + DEVICE_STRUCTURE
else
        var prefix = "error pv_name_per_detector.js"
// nblm number
var nblmNumber = nblmNumber +1;
var name = widget.getPropertyValue("name");


// ---------------- PV name selection -------------------------------------
if(name.indexOf("Graph") > -1){ // if name widget contains "Graph"
        // 1 trace: average neutrons over 1us
        var trace0 = "trace_" + 0 + "_y_pv";
        var pvName0 = prefix + nblmNumber + "-" + type + ":" + signal;
        widget.setPropertyValue(trace0, pvName0);
}
else if(name.indexOf("SY4527") > -1){ // if name widget contains "SY4527"
        prefix =  PVUtil.getString(pvs[1]);
        boardNumber = PVUtil.getString(pvs[2]);
        chanelNumber = PVUtil.getString(pvs[3]);;
        var pvName = prefix + ":" + pad(boardNumber, 2) + ":" + pad(chanelNumber,3) + ":" + signal; // ex: SY4527:02:002:Name
        widget.setPropertyValue("pv_name", pvName);

}
else{
        if(name.indexOf("HV") > -1) // if name widget contains "HV"
                var pvName = prefix + nblmNumber + "-" + type + signal; // ex: NBLM:SEDI-TEST:N1:FAST:Pedestal
        else
                var pvName = prefix + nblmNumber + "-" + type +":" + signal; // ex: NBLM:SEDI-TEST:N1:FAST:Pedestal
        widget.setPropertyValue("pv_name", pvName);

}
// ---------------- PV name selection -------------------------------------



//ConsoleUtil.writeInfo("pv name: "+pvName);
