importPackage(Packages.org.csstudio.opibuilder.scriptUtil); 

var nbOfTrace = PVUtil.getDouble(pvs[0]); //pv0
var PREFIX = PVUtil.getString(pvs[1]); //pv1
var DEVICE = PVUtil.getString(pvs[2]); //pv2 PBI-nBLM
var DETECTOR = PVUtil.getString(pvs[3]); //pv3 SLOW or FAST

var field;
var pvName;
for(var i=0; i<nbOfTrace; i++){
        field = "trace_" + i + "_y_pv";
        var temp = i+1;
        pvName = PREFIX + ":" + DEVICE + "-" + temp + "-" + DETECTOR + ":Pedestal"
        widget.setPropertyValue(field, pvName);
}

// ConsoleUtil.writeInfo("Channel: "+channel);
