importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

var macroInput = DataUtil.createMacrosInput(true);
var opiPath = widget.getPropertyValue("opi_file");

// nblm area
try {
    var AREA = PVUtil.getString(pvs[0]);
}
catch(err) {
    var AREA = "";
    ConsoleUtil.writeInfo(err);
}
macroInput.put("AREA", AREA);

// nblm index
try {
    var NBLM_IDX = PVUtil.getDouble(pvs[1]); // + 1; (+1 because strating from 1 and not 0)
}
catch(err) {
    var NBLM_IDX = "";
    ConsoleUtil.writeInfo(err);
}
macroInput.put("NBLM_IDX", NBLM_IDX);

// put new macro
widget.setPropertyValue("macros", macroInput);
// need to close and open the OPI in order new maccro are taken in effect
widget.setPropertyValue("opi_file", "");
widget.setPropertyValue("opi_file", opiPath);



