importPackage(Packages.org.csstudio.opibuilder.scriptUtil); 


//------ CONSTANT ------------
var CHECK_BOX = "Check_Box";
var GRAPHIC = "XY_Graph";

//---- END  CONSTANT ---------

//----- FUNCTION -------------
function enable_widget(enable){
	widget.setPropertyValue("enabled",enable);
}
//--- END FUNCTION ------------

//----- MAIN ------------------
var nbOfChannels = PVUtil.getDouble(pvs[0]);


var name = widget.getPropertyValue("name");
var channel = 8; //error


// check box or graphic ?
if(name.indexOf(CHECK_BOX) > -1){
	// ConsoleUtil.writeInfo("Checkbox");
	if(name.indexOf('_0_') > -1)
		channel = 0;
	else if(name.indexOf('_1_') > -1)
		channel = 1;
	else if(name.indexOf('_2_') > -1)
		channel = 2;
	else if(name.indexOf('_3_') > -1)
		channel = 3;
	else if(name.indexOf('_4_') > -1)
		channel = 4;
	else if(name.indexOf('_5_') > -1)
		channel = 5;
	else if(name.indexOf('_6_') > -1)
		channel = 6;
	else if(name.indexOf('_7_') > -1)
		channel = 7;
	else
		channel = 8; //error
	
	// ConsoleUtil.writeInfo("name widget "+name);
	// ConsoleUtil.writeInfo("nbOfChannels: "+nbOfChannels);
	// ConsoleUtil.writeInfo("Channel: "+channel);

	if(channel == 8){ //error
		widget.setPropertyValue("border_color","Invalid");
		widget.setPropertyValue("border_style",1);
		widget.setPropertyValue("border_width",3);
		enable_widget(false);
		pvs[1].setValue(0);
	}
	else if(channel <= (nbOfChannels-1)){
		enable_widget(true);
		pvs[1].setValue(1);
	}
	else{
		enable_widget(false);
		pvs[1].setValue(0);
	}
}
else if(name.indexOf(GRAPHIC) > -1){
	// ConsoleUtil.writeInfo("graphic");


	var  channels = [PVUtil.getDouble(pvs[1]),
			 PVUtil.getDouble(pvs[2]),
			 PVUtil.getDouble(pvs[3]),
			 PVUtil.getDouble(pvs[4]),
			 PVUtil.getDouble(pvs[5]),
			 PVUtil.getDouble(pvs[6]),
			 PVUtil.getDouble(pvs[7]),
			 PVUtil.getDouble(pvs[8])]	

	var startTring = "trace_";
	var endString = "_visible";
	for (var i = 0; i <= nbOfChannels-1; i++) {
		var numberSting = i.toString();
		var trace = startTring.concat(numberSting.concat(endString)); // "trace_" + i +"_visible"

		// ConsoleUtil.writeInfo("Channels[" + i + "]: "+ channels[i]);
		if(channels[i] == 1.0){
			widget.setPropertyValue(trace,true);
			// ConsoleUtil.writeInfo("trace visible: " + trace);
		}
		else{
			// ConsoleUtil.writeInfo("trace disable: " + trace);
			widget.setPropertyValue(trace,false);
		}
			
	}
}
//---- END MAIN ---------------