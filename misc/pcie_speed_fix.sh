#!/bin/sh
PCIE_ADDR="4:01.0"
echo "Detecting link speed..."
if sudo setpci -s $PCIE_ADDR 78.l | grep -q '43'; then
        echo "PCIe Gen 3 detected! No action needed."
else
        if sudo setpci -s $PCIE_ADDR 78.l | grep -q '42'; then
                echo "PCIe Gen 2 detected! No action needed."
        else
                sudo setpci -s $PCIE_ADDR 78.l=20
                if setpci -s $PCIE_ADDR 78.l | grep -q '43'; then
                        echo "PCIe Gen 3 detected! Done."
                else
                        echo "Something went wrong...no board detected!"
                fi
        fi
fi
   
