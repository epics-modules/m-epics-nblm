import epics        # CA
import sys

sys.path.insert(0, "/home/iocuser/devspace/m-epics-nblm/misc")
from epicsFunctions import * # secure caget and caput (blocant function with read back)

sys.path.insert(0, "/home/iocuser/devspace/m-epics-nblm/misc/simulation")
import config as GBL # global variables

# my nblm modules
from nblmlib import functions           # my functions for nBLM simulation
from nblmlib import dataIn              # input file for simulation
from nblmlib import dataOut             # save simulation results
from nblmlib import neutronDetection    # pre-processing and detects neutrons
from nblmlib import analysis            # pedestal calculation, counts neutons, MPV calculation, sparks and saturation detection, stability ...                            
from nblmlib import plotUtils           # plot raw data and specific events

# default value
PREFIX_DEFAULT      = "IFC1410_nBLM"    #
DEVICE_DEFAULT      = "PROTO"           # Defaut value
DEVICE_TUNING      = "PBI-nBLM"        # May be overwritten
CHANNEL_DEFAULT     = "CH0"             # 
PATH_DIR_DEFAULT    = "/export/nfsroots/ifc1410-ess-20170314/home/root/ifc1410_nBLM_proto/data/"
# MARCO
PREFIX          = ""    #
DEVICE          = ""    # Defaut value
CHANNEL         = ""    # May be overwritten
PATH_DIR        = ""    #


# Macros
# fetch arguments given in argv
if len(sys.argv) == 6 :
    # push FPGA PVs to local or "in return"
    # macros
    PREFIX          = sys.argv[1]
    DEVICE          = sys.argv[2]
    DEVICE_TUNING  = sys.argv[3]
    CHANNEL         = sys.argv[4] # "CHx"
    PUSH            = sys.argv[5] # 0: from channel PVs to local PVs, 1: from local PVs to channel PVs
# lauchn analysis
elif len(sys.argv) == 5 :
    PREFIX      = sys.argv[1]
    DEVICE      = sys.argv[2]
    CHANNEL     = sys.argv[3]
    PATH_DIR    = sys.argv[4]
elif len(sys.argv) == 4 :
    PREFIX      = sys.argv[1]
    DEVICE      = sys.argv[2]
    CHANNEL     = sys.argv[3]
    PATH_DIR    = PATH_DIR_DEFAULT
elif len(sys.argv) == 3 :
    PREFIX      = sys.argv[1]
    DEVICE      = sys.argv[2]
    CHANNEL     = CHANNEL_DEFAULT
    PATH_DIR    = PATH_DIR_DEFAULT
elif len(sys.argv) == 2 :
    PREFIX      = sys.argv[1]
    DEVICE      = DEVICE_DEFAULT
    CHANNEL     = CHANNEL_DEFAULT
    PATH_DIR    = PATH_DIR_DEFAULT
elif len(sys.argv) == 1 :
    PREFIX      = PREFIX_DEFAULT
    DEVICE      = DEVICE_DEFAULT
    CHANNEL     = CHANNEL_DEFAULT
    PATH_DIR    = PATH_DIR_DEFAULT
else:
    exit("Wrong number of arguments: " + str(len(sys.argv)))

#### pv connection #######
# source raw data
PV_sourceSelection  = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "TuningSourceSelection")
PV_filePath         = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "TuningFileName")
PV_rawDataScopeChFR = connectPV(PREFIX + ':' + DEVICE + '-' + "CH0" + "-NBLM_RAWDATA")
PV_rawDataScope1    = connectPV(PREFIX + ':' + DEVICE + '-' + "CH1" + "-NBLM_RAWDATA")
PV_rawDataScope2    = connectPV(PREFIX + ':' + DEVICE + '-' + "CH2" + "-NBLM_RAWDATA")
# PV_rawDataDOD       = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "") # to fill

# source raw data selection
PV_startRawData     = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "TuningStartRawData")
PV_stopRawData      = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "TuningStopRawData")
PV_mouseSelection   = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "TuningMouseDataSelection")

# neutron detection configuration [locals PVs]
PV_pedestalLevel            = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "TuningPedestal")
PV_eventDetectionThreshold  = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "TuningEventDetectionTh")
PV_neutronAmplitudeMin      = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "TuningNeutronAmpMin")
PV_neutronTOTmin            = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "TuningTOTmin")
PV_TOTpileUpStart           = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "TuningTOTpileUpStart")
PV_neutronChargeMPV         = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "TuningChargeMPV")

# enable parts of analysis
PV_displayNeutronPeakAverage        = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "TuningNeutronPeakAvgEna")
PV_displayNeutronPeakDistribution   = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "TuningNeutronPeakDistEna")
PV_displayFullWindow                = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "TuningFullWindowEna")
PV_displayScopeDebug                = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "TuningScopeDebugEna")
PV_calculatePedestal                = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "TuningCalculatedPedestalEna")
PV_saveLogs                         = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "TuningSaveLogsEna")

# use data under thresold
PV_useDataUnderThreshold            = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "TuningUseDataUnderThreshold")
PV_useDataUnderThresholdPreTrig     = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "TuningUseDataUnderThPreTrig")
PV_useDataUnderThresholdPostTrig    = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "TuningUseDataUnderThPostTrig")

# calculated data
PV_pedestalCalculated       = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "TuningPedestalCalculated")
PV_chargeMPVcalculated      = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "TuningChargeMPVcalculated")
PV_TOT_MPVcalculated        = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "TuningTOT_MPVcalculated")
PV_riseTimeMPVcalculated    = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "TuningRiseTimeMPVcalculated")

# scope config
PV_scopeTriggerSelection    = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + '-NBLM_TRIGGERSELECTIONSCOPELIST')
PV_scopeList1               = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + '-NBLM_TRIGGERSELECTIONSCOPE1')
PV_scopeList2               = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + '-NBLM_TRIGGERSELECTIONSCOPE2')
PV_scopeTriggerVelue        = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + '-NBLM_TRIGGERVALUESCOPE')

# FPGA calculated data
PV_MPVchargeAdviced         = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "MPVCHARGEADVICED")
PV_pedestalAdviced          = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "PEDESTALADVICED")

# FPGA neutron detection configuration
PV_pedestalLevelFPGA            = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFPEDESTAL")
PV_eventDetectionThresholdFPGA  = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFEVENTDETECTIONTHRESHOLD")
PV_neutronAmplitudeMinFPGA      = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFNEUTRONAMPLITUDEMIN")
PV_neutronTOTminFPGA            = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFNEUTRONTOTMIN")
PV_TOTpileUpStartFPGA           = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFTOTPILEUPSTART")
PV_neutronChargeMPVFPGA         = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFCHARGEMPV")

