import sys  # add python modules

### USER inputs ###
# m-epics-nblm path
PATH_M_EPICS_NBM    = "/home/iocuser/devspace/m-epics-nblm/"
simulationPath      = PATH_M_EPICS_NBM + "misc/simulation/"
### USER inputs ###

# import nblm config
sys.path.insert(0, simulationPath)
import config as GBL # global variables

# import tuning lib and defines
from tuning_neutron_detection_define    import *    # pv name and defines
from tuning_neutron_detection_lib       import *    # functions


### analysis configuration
# m-epics-nblm path
GBL.PATH_SIMULATION     = simulationPath
GBL.PATH_RAW_DATA_FILES = GBL.PATH_SIMULATION + "rawDataFiles/"
GBL.PATH_NBLM_LOGS      = GBL.PATH_SIMULATION + "logs/"

# fetch configuration for analysis from PV and configure config.py
GBL.PEDESTAL_LEVEL              = myCaget(PV_pedestalLevel)
GBL.EVENT_DETECTION_THRESHOLD   = myCaget(PV_eventDetectionThreshold)
GBL.NEUTRON_AMPLITUDE_MIN       = myCaget(PV_neutronAmplitudeMin)
GBL.NEUTRON_TOT_MIN             = myCaget(PV_neutronTOTmin)
GBL.TOT_PILE_UP_START           = myCaget(PV_TOTpileUpStart)
GBL.NEUTRON_CHARGE_MPV          = myCaget(PV_neutronChargeMPV)

# raw data selection
GBL.START_RAWDATA       = myCaget(PV_startRawData)
GBL.STOP_RAWDATA        = myCaget(PV_stopRawData)
GBL.MARKER_SELECTION    = myCaget(PV_mouseSelection)

# enable parts of analysis
GBL.COMPUTE_HISTOGRAM               = myCaget(PV_displayNeutronPeakDistribution)
GBL.COMPUTE_PEDESTAL                = myCaget(PV_calculatePedestal)
GBL.COMPUTE_AVERAGE_NEUTRON_PEAK    = myCaget(PV_displayNeutronPeakAverage)
GBL.PLOT_ALL_RAW_DATA               = myCaget(PV_displayFullWindow)
GBL.PLOT_SCOPE_DEBUG                = myCaget(PV_displayScopeDebug) 
GBL.SAVE_LOGS                       = myCaget(PV_saveLogs)

# use data under thresold
GBL.USE_DATA_UNDER_EVENT_THRESHOLD              = myCaget(PV_useDataUnderThreshold)
GBL.USE_DATA_UNDER_EVENT_THRESHOLD_PRE_TRIG     = myCaget(PV_useDataUnderThresholdPreTrig)
GBL.USE_DATA_UNDER_EVENT_THRESHOLD_POST_TRIG    = myCaget(PV_useDataUnderThresholdPostTrig)

# scope config
# trigger selection
listSelected = myCaget(PV_scopeTriggerSelection)
if listSelected == 0: 
    # triggers from 0 to 15
    GBL.SCOPE_TRIGGER_SELECTION = myCaget(PV_scopeList1)
elif listSelected == 1:
    # triggers from 15 to 31
    OFFSET_SECOND_LIST = 15
    GBL.SCOPE_TRIGGER_SELECTION = myCaget(PV_scopeList2) + OFFSET_SECOND_LIST
else:
    print "FAILED: list selection for scope trigger"
    exit()
# trigger value
GBL.SCOPE_TRIGGER_VALUE = myCaget(PV_scopeTriggerVelue)

# all prints redirected to file
f = open(GBL.PATH_SIMULATION + 'rawDataFiles/tuning_neutron_detection_logs.txt', 'w')
# sys.stdout = f

# get raw data depending on source (DOD, scope ...)
rawData, rawDataMask, NB_OF_SAMPLES_ACQUIRED = getRawData()
# tuning raw data file
tuningFile          = GBL.PATH_RAW_DATA_FILES + GBL.TUNING_FILE_NAME
GBL.RAW_DATA_FILE   = tuningFile
# save rawdata
dataOut.list_rawdata_to_file_rawdata(tuningFile, rawData)

### analysis
sys.path.insert(0, GBL.PATH_SIMULATION)
import simu_fpga_algo as simu_fpga_algo 
simu_fpga_algo.simu_fpga_algo()


# close log file
f.close()