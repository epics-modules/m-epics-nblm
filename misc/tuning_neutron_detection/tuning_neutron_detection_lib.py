import epics        # CA
import os.path      # check existing file
import sys
import numpy as np  # vector opeartions

from tuning_neutron_detection_define import *  # pv name and defines

# m-epics-nblm path
PATH_M_EPICS_NBM    = "/home/iocuser/devspace/m-epics-nblm/"

# secure caget and caput (blocant functions with read back)
sys.path.insert(0, PATH_M_EPICS_NBM + "misc")
from epicsFunctions import *

# import nblm modules
simulationPath      = PATH_M_EPICS_NBM + "misc/simulation/"
sys.path.insert(0, simulationPath)
from nblmlib import functions

def getRawData():
    """ fetch data denpending on user config (DOD, scope, file...) """
    # source selection
    dataSelection = myCaget(PV_sourceSelection)
    print "dataSelection", dataSelection
    # get data from source

    ## DOD
    if dataSelection == 0:
        print "input data from DOD1 - SEL1 (EPICS)"
        # DOD PV
        # to do
        # don't forget padding to avoid bug in scope
        exit("DOD is not implemented yet, please select another data source.")
    elif dataSelection == 1:
        print "input data from DOD2 - SEL2 (EPICS)"
        # DOD PV
        # to do
        # don't forget padding to avoid bug in scope
        exit("DOD is ot implemented yet")
    elif dataSelection == 2:
        print "input data from DOD1 - SEL3 (EPICS)"
        # DOD PV
        # to do
        # don't forget padding to avoid bug in scope
        exit("DOD is not implemented yet, please select another data source.")
    elif dataSelection == 3:
        print "input data from DOD1 - SEL4 (EPICS)"
        # DOD PV
        # to do
        # don't forget padding to avoid bug in scope
        exit("DOD is not implemented yet, please select another data source.")

    ## file
    elif dataSelection == 4:
        # directory path
        GBL.PATH_RAW_DATA_FILES = os.path.expanduser(PATH_DIR)
        # file name
        GBL.RAW_DATA_FILE = myCaget(PV_filePath)
        print "input data from the file:", GBL.RAW_DATA_FILE, "\n"
        # get data
        rawData, rawDataMask, NB_OF_SAMPLES_ACQUIRED = dataIn.get_data()
        # number of samples
        NB_OF_SAMPLES_ACQUIRED = len(rawData)

    ## scope
    elif dataSelection == 5:
        print "input data from scope chFR (EPICS)"
        # scope PV
        rawData = myCaget(PV_rawDataScopeChFR)
    elif dataSelection == 6:
        print "input data from scope 1 (EPICS)"
        # scope PV
        rawData = myCaget(PV_rawDataScope1)
    elif dataSelection == 7:
        print "input data from scope 2 (EPICS)"
        # scope PV
        rawData = myCaget(PV_rawDataScope2)
    else:
        exit("error: choice not possible: " + str(dataSelection))

    # scope
    if dataSelection == 5 or dataSelection == 6 or dataSelection == 7:
        rawDataMask = np.zeros(len(rawData))
        # add padding to avoid bug in python scope
        paddingSize     = max([functions.time_to_sample(GBL.TOT_PILE_UP_START), functions.time_to_sample(GBL.SCOPE_WINDOW)])
        padding         = np.ones(paddingSize) * GBL.EVENT_DETECTION_THRESHOLD
        rawData         = np.concatenate( (padding, rawData, padding)           ,0)
        rawDataMask     = np.concatenate( (padding, rawDataMask, padding)       ,0)
        # number of samples
        NB_OF_SAMPLES_ACQUIRED = len(rawData)

    return rawData, rawDataMask, NB_OF_SAMPLES_ACQUIRED