import epics
import time

from tuning_neutron_detection_define import *   # pv name and defines
from tuning_neutron_detection_lib import *      # lib

if PUSH == "0" :
    # push channel PV (FPGA) to local PVs (tuning neutron detection)
    myCaput(PV_pedestalLevel,                myCaget(PV_pedestalLevelFPGA),            PV_pedestalLevel)
    myCaput(PV_eventDetectionThreshold,      myCaget(PV_eventDetectionThresholdFPGA),  PV_eventDetectionThreshold)
    myCaput(PV_neutronAmplitudeMin,          myCaget(PV_neutronAmplitudeMinFPGA),      PV_neutronAmplitudeMin)
    myCaput(PV_neutronTOTmin,                myCaget(PV_neutronTOTminFPGA),            PV_neutronTOTmin)
    myCaput(PV_TOTpileUpStart,               myCaget(PV_TOTpileUpStartFPGA),           PV_TOTpileUpStart)
    myCaput(PV_neutronChargeMPV,             myCaget(PV_neutronChargeMPVFPGA),         PV_neutronChargeMPV)

elif PUSH == "1" :
    # push local Pvs (tuning neutron detection) to channel PV (FPGA) 
    myCaput(PV_pedestalLevelFPGA,            myCaget(PV_pedestalLevel),              PV_pedestalLevelFPGA)
    myCaput(PV_eventDetectionThresholdFPGA,  myCaget(PV_eventDetectionThreshold),    PV_eventDetectionThresholdFPGA)
    myCaput(PV_neutronAmplitudeMinFPGA,      myCaget(PV_neutronAmplitudeMin),        PV_neutronAmplitudeMinFPGA)
    myCaput(PV_neutronTOTminFPGA,            myCaget(PV_neutronTOTmin),              PV_neutronTOTminFPGA)
    myCaput(PV_TOTpileUpStartFPGA,           myCaget(PV_TOTpileUpStart),             PV_TOTpileUpStartFPGA)
    myCaput(PV_neutronChargeMPVFPGA,         myCaget(PV_neutronChargeMPV),           PV_neutronChargeMPVFPGA)

elif PUSH == "2":
    # push MPV charge
    myCaput(PV_neutronChargeMPVFPGA,    myCaget(PV_chargeMPVcalculated),    PV_neutronChargeMPVFPGA)
    # push pedestal adviced
    myCaput(PV_pedestalLevelFPGA,       myCaget(PV_pedestalCalculated),     PV_pedestalLevelFPGA)
    
else:
    exit("FAILED: wrong agrument")
