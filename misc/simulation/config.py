
###############################################################
# This is the configuration file of nBLM algorithm simulation #
###############################################################

### Paths ###
# directories
# ! needs to be absolute path (and not relative) for CSS. !
PATH_SIMULATION     = "/home/vnadot/development/m-epics-nblm/misc/simulation/"      # vnadot perso laptop
# PATH_SIMULATION     = "/home/vnadot/dev/m-epics-nblm/misc/simulation/"            # vnadot DELL laptop
# PATH_SIMULATION     = "/home/vnadot/mnt/devspace/m-epics-nblm/misc/simulation/"   # vnadot PC tower
# PATH_SIMULATION     = "/home/iocuser/devspace/m-epics-nblm/misc/simulation/"      # iocuser, nBLM crate
PATH_NBLM_LOGS      = PATH_SIMULATION + "logs/"
# log file name
NBLM_LOG_FILE_NAME  = "nblm_logs.txt"

### raw data file selection ###
PATH_RAW_DATA_FILES = PATH_SIMULATION + "rawDataFiles/" # directory where raw data files are stored
## generated file
RAW_DATA_FILE       = PATH_RAW_DATA_FILES + "generatedData.npy"                           # generated data, NB: you can generate your own data with "generate_neutrons_shape.py" 
# RAW_DATA_FILE       = PATH_RAW_DATA_FILES + "allEvents.txt"                               # neutrons, bad events, saturations ...
# RAW_DATA_FILE       = PATH_RAW_DATA_FILES + "dataEmulated.txt"
# RAW_DATA_FILE       = PATH_RAW_DATA_FILES + "rawData_duration0.5s_freq-14Hz_pulseOn-3ms_10neutrons.npy"
# RAW_DATA_FILE       = "/tmp/Last_rawdata.npy"   # "/tmp/rawdata_lodz_2018_11_14_12_00.npy"
RAW_DATA_FILE       = PATH_RAW_DATA_FILES + "rawData_duration0.0714285714286s_freq-14Hz_pulseOn-3ms_10000neutrons.npy"
## real acquisition
# RAW_DATA_FILE  = PATH_RAW_DATA_FILES + "birmingham_2017_11_slowDetector.txt"         # slow birmingham_2017_11, pedestal = -23 mV
# RAW_DATA_FILE  = PATH_RAW_DATA_FILES + "SLOW_IPHI_outEventDraw_run18_ev16.txt"    # slow IPHI 2018_01, pedestal = 0mV
# RAW_DATA_FILE  = PATH_RAW_DATA_FILES + "FAST_53-500-650-2.2uA_IPHI_pulses.txt"       # Fast detector IPHI (old, not good data)
# RAW_DATA_FILE  = PATH_RAW_DATA_FILES + "FAST_IPHI_48-500-750-2.2uA.txt"              # Fast detector IPHI februrary 2017 (old, not good data)

### raw data part selection ###
START_RAWDATA       = 0     # in %
STOP_RAWDATA        = 100   # in %
MARKER_SELECTION    = False # use mouse to select data

### compute and plot option ###
COMPUTE_HISTOGRAM               = True  # delay: 2/5
SAVE_LOGS                       = True  # delay: 3/5 (save and plot)
COMPUTE_PEDESTAL                = True  # delay: 3/5
COMPUTE_AVERAGE_NEUTRON_PEAK    = True  # delay: 4/5 really slow down calculation for big file (if True)
PLOT_ALL_RAW_DATA               = True  # delay: 5/5 NB: plotting all raw data may slow down your PC depending on raw data length
PLOT_SCOPE_DEBUG                = True  # delay: 0/5
USE_DATA_UNDER_EVENT_THRESHOLD  = False  # delay: 5/5
USE_DATA_UNDER_EVENT_THRESHOLD_PRE_TRIG     = 100000    # ns
USE_DATA_UNDER_EVENT_THRESHOLD_POST_TRIG    = 1000000   # ns


### neutron detection parameters ####

# amplitude settings
PEDESTAL_LEVEL              = 0    # mV
EVENT_DETECTION_THRESHOLD   = -10   # mV
NEUTRON_AMPLITUDE_MIN       = -10   # mV
SPARK_THRESHOLD             = 200   # mV

# TOT settings
NEUTRON_TOT_MIN             = 40    # ns
TOT_PILE_UP_START           = 200   # ns

# Most Probable Value (MPV)
NEUTRON_CHARGE_MPV          = -1100 # Q = mV * ns
NEUTRON_TOT_MPV             = 120   # ns
NEUTRON_RISE_TIME_MPV       = 80    # ns
NEUTRON_AMPLITUDE_MPV       = -60   # mv

# DOD/scope trigger
SCOPE_WINDOW                = 40000 # ns
SCOPE_TRIGGER_VALUE         = 20    # depending on SCOPE_TRIGGER_SELECTION
SCOPE_TRIGGER_SELECTION     = 4     # ex: 4 -> on neutron peak
# SCOPE_TRIGGER is a constant, not a parameter
SCOPE_TRIGGER               = [
                                "0. no trig (don't trig)",
                                "1. on EPICS demand only",
                                "2. on neutron counting level",
                                "3. on neutron shape",
                                "4. on neutron peak",
                                "5. on neutron pile-up",
                                "6. on neutron TOTlimitReached",
                                "7. on bad event",
                                "8. on bad event because of TOT",
                                "9. on bad event because of amplitude",
                                "10. on bad event peak",
                                "11. on bad event pile-up",
                                "12. on bad event TOT limit reached",
                                "13. on event",
                                "14. on ADC saturation",
                                "15. on ADC positive saturation",
                                "16. on ADC negative saturation",
                                "17. on driver request",
                                "18. on MPV instability",
                                "19. on pedestal instability",
                                "20. on CH0-8 trig (16 bits value)",
                                "21. on an external trig, via RTM or AMC TBC?",
                                "22. on sparks",
                                "23. spare",
                                "24. spare",
                                "25. spare",
                                "26. spare",
                                "27. spare",
                                "28. spare",
                                "29. spare",
                                "30. spare",
                                "31. spare"
                            ]

NB_NEUTRON_PEAKS_TO_DRAW_PROPER_DISTRIBUTION = 1 # neutron peaks
#####################################


### constants ###
# sampling period
TE                          = 4 #ns
# ADC saturation amplitude
SATURATION                  = 500 #mV
# max duration of one event
TOT_PILE_UP_LIMIT_REACHED   = 1000  # ns
# integration period for neutron counting
INTEGRATION_PERIOD          = 1000  # ns
# neutron generation parameters
GAUSSIAN_SIGMA              = 6     # ns ?
GAUSSIAN_WINDOW_DISPLAY     = 100   # ns ?

## defines
# for elapsedTime() fct
START_TIME  = 0
DURATION    = 1
#################


# event structure
import collections  # tuple (t_event)
t_event = collections.namedtuple	('t_event', [   'isNeutron',                    # 0: bad event, 1: neutron shape
                                                    'TOTnotValidated',              # tells you why it's considered as bad event and not as a neutron shape: 0:TOT ok, 1: TOT too small to be a neutron shape
                                                    'amplitudeNotValidated',        # tells you why it's considered as bad event and not as a neutron shape: amp ok, 1: amp too small to be a neutron shape
                                                    'pileUp',                       # 0: peak, 1: pile-up.
                                                    'TOTlimitReached',              # TOT == 1us ? 0:no, 1:Yes
                                                    'charge',                       # charge of the event
							                        'TOT',                          # TOT of the event
                                                    'amplitude',                    # amplitude of the event
                                                    'riseTime',                     # rise time of the event
                                                    'rawDataIndex',                 # index of the neuton shape end in the simulation: SIMULATION ONLY
                                                    'eventNumber',                  # event number of the simulation: SIMULATION ONLY
                                                    ]
)

# generated output file
GENERATED_FILE_NAME = "generatedData" # not ".txt" or ".npy" because at the end it could have "Mask"
TUNING_FILE_NAME    = "tuningRawDataFile.txt"