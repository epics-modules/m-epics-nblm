#!/bin/bash

echo "Remove *pyc in actual folder"
rm *.pyc

echo "Remove *pyc in sub-folders"
rm */*.pyc
