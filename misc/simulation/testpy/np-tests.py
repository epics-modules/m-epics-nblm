import numpy as np              # vector operation
import time                     # elapsed time
import matplotlib.pyplot as plt # plots

# np.set_printoptions(threshold=np.nan) # print all data

# rising edge detection
data = np.array([-1, -0.5, 0, 0.3, 1, 0.3, 2, 0, 0])
trigger_val = 0.3
maskTrig        = (data[:-1] <= trigger_val) & (data[1:] > trigger_val)
maskTrigIndexes = np.flatnonzero((data[:-1] <= trigger_val) & (data[1:] > trigger_val))
print "index trig:", maskTrigIndexes
# pre-trig and post-trig
preTrig     = 2
postTrig    = 2

# clear trig around trig in window
print "maskTrig before:", maskTrig
for i in range(len(maskTrigIndexes)):
    # size of the window
    startWindow = maskTrigIndexes[i] - preTrig
    stopWindow  = maskTrigIndexes[i] + postTrig
    for j in range(startWindow, stopWindow):
        # for the size of the window, remove multi trig
        maskTrig[j] = False
    # re -add trigger
    maskTrig [ maskTrigIndexes[i] ] = True # trigger
print "maskTrig after: ", maskTrig

exit()

data = np.array([-1, -0.5, 0, 0.3, 1, 1.5, 2, 0, 0.5])
trigger_val = 0.3
# data[:-1] = data[0 : len(data)-1 ]
# data[1:]  = data[1 : len(data) ]
print data
print data[:-1]
print data[1:]
print np.flatnonzero(data)
print data[:-1] <= trigger_val
print data[1:] > trigger_val
print (data[:-1] <= trigger_val) & (data[1:] > trigger_val)
print np.flatnonzero((data[:-1] <= trigger_val) & (data[1:] > trigger_val))+1
risingEdge  = np.flatnonzero((data[:-1] <= trigger_val) & (data[1:] > trigger_val))+1
fallingEdge = np.flatnonzero((data[:-1] >= trigger_val) & (data[1:] < trigger_val))+1
risingEdgeVector = np.zeros(len(data))
fallingEdgeVector = np.zeros(len(data))
for i in range(len(risingEdge)):
    risingEdgeVector[risingEdge[i]] = 1
for i in range(len(fallingEdge)):
    fallingEdgeVector[fallingEdge[i]] = 1
    
plt.plot(data,                              label="data")
plt.plot(trigger_val*np.ones(len(data)),    label="trigger level")
plt.plot(risingEdgeVector,                  label="rising edge")
plt.plot(fallingEdgeVector,                  label="falling edge")
plt.legend()
plt.show()
exit()

# sum vs np.sum()
myArray = np.ones(1000000)
# sum()
print "start sum().." 
startSum    = time.time()
data        = sum(myArray)
duration    = time.time()-startSum
print "end sum().. (%f s)" %(duration)
# np.sum()
print "start np.sum().." 
startNpSum  = time.time()
data        = np.sum(myArray)
duration    = time.time()-startNpSum
print "end np.sum().. (%f s)" %(duration)
exit()


####### np type overflow #######
# int8 	Byte (-128 to 127)
value = np.zeros(1, dtype=np.int8)
print value, type(value)
value[0] = 130
print value, type(value)
exit()


###### manipulate numpy data ######
data = [1, 3, 4, 500, 550, -10, -500, -750]

rawData = np.array(data)
print "rawData", rawData

rawDataPositiveStauration = rawData > 500
rawDataNegativeStauration = rawData < -500
print "rawDataPositiveStauration", rawDataPositiveStauration
print "rawDataNegativeStauration", rawDataNegativeStauration

rawDataNoSaturation = rawData * np.logical_not( rawDataPositiveStauration + rawDataNegativeStauration)
print "rawDataNoSaturation", rawDataNoSaturation

rawData = rawDataNoSaturation + rawDataPositiveStauration*500 + rawDataNegativeStauration*-500
print "rawData", rawData

myConcatenation = np.concatenate((np.array(data), np.array(data)), 0)
print myConcatenation

print "\nselect data on vector"
x = np.array([-3, -4, -8, 5,6,7])
print x
maskX = (x<3)
print np.trim_zeros( maskX * x )
print np.sort(np.trim_zeros( maskX * x  ))



###### compare list and numpy array  performances ######
tabSize = 10000
tab     = np.zeros(tabSize)
list    = [0 for i in range(tabSize)]
offset  = 5

startTime = time.time()
list = [ (x+ offset) for x in list]
print "list+offset. elapsed time:", time.time()-startTime

startTime = time.time()
tab = tab + offset
print "np elapsed time:",time.time()-startTime

startTime = time.time()
list = np.array(list)
print "convert list in np array. converstion elapsed time:",time.time()-startTime

startTime = time.time()
tab = np.array(tab)
print "convert np array in np array. elapsed time:",time.time()-startTime



##### write / read in file
nbOfDataArray   = 1000000
rawData         = 8.000001 * np.ones(nbOfDataArray) # mV
rawData         = np.round(rawData / 1000.0, 5) # to V
print " " # space between prompt

# len() VS <np_array>.size
print "len() VS <np_array>.size"
# len()
startTime = time.time()
len(rawData)
print "len() duration:", (time.time() - startTime) * 1000000 , "us"
# size()
startTime = time.time()
rawData.size
print "<np_array>.size duration:", (time.time() - startTime) * 1000000 , "us"


# write read custom (text)
fileName = "customTextReadWrite.txt"
# # write
startTime = time.time()
print ".. start time to write text (custom)"
file = open(fileName, "w+")
file.write("# Time Ampl\n")
for i in range(nbOfDataArray):
    file.write(str( 4*i ) + " " + str(rawData[i]) + "\n") # raw data are stored in V in file
file.seek(0) # come back to start of the file
file.close()
print "len(rawData)", len(rawData)
print ".. end time to write text (custom)", time.time() - startTime , "s"

# read
startTime = time.time()
print ".. start time to read text (custom)"
# get number of data in file
file = open(fileName, 'r+')
numberOfLine = 0
for line in file:
    numberOfLine += 1
numberOfRawData = numberOfLine - 1 # first line is: "Time Ampl"
# rawData creation
myRawData = np.zeros(numberOfRawData)
# read file and put data in rawData
nbLigneHeader   = 1
indexWrite      = 0
ligneCounter    = 0
with open(fileName) as rawDataFile :
    previousTime = 0
    for line in rawDataFile : # for each line in the file
        words = line.split() # plit the line in words
        # time = words[0]
        amplitude = words[1]
        if len(words) == 2 and ligneCounter >= nbLigneHeader:
            myRawData[indexWrite] = (float(amplitude)*1000) # V to mV
            indexWrite = indexWrite + 1
        ligneCounter = ligneCounter + 1
print "len(myRawData)", len(myRawData)
print ".. end time to read text (custom)", time.time() - startTime , "s\n"



# write read numpy (text)
fileName = "npTextReadWrite.txt"
timeAxis = np.linspace(0, 4*(len(rawData)-1), num=len(rawData), dtype=int) 
# write
print ".. start time to write text (numpy)"
startTime = time.time()
ab = np.zeros(rawData.size, dtype=[('var1', int), ('var2', float)])
ab['var1'] = timeAxis
ab['var2'] = rawData
np.savetxt(fileName, ab, fmt="%d %.5f", comments="#", header=" Time Ampl", delimiter=" ")
print "len(rawData)", len(rawData)
print ".. end time to write text (numpy)", time.time() - startTime , "s"

# read
print ".. start time to read text (numpy)"
startTime = time.time()
myTime, myRawData = np.loadtxt(fileName, comments="#", delimiter=" ", unpack=True)
print "len(myRawData)", len(myRawData)
print ".. end time to read text (numpy)", time.time() - startTime , "s\n"


# write read numpy (binary)
fileName = "npBinaryReadWrite.npy"
# write
print ".. start time to write binary (numpy)"
startTime = time.time()
np.save(fileName, (timeAxis, rawData))
print "len(rawData)", len(rawData)
print ".. end time to write binary (numpy)", time.time() - startTime , "s"

# read
print ".. start time to read binary (numpy)"
startTime = time.time()
timeAxis, myRawData = np.load(fileName)
print "len(myRawData)", len(myRawData)
print ".. end time to write read (numpy)", time.time() - startTime , "s"

### read 2D matrix all column
myArray = np.array( [[1,2,3,4], [5,6,7,8]] )
print "myArray", myArray

for i in range(len(myArray)):
    for j in range(len(myArray[0])):
        print myArray[i][j]

print myArray[:, 0]+ myArray[:, 1]
