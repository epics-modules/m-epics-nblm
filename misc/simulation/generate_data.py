import numpy as np              # vector operation
import matplotlib.pyplot as plt # plot
import time                     # elapsed time
import sys                      # shell args

# simulation configuration
import config as GBL                    # global variables

# my modules
from nblmlib import functions           # my functions for nBLM simulation
from nblmlib import dataIn              # input file for simulation
from nblmlib import dataOut             # save simulation results
from nblmlib import neutronDetection    # pre-processing and detects neutrons
from nblmlib import analysis            # pedestal calculation,  MPV calculation, sparks and saturation detection, stability ...                            
from nblmlib import plotUtils           # plot raw data and specific events


def continuous_data():
    """ generate continuous neutrons (no dead time) """
    # nb of neutrons to generate
    nbOfNeutrons = 5000

    # gaussian distribution parameters
    amplitudeGaussian   = GBL.NEUTRON_AMPLITUDE_MPV # mV
    sigmaGaussian       = 20 # mV. Note that his parameter could be overwritten by the script to generate only positive neutron

    # space between neutrons
    meanSamplesBetween2Neutrons = 100 # samples
    variationBetween2Neutrons   = 10 # samples. position variation amplitude

    # pedestal level
    pedestal = GBL.PEDESTAL_LEVEL # mV

    # noise amplitude
    noiseAmplitude = 2 # mV

    # add sparks at the end
    sparks = False

    # generate data
    rawDataGenerated, rawDataGeneratedMask = dataIn.generate_neutron_signal(    nbOfNeutrons,
                                                                                amplitudeGaussian,
                                                                                sigmaGaussian,
                                                                                meanSamplesBetween2Neutrons,
                                                                                variationBetween2Neutrons,
                                                                                pedestal,
                                                                                noiseAmplitude,
                                                                                sparks)

    # store genreated signals in 2 files using nBLM data structure
    startFunction = time.time()
    print "start saving generated signal", functions.elapsedTime(startFunction, GBL.START_TIME)
    # signal
    fileName = GBL.PATH_RAW_DATA_FILES + GBL.GENERATED_FILE_NAME + ".npy"
    print "Save raw data in", fileName
    dataOut.saveBinaryData(fileName, rawDataGenerated)
    # mask
    fileName =  GBL.PATH_RAW_DATA_FILES + GBL.GENERATED_FILE_NAME + "Mask.npy"
    print "Save raw data mask in", fileName
    dataOut.saveBinaryData(fileName, rawDataGeneratedMask)
    print "..end saving generated signal", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"

    # plot raw data
    print "start plotting generated signal", functions.elapsedTime(startFunction, GBL.START_TIME)
    startFunction = time.time()
    # polt size
    nbMaxDataToPlot = 1000000 # samples
    nbDataToPlot    = min(nbMaxDataToPlot, len(rawDataGenerated)) # samples
    # xaxis
    xAxis = functions.sample_to_time( np.linspace(0, nbDataToPlot-1, num=nbDataToPlot, dtype=int) )
    # plot
    plt.plot(xAxis, rawDataGenerated[0:nbDataToPlot], label="raw data generated[ 0 :" + str(nbDataToPlot) + "]")
    plt.title("Raw data generated, plot only " + str(round(100*nbDataToPlot/float(len(rawDataGenerated)), 2)) + "% of raw data signal (to NOT slow down too much computer)" )
    plt.xlabel("ns")
    plt.ylabel("mV")
    plt.legend(loc='best')
    plt.show()
    print "..end plotting generated signal", functions.elapsedTime(startFunction, GBL.START_TIME)


def pulsed_data(pulseFrequency, pulseOnPeriod, nbOfNeutronPeakDuringPulseOn, simulationDuration):
    """ generate neutrons only during pulse on (dead time (no neutron) during pulse off) """

    # time to samples
    pulseSamples                = functions.time_to_sample( (1.0/pulseFrequency) * 1E9 )   # Hz to sec to ns and then to samples
    pulseOnSamples              = functions.time_to_sample( pulseOnPeriod * 1E6 )          # to ns then to samples
    simulationDurationSamples   = functions.time_to_sample( simulationDuration * 1E9 )     # to ns then to samples
    print pulseFrequency,       "Hz\t->",    pulseSamples,              "samples"
    print pulseOnPeriod,        "ms\t->",    pulseOnSamples,            "samples"
    print simulationDuration,   "sec\t->",   simulationDurationSamples, "samples"

    # array creation
    rawdata     = np.zeros(0)
    rawdataMask = np.zeros(0)

    # gaussian distribution parameters
    amplitudeGaussian   = GBL.NEUTRON_AMPLITUDE_MPV # mV
    sigmaGaussian       = 20 # mV. Note that his parameter could be overwritten by the script to generate only positive neutron
    # space between neutrons
    variationBetween2Neutrons   = 10000 # samples. position variation amplitude
    meanSamplesBetween2Neutrons = int(pulseOnSamples / nbOfNeutronPeakDuringPulseOn) # space between neutrons during pulse on
    # pedestal level
    pedestal        = GBL.PEDESTAL_LEVEL # mV
    # noise amplitude
    noiseAmplitude  = 2 # mV
    # add sparks at the end
    sparks          = False

    # pulse off length
    pulseOffSamples = pulseSamples - pulseOnSamples

    # pulse off data length
    pulseOffdata    = noiseAmplitude * np.random.random(pulseOffSamples) - noiseAmplitude/2.0 + pedestal 
    pulseOffdataMask= np.zeros(pulseOffSamples) # no neutrons during pulse off

    while len(rawdata) < simulationDurationSamples:
        ## pulse on
        # generate burst of neutrons
        pulseOndata, pulseOndataMask = dataIn.generate_neutron_signal(  nbOfNeutronPeakDuringPulseOn,
                                                                        amplitudeGaussian,
                                                                        sigmaGaussian,
                                                                        meanSamplesBetween2Neutrons,
                                                                        variationBetween2Neutrons,
                                                                        pedestal,
                                                                        noiseAmplitude,
                                                                        sparks)
            
        ## pulse off
        # already created above, always the same

        ## concatenate pulse on + pulse off
        rawdataPulse    = np.concatenate( (pulseOndata,     pulseOffdata) )
        rawdataPulseMask= np.concatenate( (pulseOndataMask, pulseOffdataMask) )

        ## concatenate pulse to previous ones
        rawdata     = np.concatenate( (rawdata,     rawdataPulse) )
        rawdataMask = np.concatenate( (rawdataMask, rawdataPulseMask) )

    # cut the end
    rawdata = rawdata[0 : simulationDurationSamples]

    # store genreated signals in 2 files using nBLM data structure
    startFunction = time.time()
    print "start saving generated signal", functions.elapsedTime(startFunction, GBL.START_TIME)
    generatedName = "rawData_duration" + str(simulationDuration) + "s_freq-" + str(pulseFrequency) + "Hz_pulseOn-" +  str(pulseOnPeriod) + "ms_" + str(nbOfNeutronPeakDuringPulseOn) + "neutrons" 
    # signal
    fileName = GBL.PATH_RAW_DATA_FILES + generatedName + ".npy"
    print "Save raw data in", fileName
    dataOut.saveBinaryData(fileName, rawdata)
    # mask
    fileName =  GBL.PATH_RAW_DATA_FILES + generatedName + "Mask.npy"
    print "Save raw data mask in", fileName
    dataOut.saveBinaryData(fileName, rawdataMask)
    print "..end saving generated signal", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"

    # if the simulation is too long, plot only 2 pulses
    maxPlotSize = 2 * pulseSamples # samples
    if simulationDurationSamples < maxPlotSize:
        # display all raw data
        # axis in ms
        xAxis = functions.sample_to_time( np.linspace( 0, simulationDurationSamples-1, simulationDurationSamples) ) / 1000000.0 
        # plot
        myTitle = "Raw data generated, duration: " + str(simulationDuration) + "s\npulse frequency: " + str(pulseFrequency) + "Hz, pulse length: " + str(pulseOnPeriod) + "ms, number of neutrons per pulse: " + str(nbOfNeutronPeakDuringPulseOn) + "neutrons" 
    else:
        # to big to diplay all raw data
        # axis in ms
        xAxis = functions.sample_to_time( np.linspace( 0, maxPlotSize-1, maxPlotSize) ) / 1000000.0 
        # plot
        myTitle = "Raw data generated (plot only 2 pulses), duration: " + str(simulationDuration) + "s\npulse frequency: " + str(pulseFrequency) + "Hz, pulse length: " + str(pulseOnPeriod) + "ms, number of neutrons per pulse: " + str(nbOfNeutronPeakDuringPulseOn) + "neutrons" 
        # resize rawdata
        rawdata = rawdata[0 : maxPlotSize]

    # plt.title(myTitle)
    # plt.plot(xAxis, rawdata, marker=".", markersize=2)
    # plt.xlabel("ms")
    # plt.ylabel("mV")
    # plt.show()



if __name__ == "__main__":

    # check args
    print 'sys.argv: ', sys.argv
    if len(sys.argv) < 2:
        exit("Wrong arg. use: 'python genereate_data.py <mode>' with mode=0 => contiuous, mode=1 => simulate ESS pulse")
    
    # get arg
    mode = int(sys.argv[1])

    # decode
    CONTINIOUS  = 0
    PULSED      = 1

    # cehck directory path
    functions.check_simulation_paths()

    if mode == CONTINIOUS:
        # generate continuous neutrons (no dead time)
        continuous_data()
    elif mode == PULSED:
        #g enerate neutrons only during pulse on (dead time (no neutron) during pulse off)

        # number de neutrons per pulse (to test different cases: normal, accident ...)
        nbOfNeutronPeakDuringPulseOn = [10, 100, 1000, 10000] # neutrons

        # generate the file for the differents cases
        for i in range(len(nbOfNeutronPeakDuringPulseOn)):

            print "\n\n"
            print "********************************"
            print nbOfNeutronPeakDuringPulseOn[i], "neutrons in pulse on"
            print "********************************"

            # pulse paramters
            pulseFrequency                  = 14    # Hz
            pulseOnPeriod                   = 3     # ms
            # simulation duration
            simulationDuration              = 1/14.0 # sec

            # generate signal + savec it in binary file
            pulsed_data(pulseFrequency, pulseOnPeriod, nbOfNeutronPeakDuringPulseOn[i], simulationDuration)
    else:
        exit("Wrong arg. use: 'python genereate_data.py <mode>' with mode=0 => contiuous, mode=1 => simulate ESS pulse")