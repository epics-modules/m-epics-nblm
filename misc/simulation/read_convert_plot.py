import os                       # check if directory/file exists
import sys                      # shell args 
import matplotlib.pyplot as plt # plots
import numpy                    # vector operations
import os                       # check if directory/file exists

# simulation configuration
import config as GBL            # global variables

# my modules
from nblmlib import functions   # my functions for nBLM simulation
from nblmlib import dataIn      # input file for simulation
from nblmlib import dataOut     # input file for simulation

def get_shell_argument(argNumber, nLim, pLim, deafutValue):
    """ get shell arg and check limit """
    try:
        value           = int(sys.argv[argNumber])
        defautValueUsed = False
    except:
        value           = deafutValue # %
        defautValueUsed = True
    # check limits
    if value > pLim:
        value = pLim  # %
    elif value < nLim:  
        value = nLim    # %
    return value, defautValueUsed

def get_file_name(pathFile):
    """ from arg given in shell, extract file name""" 

    # split path
    pathFileSplitted    = pathFile.split("/")

    # last word splitted is the file name
    fileName =pathFileSplitted[-1]
    
    return fileName


# file name
if len(sys.argv) > 1 :
    pathFile = sys.argv[1]
    print "path file:", pathFile
else:
    exit("FAILED: no file given in arg shell\n  - python read_convert_plot.py <file .txt or .npy> <start %> <stop %>")

# start raw data (%)
startPlotPercent, startDefautValueUsed  = get_shell_argument(argNumber=2, nLim=0, pLim=100, deafutValue=0)
# stop raw data (%)
stopPlotPercent, stopDefautValueUsed    = get_shell_argument(argNumber=3, nLim=0, pLim=100, deafutValue=100)
# warning defaut value used
if startDefautValueUsed == True:
    print "[W] defaut value used for start raw data:", startPlotPercent, "%"
if stopDefautValueUsed == True:
    print "[W] defaut value used for stop raw data:", stopPlotPercent, "%"

# get path and file name
GBL.RAW_DATA_FILE = get_file_name(pathFile)
print "GBL.RAW_DATA_FILE:", GBL.RAW_DATA_FILE
# put start and stop %
GBL.START_RAWDATA   = startPlotPercent
GBL.STOP_RAWDATA    = stopPlotPercent
# get data
rawData, rawDataMask, NB_OF_SAMPLES_ACQUIRED  = dataIn.get_data()

# if .txt file, convert it in .npy file it doesn't exist
if ".txt" in pathFile:

    # name .npy file
    fileNampe_npy = pathFile[0:len(pathFile)-3] + "npy"
    print "numpy file name:", fileNampe_npy

    # check if .npy file exist
    if os.path.isfile(fileNampe_npy) == False:
        print "numpy file doesn't exit: creation of the file..."
        dataOut.saveBinaryData(fileNampe_npy, rawData) # save raw data

        # save mask if possible
        if "generated" in fileNampe_npy:
            dataOut.saveBinaryData(fileNampe_npy, rawDataMask) # save raw data mask

    else:
        print "numpy file already exits: no modification of the file..."

# plot data
plt.figure()
myTitle = "plot " + str(startPlotPercent) + " - " + str(stopPlotPercent) + " % of the file"
plt.title(myTitle)
plt.plot(rawData, label="raw data")
if "generated" in pathFile:
    plt.plot(rawData, label="raw data mask")
plt.xlabel("samples (1 sample = 4ns)")
plt.ylabel("mV")
plt.legend()
plt.show()



