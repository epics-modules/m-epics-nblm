# my module
from nblmlib import dataIn              # input file for simulation

## group file from Laura
# get data from several files

# file info
pathFile = "~/nBLMData/fast/48-500-750-2.2uA/"
nbLigneHeader = 5 # header is 5 lines long

# output file
outputFileName = "FAST_IPHI_48-500-750-2.2uA.txt"

# NB: THIS FUNCTION IS CUSTOMISABLE DEPENDING ON YOUR NEEDS.
dataIn.group_files(0, 10, pathFile, nbLigneHeader, outputFileName)