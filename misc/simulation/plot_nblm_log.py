from nblmlib import plotUtils           # plot raw data and specific events
import matplotlib.pyplot as plt         # plots

import config as GBL                    # global variables
from nblmlib import functions           # my functions for nBLM simulation
from nblmlib import plotUtils

# m-epics-nblm path
simulationPath          = "/home/vnadot/dev/m-epics-nblm/misc/simulation/"          # my laptop
# simulationPath          = "/home/iocuser/devspace/m-epics-nblm/misc/simulation/"    # MTCA crate CPU
GBL.PATH_SIMULATION     = simulationPath
GBL.PATH_RAW_DATA_FILES = GBL.PATH_SIMULATION + "rawDataFiles/"
GBL.PATH_NBLM_LOGS      = GBL.PATH_SIMULATION + "logs/"

# check simulation paths
functions.check_simulation_paths()

# plotting full window using nblm log file
GBL.SAVE_LOGS = True    # to be able to plot data
plotUtils.plot_nblm_logs()
plt.show()