import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
from scipy.optimize import curve_fit
from scipy import asarray as ar,exp

import sys
PATH_M_EPICS_NBLM = "/home/iocuser/devspace/m-epics-nblm/"
sys.path.insert(0, PATH_M_EPICS_NBLM + "misc/simulation")
import config as GBL                    # global variables

# my modules
from nblmlib import functions           # my functions for nBLM simulation
from nblmlib import dataIn              # input file for simulation
from nblmlib import dataOut             # save simulation results
from nblmlib import neutronDetection    # pre-processing and detects neutrons
from nblmlib import analysis            # pedestal calculation, counts neutons, MPV calculation, sparks and saturation detection, stability ...                            
from nblmlib import plotUtils           # plot raw data and specific events


def add_event(eventAmplitude, eventDuration, windowSize, explanation, eventNumber):
    # event shape
    eventShape = np.zeros(windowSize)
    gaussianFit = np.zeros(len(eventShape))
    start = int(windowSize/2) - int(eventDuration/2)
    for sample in range(start, start + eventDuration):
            eventShape[sample] = eventAmplitude

    if len(explanation) != 0:
        # plot event number
        annotation = str(eventNumber) + ": " + explanation + ":TOT:" + str(eventDuration) + ", amp:" + str(eventAmplitude) + "mV"
        x = (eventNumber-1)*windowSize + windowSize/2
        y = eventAmplitude - 0.3
        plt.annotate(str(eventNumber), xy=(x, y), xytext=(x,y), fontsize=8)
        plt.plot([0,0],[0,0], color='black', label=annotation)
        plt.legend(loc='best')

        # debug
        # plt.plot(eventShape, label=annotation)
        # plt.show()

    return list(eventShape), list(gaussianFit)

# signals
eventDetectionInput     = []
eventDetectionInputFit  = []
windowEventSize         = int(1.2 * GBL.INTEGRATION_PERIOD)
paddingSize = 100 # samples

# # padding start
# eventShape, gaussianFit = add_event(0, 0, paddingSize, "", 0)
# eventDetectionInput     = eventDetectionInput + eventShape
# eventDetectionInputFit  = eventDetectionInputFit + gaussianFit

# event number
eventNumber             = 1

# *** noise ***
# noise limit to be an event
eventAmplitude          = GBL.EVENT_DETECTION_THRESHOLD # mV
eventDuration           = GBL.NEUTRON_TOT_MIN
explanation             = "noise"
eventShape, gaussianFit = add_event(eventAmplitude, eventDuration, windowEventSize, explanation, eventNumber)
eventDetectionInput     = eventDetectionInput + eventShape
eventDetectionInputFit  = eventDetectionInputFit + gaussianFit
eventNumber             = eventNumber + 1


# *** bad event ***
# because of TOT and amplitude
eventAmplitude          = GBL.EVENT_DETECTION_THRESHOLD - 1 # mV
eventDuration           = GBL.NEUTRON_TOT_MIN - 1 
explanation             = "bad event (because of TOT and amplitude)"
eventShape, gaussianFit = add_event(eventAmplitude, eventDuration, windowEventSize, explanation, eventNumber)
eventDetectionInput     = eventDetectionInput + eventShape
eventDetectionInputFit  = eventDetectionInputFit + gaussianFit
eventNumber             = eventNumber + 1

# because of TOT only 
eventAmplitude          = GBL.NEUTRON_AMPLITUDE_MIN # mV
eventDuration           = GBL.NEUTRON_TOT_MIN - 1
explanation             = "bad event (because of TOT only)"
eventShape, gaussianFit = add_event(eventAmplitude, eventDuration, windowEventSize, explanation, eventNumber)
eventDetectionInput     = eventDetectionInput + eventShape
eventDetectionInputFit  = eventDetectionInputFit + gaussianFit
eventNumber             = eventNumber + 1

# because amplitude only
eventAmplitude          = GBL.EVENT_DETECTION_THRESHOLD - 1 # mV
eventDuration           = GBL.NEUTRON_TOT_MIN
explanation             = "bad event (because of amplitude only)"
eventShape, gaussianFit = add_event(eventAmplitude, eventDuration, windowEventSize, explanation, eventNumber)
eventDetectionInput     = eventDetectionInput + eventShape
eventDetectionInputFit  = eventDetectionInputFit + gaussianFit
eventNumber             = eventNumber + 1

# pile-up
eventAmplitude          = GBL.EVENT_DETECTION_THRESHOLD - 1 # mV
eventDuration           = GBL.TOT_PILE_UP_START
explanation             = "bad event (pile-up)"
eventShape, gaussianFit = add_event(eventAmplitude, eventDuration, windowEventSize, explanation, eventNumber)
eventDetectionInput     = eventDetectionInput + eventShape
eventDetectionInputFit  = eventDetectionInputFit + gaussianFit
eventNumber             = eventNumber + 1

# TOT limit reached
eventAmplitude          = GBL.EVENT_DETECTION_THRESHOLD - 1 # mV
eventDuration           = GBL.TOT_PILE_UP_LIMIT_REACHED
explanation             = "bad event (TOT limit reached)"
eventShape, gaussianFit = add_event(eventAmplitude, eventDuration, windowEventSize, explanation, eventNumber)
eventDetectionInput     = eventDetectionInput + eventShape
eventDetectionInputFit  = eventDetectionInputFit + gaussianFit
eventNumber             = eventNumber + 1

# # padding after TOT limit reached
# eventShape, gaussianFit = add_event(0, 0, paddingSize, "", 0)
# eventDetectionInput     = eventDetectionInput + eventShape
# eventDetectionInputFit  = eventDetectionInputFit + gaussianFit
# eventNumber             = eventNumber + 1


### neutron shape ###
# peak
eventAmplitude          = GBL.NEUTRON_AMPLITUDE_MIN -1  # mV
eventDuration           = GBL.NEUTRON_TOT_MIN
explanation             = "neutron (peak)"
eventShape, gaussianFit = add_event(eventAmplitude, eventDuration, windowEventSize, explanation, eventNumber)
eventDetectionInput     = eventDetectionInput + eventShape
eventDetectionInputFit  = eventDetectionInputFit + gaussianFit
eventNumber             = eventNumber + 1

# pile-up
eventAmplitude          = GBL.NEUTRON_AMPLITUDE_MIN -1  # mV
eventDuration           = GBL.TOT_PILE_UP_START
explanation             = "neutrons (pile-up)"
eventShape, gaussianFit = add_event(eventAmplitude, eventDuration, windowEventSize, explanation, eventNumber)
eventDetectionInput     = eventDetectionInput + eventShape
eventDetectionInputFit  = eventDetectionInputFit + gaussianFit
eventNumber             = eventNumber + 1

# TOT limit reached
eventAmplitude          = GBL.NEUTRON_AMPLITUDE_MIN # mV
eventDuration           = GBL.TOT_PILE_UP_LIMIT_REACHED
explanation             = "neutrons (TOT limit reached)"
eventShape, gaussianFit = add_event(eventAmplitude, eventDuration, windowEventSize, explanation, eventNumber)
eventDetectionInput     = eventDetectionInput + eventShape
eventDetectionInputFit  = eventDetectionInputFit + gaussianFit
eventNumber             = eventNumber + 1

# # padding after TOT limit reached
# eventShape, gaussianFit = add_event(0, 0, paddingSize, "", 0)
# eventDetectionInput     = eventDetectionInput + eventShape
# eventDetectionInputFit  = eventDetectionInputFit + gaussianFit
# eventNumber             = eventNumber + 1

# # add one neutron (to check rise time)
# amplitude = GBL.NEUTRON_AMPLITUDE_MIN * 2
# neutronShape = amplitude * signal.gaussian(GBL.LANDAU_WINDOW_DISPLAY, GBL.GAUSSIAN_SIGMA)
# eventDetectionInput     = eventDetectionInput + list(neutronShape)
# eventDetectionInputFit  = eventDetectionInputFit + gaussianFit
# eventNumber             = eventNumber + 1

# store signal in a file using nBLM data structure (python simulation)
GBL.GENERATED_FILE_NAME = "allEventsTheoritical"
fileName                = GBL.PATH_RAW_DATA_FILES + GBL.GENERATED_FILE_NAME + ".txt"
dataOut.list_rawdata_to_file_rawdata(fileName, eventDetectionInput)

# threshold vectors
eventDetectionth    = GBL.EVENT_DETECTION_THRESHOLD * np.ones(duration)
neutronAmpMin       = GBL.NEUTRON_AMPLITUDE_MIN * np.ones(duration)

# plot
duration = len(eventDetectionInput)
plt.title("neutron TOT min: " + str(GBL.NEUTRON_TOT_MIN) + " samples, TOT Pile-UP: " + str(GBL.TOT_PILE_UP_START) + " samples, TOT limit: " + str(GBL.TOT_PILE_UP_LIMIT_REACHED) + " samples\n")
plt.plot(eventDetectionInput,   label='signal for event detection')
plt.plot(eventDetectionth,      label='EVENT_DETECTION_THRESHOLD: ' + str(GBL.EVENT_DETECTION_THRESHOLD) + "mV")
plt.plot(neutronAmpMin,         label='NEUTRON_AMPLITUDE_MIN: '     + str(GBL.NEUTRON_AMPLITUDE_MIN) + "mV")
# plt.plot(eventDetectionInputFit, ':', label='signal gaussian fitted')
plt.xlabel("samples (1sample = 4ns)")
plt.ylabel("mV")
plt.legend(loc='best')
plt.show()