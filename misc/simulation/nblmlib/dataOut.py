import time                             # chrono (elapsed time)
import numpy as np                      # vector operation
import matplotlib.pyplot as plt         # plot

# simulation configuration
import config as GBL                    # global variables

# nblm lib
from nblmlib import functions           
from nblmlib import plotUtils           

def list_rawdata_to_file_rawdata(fileName, rawData):
    """ save data in file nblm file format (.txt). Use saveBinaryData() instead if possible."""
    # check if rawdata directory exists
    functions.check_directory_input_file()
    print "output file:", fileName

    # text format
    # creates and writes data in file 
    file = open(fileName, "w")
    file.write("# Time Ampl\n")
    rawData = rawData / 1000.0 # raw data in V
    for i in range(len(rawData)):
        file.write(str( functions.sample_to_time(i) ) + " " + str(rawData[i]) + "\n") # raw data are stored in V in file
    file.close

def saveBinaryData(fileName, rawData):
    """ save rawData into a numpy binary file """
    # write read numpy (binary)
    # print ".. start saving rawdata in a numpy binary file"
    # startTime = time.time()

    # save data in file
    np.save(fileName, rawData)

    # print number of data saved
    # print "len(rawData)", len(rawData)
    # print ".. saving rawdata in a numpy binary file (", time.time() - startTime , ")s"


def event_detection_logs(myEventList, fileName):
    """ store list events in a file """

    # startFunction = time.time()
    # print "start saving event list", functions.elapsedTime(startFunction, GBL.START_TIME)

    if GBL.SAVE_LOGS == True:

        # store in a file result of every detection
        myFile = GBL.PATH_NBLM_LOGS + fileName # event_detection_data.txt
        file = open(myFile, "w")

        for event in range(len(myEventList)):
            # difference between bad event and neutron shape
            if myEventList[event].isNeutron == False: # bad event
                top = "Event number: " + str(myEventList[event].eventNumber) + ", bad event detected: TOTnotValidated=" + str(myEventList[event].TOTnotValidated) + ", amplitudeNotValidated=" + str(myEventList[event].amplitudeNotValidated) + '\n'
            else: # neutron shape
                top = "Event number: " + str(myEventList[event].eventNumber) + ", neutron shape detected" + '\n'   

            myLog = [top +
                "    - pile-up: "                   + str(myEventList[event].pileUp)            + '\n' +
                "    - TOTlimitReached: "           + str(myEventList[event].TOTlimitReached)   + '\n' +
                "    - event shape caracteristics:"                                             + '\n' +
                "    - charge: "                    + str(myEventList[event].charge)            + '\n' +
                "    - TOT: "                       + str(myEventList[event].TOT)               + '\n' +
                "    - amplitude: "                 + str(myEventList[event].amplitude)         + '\n' +
                "    - rise time: "                 + str(myEventList[event].riseTime)          + '\n' +
                "    - rawDataIndex:"               + str(myEventList[event].rawDataIndex)      + '\n\n'
            ]
            file.write(myLog[0])
        file.close
        print "saved in", myFile
    else:
        print "Save", fileName, "skipped"

    # print "..end saving event list", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"
    return 0

def neutron_detection_logs(eventList):
    return  [
            str(eventList.isNeutron)            + "," +     
            str(eventList.TOTnotValidated)      + "," +  
            str(eventList.amplitudeNotValidated)+ "," +  
            str(eventList.pileUp)               + "," +  
            str(eventList.TOTlimitReached)      + "," +  
            str(eventList.charge)               + "," +  
            str(eventList.TOT)                  + "," +  
            str(eventList.amplitude)            + "," +  
            str(eventList.riseTime)             + "," +  
            str(eventList.rawDataIndex)         + "," +  
            str(eventList.eventNumber)
        ]

def nblm_logs(nbOfIntegration1us, rawDataCorrected, NB_OF_SAMPLES_ACQUIRED, neutronList, neutronCounter1us, neutronCounterPeak1us, neutronCounterPileUp1us, neutronCountingCharge1us, neutronCountingTOT1us, neutronCountingAmp1us, neutronCountingRiseTime1us, badEventList):
    """ store in a log file all data of neutron detection """
    startFunction = time.time()
    print "start nblm logs", functions.elapsedTime(startFunction, GBL.START_TIME)

    if GBL.SAVE_LOGS == True:

        # store in a file result of every detection
        myFile = GBL.PATH_NBLM_LOGS + GBL.NBLM_LOG_FILE_NAME
        file = open(myFile, "w")

        # check data
        if len(neutronList) == 0:
            exit("FAILED: 0 neutron detected, please check file and configuration")

        # store all neutron shape position
        nbOfNeutronShape        = len(neutronList)
        neutronShapePosition    = np.zeros(nbOfNeutronShape, dtype = np.uint32)
        for i in range(nbOfNeutronShape):
            neutronShapePosition[i] = neutronList[i].rawDataIndex
        i_nextNeutronRawData = 0 # number of the next neutron shape
        # check data
        if len(neutronList) == 0:
            nextNeutronRawData = -1
        else:   
            # normal case
            nextNeutronRawData = neutronShapePosition[i_nextNeutronRawData] # pointer on next neutron shape position

        # store all bad events position
        nbOfBadEventShape = len(badEventList)
        badEventPosition = np.zeros(nbOfBadEventShape, dtype = np.uint32)
        for i in range(nbOfBadEventShape):
            badEventPosition[i] = badEventList[i].rawDataIndex
        i_nextBadEventRawData = 0 # number of the next bad event shape
        # check data
        if len(badEventList) == 0:
            nextBadeventRawData = -1
        else:
            # normal case
            nextBadeventRawData = badEventPosition[i_nextBadEventRawData] # pointer on next bad event shape position
        

        for i in range(nbOfIntegration1us): 
            ## data 1MHz
            # charge 1us
            file.write(str(neutronCountingCharge1us[i]) + ";")
            # neutron counting
            file.write(str(neutronCounter1us[i]) + ";")
            # neutron counting (peak only)
            file.write(str(neutronCounterPeak1us[i]) + ";")
            # neutron counting (pile-up only)
            file.write(str(neutronCounterPileUp1us[i]) + ";")

            # TO DO
            # ADC saturation
            # sparks
            # stability
            # ...

            ## data 250Mhz
            integrationPeriodSamples = functions.time_to_sample(GBL.INTEGRATION_PERIOD)
            start   = i * integrationPeriodSamples
            stop    = i * integrationPeriodSamples + integrationPeriodSamples
            ############ WARNING: not optimized at all !! ############################
            for j in range(start, stop):
                ## neutron list
                if j == nextNeutronRawData:
                    log = neutron_detection_logs( neutronList[i_nextNeutronRawData])
                    file.write(log[0] + ";")
                    # point next neutron 
                    if i_nextNeutronRawData < (nbOfNeutronShape-1):
                        i_nextNeutronRawData = i_nextNeutronRawData + 1
                    else:
                        pass # overflow
                    nextNeutronRawData = neutronShapePosition[i_nextNeutronRawData]


                ## bad event list
                if j == nextBadeventRawData:
                    log = neutron_detection_logs( badEventList[i_nextBadEventRawData])
                    file.write(log[0] + ";")
                    # point next bad event 
                    if i_nextBadEventRawData < (nbOfBadEventShape-1):
                        i_nextBadEventRawData = i_nextBadEventRawData + 1
                    else:
                        pass # overflow
                    nextBadeventRawData = badEventPosition[i_nextBadEventRawData]

            ## 1us seperation
            file.write("\n")
    else:
        print "Save nblm logs skipped"
    
    duration = time.time()-startFunction
    print "..end nblm logs", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"
    return duration

    