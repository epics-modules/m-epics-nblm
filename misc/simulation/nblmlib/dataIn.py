# python modules
import random                   # for noise
import time                     # chrono (elapsed time)
import os.path                  # check existing file
import numpy as np              # vector operation
import matplotlib.pyplot as plt # plot

# simulation configuration
import config as GBL            # global variables

from nblmlib import functions   # my functions for nBLM simulation
from nblmlib import dataOut     # save simulation results

# gaussian fit
from scipy.optimize import curve_fit
from scipy import asarray as ar,exp
from scipy import signal

def get_sampling(file, nbLigneHeader):
    """ get file data sampling (time between the 2 first value)
        file format:
        Time Ampl
        0 -20.3
        4 -25.4
        8 -45.9
    """
    firstTime = True
    ligneCounter = 0
    with open(file) as rawData :
        previousTime = 0
        for line in rawData : # for each line in the file
            words = line.split() # plit the line in words
            time = words[0]
            amplitude = words[1]
            if len(words) == 2 and ligneCounter >= nbLigneHeader:  # start after header
                a = round(float(time), 12)
                if firstTime == True:
                    # second line of the file: fisrt value
                    firstTime = False
                    b = a; # first sample time saved
                else:
                    # third line of the file
                    Te = a-b
                    if Te <= 0:
                        exit("FAILED: sampling time <= 0")
                    return a-b # difference between first and second sample time
            else:
                # fisrt line of file
                pass
            ligneCounter = ligneCounter + 1
    return -1

def concat_rawData (pathFile, TEFile, nbLigneHeader):
    """ add data of the file to rawData list (time in ns and amp in V)"""

    # check file exist
    if os.path.isfile(pathFile) == False:
        exit("FAILED: wrong location, check the file exist. File path: " + str(pathFile))

    # get number of data in file
    f = open(pathFile, 'r')
    numberOfLine = 0
    for line in f:
        numberOfLine += 1
    numberOfRawData = numberOfLine - 1 # first line is: "Time Ampl"

    # rawData creation
    rawData = np.zeros(numberOfRawData, dtype=np.float16)

    # if over sampling, you don't take each data
    cptMissData = GBL.TE / TEFile
    if TEFile != GBL.TE:
        print "WARNING: sampling time of the file is not equal to GBL.TE"
    
    # read file and put data in rawData
    cptTe           = 0
    indexWrite      = 0
    ligneCounter    = 0
    with open(pathFile) as rawDataFile :
        previousTime = 0
        for line in rawDataFile : # for each line in the file
            words = line.split() # plit the line in words
            time = words[0]
            amplitude = words[1]
            if len(words) == 2 and ligneCounter >= nbLigneHeader:
                if (cptTe % cptMissData) == 0:
                    rawData[indexWrite] = (float(amplitude)*1000) # V to mV
                    indexWrite = indexWrite + 1
                else:
                    pass
                cptTe = cptTe + 1
            ligneCounter = ligneCounter + 1

    return rawData, numberOfRawData

def loadBinaryData(fileName):
    """ load rawData from a numpy binary file """
    # print ".. start loading binary rawdata file"
    # startTime = time.time()

    # load data from numpy file
    rawData = np.load(fileName)

    # print number of data read
    # print "len(rawData)", len(rawData)
    # print ".. end loading binary rawdata file", time.time() - startTime , ")s"
    return rawData


def group_files(start, end, pathFile, nbLigneHeader, outputFileName):
    """
    groups several rawdata file in one file. Thomas and Laura give me often several files, I need to concatenate them.
    NB: THIS FUNCTION IS CUSTOMISABLE DEPENDING ON YOUR NEEDS.
    """

    # function duration
    startFunction = time.time()
    print "start grouping files", functions.elapsedTime(startFunction, GBL.START_TIME)

    # read files in folder from start to end and save rawdata
    for i in range(start, end):
        # select file in folder
        channel = "C2"
        fileName = channel + "_Vm500_Vd750_" + "{:05.0f}".format(i) + ".txt"
        myFile = pathFile + fileName
        print myFile

        # file sampling
        samplingFile = get_sampling(myFile, nbLigneHeader)
        # force sampling !! dangerous !! but necessary in this case because time in the file is in s and not in ns
        samplingFile = 4

        # fetch data from the file
        rawData2, numberOfRawData = concat_rawData (myFile, samplingFile, nbLigneHeader)

        # add new data to previous
        if i > start:
            # not first file (rawData is not empty)
            rawData = np.concatenate((rawData, rawData2))
        else:
            # first file (rawData is empty)
            rawData = rawData2

    print "..end grouping files", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"

    # see rawdata
    plt.plot(rawData)
    plt.show()

    print "start saving data in a file", functions.elapsedTime(startFunction, GBL.START_TIME)
    # save data in a file using nBLM data structure
    outputFile =  GBL.PATH_RAW_DATA_FILES + outputFileName
    dataOut.list_rawdata_to_file_rawdata(outputFile, rawData)
    print "output file:", outputFile
    print "..end saving data in a file", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"

    return 0


# global varibles for rawdata selection with mouse
g_YaxisMin = 0
g_YaxisMax = 0
g_indexRawDataStart = 0
g_indexRawDataStop = 0
g_clickNumber = 0
g_XaxisMax = 0
def callbackClickMouse (event):
    """ get data set by the user with the mouse """
    global g_clickNumber
    global g_indexRawDataStart
    global g_indexRawDataStop

    # increment number of click
    g_clickNumber = g_clickNumber + 1

    # get mouse position
    xMouse = int(event.xdata)
    yMouse = round(event.ydata,3)

    # first click
    if g_clickNumber == 1: 
        # print "start index rawdata selection:"
        g_indexRawDataStart = xMouse
        if g_indexRawDataStart < 0:
            g_indexRawDataStart = 0
        # print "  -", g_indexRawDataStart, "samples"
    # second click
    elif g_clickNumber == 2:
        # print "stop index rawdata selection:"
        g_indexRawDataStop = xMouse
        # print g_XaxisMax, g_XaxisMax
        if g_indexRawDataStop > g_XaxisMax:
            g_indexRawDataStop = g_XaxisMax
        # print "  -", g_indexRawDataStop, "samples"
    
    # check click seclection
    if g_clickNumber > 1 and g_indexRawDataStart >= g_indexRawDataStop:
        # reset
        print "FAILED: restart selection"
        g_indexRawDataStart = 0
        g_indexRawDataStop = 0
        g_clickNumber = 0
        # close plt.show()
    else:    

        # first click
        if g_clickNumber == 1: 
            plt.plot([g_indexRawDataStart, g_indexRawDataStart], [g_YaxisMin, g_YaxisMax], label="start index raw data")
            plt.legend(loc='best')
            plt.show()
        # second and last click
        elif  g_clickNumber == 2:
            plt.plot([g_indexRawDataStop, g_indexRawDataStop], [g_YaxisMin, g_YaxisMax], label="stop index raw data")
            plt.legend(loc='best')
            plt.pause(1)    # plot for 1 sec
            plt.close()     # then close it

def get_data():
    """ get data from a file:
    You can select part of the signal using GBL.START_RAWDATA and GBL.STOP_RAWDATA in %
    You can also use marker to select the part of the signal you want (GBL.MARKER_SELECTION)
    """

    # function duration
    startFunction = time.time()
    print "start loading data..", functions.elapsedTime(startFunction, GBL.START_TIME)

    # check if rawdata directory exists
    functions.check_directory_input_file()

    # path + file name
    myFile = GBL.RAW_DATA_FILE
    print "file:", myFile

    # check file exist
    if os.path.isfile(myFile) == False:
        exit("FAILED: wrong location, check the file exist. File path: " + str(myFile))

    # neutron mask name
    generatedDataMaskFileName   = "generatedDataMask"

    if ".txt" in myFile:
        # text file
        print "[W] the input file is a text file. Loading it is taking time. Convert it in binary format (.npy), it will be much master."
        # header
        nbLigneHeader = 1
        # file sampling
        samplingFile = get_sampling(myFile, nbLigneHeader)
        # get raw data
        rawData, rawDataLength = concat_rawData(myFile, samplingFile, nbLigneHeader); # add to rawData the data of the new file
        # get neutron position (only for generated ones)
        if "generated" in myFile:
            myFileMask                  = GBL.PATH_RAW_DATA_FILES + generatedDataMaskFileName + ".txt"
            rawDataMask, null           = concat_rawData(myFileMask, samplingFile, 1); # add to rawData the data of the new file
        else:
            rawDataMask = np.zeros(rawDataLength, dtype=np.int) # no information about nuetrons position because it is real data
    elif ".npy" in myFile:
        # numpy binary file
        rawData         = loadBinaryData(myFile)
        rawDataLength   = len(rawData)
        if "generated" in myFile:
            myFileMask = GBL.PATH_RAW_DATA_FILES + generatedDataMaskFileName + ".npy"
            rawDataMask= loadBinaryData(myFileMask)
        else:
            rawDataMask = np.zeros(rawDataLength, dtype=np.int) # no information about nuetrons position because it is real data
    else:
        exit("FAILED: file format")

    # debug only
    # nbOFTimeToRepeatFile    = 100
    # myRawdata               = rawData
    # myRawdataMask           = rawDataMask
    # for i in range(nbOFTimeToRepeatFile):
    #     myRawdata               = np.concatenate( (myRawdata, rawData),         0)
    #     myRawdataMask           = np.concatenate( (myRawdataMask, rawDataMask), 0)
    # rawData         = myRawdata
    # rawDataMask     = myRawdataMask
    # rawDataLength   = len(rawData)

    # number of samples in the file
    print rawDataLength, "samples loaded from file -> ", functions.sample_to_time(rawDataLength)/1000000.0, " ms"
        
    # data part selection in %
    startPercent    = GBL.START_RAWDATA
    endPercent      = GBL.STOP_RAWDATA
    if startPercent < 0 or startPercent > 100 or  endPercent < 0 or endPercent > 100: # check data input is in %
        exit("FAILED: startPercent and endPercent are not coherent")
    iStart  = int(startPercent  * rawDataLength/100.0)
    iStop   = int(endPercent    * rawDataLength/100.0)
    if iStart >= iStop:
        print "index start:", iStart, "sample"
        print "index stop:", iStop, "sample"
        exit("FAILED: startPercent and endPercent are not coherent")
    rawData         = rawData[iStart:iStop]
    rawDataMask     = rawDataMask[iStart:iStop]
    rawDataLength   = len(rawData)
    print rawDataLength, "samples selected with % (from", startPercent, "% to",  endPercent, "% of the file signal)"

    # data selection with markers
    if GBL.MARKER_SELECTION == True:
        # selection with cursor
        fig, ax = plt.subplots()
        plt.title("Raw data selection with markers")
        # need to be global, otherwise python create a local vabiable for the function
        global g_YaxisMin
        global g_YaxisMax
        global g_XaxisMax
        # min amplitude raw data
        g_YaxisMin = min(rawData)
        # max amplitude raw data
        g_YaxisMax = max(rawData)
        # size rawData
        g_XaxisMax = rawDataLength
        plt.plot(rawData, label='rawData')
        plt.legend(loc='best')

        # when a mous click on the plot happens, callbackClickMouse function is called
        cid = fig.canvas.callbacks.connect('button_press_event', callbackClickMouse)
        plt.show()

        # disconect callback event
        fig.canvas.mpl_disconnect(cid)

        # markers selection
        rawData = rawData[g_indexRawDataStart : g_indexRawDataStop]
        rawDataLength = len(rawData)
        print rawDataLength, "samples selected with markers"

    if GBL.USE_DATA_UNDER_EVENT_THRESHOLD == False:
        # remove range of data where there is no neutrons
        # goals:
        #  - less space in RAM
        #  - faster process

        # output data
        rawDataInteresting      = np.zeros(0)
        rawDataInterestingMask  = np.zeros(0)

        # get position of event (falling edge)
        triggerLevel    = GBL.EVENT_DETECTION_THRESHOLD + GBL.PEDESTAL_LEVEL
        maskTrig        = (rawData[:-1] <= triggerLevel) & (rawData[1:] > triggerLevel)
        maskTrigIndexes = np.flatnonzero((rawData[:-1] <= triggerLevel) & (rawData[1:] > triggerLevel))
        print "Number of triggers (failling edge):", len(maskTrigIndexes)
        # check there are triggers
        if len(maskTrigIndexes) == 0:
            if GBL.USE_DATA_UNDER_EVENT_THRESHOLD == False:
                print"[W] no data loaded, script aborted. Please check your event detection threshold"
            else:
                print"[W] no data loaded, script aborted."
            # debug: plot data
            nbDataToPlot = 1000
            xAxis = functions.sample_to_time( np.linspace(0, nbDataToPlot-1, num=nbDataToPlot, dtype=int) )
            plt.title("debug: plot " + str(nbDataToPlot) + " samples")
            plt.plot(xAxis, rawData[0:nbDataToPlot], label="rawdata")
            plt.plot(xAxis, triggerLevel*np.ones(nbDataToPlot), label="triggerLevel")
            plt.xlabel("ns")
            plt.ylabel("mV")
            plt.legend()
            plt.show()
            exit()

        # pre and post trig
        preTrig     = functions.time_to_sample(GBL.USE_DATA_UNDER_EVENT_THRESHOLD_PRE_TRIG)  # ns to samples
        postTrig    = functions.time_to_sample(GBL.USE_DATA_UNDER_EVENT_THRESHOLD_POST_TRIG) # ns to samples

        locked          = False
        lockedSample    = 0
        for i in range(len(maskTrigIndexes)):

            if (locked == True) and (maskTrigIndexes[i] > (lockedSample + postTrig)):
                # unlock
                # print "locked"
                locked = False   # will use this peak as first peak in the window (trigger)

            if locked == False:
                # print "Triggered at", maskTrigIndexes[i], "samples"
                # first event in the window (trigger)
                locked          = True   # will ignore other peak in the window
                lockedSample    = maskTrigIndexes[i]
                # save data
                start   = maskTrigIndexes[i] - preTrig      # samples
                if (maskTrigIndexes[i] + postTrig) < len(rawData):
                    # no overflow
                    stop = maskTrigIndexes[i] + postTrig     # samples
                else:
                    # overflow
                    stop = len(rawData) # samples
                rawDataInteresting      = np.concatenate( (rawDataInteresting,      rawData     [start : stop]))  # save pre event and post event
                rawDataInterestingMask  = np.concatenate( (rawDataInterestingMask,  rawDataMask [start : stop]))

        # save rawdata
        rawData     = rawDataInteresting
        rawDataMask = rawDataInterestingMask
        print "Removed all data ranges with no events.", len(rawData), "samples will be used(", functions.sample_to_time(len(rawData))/1000 ,"us)."
        print "Gain of", round( (1 - len(rawData)/float(rawDataLength) )*100, 2), "%"

    # add padding to avoid bug in scope
    paddingSize = functions.get_padding_size()
    padding     = np.ones(paddingSize) * GBL.PEDESTAL_LEVEL
    rawData     = np.concatenate( (padding, rawData, padding)       ,0)
    rawDataMask = np.concatenate( (padding, rawDataMask, padding)   ,0)

    # final size rawData
    NB_OF_SAMPLES_ACQUIRED = len(rawData)
    
    # # debug
    # plt.plot(rawData[0:1000000])
    # plt.show()
    # exit()

    duration = time.time()-startFunction
    print "..end loading data", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"
    return rawData, rawDataMask, NB_OF_SAMPLES_ACQUIRED, duration


def add_neutron(output, maskNeutrons, index, amplitude):
    """ Add a neutron (gaussian shape) to a main output buffer at a specific position"""

    # generate gaussian signal
    sigma           = GBL.GAUSSIAN_SIGMA
    neutronShape    = amplitude * signal.gaussian(GBL.GAUSSIAN_WINDOW_DISPLAY, std=sigma)

    # add it to table
    start = index
    if index + len(neutronShape) > len(output):
        # out of the window
        stop = len(output)
        # end neutrons shape
    else:
        # neutron shape fit the window
        stop = index + len(neutronShape)
        # end neutrons shape

    # index max of neutronShapeList
    iAmpMax = np.argmin(neutronShape)

    # save position
    maskNeutrons[index + iAmpMax] = amplitude + GBL.PEDESTAL_LEVEL

    # add neutronShape to output
    output[start : stop] = output[start : stop] + neutronShape[0 : stop-start]


def generate_neutron_signal(nbOfNeutrons, amplitudeGaussian, sigmaGaussian, meanSamplesBetween2Neutrons, variationBetween2Neutrons, pedestal, noiseAmplitude, sparks):
    """ generate neutron signal """

    # function duration
    startFunction = time.time()
    print "start generating neutron signal data", functions.elapsedTime(startFunction, GBL.START_TIME)

    # check amplitude is negative
    if amplitudeGaussian > 0:
        exit("FAILED, please select a negative mean amplitude")

    # check sigmaGaussian which is depending on amplitudeGaussian in order to NOT generate positive neutrons
    if -amplitudeGaussian < 4 * sigmaGaussian : # (3*sigmaGaussian <=> 99.7% of the gaussian energy)
        sigmaGaussian = -amplitudeGaussian / 4 # mV

    # generate amplitude for neutrons
    amplitudeNeutrons = np.random.normal(amplitudeGaussian, sigmaGaussian, nbOfNeutrons)  # mean and standard deviation and nb of value

    # get number of negative amplitude
    nbOfPositiveAmplitude = np.sum (amplitudeNeutrons > 0)
    if nbOfPositiveAmplitude > 0:
        print "[W]:", nbOfPositiveAmplitude, "positive amplitude(s) over", np.sum (amplitudeNeutrons < 0), "negative amplitude(s)"

    # sample position
    start_position = variationBetween2Neutrons/2
    durationSimulation = start_position + meanSamplesBetween2Neutrons * nbOfNeutrons + int(variationBetween2Neutrons/2.0) + GBL.GAUSSIAN_WINDOW_DISPLAY #samples

    # pedestal and noise
    detector_signal_gaussian = noiseAmplitude * np.random.random(durationSimulation) - noiseAmplitude/2.0 + pedestal 

    # add sparks
    # if sparks == True:
    #     sparkSignal = [-12.306299999999998, -12.306299999999998, -10.7063, -13.9062, -12.306299999999998, -12.306299999999998, -17.106099999999998, -15.5061, -15.5061, -15.5061, -17.106099999999998, -18.706, -17.106099999999998, -18.706, -18.706, -21.9059, -20.306, -20.306, -23.5058, -21.9059, -20.306, -21.9059, -23.5058, -23.5058, -23.5058, -23.5058, -23.5058, -23.5058, -25.105800000000002, -25.105800000000002, -26.7057, -26.7057, -26.7057, -26.7057, -28.305699999999998, -29.9056, -28.305699999999998, -31.505499999999998, -29.9056, -28.305699999999998, -31.505499999999998, -28.305699999999998, -31.505499999999998, -36.305299999999995, -33.105500000000006, -29.9056, -36.305299999999995, -34.7054, -34.7054, -34.7054, -34.7054, -37.905300000000004, -39.505199999999995, -36.305299999999995, -37.905300000000004, -39.505199999999995, -36.305299999999995, -39.505199999999995, -41.1052, -37.905300000000004, -42.7051, -41.1052, -41.1052, -41.1052, -41.1052, -42.7051, -44.305, -42.7051, -45.905, -44.305, -44.305, -45.905, -23.5058, -15.5061, -20.306, -26.7057, -28.305699999999998, -21.9059, -29.9056, -26.7057, -20.306, -18.706, -20.306, -26.7057, -29.9056, -26.7057, -20.306, -20.306, -25.105800000000002, -25.105800000000002, -28.305699999999998, -26.7057, -28.305699999999998, -23.5058, -20.306, -21.9059, -25.105800000000002, -28.305699999999998, -26.7057, -23.5058, -21.9059, -23.5058, -23.5058, -26.7057, -28.305699999999998, -23.5058, -25.105800000000002, -25.105800000000002, -21.9059, -21.9059, -25.105800000000002, -26.7057, -25.105800000000002, -23.5058, -21.9059, -20.306, -26.7057, -25.105800000000002, -26.7057, -28.305699999999998, -21.9059, -21.9059, -20.306, -21.9059, -23.5058, -28.305699999999998, -26.7057, -21.9059, -21.9059, -21.9059, -23.5058, -28.305699999999998, -26.7057, -25.105800000000002, -26.7057, -20.306, -23.5058, -26.7057, -25.105800000000002, -25.105800000000002, -25.105800000000002, -23.5058, -21.9059, -23.5058, -25.105800000000002, -25.105800000000002, -25.105800000000002, -23.5058, -25.105800000000002, -25.105800000000002, -21.9059, -23.5058, -26.7057, -23.5058, -23.5058, -23.5058, -23.5058, -25.105800000000002, -25.105800000000002, -23.5058, -23.5058, -23.5058, -23.5058, -23.5058, -20.306, -21.9059, -25.105800000000002, -26.7057, -23.5058, -21.9059, -23.5058, -23.5058, -23.5058, -21.9059, -25.105800000000002, -25.105800000000002, -23.5058, -21.9059, -21.9059, -25.105800000000002, -21.9059, -25.105800000000002, -23.5058, -21.9059, -25.105800000000002, -23.5058, -23.5058, -25.105800000000002, -25.105800000000002, -23.5058, -21.9059, -25.105800000000002, -23.5058, -23.5058, -25.105800000000002, -21.9059, -21.9059, -25.105800000000002, -23.5058, -23.5058, -23.5058, -25.105800000000002, -23.5058, -25.105800000000002, -23.5058, -25.105800000000002, -21.9059, -21.9059, -25.105800000000002, -25.105800000000002, -23.5058, -21.9059, -20.306, -23.5058, -23.5058, -25.105800000000002, -23.5058, -23.5058, -20.306, -20.306, -23.5058, -25.105800000000002, -21.9059, -23.5058, -25.105800000000002, -21.9059, -23.5058, -21.9059, -21.9059, -23.5058, -21.9059, -23.5058, -25.105800000000002, -23.5058, -23.5058, -23.5058, -25.105800000000002, -21.9059, -23.5058, -23.5058, -20.306, -23.5058, -21.9059, -21.9059, -25.105800000000002, -23.5058, -21.9059, -25.105800000000002, -21.9059, -21.9059, -23.5058, -23.5058, -18.706, -21.9059, -20.306, -21.9059, -21.9059, -21.9059, -17.106099999999998, -18.706, -9.106390000000001, 27.6922, 27.6922, 27.6922, 27.6922, -380.29200000000003, -380.29200000000003, 27.6922, 27.6922, 27.6922, 27.6922, 27.6922, -380.29200000000003, -265.09700000000004, 27.6922, 27.6922, 27.6922, 27.6922, -380.29200000000003, -380.29200000000003, -380.29200000000003, -45.905, 27.6922, 27.6922, 27.6922, 27.6922, -380.29200000000003, -372.29200000000003, 27.6922, 27.6922, 27.6922, 27.6922, -380.29200000000003, -380.29200000000003, -380.29200000000003, -124.30199999999999, 27.6922, 27.6922, 27.6922, 27.6922, -380.29200000000003, -380.29200000000003, -15.5061, 27.6922, 27.6922, 27.6922, -266.69599999999997, -380.29200000000003, -317.895, 21.2924, 27.6922, 27.6922, 27.6922, -143.50099999999998, -380.29200000000003, -380.29200000000003, -116.302, 27.6922, 27.6922, 27.6922, -60.3044, -300.29499999999996, -207.499, 27.6922, 27.6922, 27.6922, 27.6922, -199.49900000000002, -357.89300000000003, -351.493, -106.703, 27.6922, 27.6922, 27.6922, 8.49293, -234.69799999999998, -204.299, 10.0929, 27.6922, 27.6922, 27.6922, -157.901, -269.896, -237.898, -50.7048, 27.6922, 27.6922, 27.6922, -5.90651, -202.69899999999998, -205.899, -41.1052, 27.6922, 27.6922, 27.6922, -82.70360000000001, -188.29999999999998, -154.701, -21.9059, 27.6922, 27.6922, 27.6922, -37.905300000000004, -183.5, -202.69899999999998, -61.904399999999995, 27.6922, 27.6922, 27.6922, -25.105800000000002, -129.102, -116.302, -20.306, 27.6922, 27.6922, 27.6922, -52.304700000000004, -151.501, -164.3, -53.9047, 27.6922, 27.6922, 27.6922, -12.306299999999998, -109.903, -111.50200000000001, -36.305299999999995, 27.6922, 27.6922, 27.6922, -52.304700000000004, -117.902, -125.90199999999999, -37.905300000000004, 27.6922, 27.6922, 27.6922, -7.506449999999999, -98.7029, -103.503, -45.905, 26.092299999999998, 27.6922, 27.6922, -36.305299999999995, -89.10329999999999, -97.103, -41.1052, 27.6922, 27.6922, 27.6922, -23.5058, -90.7033, -105.10300000000001, -50.7048, 10.0929, 27.6922, 27.6922, -25.105800000000002, -74.7039, -79.5037, -44.305, 13.2928, 27.6922, 21.2924, -36.305299999999995, -82.70360000000001, -93.90310000000001, -49.1049, -1.1067, 27.6922, 18.0926, -23.5058, -69.9041, -73.1039, -52.304700000000004, -5.90651, 18.0926, 0.49324199999999996, -36.305299999999995, -69.9041, -81.1036, -50.7048, -10.7063, 13.2928, 5.29306, -31.505499999999998, -68.3041, -79.5037, -55.5046, -21.9059, 0.49324199999999996, -10.7063, -37.905300000000004, -66.7042, -73.1039, -52.304700000000004, -20.306, -1.1067, -10.7063, -36.305299999999995, -68.3041, -74.7039, -58.7045, -29.9056, -9.106390000000001, -17.106099999999998, -41.1052, -65.1042, -74.7039, -53.9047, -29.9056, -15.5061, -21.9059, -42.7051, -65.1042, -71.504, -58.7045, -36.305299999999995, -23.5058, -23.5058, -41.1052, -60.3044, -65.1042, -57.1045, -41.1052, -25.105800000000002, -28.305699999999998, -42.7051, -63.5043, -66.7042, -57.1045, -37.905300000000004, -25.105800000000002, -29.9056, -45.905, -58.7045, -61.904399999999995, -53.9047, -41.1052, -31.505499999999998, -28.305699999999998, -41.1052, -55.5046, -60.3044, -52.304700000000004, -37.905300000000004, -29.9056, -29.9056, -45.905, -55.5046, -58.7045, -50.7048, -37.905300000000004, -28.305699999999998, -33.105500000000006, -37.905300000000004, -50.7048, -52.304700000000004, -49.1049, -37.905300000000004, -33.105500000000006, -31.505499999999998, -37.905300000000004, -49.1049, -50.7048, -44.305, -39.505199999999995, -26.7057, -26.7057, -33.105500000000006, -47.5049, -49.1049, -44.305, -37.905300000000004, -29.9056, -28.305699999999998, -33.105500000000006, -39.505199999999995, -42.7051, -41.1052, -31.505499999999998, -28.305699999999998, -23.5058, -31.505499999999998, -37.905300000000004, -41.1052, -36.305299999999995, -29.9056, -26.7057, -23.5058, -31.505499999999998, -34.7054, -34.7054, -33.105500000000006, -29.9056, -23.5058, -26.7057, -25.105800000000002, -31.505499999999998, -33.105500000000006, -31.505499999999998, -26.7057, -21.9059, -21.9059, -21.9059, -28.305699999999998, -29.9056, -28.305699999999998, -25.105800000000002, -18.706, -20.306, -23.5058, -26.7057, -26.7057, -26.7057, -26.7057, -26.7057, -25.105800000000002, -28.305699999999998, -21.9059, -23.5058, -26.7057, -23.5058, -26.7057, -26.7057, -23.5058, -25.105800000000002, -28.305699999999998, -23.5058, -26.7057, -26.7057, -25.105800000000002, -25.105800000000002, -26.7057, -25.105800000000002, -23.5058, -26.7057, -23.5058, -25.105800000000002, -26.7057, -23.5058, -25.105800000000002, -26.7057, -25.105800000000002, -25.105800000000002, -26.7057, -25.105800000000002, -26.7057, -28.305699999999998, -21.9059, -21.9059, -25.105800000000002, -25.105800000000002, -26.7057, -26.7057, -25.105800000000002, -25.105800000000002, -26.7057, -25.105800000000002, -23.5058, -25.105800000000002, -25.105800000000002, -23.5058, -25.105800000000002, -23.5058, -25.105800000000002, -20.306, -21.9059, -25.105800000000002, -26.7057, -25.105800000000002, -23.5058, -23.5058, -21.9059, -25.105800000000002, -25.105800000000002, -25.105800000000002, -23.5058, -23.5058, -25.105800000000002, -25.105800000000002, -26.7057, -21.9059, -23.5058, -23.5058, -21.9059, -25.105800000000002, -23.5058, -20.306, -21.9059, -23.5058, -23.5058, -21.9059, -21.9059, -21.9059, -25.105800000000002, -21.9059, -23.5058, -23.5058, -21.9059, -25.105800000000002, -21.9059, -20.306, -23.5058, -21.9059, -23.5058, -21.9059, -23.5058, -23.5058, -20.306, -21.9059, -23.5058, -20.306, -23.5058, -20.306, -20.306, -25.105800000000002, -20.306, -18.706, -20.306, -23.5058, -21.9059, -21.9059, -20.306, -21.9059, -21.9059, -20.306, -20.306, -20.306, -20.306, -21.9059, -23.5058, -20.306, -18.706, -21.9059, -21.9059, -20.306, -21.9059, -23.5058]
    #     for i in range(len(sparkSignal)):
    #         detector_signal_gaussian.append(sparkSignal[i] + 25 + pedestal)
    #     durationSimulation = durationSimulation + len(sparkSignal)

    # add RF noise (RFQ)
    # to do 

    # neutron mask 
    maskNeutrons_gaussian = GBL.PEDESTAL_LEVEL * np.ones(durationSimulation)

    # generate signal
    for i in range(nbOfNeutrons):
        plusMinus   = int(variationBetween2Neutrons*(random.random()-1/2.0))
        position    = start_position + i*meanSamplesBetween2Neutrons + plusMinus
        add_neutron(detector_signal_gaussian, maskNeutrons_gaussian, position, amplitudeNeutrons[i]) 

    # print signal size
    signalLength = len(detector_signal_gaussian)
    print "generated signal size:", signalLength, "samples ->", functions.sample_to_time(signalLength) / 1000000.0, "ms"

    # info neutrons generated
    print "Nb of neutrons generated:", nbOfNeutrons, "neutrons"

    # check saturation
    # positive saturation
    positiveSaturationMask      = detector_signal_gaussian > GBL.SATURATION
    detector_signal_gaussian    = detector_signal_gaussian * (positiveSaturationMask==False) + positiveSaturationMask * GBL.SATURATION
    # negative saturation
    negativeSaturationMask      = detector_signal_gaussian > GBL.SATURATION
    detector_signal_gaussian    = detector_signal_gaussian * (negativeSaturationMask==False) + negativeSaturationMask * -GBL.SATURATION
   
    # Plot
    # fig = plt.figure()
    # # generated signals
    # plt.step(np.arange(signalLength), detector_signal_gaussian, label='genrated signal (gaussian shape)')
    # plt.xlabel("samples")
    # plt.ylabel("mV")
    # # plt.step(np.arange(signalLength), maskNeutrons_gaussian, label='genrated signal neutron mask')
    # plt.legend()
    # # neutron distribution
    # fig = plt.figure()
    # maxY, maxX, null = plt.hist(amplitudeNeutrons, bins='auto', label='histo amplitude genrated')
    # # gaussian fit
    # x = maxX[0:len(maxX)-1]
    # y = maxY
    # try:
    #     # initial guess for the parameters
    #     p0=[max(y), amplitudeGaussian, sigmaGaussian] 
    #     popt ,pcov = curve_fit(fct.gaus,x,y ,p0)
    #     myString = [    "gaussian fit:\n" +
    #             "    - mean: " + str(round(popt[1],2)) + " mV"  + "\n"
    #             "    - sigma: " + str(round(popt[2],2)) + " mV"
    #     ]
    #     print myString[0]
    #     # plot fit
    #     plt.plot(x,fct.gaus(x,*popt),':',label='gaussian fit')
    #     # plot mean and sigma as marker
    #     xStart = popt[1]
    #     xStop =  popt[1] + popt[2]
    #     ySigma = fct.gaus(xStop, popt[0], popt[1], popt[2])
    #     plt.plot([xStart, xStop], [ySigma, ySigma], label='fit gaussianWitdh (sigma = ' + str(round(popt[1],2)) + " mV)")
    #     plt.plot([popt[1], popt[1]], [0, popt[0]], label='fit amplitudeGaussian (mu = ' + str(round(popt[2],2)) + " mV)")
    # except:
    #     print "WARNING: failed to compute gaussian histogram fit"
    # plt.legend(loc='best')
    # plt.show()

    print "..end generate neutron signal", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"
    return detector_signal_gaussian, maskNeutrons_gaussian
