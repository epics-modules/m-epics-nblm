import numpy as np              # vector operation
import math                     # ceil()
import matplotlib.pyplot as plt # plot
import os                       # check directory
import time                     # elapsed time
import subprocess               # execute bash command: wc to count number of lines in file
import datetime                 # formats time in sec to date

import config as GBL            # global variables

# np.set_printoptions(threshold=np.nan) # print whole array

def gaus(x, a, x0, sigma):
    """ gaussian function """
    return a*np.exp(-(x-x0)**2/(2*sigma**2))

def get_number_of_integration(NB_OF_SAMPLES_ACQUIRED):
    """ number of integration period in signal length """
    nbOfIntegration1us = int(math.ceil(NB_OF_SAMPLES_ACQUIRED / time_to_sample(GBL.INTEGRATION_PERIOD)))+1 # +1 because you integrate for the next window (scope)
    return nbOfIntegration1us

def adjust_subplot_position():
    left    = 0.125 # the left side of the subplots of the figure
    right   = 0.9   # the right side of the subplots of the figure
    bottom  = 0.1   # the bottom of the subplots of the figure
    top     = 0.9   # the top of the subplots of the figure
    wspace  = 0.1   # the amount of width reserved for blank space between subplots
    hspace  = 0.4   # the amount of height reserved for white space between subplots
    plt.subplots_adjust(left=left, bottom=bottom, right=right, top=top, wspace=wspace, hspace=hspace)

def check_directory_input_file():
    """ check if rawdata directory exists"""
    if os.path.isdir(GBL.PATH_RAW_DATA_FILES) == False:
        exit("FAILED: directory '" + GBL.PATH_RAW_DATA_FILES + "' doens't exist so input raw data file can't be found. Please change 'PATH_RAW_DATA_FILES' in config.py (or 'GBL.PATH_RAW_DATA_FILES' in your script if you have overwritten 'PATH_RAW_DATA_FILES' in config.py)")

def sample_to_time(sample):
        """ 1sample = 4ns """
        return sample * GBL.TE

def time_to_sample(time):
        """ 4ns = 1 sample """
        return int( time / float(GBL.TE) )

def get_reset_integration(NB_OF_SAMPLES_ACQUIRED):
    """ return a vector of NB_OF_SAMPLES_ACQUIRED elements with '1' every time_to_sample(GBL.INTEGRATION_PERIOD)"""

    startFunction = time.time()
    print "start reset integration..", elapsedTime(startFunction, GBL.START_TIME)
    
    # 
    resetIntegration1us = np.zeros(NB_OF_SAMPLES_ACQUIRED, dtype=np.bool)
    for i in range(NB_OF_SAMPLES_ACQUIRED / time_to_sample(GBL.INTEGRATION_PERIOD)):
        resetIntegration1us[i * time_to_sample(GBL.INTEGRATION_PERIOD)] = 1

    print "..end reset integration", elapsedTime(startFunction, GBL.START_TIME)
    duration = time.time() - startFunction
    return resetIntegration1us, duration

def get_index_integration(rawDataIndex):
    """ inform in wich window of 1us is the rawdata given """      
    indexIntegration = int( rawDataIndex / time_to_sample(GBL.INTEGRATION_PERIOD) )
    return indexIntegration

def indexesToVector(NB_OF_SAMPLES_ACQUIRED, indexes):
    """ 
        from vector of indexes generate ouput signal
        exemple:
            NB_OF_SAMPLES_ACQUIRED  = 5
            indexes                 = [1 3]
            outputVector            = [0 1 0 1 0 0]
    """
    outputVector = np.zeros(NB_OF_SAMPLES_ACQUIRED)
    for i in range(len(indexes)):
        outputVector[ indexes[i] ] = 1

    return outputVector

def safe_div(x,y):
    """ in case of y=0, avoid 'ZeroDivisionError'error """
    if y == 0:
        print "[W] divison by 0"
        return 0
    return x / y

def display_configuration():
        """ prints the actual configuration (config.py file)"""

        print "Python analysis tool configuration:"
        print "    * directory paths *"
        print "        - simulation:\t\t",      GBL.PATH_SIMULATION
        print "        - raw data files:\t",    GBL.PATH_RAW_DATA_FILES
        print "        - nBLM logs:\t\t",       GBL.PATH_NBLM_LOGS
        print ""
        print "    * file names *"
        print "        - raw data file:\t",     GBL.RAW_DATA_FILE
        print "        - nBLM log file:\t",     GBL.NBLM_LOG_FILE_NAME
        print ""
        print "    * neutron detection configuration *"
        print "        - amplitudes:"
        print "            - pedestal level:\t\t\t",        GBL.PEDESTAL_LEVEL,             "mV"
        print "            - event detection threshold:\t", GBL.EVENT_DETECTION_THRESHOLD,  "mV"
        print "            - neutron amplitude min:\t\t",   GBL.NEUTRON_AMPLITUDE_MIN,      "mV"
        print "            - sparks level:\t\t\t",          GBL.SPARK_THRESHOLD,            "mV"
        print "        - time:"
        print "            - neutron TOT min:\t\t\t",           GBL.NEUTRON_TOT_MIN,        "ns"
        print "            - neutron TOT pile-up start:\t",     GBL.TOT_PILE_UP_START,      "ns"
        print "        - MPV (Most Probable Values):"
        print "            - charge MPV:\t\t\t",        GBL.NEUTRON_CHARGE_MPV,             "Q"
        print "            - TOT MPV:\t\t\t\t",         GBL.NEUTRON_TOT_MPV,                "ns"
        print "            - rise time MPV:\t\t\t",     GBL.NEUTRON_RISE_TIME_MPV,          "ns"
        print "            - amplitude MPV:\t\t\t",     GBL.NEUTRON_AMPLITUDE_MPV,          "mV"
        print ""
        print "    * scope configuration *"
        print "        - scope window:\t\t",    GBL.SCOPE_WINDOW, "ns"
        print "        - scope value:\t\t",     GBL.SCOPE_TRIGGER_VALUE       
        print "        - scope selection:\t",   GBL.SCOPE_TRIGGER[GBL.SCOPE_TRIGGER_SELECTION]
        print ""
        print "    * enabled /disabled fonctions *"
        print "        - compute histogram:\t\t",           GBL.COMPUTE_HISTOGRAM
        print "        - compute pedestal:\t\t",            GBL.COMPUTE_PEDESTAL
        print "        - compute average neutron peak:\t",  GBL.COMPUTE_AVERAGE_NEUTRON_PEAK
        print "        - plot raw data:\t\t",               GBL.PLOT_ALL_RAW_DATA
        print "        - plot debug:\t\t\t",                GBL.PLOT_SCOPE_DEBUG
        print "        - save and plot nBLM logs:\t",       GBL.SAVE_LOGS
        print ""
        print "    * raw data selection *"
        print "        - start:\t\t",           GBL.START_RAWDATA,      "%"
        print "        - stop:\t\t\t",          GBL.STOP_RAWDATA,       "%"
        print "        - mouse selection:\t",   GBL.MARKER_SELECTION
        print ""
        print "    * use data under threshold *"
        print "        - enable:\t",          GBL.USE_DATA_UNDER_EVENT_THRESHOLD
        if not GBL.USE_DATA_UNDER_EVENT_THRESHOLD:
            print "        - pre trig:\t",  GBL.USE_DATA_UNDER_EVENT_THRESHOLD_PRE_TRIG,    "ns"
            print "        - post trig:\t", GBL.USE_DATA_UNDER_EVENT_THRESHOLD_POST_TRIG,   "ns"
        print ""

def check_simulation_paths():
    """ check if the user gave valid directory path """
    if not os.path.isdir(GBL.PATH_SIMULATION):
        exit("FAILED: wrong GBL.PATH_SIMULATION -> "        + GBL.PATH_SIMULATION       + ". Check if the directory exists.")
    if not os.path.isdir(GBL.PATH_RAW_DATA_FILES):
        exit("FAILED: wrong GBL.PATH_RAW_DATA_FILES -> "    + GBL.PATH_RAW_DATA_FILES   + ". Check if the directory exists.")
    if not os.path.isdir(GBL.PATH_NBLM_LOGS):
        exit("FAILED: wrong GBL.PATH_NBLM_LOGS -> "         + GBL.PATH_NBLM_LOGS        + ". Check if the directory exists.")
    if not os.path.isfile(GBL.RAW_DATA_FILE):
        exit("FAILED: wrong GBL.RAW_DATA_FILE -> "          + GBL.RAW_DATA_FILE         + ". Check if the directory exists.")

def check_file_exist(pathFile):
    """ check if the input file exist """
    if not os.path.isfile(pathFile):
        # file dos not exist
        print "FAILED file does not exist:", pathFile
        exit()
    else:
        # file exists
        pass

def time_report(getDataDuration, saturationDectectionDuration, preProcessingDuration, derivativeDuration, sparkDetectionDuration, eventDetectionDuration, pedestalCalculationDuration, neutronCounter1usDuration, neutronCounterCheapDuration, MPVestimationDuration, neutronPeakAverageDuration, neutronPeakDistributionDuration, resetintegrationDuration, nblmLogsDuration):
    """ reports python analysis tool functions duration """

    print "*** Timing report ***"
    print "    functions\t\t\t\t",                  "duration\t\t",                                                         "skipped"
    print "    - get_data()\t\t\t",                 elapsedTime( getDataDuration,                   GBL.DURATION),  "\t",   not True
    print "    - saturation_detection()\t\t",       elapsedTime( saturationDectectionDuration,      GBL.DURATION),  "\t",   not True
    print "    - pre_processing()\t\t\t",           elapsedTime( preProcessingDuration,             GBL.DURATION),  "\t",   not True
    print "    - derivative()\t\t\t",               elapsedTime( derivativeDuration,                GBL.DURATION),  "\t",   not True
    print "    - sparks_detection()\t\t",           elapsedTime( sparkDetectionDuration,            GBL.DURATION),  "\t",   not True
    print "    - event_detection()\t\t\t",          elapsedTime( eventDetectionDuration,            GBL.DURATION),  "\t",   not True
    print "    - pedestal_calculation()\t\t",       elapsedTime( pedestalCalculationDuration,       GBL.DURATION),  "\t",   not GBL.COMPUTE_PEDESTAL
    print "    - neutron_counter_1us()\t\t",        elapsedTime( neutronCounter1usDuration,         GBL.DURATION),  "\t",   not True
    print "    - neutron_counter_cheap()\t\t",      elapsedTime( neutronCounterCheapDuration,       GBL.DURATION),  "\t",   not True
    print "    - MPV_estimation()\t\t\t",           elapsedTime( MPVestimationDuration,             GBL.DURATION),  "\t",   not True
    print "    - neutron_peak_average()\t\t",       elapsedTime( neutronPeakAverageDuration,        GBL.DURATION),  "\t",   not GBL.COMPUTE_AVERAGE_NEUTRON_PEAK
    print "    - neutron_peak_distribution()\t",    elapsedTime( neutronPeakDistributionDuration,   GBL.DURATION),  "\t",   not GBL.COMPUTE_HISTOGRAM
    print "    - get_reset_integration()\t\t",      elapsedTime( resetintegrationDuration,          GBL.DURATION),  "\t",   not True
    print "    - nblm_logs()\t\t\t",                elapsedTime( nblmLogsDuration,                  GBL.DURATION),  "\t",   not GBL.SAVE_LOGS
    print ""

def getNumberOfLines(pathFile):

    ## use bash command ! LINUX DEPENDANCY !
    print "get nb of lines (wc) ..."
    startGetLastLine = time.time()
    cmd             = ["wc", "-l", pathFile]
    wc              = subprocess.check_output(cmd)
    wcSplitted      = wc.split()
    nbOfLines       = int(wcSplitted[0])
    print "number of lines:", nbOfLines
    print "get nb of lines ...", time.time()-startGetLastLine, "sec"

    ## read all the file in python, not really fast but ! NO LINUX DEPENDANCY !
    # print "\nget nb of lines (read file) ..."
    # startGetLastLine = time.time()
    # with open(pathFile, "r") as f:
    #     for line in f: pass
    # print "number of lines:", line.split()[0] 
    # print "get nb of lines ...", time.time()-startGetLastLine, "sec"

    ## use last line (specific to Lodz dev)
    # # get number of lines
    # print "\nget last line of the file ..."
    # startGetLastLine = time.time()
    # cmd             = ["tail", pathFile]            # tail command
    # tail            = subprocess.check_output(cmd)
    # tailSpiltted    = tail.split()
    # nbOfLines       = int(tailSpiltted[18]) + 1     # samples
    # print "number of lines:", nbOfLines
    # print "get last line of the file ...", time.time()-startGetLastLine, "sec"

    return nbOfLines

def get_padding_size():
    """ get_data() fct add padding before and after signal to avoid bug in debug_scope()"""
    return time_to_sample(GBL.SCOPE_WINDOW)

def formated_duration(timeSec):
    """ sec to formated time """
    # to do: round()

    # format timeSec
    formatedTime = str(datetime.timedelta(seconds=timeSec))

    return formatedTime

def elapsedTime(myTime, startTimeOrDuration):
    """ returns the formated elasped time """

    if startTimeOrDuration == GBL.START_TIME:
        # arg given is starttime
        duration = time.time()-myTime
    elif startTimeOrDuration == GBL.DURATION:
        # arg given is duration
        duration = myTime
    else:
        print "FAILED: unknow argument"
        exit()

    # format result
    result = "(" + formated_duration(duration) + " sec)"

    return result
