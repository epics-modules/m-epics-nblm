import time                             # chrono (elapsed time)
import numpy as np                      # vector operation
import matplotlib.pyplot as plt         # plot
from matplotlib.widgets import Button   # quit button when plotting
from scipy.optimize import curve_fit    # gaussian fit
from scipy import asarray as ar,exp     # gaussian fit
from scipy import signal                # gaussian fit

import config as GBL            # global variables
from nblmlib import functions   # my functions for nBLM simulation

# quit button
class Index(object):
    def quit(self, event):
        print "Stopped by debug/scope 'quit' button"
        exit()

def oversampling(myInput, mask):
    """
    use the mask to oversample myInput
    ex: - myInput = [5, 6, 7, 8]
        - mask = [0, not(0), 0, 0, 0, 0, not(0), not(0)]
        - output = [0, 5, 0, 0, 0, 0, 6, 7]
    """

    # check that mask is covering input
    numberOfNot0 = np.sum( (mask != 0) ) 
    if numberOfNot0 > len(myInput):
        print "[W!] strange: mask doesn't cover myInput", numberOfNot0, len(myInput)

    # print "len(mask)", len(mask)
    # print "len(myInput)", len(myInput)
    # plt.figure()
    # plt.plot(myInput)
    # plt.plot(mask)
    # plt.show()

    # output vector
    lengthOutput    = len(mask)
    output          = np.zeros(lengthOutput)

    # oversampling
    indexInput = 0
    for i in range(lengthOutput):

        if indexInput == 0: # cover first case
            output[i] = 0
        else:
            output[i] = myInput[indexInput-1]

        if mask[i] != 0:
            if indexInput < len(myInput):
                # increment only if possible
                indexInput = indexInput + 1
        else:
            pass

    return output

def offset_list(myArray, offset):
    """ add offset (scalar value) to every value of the array"""
    return myArray + offset # add for every element of the array the offset

def plot_neutron_detection(NB_OF_SAMPLES_ACQUIRED,  neutronList, canvasTitle, suptitle, rawData, rawDataDerivated, neutronCounter1us, neutronCounterPeak1us, neutronCounterPileUp1us, neutronCountingCharge1us, neutronCountingTOT1us, neutronCountingAmp1us, neutronCountingRiseTime1us, badEventList, resetIntegration1us, sparkDetected, saturation, positiveSaturation, negativeSaturation):
    # x axis in ns
    timeAcq = np.linspace(0, functions.sample_to_time(NB_OF_SAMPLES_ACQUIRED), NB_OF_SAMPLES_ACQUIRED)

    # create threshold vectors
    eventDetectionVector    = GBL.EVENT_DETECTION_THRESHOLD * np.ones(NB_OF_SAMPLES_ACQUIRED)
    neutronAmpMinVector     = GBL.NEUTRON_AMPLITUDE_MIN     * np.ones(NB_OF_SAMPLES_ACQUIRED)
    resetIntegrationVector  = - min(rawData) * 5/100.0      * resetIntegration1us               # amplitude: 5% of max amplitude 

    fig2 = plt.figure()
    fig2.canvas.set_window_title(canvasTitle) 
    fig2.suptitle(suptitle +
            "TOT min: " + str(GBL.NEUTRON_TOT_MIN) + " samples, TOT Pile-UP: " + str(GBL.TOT_PILE_UP_START) + " ns, TOT limit: " + str(GBL.TOT_PILE_UP_LIMIT_REACHED) + " samples\n" +
            "charge MPV: "      + str(GBL.NEUTRON_CHARGE_MPV)       + " Q, " +
            "TOT MPV: "         + str(GBL.NEUTRON_TOT_MPV)          + " ns, " +
            "amplitude MPV: "   + str(GBL.NEUTRON_AMPLITUDE_MPV)    + " mV, " +
            "rise time MPV: "   + str(GBL.NEUTRON_RISE_TIME_MPV)    + " samples", fontsize=14)

    ax = plt.subplot(311)
    plt.title("Input signal", fontsize=12)
    # plot raw data + raw data derivated + event detection threshold + neutron amplitude min + integration 1us
    plt.plot(timeAcq, rawData,                  label='raw data from detector')
    plt.plot(timeAcq, rawDataDerivated,         label='raw data from detector derivated')
    plt.plot(timeAcq, eventDetectionVector,     label='event detection threshlod')
    plt.plot(timeAcq, neutronAmpMinVector,      label='neutron amplitude minimum')
    plt.step(timeAcq, resetIntegrationVector,   label='reset counter 1us')

    # write some info on the plot concerning neutron shape
    if "scope" not in canvasTitle: # if it's the scope, infos are already written
        for neutron in range(len(neutronList)):
            annotation      = str(neutronList[neutron].eventNumber)
            x_amplitudeMax  =  functions.sample_to_time( (neutronList[neutron].rawDataIndex - neutronList[neutron].TOT + neutronList[neutron].riseTime) )
            y_amplitudeMax  = neutronList[neutron].amplitude
            plt.annotate(annotation, xy=(x_amplitudeMax, y_amplitudeMax), xytext=(x_amplitudeMax,y_amplitudeMax), fontsize=8, verticalalignment='top')
    plt.xlabel('ns')
    plt.ylabel('mV')
    plt.legend(loc='best')

    # plot neutron counting
    ax3 = plt.subplot(312, sharex=ax)
    plt.title("neutron counting", fontsize=12)
    plt.xlabel('ns')
    plt.ylabel('neutron counting')
    # counting 
    plt.step(timeAcq, oversampling(neutronCounter1us,           resetIntegration1us), label='neutron counter: neutron peak counting 1us + charge1us')
    plt.step(timeAcq, oversampling(neutronCountingCharge1us,    resetIntegration1us), label='cheap neutron counter: charge1us only')
    # plt.step(timeAcq, oversampling(neutronCountingTOT1us,      resetIntegration1us), label='NOT combined: TOT1us')
    # plt.step(timeAcq, oversampling(neutronCountingAmp1us,      resetIntegration1us), label='NOT combined: amplitude1us')
    # plt.step(timeAcq, oversampling(neutronCountingRiseTime1us, resetIntegration1us), label='NOT combined: riseTime1us')
    # reset
    plt.step(timeAcq, offset_list(resetIntegration1us, -2), label='reset counter 1us')
    plt.legend()

    # plot status
    # neutron masks
    neutronPeakMask                 = np.zeros(NB_OF_SAMPLES_ACQUIRED)
    neutronPileUpMask               = np.zeros(NB_OF_SAMPLES_ACQUIRED)
    neutronPileUpLimitReachedMask   = np.zeros(NB_OF_SAMPLES_ACQUIRED)
    for neutron in range(len(neutronList)):
        neutronPeakMask                 [neutronList[neutron].rawDataIndex] = int(not(neutronList[neutron].pileUp))
        neutronPileUpMask               [neutronList[neutron].rawDataIndex] = neutronList[neutron].pileUp
        neutronPileUpLimitReachedMask   [neutronList[neutron].rawDataIndex] = neutronList[neutron].TOTlimitReached

    # badEvent masks
    badEventTOTnotValidatedMask         = np.zeros(NB_OF_SAMPLES_ACQUIRED)
    badEventAmplitudeNotValidatedMask   = np.zeros(NB_OF_SAMPLES_ACQUIRED)
    badEventPileUpMask                  = np.zeros(NB_OF_SAMPLES_ACQUIRED)
    badEventPileUpLimitReachedMask      = np.zeros(NB_OF_SAMPLES_ACQUIRED)
    for badEvent in range(len(badEventList)):
        badEventTOTnotValidatedMask         [badEventList[badEvent].rawDataIndex] = badEventList[badEvent].TOTnotValidated
        badEventAmplitudeNotValidatedMask   [badEventList[badEvent].rawDataIndex] = badEventList[badEvent].amplitudeNotValidated
        badEventPileUpMask                  [badEventList[badEvent].rawDataIndex] = badEventList[badEvent].pileUp
        badEventPileUpLimitReachedMask      [badEventList[badEvent].rawDataIndex] = badEventList[badEvent].TOTlimitReached
    # plot
    ax2 = plt.subplot(313, sharex=ax)
    plt.title("detection status", fontsize=12)
    plt.xlabel('ns')
    # neutrons
    plt.step(timeAcq, offset_list(neutronPeakMask,                  14), label='neutron peak')
    plt.step(timeAcq, offset_list(neutronPileUpMask,                12), label='neutron pile-up')
    plt.step(timeAcq, offset_list(neutronPileUpLimitReachedMask,    10), label='neutron TOT limit reached')
    # badEvents
    plt.step(timeAcq, offset_list(badEventTOTnotValidatedMask,          6), label='bad event  because of TOT')
    plt.step(timeAcq, offset_list(badEventAmplitudeNotValidatedMask,    4), label='bad event because of amplitude')
    plt.step(timeAcq, offset_list(badEventPileUpMask,                   2), label='bad event pile-up')
    plt.step(timeAcq, offset_list(badEventPileUpLimitReachedMask,       0), label='bad event TOT limit reached')
    # saturations
    plt.step(timeAcq, offset_list(saturation,                           -6), label='saturation')
    plt.step(timeAcq, offset_list(positiveSaturation,                   -6), label='positive saturation')
    plt.step(timeAcq, offset_list(negativeSaturation,                   -6), label='negative saturation')
    # sparks detected
    plt.step(timeAcq, offset_list(sparkDetected,                        -4), label='sparks detected')
    # To hide the Y-axis of a plot
    cur_axes = plt.gca()
    cur_axes.axes.get_yaxis().set_visible(False)
    plt.legend()

    # adjust subplot position
    functions.adjust_subplot_position()

    return 0


def plotting_full_window(rawDataCorrected, rawDataCorrectedDerivated, NB_OF_SAMPLES_ACQUIRED, neutronList, neutronCounter1us, neutronCounterPeak1us, neutronCounterPileUp1us, neutronCountingCharge1us, neutronCountingTOT1us, neutronCountingAmp1us, neutronCountingRiseTime1us, badEventList, resetIntegration1us, sparkDetected, saturation, positiveSaturation, negativeSaturation):

    startFunction = time.time()
    print "start plotting full window", functions.elapsedTime(startFunction, GBL.START_TIME)

    # sparks detected data
    sparkDetectedVector         = functions.indexesToVector(NB_OF_SAMPLES_ACQUIRED, sparkDetected)
    saturationVector            = functions.indexesToVector(NB_OF_SAMPLES_ACQUIRED, saturation)
    positiveSaturationVector    = functions.indexesToVector(NB_OF_SAMPLES_ACQUIRED, positiveSaturation)
    negativeSaturationVector    = functions.indexesToVector(NB_OF_SAMPLES_ACQUIRED, negativeSaturation)

    if GBL.PLOT_ALL_RAW_DATA == True:

        plot_neutron_detection( NB_OF_SAMPLES_ACQUIRED      = NB_OF_SAMPLES_ACQUIRED,
                                neutronList                 = neutronList,
                                canvasTitle                 = "plotting full window",
                                suptitle                    = "FPGA algo simulation - full window\n",
                                rawData                     = rawDataCorrected,
                                rawDataDerivated            = rawDataCorrectedDerivated,
                                neutronCounter1us           = neutronCounter1us, 
                                neutronCounterPeak1us       = neutronCounterPeak1us,
                                neutronCounterPileUp1us     = neutronCounterPileUp1us, 
                                neutronCountingCharge1us    = neutronCountingCharge1us, 
                                neutronCountingTOT1us       = neutronCountingTOT1us, 
                                neutronCountingAmp1us       = neutronCountingAmp1us,
                                neutronCountingRiseTime1us  = neutronCountingRiseTime1us, 
                                badEventList                = badEventList,
                                resetIntegration1us         = resetIntegration1us,
                                sparkDetected               = sparkDetectedVector,
                                saturation                  = saturationVector,
                                positiveSaturation          = positiveSaturationVector,
                                negativeSaturation          = negativeSaturationVector
                    )    
    else:
        print "Plot all raw data skipped (see config.py)"

    print "..end plotting full window", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"

def get_trig_event_shape_mask(NB_OF_SAMPLES_ACQUIRED, neutronList, badEventList, isNeutronEnable, isNeutron, TOTnotValidatedEnable, TOTnotValidated, amplitudeNotValidatedEnable, amplitudeNotValidated, pileUpEnable, pileUp, TOTlimitReachedEnable, TOTlimitReached):
    """ get the mask of events selectionned in parameters"""
    trigEventShapeMask = np.zeros(NB_OF_SAMPLES_ACQUIRED)

    # neutrons and bad events are separeted in 2 tuples for python simulation

    # neutron
    for neutron in range(len(neutronList)):
        if (    ( neutronList[neutron].isNeutron                == isNeutron                or not(isNeutronEnable)             ) and 
                ( neutronList[neutron].TOTnotValidated          == TOTnotValidated          or not(TOTnotValidatedEnable)       ) and 
                ( neutronList[neutron].amplitudeNotValidated    == amplitudeNotValidated    or not(amplitudeNotValidatedEnable) ) and 
                ( neutronList[neutron].pileUp                   == pileUp                   or not(pileUpEnable)                ) and 
                ( neutronList[neutron].TOTlimitReached          == TOTlimitReached          or not(TOTlimitReachedEnable)       )
        ):
            trigEventShapeMask[neutronList[neutron].rawDataIndex] = 1

    # bad event
    for badEvent in range(len(badEventList)):
        if (    ( badEventList[badEvent].isNeutron              == isNeutron                or not(isNeutronEnable)             ) and 
                ( badEventList[badEvent].TOTnotValidated        == TOTnotValidated          or not(TOTnotValidatedEnable)       ) and 
                ( badEventList[badEvent].amplitudeNotValidated  == amplitudeNotValidated    or not(amplitudeNotValidatedEnable) ) and 
                ( badEventList[badEvent].pileUp                 == pileUp                   or not(pileUpEnable)                ) and 
                ( badEventList[badEvent].TOTlimitReached        == TOTlimitReached          or not(TOTlimitReachedEnable)       )
        ):
            trigEventShapeMask[badEventList[badEvent].rawDataIndex] = 1

    return trigEventShapeMask


def rising_edge_detector(signal):
    """
    signal      = [0,1,1,1,0,0,1,0,1,0,1,1,1,1,0]
    risingEdge      = [0,1,0,0,0,0,1,0,1,0,1,0,0,0,0]
    """

    # output vector creation
    risingEdge = np.zeros(len(signal))

    # detects rising edge
    previousValue = signal[0]
    for i in range(1, len(signal)):
        if previousValue == 0 and signal[i] == 1:
            risingEdge[i] = 1

    return risingEdge


def get_trig_mask(NB_OF_SAMPLES_ACQUIRED, neutronList, badEventList, saturation, positiveSaturation, negativeSaturation, neutronCounter1us):
    """
    GBL.SCOPE_TRIGGER_SELECTION: GBL.SCOPE_TRIGGER for details (in config.py):
        "0. no trig (don't trig)",
        "1. on EPICS demand only",
        "2. on neutron counting level",
        "3. on neutron shape",
        "4. on neutron peak",
        "5. on neutron pile-up",
        "6. on neutron TOTlimitReached",
        "7. on bad event",
        "8. on bad event because of TOT",
        "9. on bad event because of amplitude",
        "10. on bad event peak",
        "11. on bad event pile-up",
        "12. on bad event TOT limit reached",
        "13. on event",
        "14. on ADC saturation",
        "15. on ADC positive saturation",
        "16. on ADC negative saturation",
        "17. on driver request",
        "18. on MPV instability",
        "19. on pedestal instability",
        "20. on CH0-8 trig (16 bits value)",
        "21. on an external trig, via RTM or AMC TBC?",
        "22. on sparks",
        "23. spare",
        "24. spare",
        "25. spare",
        "26. spare",
        "27. spare",
        "28. spare",
        "29. spare",
        "30. spare",
        "31. spare"
    """


    if GBL.SCOPE_TRIGGER_SELECTION == 0:
        # no trigger (don't trig)
        trigMask = np.zeros(NB_OF_SAMPLES_ACQUIRED)

    elif GBL.SCOPE_TRIGGER_SELECTION == 1:
        # on EPICS demand only: not implemented
        print "NB: not implemented"
        trigMask = np.zeros(NB_OF_SAMPLES_ACQUIRED)

    elif GBL.SCOPE_TRIGGER_SELECTION == 2:
        # on neutron counting level (coutning updated every 1us)
        trigMask = np.zeros(NB_OF_SAMPLES_ACQUIRED)
        for integration1us in range(len(neutronCounter1us)):
            if neutronCounter1us[integration1us] >= GBL.SCOPE_TRIGGER_VALUE:
                trigMask[integration1us * functions.time_to_sample(GBL.INTEGRATION_PERIOD)] = 1
        
    elif GBL.SCOPE_TRIGGER_SELECTION == 3:
        # on neutron
        isNeutron               = True;     isNeutronEnable             = True
        TOTnotValidated         = False;    TOTnotValidatedEnable       = False
        amplitudeNotValidated   = False;    amplitudeNotValidatedEnable = False
        pileUp                  = False;    pileUpEnable                = False
        TOTlimitReached         = False;    TOTlimitReachedEnable       = False
        trigMask = get_trig_event_shape_mask(NB_OF_SAMPLES_ACQUIRED, neutronList, badEventList, isNeutronEnable, isNeutron, TOTnotValidatedEnable, TOTnotValidated, amplitudeNotValidatedEnable, amplitudeNotValidated, pileUpEnable, pileUp, TOTlimitReachedEnable, TOTlimitReached)

    elif GBL.SCOPE_TRIGGER_SELECTION == 4:
        # on neutron peak
        isNeutron               = True;     isNeutronEnable             = True
        TOTnotValidated         = False;    TOTnotValidatedEnable       = False
        amplitudeNotValidated   = False;    amplitudeNotValidatedEnable = False
        pileUp                  = False;    pileUpEnable                = True
        TOTlimitReached         = False;    TOTlimitReachedEnable       = False
        trigMask = get_trig_event_shape_mask(NB_OF_SAMPLES_ACQUIRED, neutronList, badEventList, isNeutronEnable, isNeutron, TOTnotValidatedEnable, TOTnotValidated, amplitudeNotValidatedEnable, amplitudeNotValidated, pileUpEnable, pileUp, TOTlimitReachedEnable, TOTlimitReached)

    elif GBL.SCOPE_TRIGGER_SELECTION == 5:
        # on neutron pile-up
        isNeutron               = True;     isNeutronEnable             = True
        TOTnotValidated         = False;    TOTnotValidatedEnable       = False
        amplitudeNotValidated   = False;    amplitudeNotValidatedEnable = False
        pileUp                  = True;     pileUpEnable                = True
        TOTlimitReached         = False;    TOTlimitReachedEnable       = False
        trigMask = get_trig_event_shape_mask(NB_OF_SAMPLES_ACQUIRED, neutronList, badEventList, isNeutronEnable, isNeutron, TOTnotValidatedEnable, TOTnotValidated, amplitudeNotValidatedEnable, amplitudeNotValidated, pileUpEnable, pileUp, TOTlimitReachedEnable, TOTlimitReached)

    elif GBL.SCOPE_TRIGGER_SELECTION == 6:
        # on neutron TOT limit reached
        isNeutron               = True;     isNeutronEnable             = True
        TOTnotValidated         = False;    TOTnotValidatedEnable       = False
        amplitudeNotValidated   = False;    amplitudeNotValidatedEnable = False
        pileUp                  = False;    pileUpEnable                = False
        TOTlimitReached         = True;     TOTlimitReachedEnable       = True
        trigMask = get_trig_event_shape_mask(NB_OF_SAMPLES_ACQUIRED, neutronList, badEventList, isNeutronEnable, isNeutron, TOTnotValidatedEnable, TOTnotValidated, amplitudeNotValidatedEnable, amplitudeNotValidated, pileUpEnable, pileUp, TOTlimitReachedEnable, TOTlimitReached)
    
    elif GBL.SCOPE_TRIGGER_SELECTION == 7:
        # on bad event
        isNeutron               = False;    isNeutronEnable             = True
        TOTnotValidated         = False;    TOTnotValidatedEnable       = False
        amplitudeNotValidated   = False;    amplitudeNotValidatedEnable = False
        pileUp                  = False;    pileUpEnable                = False
        TOTlimitReached         = False;    TOTlimitReachedEnable       = False
        trigMask = get_trig_event_shape_mask(NB_OF_SAMPLES_ACQUIRED, neutronList, badEventList, isNeutronEnable, isNeutron, TOTnotValidatedEnable, TOTnotValidated, amplitudeNotValidatedEnable, amplitudeNotValidated, pileUpEnable, pileUp, TOTlimitReachedEnable, TOTlimitReached)

    elif GBL.SCOPE_TRIGGER_SELECTION == 8:
        # on bad event because of TOT
        isNeutron               = False;    isNeutronEnable             = True
        TOTnotValidated         = True;     TOTnotValidatedEnable       = True
        amplitudeNotValidated   = False;    amplitudeNotValidatedEnable = True
        pileUp                  = False;    pileUpEnable                = False
        TOTlimitReached         = False;    TOTlimitReachedEnable       = False
        trigMask = get_trig_event_shape_mask(NB_OF_SAMPLES_ACQUIRED, neutronList, badEventList, isNeutronEnable, isNeutron, TOTnotValidatedEnable, TOTnotValidated, amplitudeNotValidatedEnable, amplitudeNotValidated, pileUpEnable, pileUp, TOTlimitReachedEnable, TOTlimitReached)

    elif GBL.SCOPE_TRIGGER_SELECTION == 9:
        # on bad event because of amplitude
        isNeutron               = False;    isNeutronEnable             = True
        TOTnotValidated         = False;    TOTnotValidatedEnable       = True
        amplitudeNotValidated   = True;     amplitudeNotValidatedEnable = True
        pileUp                  = False;    pileUpEnable                = False
        TOTlimitReached         = False;    TOTlimitReachedEnable       = False
        trigMask = get_trig_event_shape_mask(NB_OF_SAMPLES_ACQUIRED, neutronList, badEventList, isNeutronEnable, isNeutron, TOTnotValidatedEnable, TOTnotValidated, amplitudeNotValidatedEnable, amplitudeNotValidated, pileUpEnable, pileUp, TOTlimitReachedEnable, TOTlimitReached)

    elif GBL.SCOPE_TRIGGER_SELECTION == 10:
        # on bad event peak
        isNeutron               = False;    isNeutronEnable             = True
        TOTnotValidated         = False;    TOTnotValidatedEnable       = False
        amplitudeNotValidated   = False;    amplitudeNotValidatedEnable = False
        pileUp                  = False;    pileUpEnable                = True
        TOTlimitReached         = False;    TOTlimitReachedEnable       = False
        trigMask = get_trig_event_shape_mask(NB_OF_SAMPLES_ACQUIRED, neutronList, badEventList, isNeutronEnable, isNeutron, TOTnotValidatedEnable, TOTnotValidated, amplitudeNotValidatedEnable, amplitudeNotValidated, pileUpEnable, pileUp, TOTlimitReachedEnable, TOTlimitReached)

    elif GBL.SCOPE_TRIGGER_SELECTION == 11:
        # on bad event pile-up
        isNeutron               = False;    isNeutronEnable             = True
        TOTnotValidated         = False;    TOTnotValidatedEnable       = False
        amplitudeNotValidated   = False;    amplitudeNotValidatedEnable = False
        pileUp                  = True;     pileUpEnable                = True
        TOTlimitReached         = False;    TOTlimitReachedEnable       = False
        trigMask = get_trig_event_shape_mask(NB_OF_SAMPLES_ACQUIRED, neutronList, badEventList, isNeutronEnable, isNeutron, TOTnotValidatedEnable, TOTnotValidated, amplitudeNotValidatedEnable, amplitudeNotValidated, pileUpEnable, pileUp, TOTlimitReachedEnable, TOTlimitReached)

    elif GBL.SCOPE_TRIGGER_SELECTION == 12:
        # on bad event TOT limit reached
        isNeutron               = False;    isNeutronEnable             = True
        TOTnotValidated         = False;    TOTnotValidatedEnable       = False
        amplitudeNotValidated   = False;    amplitudeNotValidatedEnable = False
        pileUp                  = False;    pileUpEnable                = False
        TOTlimitReached         = True;     TOTlimitReachedEnable       = True
        trigMask = get_trig_event_shape_mask(NB_OF_SAMPLES_ACQUIRED, neutronList, badEventList, isNeutronEnable, isNeutron, TOTnotValidatedEnable, TOTnotValidated, amplitudeNotValidatedEnable, amplitudeNotValidated, pileUpEnable, pileUp, TOTlimitReachedEnable, TOTlimitReached)

    elif GBL.SCOPE_TRIGGER_SELECTION == 13:
        # on event
        isNeutron               = False;    isNeutronEnable             = False
        TOTnotValidated         = False;    TOTnotValidatedEnable       = False
        amplitudeNotValidated   = False;    amplitudeNotValidatedEnable = False
        pileUp                  = False;    pileUpEnable                = False
        TOTlimitReached         = False;    TOTlimitReachedEnable       = False
        trigMask = get_trig_event_shape_mask(NB_OF_SAMPLES_ACQUIRED, neutronList, badEventList, isNeutronEnable, isNeutron, TOTnotValidatedEnable, TOTnotValidated, amplitudeNotValidatedEnable, amplitudeNotValidated, pileUpEnable, pileUp, TOTlimitReachedEnable, TOTlimitReached)

    elif GBL.SCOPE_TRIGGER_SELECTION == 14:
        # on ADC saturation
        trigMask = np.zeros(NB_OF_SAMPLES_ACQUIRED)
        for i in range(len(saturation)):
            trigMask[ saturation[i] ] = 1
    
    elif GBL.SCOPE_TRIGGER_SELECTION == 15:
        # on ADC positive saturation
        trigMask = np.zeros(NB_OF_SAMPLES_ACQUIRED)
        for i in range(len(positiveSaturation)):
            trigMask[ positiveSaturation[i] ] = 1

    elif GBL.SCOPE_TRIGGER_SELECTION == 16:
        # on ADC negative saturation
        trigMask = np.zeros(NB_OF_SAMPLES_ACQUIRED)
        for i in range(len(negativeSaturation)):
            trigMask[ negativeSaturation[i] ] = 1

    elif GBL.SCOPE_TRIGGER_SELECTION == 17:
        # on driver request
        print "NB: not implemented"
        trigMask = rising_edge_detector(saturation1us>1)
    
    elif GBL.SCOPE_TRIGGER_SELECTION == 18:
        # on MPV instability
        print "NB: not implemented"
        trigMask = rising_edge_detector(saturation1us>1)
    
    elif GBL.SCOPE_TRIGGER_SELECTION == 19:
        # on pedestal instability
        print "NB: not implemented"
        trigMask = rising_edge_detector(saturation1us>1)

    elif GBL.SCOPE_TRIGGER_SELECTION == 20:
        # on CH0-8 trig (16 bits value)
        print "NB: not implemented"
        trigMask = rising_edge_detector(saturation1us>1)

    elif GBL.SCOPE_TRIGGER_SELECTION == 21:
        # on an external trig, via RTM or AMC TBC
        print "NB: not implemented"
        trigMask = rising_edge_detector(saturation1us>1)

    elif GBL.SCOPE_TRIGGER_SELECTION == 22:
        # on sparks
        print "NB: not implemented"
        trigMask = np.zeros(NB_OF_SAMPLES_ACQUIRED)

    elif GBL.SCOPE_TRIGGER_SELECTION >= 23 or GBL.SCOPE_TRIGGER_SELECTION <= 31:
        print "NB: not implemented"
        trigMask = np.zeros(NB_OF_SAMPLES_ACQUIRED)

    else:
        exit("FAILED scope/debug: wrong trigger selection")

    return trigMask



def scope_debug(rawDataCorrected, rawDataCorrectedDerivated, rawDataMask, NB_OF_SAMPLES_ACQUIRED, neutronList, neutronCounter1us, neutronCounterPeak1us, neutronCounterPileUp1us, neutronCountingCharge1us, neutronCountingTOT1us, neutronCountingAmp1us, neutronCountingRiseTime1us, badEventList, resetIntegration1us, sparkDetected, saturation, positiveSaturation, negativeSaturation):
    """ scope on trigger. window of 10 us.
    GBL.SCOPE_TRIGGER_SELECTION: GBL.SCOPE_TRIGGER for details (in config.py)
    """
    startFunction = time.time()
    print "start scope_debug() ", functions.elapsedTime(startFunction, GBL.START_TIME)

    if GBL.PLOT_SCOPE_DEBUG == True:

        print "Scope trig ->", GBL.SCOPE_TRIGGER[GBL.SCOPE_TRIGGER_SELECTION]

        # window size
        if GBL.SCOPE_WINDOW < GBL.INTEGRATION_PERIOD:
            exit("FAILED: window too small, scope window should be equal or greater than integration period")
        WINDOW_DISPLAY  = functions.time_to_sample(GBL.SCOPE_WINDOW)    # samples
        preTrig         = int(WINDOW_DISPLAY/2.0)                       # samples
        postTrig        = preTrig                                       # samples

        # x axis in ns (plotting purpose)
        timeAcq = np.linspace(0, GBL.SCOPE_WINDOW, WINDOW_DISPLAY)

        # trig mask
        # startTrigMask = time.time()
        # print "start get trigger.. (%f s)" %(time.time()-startTrigMask)
        trigMask    = get_trig_mask(NB_OF_SAMPLES_ACQUIRED, neutronList, badEventList, saturation, positiveSaturation, negativeSaturation, neutronCounter1us)
        trigMaskSum = np.sum(trigMask)
        print "Number of trigger:", trigMaskSum
        # print "..end get trigger (%f s)" %(time.time()-startTrigMask)

        # sparks detected data
        sparkDetectedVector         = functions.indexesToVector(NB_OF_SAMPLES_ACQUIRED, sparkDetected)
        saturationVector            = functions.indexesToVector(NB_OF_SAMPLES_ACQUIRED, saturation)
        positiveSaturationVector    = functions.indexesToVector(NB_OF_SAMPLES_ACQUIRED, positiveSaturation)
        negativeSaturationVector    = functions.indexesToVector(NB_OF_SAMPLES_ACQUIRED, positiveSaturation)
    
        # plot every window triggered
        sampleIndex = 0
        triggerCounter = 0
        while sampleIndex < NB_OF_SAMPLES_ACQUIRED: # go trhough all events

            startPlotWindow = time.time()

            # trig selection
            if trigMask[sampleIndex] == True: # allow display in function of GBL.SCOPE_TRIGGER_SELECTION

                triggerCounter = triggerCounter + 1

                # index rawdata start and stop for the window
                iStartRawData           = sampleIndex - preTrig
                iStopRawData            = sampleIndex + postTrig
                myWindow                = rawDataCorrected          [iStartRawData : iStopRawData]
                rawDataDerivated        = rawDataCorrectedDerivated [iStartRawData : iStopRawData]
                resetIntegrationWindow  = resetIntegration1us       [iStartRawData : iStopRawData]
                sparkDetectedWindow     = sparkDetectedVector       [iStartRawData : iStopRawData]
                saturationWindow        = saturationVector          [iStartRawData : iStopRawData]
                positiveSaturationWindow= positiveSaturationVector  [iStartRawData : iStopRawData]
                negativeSaturationWindow= negativeSaturationVector  [iStartRawData : iStopRawData]

                # neutron list for the window
                neutronListWindow = []
                for neutron in range(len(neutronList)):
                    if neutronList[neutron].rawDataIndex >= iStartRawData and  neutronList[neutron].rawDataIndex < iStopRawData:
                        myNeutronShape = GBL.t_event(       
                            isNeutron               = neutronList[neutron].isNeutron,
                            TOTnotValidated         = neutronList[neutron].TOTnotValidated,
                            amplitudeNotValidated   = neutronList[neutron].amplitudeNotValidated,
                            pileUp                  = neutronList[neutron].pileUp,
                            TOTlimitReached         = neutronList[neutron].TOTlimitReached,
                            charge                  = neutronList[neutron].charge,
                            TOT                     = neutronList[neutron].TOT,
                            amplitude               = neutronList[neutron].amplitude,
                            riseTime                = neutronList[neutron].riseTime,
                            rawDataIndex            = neutronList[neutron].rawDataIndex - iStartRawData, # the only field changed
                            eventNumber             = neutronList[neutron].eventNumber                              
                        )
                        neutronListWindow.append(myNeutronShape)


                # badEvent list for the window
                badEventListWindow = []
                for badEvent in range(len(badEventList)):
                    if badEventList[badEvent].rawDataIndex >= iStartRawData and  badEventList[badEvent].rawDataIndex < iStopRawData:
                        myBadEventShape = GBL.t_event(       
                            isNeutron               = badEventList[badEvent].isNeutron,
                            TOTnotValidated         = badEventList[badEvent].TOTnotValidated,
                            amplitudeNotValidated   = badEventList[badEvent].amplitudeNotValidated,
                            pileUp                  = badEventList[badEvent].pileUp,
                            TOTlimitReached         = badEventList[badEvent].TOTlimitReached,
                            charge                  = badEventList[badEvent].charge,
                            TOT                     = badEventList[badEvent].TOT,
                            amplitude               = badEventList[badEvent].amplitude,
                            riseTime                = badEventList[badEvent].riseTime,
                            rawDataIndex            = badEventList[badEvent].rawDataIndex - iStartRawData, # the only field changed
                            eventNumber             = badEventList[badEvent].eventNumber                              
                        )
                        badEventListWindow.append(myBadEventShape)

                # data 1us for the window
                i1usStart                       = iStartRawData/ functions.time_to_sample(GBL.INTEGRATION_PERIOD)
                i1usStop                        = iStopRawData / functions.time_to_sample(GBL.INTEGRATION_PERIOD)
                neutronCounter1usWindow         = neutronCounter1us         [i1usStart : i1usStop]
                neutronCounterPeak1usWindow     = neutronCounterPeak1us     [i1usStart : i1usStop]
                neutronCounterPileUp1usWindow   = neutronCounterPileUp1us   [i1usStart : i1usStop]
                neutronCountingCharge1usWindow  = neutronCountingCharge1us  [i1usStart : i1usStop]
                neutronCountingTOT1usWindow     = neutronCountingTOT1us     [i1usStart : i1usStop]
                neutronCountingAmp1usWindow     = neutronCountingAmp1us     [i1usStart : i1usStop]
                neutronCountingRiseTime1usWindow= neutronCountingRiseTime1us[i1usStart : i1usStop]

                suptitle = "FPGA algo simulation - DOD/scope/degub, trig on: " + GBL.SCOPE_TRIGGER[GBL.SCOPE_TRIGGER_SELECTION] + "\n" 
                plot_neutron_detection( NB_OF_SAMPLES_ACQUIRED      = WINDOW_DISPLAY,
                                        neutronList                 = neutronListWindow,
                                        canvasTitle                 = "Degug/scope",
                                        suptitle                    = suptitle,
                                        rawData                     = myWindow,
                                        rawDataDerivated            = rawDataDerivated,
                                        neutronCounter1us           = neutronCounter1usWindow, 
                                        neutronCounterPeak1us       = neutronCounterPeak1usWindow,
                                        neutronCounterPileUp1us     = neutronCounterPileUp1usWindow, 
                                        neutronCountingCharge1us    = neutronCountingCharge1usWindow, 
                                        neutronCountingTOT1us       = neutronCountingTOT1usWindow, 
                                        neutronCountingAmp1us       = neutronCountingAmp1usWindow,
                                        neutronCountingRiseTime1us  = neutronCountingRiseTime1usWindow, 
                                        badEventList                = badEventListWindow,
                                        resetIntegration1us         = resetIntegrationWindow,
                                        sparkDetected               = sparkDetectedWindow,
                                        saturation                  = saturationWindow,
                                        positiveSaturation          = positiveSaturationWindow,
                                        negativeSaturation          = negativeSaturationWindow
                                        )  

                # subplot selection
                ax = plt.subplot(311)

                # rawdata mask if data are generated
                if "generated" in GBL.RAW_DATA_FILE:
                    plt.plot(timeAcq, rawDataMask[iStartRawData : iStopRawData], label="generated neutron mask")
                    plt.legend(loc='best')

                # neutron peaks: draw gaussian fit
                if GBL.SCOPE_TRIGGER_SELECTION == 4 :
                    ####
                    # IS THAT REALLY NECESSARY ?
                    ####
                    ####################### NOT WORKING ################
                    # gaussian fit
                    # my signal
                    center                  = int(WINDOW_DISPLAY/2)
                    peakDurationDivideByTwo = functions.time_to_sample(GBL.TOT_PILE_UP_START) / 2
                    x                       = timeAcq[ (center - peakDurationDivideByTwo) : (center + peakDurationDivideByTwo)]
                    y                       = myWindow[(center - peakDurationDivideByTwo) : (center + peakDurationDivideByTwo)]
                    # print "len(x):", len(x), "center:", center, "peakDurationDivideByTwo:", peakDurationDivideByTwo, "len(timeAcq):", len(timeAcq)
                    # initial guess for the parameters
                    x0          = peakDurationDivideByTwo       # position
                    amplitude       = y[x0]             # amplitude gaussian
                    sigma       = GBL.GAUSSIAN_SIGMA        # approximation
                    p0          = [amplitude, x0, sigma]    # coeffs

                    try:
                        popt, pcov = curve_fit(functions.gaus,x,y ,p0)
                        myString = [    "gaussian fit:\n" +
                                "    - Xmean :" + str(round(popt[1], 2)) + " mV"  + "\n"
                                "    - sigma :" + str(round(popt[2], 2)) + " mV"
                        ]
                        # print myString[0]
                        plt.plot(x, functions.gaus(x,*popt),':',label='gaussian fit')
                        plt.legend(loc='best')
                    except:
                        print "WARNING: failed to compute gaussian fit"

                # neutron shape annotation when trig on neutron shape
                if GBL.SCOPE_TRIGGER_SELECTION >=3 and  GBL.SCOPE_TRIGGER_SELECTION <= 6:
                    # write some info on the plot concerning neutron shape
                    for neutron in range(len(neutronListWindow)):
                        annotation = [  "Event number: "+ str(                              neutronListWindow[neutron].eventNumber)             + "\n" +
                                        "index: "       + str( functions.sample_to_time(    sampleIndex + (neutronListWindow[neutron].rawDataIndex - preTrig))) + " ns" + "\n" +
                                        "Charge: "      + str( round(                       neutronListWindow[neutron].charge, 2))      + " Q"  + "\n" +
                                        "TOT: "         + str( functions.sample_to_time(    neutronListWindow[neutron].TOT))            + " ns" + "\n" +
                                        "Amplitude: "   + str( round(                       neutronListWindow[neutron].amplitude, 2))   + " mV" + "\n" +
                                        "Rise time: "   + str(functions.sample_to_time(     neutronListWindow[neutron].riseTime))       + " ns" ]
                        x_amplitudeMax = functions.sample_to_time( (neutronListWindow[neutron].rawDataIndex - neutronListWindow[neutron].TOT + neutronListWindow[neutron].riseTime) )
                        y_amplitudeMax = neutronListWindow[neutron].amplitude
                        plt.annotate(annotation[0], xy=(x_amplitudeMax, y_amplitudeMax), xytext=(x_amplitudeMax,y_amplitudeMax), fontsize=8, verticalalignment='top')

                        # # marker charge, TOT, aplitude and rise time
                        # # charge
                        # plt.plot(0, 0, linewidth=2.0, label="neutron charge")
                        # # TOT
                        # x_TOTstart = neutronListWindow[neutron].rawDataIndex - neutronListWindow[neutron].TOT
                        # x_TOTstop = neutronListWindow[neutron].rawDataIndex
                        # y_TOT = GBL.EVENT_DETECTION_THRESHOLD
                        # plt.plot([x_TOTstart, x_TOTstop], [y_TOT, y_TOT], linewidth=2.0, label="neutron TOT")
                        # # amplitude
                        # x_amp = x_amplitudeMax
                        # y_ampStart = GBL.EVENT_DETECTION_THRESHOLD
                        # y_ampStop = y_amplitudeMax
                        # plt.plot([x_amp, x_amp], [y_ampStart, y_ampStop], linewidth=2.0, label="neutron amplitude")
                        # # rise time
                        # x_RiseTimeStart = x_TOTstart
                        # x_RiseTimeStop = x_RiseTimeStart +  neutronListWindow[neutron].riseTime
                        # y_RiseTime = y_amplitudeMax
                        # plt.plot([x_RiseTimeStart, x_RiseTimeStop], [y_RiseTime, y_RiseTime], linewidth=2.0, label="neutron rise time")

                # bad events annotation when trig on bad events
                if GBL.SCOPE_TRIGGER_SELECTION >=7 and  GBL.SCOPE_TRIGGER_SELECTION <= 12:
                    # write some info on the plot concerning bad event
                    for badEvent in range(len(badEventListWindow)):
                        annotation = [  "Event number: "+ str(                              badEventListWindow[badEvent].eventNumber)           + "\n" +
                                        "index: "       + str( functions.sample_to_time(    sampleIndex + (badEventListWindow[badEvent].rawDataIndex - preTrig))) + " ns" + "\n" +
                                        "Charge: "      + str( round(                       badEventListWindow[badEvent].charge, 2))    + " Q"  + "\n" +
                                        "TOT: "         + str( functions.sample_to_time(    badEventListWindow[badEvent].TOT))          + " ns" + "\n" +
                                        "Amplitude: "   + str( round(                       badEventListWindow[badEvent].amplitude, 2)) + " mV" + "\n" +
                                        "Rise time: "   + str(functions.sample_to_time(     badEventListWindow[badEvent].riseTime))     + " ns" ]
                        x_amplitudeMax = functions.sample_to_time((badEventListWindow[badEvent].rawDataIndex - badEventListWindow[badEvent].TOT + badEventListWindow[badEvent].riseTime))
                        y_amplitudeMax = badEventListWindow[badEvent].amplitude
                        plt.annotate(annotation[0], xy=(x_amplitudeMax, y_amplitudeMax), xytext=(x_amplitudeMax,y_amplitudeMax), fontsize=8, verticalalignment='top')                  

                # end of every event if TOT limit reached
                # neutron shape
                for neutron in range(len(neutronListWindow)):
                    if neutronListWindow[neutron].TOTlimitReached == 1:
                        # start event
                        x_amplitudeMax = functions.sample_to_time(neutronListWindow[neutron].rawDataIndex - neutronListWindow[neutron].TOT)
                        y_amplitudeMin = min(myWindow)
                        y_amplitudeMax = max(myWindow)
                        plt.plot([x_amplitudeMax,x_amplitudeMax],[y_amplitudeMin,y_amplitudeMax], color='black')
                        plt.annotate("start", xy=(x_amplitudeMax, y_amplitudeMin), xytext=(x_amplitudeMax,y_amplitudeMin), fontsize=8, verticalalignment='top')
                        # end event
                        x_amplitudeMax = functions.sample_to_time(neutronListWindow[neutron].rawDataIndex)
                        y_amplitudeMin = min(myWindow)
                        y_amplitudeMax = max(myWindow)
                        plt.plot([x_amplitudeMax,x_amplitudeMax],[y_amplitudeMin,y_amplitudeMax], color='black')
                        plt.annotate("stop", xy=(x_amplitudeMax, y_amplitudeMin), xytext=(x_amplitudeMax,y_amplitudeMin), fontsize=8, verticalalignment='top')
                # bad events
                for badEvent in range(len(badEventListWindow)):
                    if badEventListWindow[badEvent].TOTlimitReached == 1:
                        # start event
                        x_amplitudeMax = functions.sample_to_time(badEventListWindow[badEvent].rawDataIndex - badEventListWindow[badEvent].TOT)
                        y_amplitudeMin = min(myWindow)
                        y_amplitudeMax = max(myWindow)
                        plt.plot([x_amplitudeMax,x_amplitudeMax],[y_amplitudeMin,y_amplitudeMax], color='black')
                        plt.annotate("start", xy=(x_amplitudeMax, y_amplitudeMin), xytext=(x_amplitudeMax,y_amplitudeMin), fontsize=8, verticalalignment='top')
                        # end event
                        x_amplitudeMax = functions.sample_to_time(badEventListWindow[badEvent].rawDataIndex)
                        y_amplitudeMin = min(myWindow)
                        y_amplitudeMax = max(myWindow)
                        plt.plot([x_amplitudeMax,x_amplitudeMax],[y_amplitudeMin,y_amplitudeMax], color='black')
                        plt.annotate("stop", xy=(x_amplitudeMax, y_amplitudeMin), xytext=(x_amplitudeMax,y_amplitudeMin), fontsize=8, verticalalignment='top')

                callback    = Index()
                axnext      = plt.axes([0.9, 0.05, 0.091, 0.07]) # left, bottom, width, height
                bnext       = Button(axnext, 'Quit')
                bnext.on_clicked(callback.quit)

                # print trig number
                print "Trig number:", triggerCounter ,"/", int( trigMaskSum ), "(in %f s)" %(time.time()-startPlotWindow)

                # show plot
                plt.legend(loc='best')
                plt.show()
            
            sampleIndex = sampleIndex + 1

    else:
        print "plot scope/debug skipped"

    print "..end scope_debug()", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"

def plot_nblm_logs():
    """ plot a nblm log file """
    startFunction = time.time()
    print "start plotting nblm logs", functions.elapsedTime(startFunction, GBL.START_TIME)

    if GBL.SAVE_LOGS == True:

        # log file
        myFile = GBL.PATH_NBLM_LOGS + GBL.NBLM_LOG_FILE_NAME
        print "nBLM log file:", myFile

        # number of lines
        nbOfIntegration = functions.getNumberOfLines(myFile)

        # charge
        neutronCountingCharge1us    = np.zeros(nbOfIntegration)
        # neutron counting
        neutronCounter1us           = np.zeros(nbOfIntegration)
        # neutron counting peaks
        neutronCounterPeak1us       = np.zeros(nbOfIntegration)
        # neutron counting pile up
        neutronCounterPileUp1us     = np.zeros(nbOfIntegration)

        # event lists
        neutronList 	= []
        badEventList 	= []

        # fetch data from a nblm log file
        lineCounter = 0
        with open(myFile) as logfile :
            for line in logfile : # for each line in the file
                words = line.split(";") # split the line in words

                for i in range(len(words)):
                    if i == 0:
                        # charge
                        neutronCountingCharge1us[lineCounter]   = words[i]
                    elif i == 1:
                        # neutron counting
                        neutronCounter1us       [lineCounter]   = words[i]
                    elif i == 2:
                        # neutron counting peaks
                        neutronCounterPeak1us   [lineCounter]   = words[i]
                    elif i == 3:
                        # neutron counting pile-up
                        neutronCounterPileUp1us [lineCounter]   = words[i]
                    elif  words[i] != "\n":

                        # event
                        event = words[i].split(",")
                        myEvent = GBL.t_event(  
                                    isNeutron               = (event[0] == "True"),
                                    TOTnotValidated         = (event[1] == "True"),
                                    amplitudeNotValidated   = (event[2] == "True"),
                                    pileUp                  = (event[3] == "True"),
                                    TOTlimitReached         = (event[4] == "True"),
                                    charge                  = float(    event[5]),
                                    TOT                     = int(      event[6]),
                                    amplitude               = float(    event[7]),
                                    riseTime                = int(      event[8]),
                                    rawDataIndex            = int(      event[9]),
                                    eventNumber             = int(      event[10])) 
                        if myEvent.isNeutron == True:
                            # neutron
                            neutronList.append(myEvent)
                        else:
                            # bad event
                            badEventList.append(myEvent)

                    else:
                        pass #  words[i] = "\n"
                                            
                # next line
                lineCounter = lineCounter + 1

        # number of raw data
        NB_OF_SAMPLES_ACQUIRED  = nbOfIntegration * functions.time_to_sample(GBL.INTEGRATION_PERIOD)

        # resetIntegration1us
        resetIntegration1us, duration = functions.get_reset_integration(NB_OF_SAMPLES_ACQUIRED)
        
        fig = plt.figure()
        canvasTitle = "plotting full window"
        fig.canvas.set_window_title(canvasTitle) 
        suptitle    = "FPGA algo simulation - full window from a nblm log file\n"
        fig.suptitle(suptitle, fontsize=14)
        
        # x axis in 4ns
        timeAcq = np.linspace(0, functions.sample_to_time(NB_OF_SAMPLES_ACQUIRED), NB_OF_SAMPLES_ACQUIRED)

        # plot neutron counting
        ax = plt.subplot(211)
        plt.title("neutron counting", fontsize=12)
        # counting 
        plt.step(timeAcq, oversampling(neutronCounter1us,           resetIntegration1us),   label='neutron counter: neutron peak counting 1us + charge1us')
        plt.step(timeAcq, oversampling(neutronCountingCharge1us,    resetIntegration1us),   label='neutron counter: charge1us of events / charge_MPV')
        # plt.step(timeAcq, oversampling(neutronCountingTOT1us, resetIntegration1us), label='NOT combined: TOT1us')
        # plt.step(timeAcq, oversampling(neutronCountingAmp1us, resetIntegration1us), label='NOT combined: amplitude1us')
        # plt.step(timeAcq, oversampling(neutronCountingRiseTime1us, resetIntegration1us), label='NOT combined: riseTime1us')
        # reset
        plt.step(timeAcq, offset_list(resetIntegration1us, -2), label='reset counter 1us')
        plt.xlabel('ns')
        plt.ylabel('neutron counting')
        plt.legend(loc='best')

        # plot status
        # neutron masks
        neutronPeakMask         = np.zeros(NB_OF_SAMPLES_ACQUIRED)
        neutronPileUpMask           = np.zeros(NB_OF_SAMPLES_ACQUIRED)
        neutronPileUpLimitReachedMask   = np.zeros(NB_OF_SAMPLES_ACQUIRED)
        for neutron in range(len(neutronList)):
            neutronPeakMask[neutronList[neutron].rawDataIndex] = int(not(neutronList[neutron].pileUp))
            neutronPileUpMask[neutronList[neutron].rawDataIndex] = neutronList[neutron].pileUp
            neutronPileUpLimitReachedMask[neutronList[neutron].rawDataIndex] = neutronList[neutron].TOTlimitReached

        # badEvent masks
        badEventTOTnotValidatedMask  = np.zeros(NB_OF_SAMPLES_ACQUIRED)
        badEventAmplitudeNotValidatedMask  = np.zeros(NB_OF_SAMPLES_ACQUIRED)
        badEventPileUpMask  = np.zeros(NB_OF_SAMPLES_ACQUIRED)
        badEventPileUpLimitReachedMask = np.zeros(NB_OF_SAMPLES_ACQUIRED)
        for badEvent in range(len(badEventList)):
            badEventTOTnotValidatedMask         [badEventList[badEvent].rawDataIndex]   = badEventList[badEvent].TOTnotValidated
            badEventAmplitudeNotValidatedMask       [badEventList[badEvent].rawDataIndex]   = badEventList[badEvent].amplitudeNotValidated
            badEventPileUpMask              [badEventList[badEvent].rawDataIndex]   = badEventList[badEvent].pileUp
            badEventPileUpLimitReachedMask      [badEventList[badEvent].rawDataIndex]   = badEventList[badEvent].TOTlimitReached
        # plot
        ax2 = plt.subplot(212, sharex=ax)
        plt.title("detection status", fontsize=12)
        plt.xlabel('ns')
        # neutrons
        plt.step(timeAcq, offset_list(neutronPeakMask,              14), label='neutron peak')
        plt.step(timeAcq, offset_list(neutronPileUpMask,            12), label='neutron pile-up')
        plt.step(timeAcq, offset_list(neutronPileUpLimitReachedMask,        10), label='neutron TOT limit reached')
        # badEvents
        plt.step(timeAcq, offset_list(badEventTOTnotValidatedMask,          6), label='bad event  because of TOT')
        plt.step(timeAcq, offset_list(badEventAmplitudeNotValidatedMask,    4), label='bad event because of amplitude')
        plt.step(timeAcq, offset_list(badEventPileUpMask,                   2), label='bad event pile-up')
        plt.step(timeAcq, offset_list(badEventPileUpLimitReachedMask,       0), label='bad event TOT limit reached')
        # To hide the Y-axis of a plot
        cur_axes = plt.gca()
        cur_axes.axes.get_yaxis().set_visible(False)
        plt.legend(loc='best')

        # adjust subplot position
        functions.adjust_subplot_position()
    else:
        print "plot skipped"

    print "..end plotting nblm logs", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"
    return 0

def overlapingBeamPulses(rawData):
    """ overlapping beam pulses: plot on the same figure (subplot or one plot)""" 

    startFunction = time.time()
    print "start overlaping all beam pulses", functions.elapsedTime(startFunction, GBL.START_TIME)

    if GBL.USE_DATA_UNDER_EVENT_THRESHOLD == False:

        ## parameters
        # subplotOption (subplot on only one plot (all windows on the same plot))
        subplot         = True
        nbMaxSubplot    = 10
        # x axis
        axisConversion  = 1000 # ns
        axisUnit        = "us"

        # pre and post trig
        paddingScope= functions.get_padding_size()
        preTrig     = functions.time_to_sample(GBL.USE_DATA_UNDER_EVENT_THRESHOLD_PRE_TRIG)
        postTrig    = functions.time_to_sample(GBL.USE_DATA_UNDER_EVENT_THRESHOLD_POST_TRIG)
        sizeWindow  = preTrig + postTrig

        # number of window to plot
        nbOfTrig    = (len(rawData) - 2*paddingScope) / sizeWindow
        # print "number of window:", nbOfTrig
        # print "len(rawData)", len(rawData)
        # print "2*paddingScope", 2*paddingScope
        # print "sizeWindow", sizeWindow

        # x axis
        timeAcq     = functions.sample_to_time(np.linspace(-preTrig, postTrig, sizeWindow))/axisConversion

        # plotting
        fig = plt.figure()
        canvasTitle = "plotting events"
        fig.canvas.set_window_title(canvasTitle) 
        suptitle    = "plot events"
        fig.suptitle(suptitle, fontsize=14)
        for i in range(nbOfTrig):
            start   = paddingScope + sizeWindow*i
            stop    = start + sizeWindow
            # check configuration
            if preTrig > start or postTrig > (len(rawData) - start):
                print "[W] overlapingBeamPulses() configuration error, window skipped. Window number:", i+1, "/", nbOfTrig
                print "    - pre trig:\t\t\t",              preTrig,        "samples"
                print "    - post trig:\t\t",               postTrig,       "samples"
                print "    - rawdata length:\t\t",          len(rawData),   "samples"  
                print "    - start trigger position:\t",    start,          "samples"
            else:
                # subplot or all windows on the same plot
                if subplot == True:
                    if nbOfTrig > nbMaxSubplot:
                        # too much sublpots, not readable
                        print "[W] too much subplots, not readable. plot them on the same plot. Change 'nbMaxSubplot' parameter to get subplot display."
                    else:
                        plt.subplot(nbOfTrig, 1, i+1)
                plt.title("raw data")
                plt.plot(timeAcq, rawData[start : stop], label="raw data in mV")
                plt.plot(timeAcq, GBL.EVENT_DETECTION_THRESHOLD * np.ones(sizeWindow), label="trigger level")
                plt.ylabel("mV")
                plt.xlabel(axisUnit)
                # plt.ylim( (-GBL.SATURATION, GBL.SATURATION) )
                plt.legend(loc="best")
                ## pre trigger marker
                # x
                xStartPreTrig   = timeAcq[0] # time
                xEndPreTrig     = xStartPreTrig + functions.sample_to_time(preTrig)/axisConversion
                # y
                maxRawData      = max(rawData)
                yStartPreTrig   = maxRawData
                yEndPreTrig     = maxRawData
                plt.plot([xStartPreTrig, xEndPreTrig], [maxRawData, maxRawData], color='k', linewidth=2.0)
                plt.annotate('pre trig', xy=(xStartPreTrig, maxRawData), xytext=(xStartPreTrig, maxRawData), fontsize=8)
                ## post trigger marker
                # x
                xStartPostTrig  = xEndPreTrig
                xEndPostTrig    = xEndPreTrig + functions.sample_to_time(postTrig)/axisConversion
                # y
                maxRawData      = max(rawData)
                yStartPostTrig  = maxRawData
                yEndPostTrig    = maxRawData
                plt.plot([xStartPostTrig, xEndPostTrig], [maxRawData, maxRawData], color='b', linewidth=2.0)
                plt.annotate('post trig', xy=(xStartPostTrig, maxRawData), xytext=(xStartPostTrig, maxRawData), color='b', fontsize=8)
    else:
        print "plot skipped"

    duration = time.time()-startFunction
    print "..end overlaping all beam pulses", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"
    return duration