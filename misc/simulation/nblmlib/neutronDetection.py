import config as GBL            # global variables
import time                     # chrono (elapsed time)
import numpy as np              # vector operation
import matplotlib.pyplot as plt # plots

# nblm lib
from nblmlib import functions   # my functions for nBLM simulation
from nblmlib import dataOut     # save simulation results

def pre_processing(rawData, NB_OF_SAMPLES_ACQUIRED, verbose):
    """ removes pedestal given by the user """

    startFunction = time.time()
    print "start pre-processing raw data", functions.elapsedTime(startFunction, GBL.START_TIME)

    # remove pedestal
    rawDataCorreted = rawData - GBL.PEDESTAL_LEVEL

    # check saturation
    # saturation mask
    rawDataPositiveStaurationMask   = rawDataCorreted > GBL.SATURATION
    rawDataNegativeStaurationMask   = rawDataCorreted < - GBL.SATURATION
    # no saturation mask
    rawDataNoSaturationMask         = np.logical_not( rawDataPositiveStaurationMask + rawDataNegativeStaurationMask)
    # overlap mask
    rawDataCorreted                 = rawDataCorreted * rawDataNoSaturationMask + rawDataPositiveStaurationMask * GBL.SATURATION + rawDataNegativeStaurationMask * -GBL.SATURATION

    print "Pedestal removed:", GBL.PEDESTAL_LEVEL, "mV"

    if verbose:
        # plot results
        fig = plt.figure()
        fig.canvas.set_window_title('Pre-processing')
        fig.suptitle("Pre-processing")
        plt.plot(rawData, label='input data')
        plt.plot(rawDataCorreted, label='output data')
        plt.xlabel("samples")
        plt.ylabel("mV")
        plt.legend()

    duration = time.time()-startFunction
    print "..end pre-processing raw data", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"
    return rawDataCorreted, duration

def badEvent_or_neutron_decision (neutronList, badEventList, charge, TOT, amplitude, riseTime, index4ns, detectedEventNumber):
    """validate the event as a neuton shape or bad event depending on TOT and amplitude and save the event in the event list"""

    if TOT >= functions.time_to_sample(GBL.NEUTRON_TOT_MIN) and amplitude <= GBL.NEUTRON_AMPLITUDE_MIN: # neutron shape validated

        # save event caracteristics
        myNeutronShape = GBL.t_event(   
                        isNeutron               = True,
                        TOTnotValidated         = False,
                        amplitudeNotValidated   = False,
                        pileUp                  = ( TOT >= functions.time_to_sample(GBL.TOT_PILE_UP_START) ),
                        TOTlimitReached         = ( TOT >= functions.time_to_sample(GBL.TOT_PILE_UP_LIMIT_REACHED) ),
                        charge                  = charge,
                        TOT                     = TOT,
                        amplitude               = amplitude,
                        riseTime                = riseTime,
                        rawDataIndex            = (index4ns-1),
                        eventNumber             = detectedEventNumber
                    )
        # print myNeutronShape

        # add current neutron to neutron list
        neutronList.append(myNeutronShape)

        return myNeutronShape.isNeutron # event is a neutron

    else: # NOT a neutron (bad event only)

        # save bad event caracteristics
        myBadEvent = GBL.t_event     (  
                        isNeutron               = False,
                        TOTnotValidated         = ( TOT < functions.time_to_sample(GBL.NEUTRON_TOT_MIN) ),
                        amplitudeNotValidated   = (amplitude > GBL.NEUTRON_AMPLITUDE_MIN),
                        pileUp                  = ( TOT >= functions.time_to_sample(GBL.TOT_PILE_UP_START) ),
                        TOTlimitReached         = ( TOT >= functions.time_to_sample(GBL.TOT_PILE_UP_LIMIT_REACHED) ),
                        charge                  = charge,
                        TOT                     = TOT,
                        amplitude               = amplitude,
                        riseTime                = riseTime,
                        rawDataIndex            = (index4ns-1),
                        eventNumber             = detectedEventNumber
                    )
        # print myBadEvent

        # add current bad event to bad event list
        badEventList.append(myBadEvent)

        return myBadEvent.isNeutron # event is a bad event

def get_peak_and_pileUp_counter(isNeutron, neutronList, badEventList, neutronPeakCounter, neutronPileUpCounter, badEventPeakCounter, badEventPileUpCounter):
    """ increment peak/pile-up counter depending on last event """

    if isNeutron == True:
        # neutron
        if neutronList[-1].pileUp == False: # last event
            # neutron peak
            neutronPeakCounter      = neutronPeakCounter    + 1
        else:
            # neutron pile-up
            neutronPileUpCounter    = neutronPileUpCounter  + 1
    else:
        # bad event
        if badEventList[-1].pileUp == False: # last event
            # bad event peak
            badEventPeakCounter     = badEventPeakCounter    + 1
        else:
            # bad event pile-up
            badEventPileUpCounter   = badEventPileUpCounter  + 1

    return neutronPeakCounter, neutronPileUpCounter, badEventPeakCounter, badEventPileUpCounter

def get_charge_event(isNeutron, neutronList, badEventList, chargeNeutron, chargeBadEvent):
    """ get the charge of last event """

    if isNeutron == True:
        # neutron
        chargeNeutron   = chargeNeutron + neutronList[-1].charge    # charge of last event
    else:
        # bad event
        chargeBadEvent  = chargeBadEvent + badEventList[-1].charge  # charge of last event
        
    return chargeNeutron, chargeBadEvent

def event_detection(rawData, NB_OF_SAMPLES_ACQUIRED, verbose):
    """
        for each event, function deternimes whether it's a bad event (noise, gammas, sparks ...) or a neutron shape
        return 2 lists (one for the bad events, one for the neutron shapes) of t_event with all information in it (isNeutron, charge, TOT, pileUp...)
        In the simulation, we have split neutron shapes and badEvents in 2 structures. The aim of this is for faster simulation. Indeed, the rest of the simulation uses mostly only neutrons (and not badEvents).
    """

    # init necessary for no verbose mode
    file = 0

    startFunction = time.time()
    if verbose == True:
        print "start event detection", functions.elapsedTime(startFunction, GBL.START_TIME)

    # neutron shapes (peaks and pil-ups)
    neutronList     = []
    # badEvent: event which are NOT a neutron (ex: gammas, sparks, noise)
    badEventList    = []

    # process raw data
    index4ns            = 0
    detectedEventNumber = 0 # number of peak detection

    # shape caracteristics init
    event_detection_running, charge, TOT, amplitude, riseTime = 0, 0, 0, 0, 0

    # peak and pile-up counter init
    neutronPeakCounter, neutronPileUpCounter, badEventPeakCounter, badEventPileUpCounter = 0, 0, 0, 0

    # charge neutron shape and bad event init
    chargeNeutron, chargeBadEvent = 0, 0

    # TOT limit reach counters init
    neutronTOTlimitReachedCounter, badEventTOTlimitReachedCounter = 0, 0

    # start processing data
    startProcess = time.time()
    print "start process.. (%f s)" %(time.time()-startProcess)
    for index4ns in range(NB_OF_SAMPLES_ACQUIRED):

        # event detection
        if(rawData[index4ns] < GBL.EVENT_DETECTION_THRESHOLD): # intresting data => event detected
            # startBufferingEvent = time.time()

            # save start index of the new event
            if event_detection_running == False:
                eventStartIndex = index4ns
            
            # in event
            event_detection_running = True

            # TOT = 1us
            if TOT >= functions.time_to_sample(GBL.TOT_PILE_UP_LIMIT_REACHED):

                # end of the current event (reached TOT_PILE_UP_LIMIT_REACHED)
                detectedEventNumber     = detectedEventNumber + 1

                # charge calculation
                charge  = np.sum(rawData[eventStartIndex : index4ns])   # sum of mV
                
                # decide if the event is a badEvent or a neutron
                isNeutron = badEvent_or_neutron_decision (neutronList, badEventList, charge, TOT, amplitude, riseTime, index4ns, detectedEventNumber)

                # TOT limit reched counter
                if isNeutron == True:
                    neutronTOTlimitReachedCounter  = neutronTOTlimitReachedCounter + 1
                else:
                    badEventTOTlimitReachedCounter  = badEventTOTlimitReachedCounter + 1

                # peak and pile-up counter
                neutronPeakCounter, neutronPileUpCounter, badEventPeakCounter, badEventPileUpCounter = get_peak_and_pileUp_counter(isNeutron, neutronList, badEventList, neutronPeakCounter, neutronPileUpCounter, badEventPeakCounter, badEventPileUpCounter)

                # charge neutron shape and bad event shape
                chargeNeutron, chargeBadEvent = get_charge_event(isNeutron, neutronList, badEventList, chargeNeutron, chargeBadEvent)

                # new event but we just finished processing last event
                amplitude       = rawData[index4ns]
                TOT             = 0
                riseTime        = 0
                eventStartIndex = index4ns

            # normal case, just saving event
            else:
                # continue actual bufferisation
                TOT = TOT + 1
                if amplitude > rawData[index4ns]:
                    # lower amplitude than the one saved
                    amplitude   = rawData[index4ns]
                    riseTime    = TOT

            # print "end buffering event duration.. (%f s)" %(time.time()-startBufferingEvent)

        # end of event detection
        else:
            if event_detection_running == True:
                # startEndOfEvent = time.time()

                # end of event
                event_detection_running = False

                # +1 event
                detectedEventNumber = detectedEventNumber + 1

                # charge calculation
                # startFunction = time.time()
                charge  = np.sum(rawData[eventStartIndex : index4ns])   # sum of mV
                # print "end charge", functions.elapsedTime(startFunction, GBL.START_TIME)

                # decide if the event is a badEvent or a neutron
                # startFunction = time.time()
                isNeutron = badEvent_or_neutron_decision (neutronList, badEventList, charge, TOT, amplitude, riseTime, index4ns, detectedEventNumber)
                # print "end is neutron", functions.elapsedTime(startFunction, GBL.START_TIME)

                # peak and pile-up counter
                # startFunction = time.time()
                neutronPeakCounter, neutronPileUpCounter, badEventPeakCounter, badEventPileUpCounter = get_peak_and_pileUp_counter(isNeutron, neutronList, badEventList, neutronPeakCounter, neutronPileUpCounter, badEventPeakCounter, badEventPileUpCounter)
                # print "end peak and pile-up counter", functions.elapsedTime(startFunction, GBL.START_TIME)

                # charge neutron shape and bad event shape
                # startFunction = time.time()
                chargeNeutron, chargeBadEvent = get_charge_event(isNeutron, neutronList, badEventList, chargeNeutron, chargeBadEvent)
                # print "end charge neutron shape and bad event shape", functions.elapsedTime(startFunction, GBL.START_TIME)

                # reset
                amplitude, TOT, riseTime = 0, 0, 0

                # print "end of event duration.. (%f s)" %(time.time()-startEndOfEvent)

            else: # wainting for next event
                pass
    print "end process.. (%f s)" %(time.time()-startProcess)

    # check there are some events
    duration = time.time()-startFunction
    if detectedEventNumber == 0:
        print "WARNING: 0 event detected"
        return neutronList, badEventList, duration
    if len(neutronList) == 0:
        print "WARNING: 0 neutron dectected"
        return neutronList, badEventList, duration

    if verbose == True:
        # check pile-up rate
        # for every neutron, get pile-up data
        pileUp = np.zeros(len(neutronList))
        for neutron in range(len(neutronList)):
            pileUp[neutron] = neutronList[neutron].pileUp
        # pile-up rate calculation
        ratePileUp = round(np.sum(pileUp)/float(len(pileUp)) * 100 , 2)
        print "Pile-up rate:", ratePileUp, "% of events identified as neutron shapes are in pile-up"

        print "Number of events:", detectedEventNumber
        if detectedEventNumber > 0:
            print "  - neutron shapes:\t",                  len(neutronList),               "\t(",      round(functions.safe_div(len(neutronList)                 , float(detectedEventNumber))   *100,2),    "%)"
            print "    - nb of peaks:\t  ",                 neutronPeakCounter,             "\t  (",    round(functions.safe_div(neutronPeakCounter               , float(len(neutronList)))      *100,2),    "%)"
            print "    - nb of pile-up:\t  ",               neutronPileUpCounter,           "\t  (",    round(functions.safe_div(neutronPileUpCounter             , float(len(neutronList)))      *100,2),    "%)"
            print "      - nb of TOT limit reached:\t  ",   neutronTOTlimitReachedCounter,  "\t  (",    round(functions.safe_div(neutronTOTlimitReachedCounter    , float(neutronPileUpCounter))  *100,2),    "%)"
            print "  - bad events:\t\t",                    len(badEventList),              "\t(",      round(functions.safe_div(len(badEventList)                , float(detectedEventNumber))   *100,2),    "%)"
            print "    - nb of peaks:\t  ",                 badEventPeakCounter,            "\t  (",    round(functions.safe_div(badEventPeakCounter              , float(len(badEventList)))     *100,2),    "%)"
            print "    - nb of pile-up:\t  ",               badEventPileUpCounter,          "\t  (",    round(functions.safe_div(badEventPileUpCounter            , float(len(badEventList)))     *100,2),    "%)"
            print "      - nb of TOT limit reached:\t  ",   badEventTOTlimitReachedCounter, "\t  (",    round(functions.safe_div(badEventTOTlimitReachedCounter   , float(badEventPileUpCounter)) *100,2),    "%)"

        startCharge = time.time()
        print "start charge calculation.. (%f s)" %(time.time()-startCharge)
        # total charge signal
        chargeSignal = np.sum(rawData * (rawData<0)) # faster than doing it in the for loop just above
        print "charge signal (", int(chargeSignal), "Q) = charge bad events (" , int(chargeBadEvent), "Q) + charge neutron (", int(chargeNeutron), "Q)"
        print "end charge calculation.. (%f s)" %(time.time()-startCharge)

        # save logs
        startSaveLogs = time.time()
        print "start save logs.. (%f s)" %(time.time()-startSaveLogs)
        dataOut.event_detection_logs(neutronList, "neutron_shapes.txt")
        dataOut.event_detection_logs(badEventList, "bad_event_shapes.txt")
        print "see logs/bad_event_shapes.txt and logs/neutron_shapes.txt for more details"
        print "end save logs.. (%f s)" %(time.time()-startSaveLogs)

        duration = time.time()-startFunction
        print "..end event detection", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"
    return neutronList, badEventList, duration

def neutron_shape_integration1us(neutronList, nbOfIntegration1us):
    """ integrates neutron shapes over 1us"""

    startFunction = time.time()
    print "start integration 1us", functions.elapsedTime(startFunction, GBL.START_TIME)

    nbOfNeutron = len(neutronList)

    # if len(neutronList) == 0:
    #         print "FAILED: no data to integrate"
    #         return [0 for i in range(nbOfIntegration1us)]

    # check events are neutrons
    # nbOfNeutrons = 0
    # for i in range(nbOfNeutron):
    #    if neutronList.isNeutron == True:
    #       nbOfNeutrons = nbOfNeutrons + 1

    # integration window vector creation
    numberOfNeutronPeak1us      = np.zeros(nbOfIntegration1us)
    neutronChargePileUp1us      = np.zeros(nbOfIntegration1us)
    numberOfNeutronPileUp1us    = np.zeros(nbOfIntegration1us)

    # fill window of 1us
    for neutron in range(nbOfNeutron):

        # select integration window
        indexIntegration = functions.get_index_integration(neutronList[neutron].rawDataIndex)

        # 2 cases: pile-up or not
        if neutronList[neutron].pileUp == False: # no pile-up
            numberOfNeutronPeak1us[indexIntegration]    = numberOfNeutronPeak1us    [indexIntegration]  + 1
        else: # pile-up
            # add relevant value
            neutronChargePileUp1us[indexIntegration]    = neutronChargePileUp1us    [indexIntegration]  + neutronList[neutron].charge
            numberOfNeutronPileUp1us[indexIntegration]  = numberOfNeutronPileUp1us  [indexIntegration]  + 1

    duration = time.time() - startFunction
    print "..end integration 1us", functions.elapsedTime(startFunction, GBL.START_TIME)
    return numberOfNeutronPeak1us, neutronChargePileUp1us, numberOfNeutronPileUp1us

def neutron_counter_1us(neutronList, nbOfIntegration1us):
    """
        gives an accurate value of the number of neutron over 1us
        if no pile-up: 1 event validated as neutron peak by event_detection = 1 neutron
        else : counting = charge/charge_MPV
        If in 1us, pile-up and no pile-up, couters are summed
    """
    startFunction = time.time()
    print "start neutron counter ", functions.elapsedTime(startFunction, GBL.START_TIME)

    # neutron shape integration, result is split between neutronpeaks and neutron pile-up
    neutronCounterPeak1us, neutronChargePileUp1us, numberOfNeutronPileUp1us = neutron_shape_integration1us(neutronList, nbOfIntegration1us)
    print "Number of neutron peaks:",   np.sum(neutronCounterPeak1us)
    print "Number of neutron pile-up:", np.sum(numberOfNeutronPileUp1us)
    # print "Neutron pile-up rate",  round(np.sum(numberOfNeutronPileUp1us)/(np.sum(numberOfNeutronPileUp1us)+np.sum(neutronCounterPeak1us))*100,2), "%"

    # pile-up neutron counter
    neutronCounterPileUp1us = neutronChargePileUp1us / GBL.NEUTRON_CHARGE_MPV

    # add neutron counting during pile-up and no pile-up
    neutronCounter1us = neutronCounterPeak1us + neutronCounterPileUp1us

    print "Nb of neutron counted:\t", round(np.sum(neutronCounter1us),       2), "neutrons"
    print "    - neutron peaks:\t",    round(np.sum(neutronCounterPeak1us),   2), "neutrons"
    print "    - neutron pile-up:\t",  round(np.sum(neutronCounterPileUp1us), 2), "neutrons"

    duration = time.time()-startFunction
    print "..end neutron counter", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"
    return neutronCounter1us, neutronCounterPeak1us, neutronCounterPileUp1us, numberOfNeutronPileUp1us, duration

def neutron_counter_cheap(neutronList, nbOfIntegration1us):
    """ use MPV to compute neutron counting """

    startFunction = time.time()
    print "start neutron counter cheap (charge only, TOT only ...)", functions.elapsedTime(startFunction, GBL.START_TIME)

    nbOfEvent = len(neutronList)
    print "nbOfEvent:", nbOfEvent
    if nbOfEvent == 0:
        # no neutron shapes
        zeroArray = np.zeros(nbOfIntegration1us)
        return zeroArray, zeroArray, zeroArray, zeroArray, time.time()-startFunction
    else:
        # get neutron shapes caracteristics
        charge1us       = np.zeros(nbOfIntegration1us)
        TOT1us          = np.zeros(nbOfIntegration1us)
        amplitude1us    = np.zeros(nbOfIntegration1us)
        riseTime1us     = np.zeros(nbOfIntegration1us)

        # fill window of 1us
        for event in range(nbOfEvent):

            # check if event is neutron
            if neutronList[event].isNeutron == True: # should be uselles (only for security)

                # select integration window
                indexIntegration = functions.get_index_integration(neutronList[event].rawDataIndex)

                charge1us[indexIntegration]         = charge1us[indexIntegration]       + neutronList[event].charge
                TOT1us[indexIntegration]            = TOT1us[indexIntegration]          + neutronList[event].TOT
                amplitude1us[indexIntegration]      = amplitude1us[indexIntegration]    + neutronList[event].amplitude
                riseTime1us[indexIntegration]       = riseTime1us[indexIntegration]     + neutronList[event].riseTime

        if GBL.NEUTRON_CHARGE_MPV == 0:
            print "[WARNING] MPV charge = 0 !"
        neutronCountingCharge1us   = charge1us      /                           GBL.NEUTRON_CHARGE_MPV
        neutronCountingTOT1us      = TOT1us         / functions.time_to_sample( GBL.NEUTRON_TOT_MPV)
        neutronCountingAmp1us      = amplitude1us   /                           GBL.NEUTRON_AMPLITUDE_MPV
        neutronCountingRiseTime1us = riseTime1us    / functions.time_to_sample( GBL.NEUTRON_RISE_TIME_MPV)

        print "neutron counting:"
        print "    - with charge1us:\t",    round(np.sum(neutronCountingCharge1us)     ,2), "neutrons"
        print "    - with TOT1us:\t",       round(np.sum(neutronCountingTOT1us)        ,2), "neutrons"
        print "    - with amp1us:\t",       round(np.sum(neutronCountingAmp1us)        ,2), "neutrons"
        print "    - with riseTime1us:",    round(np.sum(neutronCountingRiseTime1us)   ,2), "neutrons"

        duration = time.time()-startFunction
        print "..end neutron counter cheap (charge only, TOT only ...)", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"
        return neutronCountingCharge1us, neutronCountingTOT1us, neutronCountingAmp1us, neutronCountingRiseTime1us, duration