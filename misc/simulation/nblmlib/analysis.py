import time                             # chrono (elapsed time)
import numpy as np                      # vector operation
import matplotlib.pyplot as plt         # plots
import math                             # ceil()
from scipy.optimize import curve_fit    # gaussian fit
from scipy import asarray as ar,exp     # gaussian fit
from scipy import signal                # gaussian fit

import config as GBL                    # global variables

from nblmlib import functions           # my functions for nBLM simulation
from nblmlib import neutronDetection    # pre-processing and detects neutrons


def pedestal_calculation(rawData, NB_OF_SAMPLES_ACQUIRED, neutronList, badEventList):
    """ advice the user to select the right PEDESTAL_LEVEL"""

    startFunction = time.time()
    print "start pedestal calculation", functions.elapsedTime(startFunction, GBL.START_TIME)

    if GBL.COMPUTE_PEDESTAL == True:

        # figure
        fig = plt.figure()
        fig.canvas.set_window_title('Pedestal level selection') 
        fig.suptitle("Pedestal raw data")
        plt.xlabel("mV")
        plt.ylabel("nb of events")
        # raw data histogram
        histY, histX, Null = plt.hist(rawData, normed=False, histtype='step', bins='auto')
        # actual pedestal
        plt.plot([GBL.PEDESTAL_LEVEL, GBL.PEDESTAL_LEVEL], [min(histY), max(histY)], label="Actual pedestal level")
        print "current pedestal set by user:", GBL.PEDESTAL_LEVEL, "mV"
        # event detection threshold
        plt.plot([GBL.EVENT_DETECTION_THRESHOLD, GBL.EVENT_DETECTION_THRESHOLD], [min(histY), max(histY)], label="event detection threshold")
        # neutron amplitude min
        plt.plot([GBL.NEUTRON_AMPLITUDE_MIN, GBL.NEUTRON_AMPLITUDE_MIN], [min(histY), max(histY)], label="neutron amplitude min")

        # pedestal calculated with histogram (difficult in FPGA)
        indiceMaxY = int(histY.argmax())
        maxDistrib = histY[indiceMaxY]
        ordinateMaxDistrib = histX[indiceMaxY]
        pedestalCalulationHisto = ordinateMaxDistrib
        plt.plot([pedestalCalulationHisto, pedestalCalulationHisto], [min(histY), max(histY)], label="pedestal calculated from histogram (advised value)")
        print "pedestal computed (histogram):", round(pedestalCalulationHisto,1), "mV"

        # neutron energy mask
        neutronEnergynMask = np.zeros(NB_OF_SAMPLES_ACQUIRED)
        for neutron in range(len(neutronList)):
            neutronEnergynMask[neutronList[neutron].rawDataIndex] = 1
            # energy deposited by neutron
            startNeutron = neutronList[neutron].rawDataIndex - neutronList[neutron].TOT
            stopNeutron = neutronList[neutron].rawDataIndex
            for indexRawData in range(startNeutron, stopNeutron):
                neutronEnergynMask[indexRawData] = 1

        # bad event energy mask
        badEventEnergynMask = np.zeros(NB_OF_SAMPLES_ACQUIRED)
        for badEvent in range(len(badEventList)):
            badEventEnergynMask[badEventList[badEvent].rawDataIndex] = 1
            # energy deposited by neutron
            startBadEvent = badEventList[badEvent].rawDataIndex - badEventList[badEvent].TOT
            stopBadEvent = badEventList[badEvent].rawDataIndex
            for indexRawData in range(startBadEvent, stopBadEvent):
                badEventEnergynMask[indexRawData] = 1

        # pedestal calculated when no neutron detected
        sumPedestalNoNeutron = 0
        nbDataAddedNoNeutron = 0
        for indexRawData in range(NB_OF_SAMPLES_ACQUIRED):
            if neutronEnergynMask[indexRawData] == 0 :
                # no neutron energy <=> bad event, noise and pedestal only
                sumPedestalNoNeutron = sumPedestalNoNeutron + rawData[indexRawData]
                nbDataAddedNoNeutron = nbDataAddedNoNeutron + 1
        pedestalCalculatedNoNeutron = round(sumPedestalNoNeutron / float(nbDataAddedNoNeutron),1)
        plt.plot([pedestalCalculatedNoNeutron, pedestalCalculatedNoNeutron], [min(histY), max(histY)], label="pedestal calculated when no neutron detected")
        print "pedestal computed (no neutron):", pedestalCalculatedNoNeutron, "mV (", nbDataAddedNoNeutron, "raw data used)"

        # pedestal calculated when no neutron AND no bad event detected
        sumPedestalNoNeutronNoBadEvent = 0
        nbDataAddedNoNeutronNoBadEvent = 0
        for indexRawData in range(NB_OF_SAMPLES_ACQUIRED):
            if neutronEnergynMask[indexRawData] == 0 and badEventEnergynMask[indexRawData] == 0:
                # no neutron and no bad event energy <=> noise and pedestal only
                sumPedestalNoNeutronNoBadEvent = sumPedestalNoNeutronNoBadEvent + rawData[indexRawData]
                nbDataAddedNoNeutronNoBadEvent = nbDataAddedNoNeutronNoBadEvent + 1
        pedestalCalculatedNoNeutronNoBadEvent = round(sumPedestalNoNeutronNoBadEvent / float(nbDataAddedNoNeutronNoBadEvent),1)
        plt.plot([pedestalCalculatedNoNeutronNoBadEvent, pedestalCalculatedNoNeutronNoBadEvent], [min(histY), max(histY)], label="pedestal calculated when no neutron detected and no bad event")
        plt.legend()
        print "pedestal computed (no neutron and no bad event):", pedestalCalculatedNoNeutronNoBadEvent, "mV (", nbDataAddedNoNeutronNoBadEvent, "raw data used)"

    else:
        print "Compute pedestal skipped"
        pedestalCalulationHisto = 0

    duration = time.time()-startFunction
    print "..end pedestal calculation", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"
    return pedestalCalulationHisto, duration


def binary_signal_integration1us(signal, nbOfIntegration1us):
    """ Integrates a binary signal using a window of 1us. Signal contains the index of the '1' value"""

    startFunction = time.time()
    print "start binary integration 1us", functions.elapsedTime(startFunction, GBL.START_TIME)

    # signal length
    nbOfHighState = len(signal)
    if nbOfHighState == 0:
        print "WARNING: no data to integrate"
        duration = time.time() - startFunction
        print "..end binary integration 1us", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"
        return np.zeros(nbOfIntegration1us)

    binarySignal1us = np.zeros(nbOfIntegration1us)

    for highStateSignal in range(nbOfHighState):

        # select integration window
        indexIntgration = int( math.ceil(signal[highStateSignal]/ functions.time_to_sample(GBL.INTEGRATION_PERIOD) ))

        binarySignal1us[indexIntgration] = binarySignal1us[indexIntgration] + 1

    duration = time.time() - startFunction
    print "..end binary integration 1us", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"

    return binarySignal1us


def saturation_detection(rawData, NB_OF_SAMPLES_ACQUIRED, nbOfIntegration1us):
    """ return all raw data index in which ADC is in saturation"""

    startFunction = time.time()
    print "start checking saturation", functions.elapsedTime(startFunction, GBL.START_TIME)

    # vector: 1,2,3 .... NB_OF_SAMPLES_ACQUIRED+1
    axis = np.linspace(1, NB_OF_SAMPLES_ACQUIRED+1, num=NB_OF_SAMPLES_ACQUIRED, dtype=np.int)

    # mask of positive saturation
    # 0 0 1 0 0 0 0 1 0 .....
    maskPositiveSaturation = rawData >= GBL.SATURATION

    # index of rawdata in positive saturation
    # 0 0 3 0 0 0 0 8 0 .....
    positiveSaturationIndex = (maskPositiveSaturation * axis) 

    # removing 0 is costing time now but binary_signal_integration1us() is much faster 
    positiveSaturation = (positiveSaturationIndex [maskPositiveSaturation]) -1

    # mask of negative saturation
    maskNegativeSaturation = rawData <= -GBL.SATURATION
    
    # index of rawdata in negative saturation 
    negativeSaturation = (maskNegativeSaturation * axis)

    # # removing 0 is costing time now but binary_signal_integration1us() is much faster
    negativeSaturation = (negativeSaturation[maskNegativeSaturation]) -1

    # negative saturation and positive saturation
    saturation = np.sort( np.concatenate( (negativeSaturation, positiveSaturation),0) )

    print "Number of saturation(s):", len(saturation), "(positive saturation(s):", len(positiveSaturation), "+ negative saturation(s)", len(negativeSaturation), ")"

    # integration 1us
    saturation1us = binary_signal_integration1us(saturation, nbOfIntegration1us)

    duration = time.time()-startFunction
    print "..end checking saturation", functions.elapsedTime(startFunction, GBL.START_TIME) , "\n"
    return saturation1us, saturation, positiveSaturation, negativeSaturation, duration

def sparks_detection(rawDataCorrectedDerivated, NB_OF_SAMPLES_ACQUIRED, nbOfIntegration1us):
    """ spark detection: if rawdata variation is too large, spark is detected """

    startFunction = time.time()
    print "start spark detection", functions.elapsedTime(startFunction, GBL.START_TIME)

    # vector: 1,2,3 .... NB_OF_SAMPLES_ACQUIRED+1
    axis = np.linspace(1, NB_OF_SAMPLES_ACQUIRED+1, num=NB_OF_SAMPLES_ACQUIRED, dtype=np.int)

    # if |varation| are > GBL.SPARK_THRESHOLD, then spark is detected
    # 0 0 1 0 0 0 0 1 0 .....
    sparkDetectedMask = (rawDataCorrectedDerivated > GBL.SPARK_THRESHOLD) + (rawDataCorrectedDerivated < -GBL.SPARK_THRESHOLD)

    # index of rawdata in positive saturation
    # 0 0 3 0 0 0 0 8 0 .....
    sparkDetectedIndex = (sparkDetectedMask * axis) 

    # removing 0 is costing time now but binary_signal_integration1us() is much faster 
    sparkDetected = (sparkDetectedIndex [sparkDetectedMask]) -1

    # integration 1us
    sparkDetected1us = binary_signal_integration1us(sparkDetected, nbOfIntegration1us)

    print "Number of sparks (in developement)", len(sparkDetected)

    duration = time.time()-startFunction
    print "..end spark detection", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"
    return sparkDetected1us, sparkDetected, duration

def MPV_estimation(neutronList):
    """MPV estimation using event counting: sum( charge of each neutron peaks) / number of neutron peaks """

    startFunction = time.time()
    print "start MPV estimation using event counting", functions.elapsedTime(startFunction, GBL.START_TIME)

    # number of neutron to compute MPV_estimation()
    neutronListLength = len(neutronList)

    chargeEstimation_MPV    = 0
    TOTestimation_MPV       = 0  
    ampEstimation_MPV       = 0
    riseTimeEstimation_MPV  = 0
    neutronPeakCounter      = 0
    for neutron in range(neutronListLength):
        if neutronList[neutron].pileUp == 0:
            # only neutron peaks
            chargeEstimation_MPV    = chargeEstimation_MPV  + neutronList[neutron].charge    
            TOTestimation_MPV       = TOTestimation_MPV     + 4*neutronList[neutron].TOT        # ns
            ampEstimation_MPV       = ampEstimation_MPV     + neutronList[neutron].amplitude
            riseTimeEstimation_MPV  = riseTimeEstimation_MPV+ 4*neutronList[neutron].riseTime   # ns
            neutronPeakCounter      = neutronPeakCounter    + 1

    # check enough neutron not in pule-up to draw a proper neutron peak average
    if neutronPeakCounter < GBL.NB_NEUTRON_PEAKS_TO_DRAW_PROPER_DISTRIBUTION:
        print "Warning: not enough neutron peaks to compute a proper neutron peak average"
        # 0 neutron peak given
        if neutronPeakCounter == 0:
            return 0,0,0,0, time.time()-startFunction

    chargeEstimation_MPV    = chargeEstimation_MPV  / neutronPeakCounter
    TOTestimation_MPV       = TOTestimation_MPV     / neutronPeakCounter
    ampEstimation_MPV       = ampEstimation_MPV     / neutronPeakCounter
    riseTimeEstimation_MPV  = riseTimeEstimation_MPV/ neutronPeakCounter

    print "MPV estimation using event counting computed with:", neutronPeakCounter, "neutron peaks"
    print "MPV suggested (not adviced):"
    print "    - chargeEstimation_MPV:\t",    round(chargeEstimation_MPV,2),  "Q"
    print "    - TOTestimation_MPV:\t",       round(TOTestimation_MPV,2),     "ns"
    print "    - ampEstimation_MPV:\t",       round(ampEstimation_MPV,2),     "mV"
    print "    - riseTimeEstimation_MPV:\t",  round(riseTimeEstimation_MPV,2),"ns"

    duration = time.time()-startFunction
    print "..end MPV estimation using event counting", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"
    return chargeEstimation_MPV, TOTestimation_MPV, ampEstimation_MPV, riseTimeEstimation_MPV, duration


def neutron_peak_average(rawDataCorrected, NB_OF_SAMPLES_ACQUIRED, neutronList):
    """ sum all neutrons to calcultate neutron peak average"""

    startFunction = time.time()
    print "start neutron peak average", functions.elapsedTime(startFunction, GBL.START_TIME)


    if GBL.COMPUTE_AVERAGE_NEUTRON_PEAK == True:
        # May really slow down calculation in case of big file

        # number of neutron shape given
        nbOfNeutrons = len(neutronList)

        # figure
        fig = plt.figure()
        fig.canvas.set_window_title('neutron peak average') 
        ax = plt.subplot(211)
        ax.set_title("All neutron peaks overlapped", fontsize=12)
        ax.set_xlabel('ns')
        ax.set_ylabel('mV')

        # get all neutrons and plot them on the same graph
        preNeutron  = functions.time_to_sample(GBL.TOT_PILE_UP_START)
        postNeutron = preNeutron
        WINDOW_AVERAGE_NEUTRON_SHAPE = preNeutron + postNeutron # samples
        sumAllNeutrons = np.zeros(WINDOW_AVERAGE_NEUTRON_SHAPE)
        start   = functions.sample_to_time(- WINDOW_AVERAGE_NEUTRON_SHAPE/2) # ns
        stop    = functions.sample_to_time(+ WINDOW_AVERAGE_NEUTRON_SHAPE/2) # ns
        num     = WINDOW_AVERAGE_NEUTRON_SHAPE
        timeAcq = np.linspace(start, stop, num) # axis in ns
        neutronPeakCounter = 0
        for neutron in range(nbOfNeutrons):
            if neutronList[neutron].pileUp == 0 and (neutron == 0 or neutronList[neutron-1].TOTlimitReached == 0): # don't use pile-up neutron and don't take it if previous neutron was in TOTlimitReached
                # save number of number used to compute average peak neutron shape
                neutronPeakCounter = neutronPeakCounter + 1

                # start and stop index of raw data
                ampMaxIndex = neutronList[neutron].rawDataIndex - neutronList[neutron].TOT + neutronList[neutron].riseTime
                start       = ampMaxIndex - preNeutron
                stop        = ampMaxIndex + postNeutron
                
                # add window to sumAllNeutrons
                myWindow        = rawDataCorrected[start : stop]
                sumAllNeutrons  = sumAllNeutrons + myWindow
                plt.step(timeAcq, myWindow)
        print "..end summing neutron peak", functions.elapsedTime(startFunction, GBL.START_TIME), "\n", "\n"

        # check enough neutron peaks to draw a proper neutron peak average
        if neutronPeakCounter < GBL.NB_NEUTRON_PEAKS_TO_DRAW_PROPER_DISTRIBUTION:
            print "Warning: not enough neutron peaks to compute a proper neutron peak average"

            if neutronPeakCounter == 0:
                print "FAILED: not enough neutron peaks to compute neutron peak average"
                return 0, 0, 0, 0, time.time()-startFunction
        
        # plot threshold event
        plt.plot(timeAcq, [GBL.EVENT_DETECTION_THRESHOLD for k in range(WINDOW_AVERAGE_NEUTRON_SHAPE)], color="C1", label="event detection threshold")
        plt.plot(timeAcq, [GBL.NEUTRON_AMPLITUDE_MIN for k in range(WINDOW_AVERAGE_NEUTRON_SHAPE)],     color="C2",  label="min amplitude for a neutron")
        plt.legend(loc='best')

        # neutron peak average
        meanAllNeutrons = sumAllNeutrons / neutronPeakCounter

        # plot mean neutron curve
        ax1 = plt.subplot(212, sharex=ax)
        ax1.set_title("Neutron peak average", fontsize=12)
        ax1.set_xlabel('ns')
        ax1.set_ylabel('mV')
        # plot mean all events
        plt.step(timeAcq, meanAllNeutrons, label="neutron peak average")
        plt.plot(timeAcq, [GBL.EVENT_DETECTION_THRESHOLD for k in range(WINDOW_AVERAGE_NEUTRON_SHAPE)], color="C1", label="event detection threshold")
        plt.plot(timeAcq, [GBL.NEUTRON_AMPLITUDE_MIN for k in range(WINDOW_AVERAGE_NEUTRON_SHAPE)],     color="C2",  label="min amplitude for a neutron")
        plt.legend(loc='best')

        # adjust subplot position
        functions.adjust_subplot_position()

        # compute chargeNeutronPeakAverage, TOTneutronPeakAverage, amplitudeNeutronPeakAverage, riseTimeNeutronPeakAverage
        verbose = 0
        neutronListMean, badEventListMean, duration = neutronDetection.event_detection(meanAllNeutrons, WINDOW_AVERAGE_NEUTRON_SHAPE, verbose)

        if len(neutronListMean) == 0: # 0 neutron counted
            print "FAILED: mean neutron can't be computed properly (see shape: 0 event completed(signal goes unded EVENT_DETECTion_TH)). Tip: try to reduce TOT_PILE_UP_START in config.py"
            return 0,0,0,0, time.time()-startFunction
        elif len(neutronListMean) == 1: # only one neutron => correct mean shape
            # plot result
            x_start_TOT         = - functions.sample_to_time(neutronListMean[0].riseTime)                     # ns
            x_end_TOT           = x_start_TOT + functions.sample_to_time(neutronListMean[0].TOT)              # ns
            x_start_riseTime    = x_start_TOT
            x_end_riseTime      = x_start_riseTime + functions.sample_to_time(neutronListMean[0].riseTime)    # ns
            x_amp               = x_end_riseTime

            # TOT
            plt.plot([x_start_TOT, x_end_TOT], [GBL.EVENT_DETECTION_THRESHOLD, GBL.EVENT_DETECTION_THRESHOLD], linewidth=3.0, label='suggested TOT MPV')
            # amp
            plt.plot([x_amp, x_amp], [0, neutronListMean[0].amplitude], linewidth=3.0, label='suggested amplitude MPV')
            # riseTime
            plt.plot([x_start_riseTime, x_end_riseTime], [neutronListMean[0].amplitude, neutronListMean[0].amplitude], linewidth=3.0, label='suggested rise time MPV')

            print "Neutron peak average shape has been computed with", neutronPeakCounter, "neutron peaks"
            print "MPV suggested (not adviced):"
            print "    - charge\t",       round(                      neutronListMean[0].charge, 2),                    "Q"
            print "    - TOT\t",          functions.sample_to_time(   neutronListMean[0].TOT),         "ns"
            print "    - amplitude\t",    round(                      neutronListMean[0].amplitude, 2),                 "mV"
            print "    - rise time\t",    functions.sample_to_time(   neutronListMean[0].riseTime),    "ns"

            # gaussian fit
            # my signal
            x = timeAcq
            y = meanAllNeutrons
            # initial guess for the parameters
            x0          = 0 # window center
            amplitude   = neutronListMean[0].amplitude
            sigma       = GBL.GAUSSIAN_SIGMA # neutronListMean[0].riseTime / 2.0 # approximation
            p0          = [amplitude, x0, sigma]

            try:
                # gaussian fit
                popt, pcov = curve_fit(functions.gaus, x, y ,p0)
                plt.plot(x, functions.gaus(x,*popt),':', label='gaussian fit')
            except:
                print "WARNING: failed to compute gaussian fit"
            plt.legend(loc="best")
            duration = time.time()-startFunction
            print "..end neutron peak average", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"
            return neutronListMean[0].charge, neutronListMean[0].TOT, neutronListMean[0].amplitude, neutronListMean[0].riseTime, duration

        else: # more than one neutron/badEvent => false shape, could happend in case of noise/pilu-up/sparks...
            print "FAILED: mean neutron can't be computed properly (see shape: more than one event). Tip: try to reduce 'WINDOW_AVERAGE_NEUTRON_SHAPE' value in neutron_peak_average()"
            duration = time.time()-startFunction
            print "..end neutron peak average", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"
            return 0, 0, 0, 0, duration
    
    else:
        # skip 
        print "neutron peak average skipped"
        duration = time.time()-startFunction
        print "..end neutron peak average", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"
        return 0, 0, 0, 0, duration


def draw_histogram(subplotNumber, title, xlabel, ylabel, data):

    # subplo selection, axis lables and title
    ax = plt.subplot(subplotNumber)
    ax.set_title(title)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    option =[
            # 'fd',
            # 'doane',
            # 'rice',
            # 'sturges',
            # 'sqrt',
            'auto',
            # 'scott',
    ]      

    for i in range(len(option)):
        # primary histo
        histY, histX, Null = plt.hist(data, normed=False, histtype='step', bins=option[i])
        indiceMaxY = int(histY.argmax())
        maxDistrib = histY[indiceMaxY]
        ordinateMaxDistrib = (histX[indiceMaxY] + histX[indiceMaxY+1]) / 2.0

    # zoomed histo
    for i in range(len(option)):
        for j in range(len(histX)-1):
            # primary histo
            Null, Null, Null = plt.hist(data, normed=False, histtype='stepfilled', bins=option[i], range=(histX[j],histX[j+1]))
            # indiceMaxY = int(histY.argmax())
            # maxDistrib = histY[indiceMaxY]
            # ordinateMaxDistrib = histX[indiceMaxY]

    return ordinateMaxDistrib, maxDistrib


def neutron_peak_distribution(neutronList, chargeNeutronPeakAverage, TOTneutronPeakAverage, amplitudeNeutronPeakAverage, riseTimeNeutronPeakAverage, chargeEstimation_MPV, TOTestimation_MPV, ampEstimation_MPV, riseTimeEstimation_MPV):
    """ draw neutron distibutions """

    startFunction = time.time()
    print "start neutron peak distribution", functions.elapsedTime(startFunction, GBL.START_TIME)

    if GBL.COMPUTE_HISTOGRAM == True:

        nbOfNeutrons = len(neutronList)

        #figure
        neutronDistribution = plt.figure()
        neutronDistribution.canvas.set_window_title('Neutron distribution') 
        neutronDistribution.suptitle("Neutron peak distribution (only neutrons which are NOT in pile-up)", fontsize=14)

        # fetch all neutron peaks
        charge      = []
        TOT         = []
        amplitude   = []
        riseTime    = []
        neutronPeakCounter = 0
        for neutron in range(nbOfNeutrons):
            if neutronList[neutron].pileUp == 0 and (neutronList[neutron-1].TOTlimitReached == 0 or neutron == 0): # don't use pile-up neutron and don't take it if previous neutron was in TOTlimitReached
                charge.append(                                  neutronList[neutron].charge)
                TOT.append(         functions.sample_to_time(   neutronList[neutron].TOT) )
                amplitude.append(                               neutronList[neutron].amplitude)
                riseTime.append(    functions.sample_to_time(   neutronList[neutron].riseTime) )
                # counts number of neutron peaks
                neutronPeakCounter = neutronPeakCounter + 1

        # check enough neutron peak to plot distribution
        if neutronPeakCounter < GBL.NB_NEUTRON_PEAKS_TO_DRAW_PROPER_DISTRIBUTION:
            print "Warning: not enough neutron peaks to compute a proper neutron distribution"
            if neutronPeakCounter == 0:
                print "FAILED: 0 neutron peak given"
                return 0,0,0,0, time.time()-startFunction

        # neutrons charge distribution
        ordinateMaxDistrib, maxDistrib = draw_histogram(411, "Charge", "Q", "Number of neutron peaks", charge)
        maxDistribCharge = ordinateMaxDistrib
        # marker MPV
        plt.plot([GBL.NEUTRON_CHARGE_MPV, GBL.NEUTRON_CHARGE_MPV], [0, maxDistrib], color='r', linewidth=3.0, label='actual charge_MPV')
        plt.plot([maxDistribCharge, maxDistribCharge], [0, maxDistrib], color='b', linewidth=3.0, label='suggested MPV (with histogram)')
        plt.plot([chargeNeutronPeakAverage, chargeNeutronPeakAverage], [0, maxDistrib], color='orange', linewidth=3.0, label='suggested MPV (with neutron peak average)')
        plt.plot([chargeEstimation_MPV, chargeEstimation_MPV], [0, maxDistrib], color='g', linewidth=3.0, label='suggested MPV (with peak counting)')    
        plt.legend(loc='best')
        
        # neutrons TOT distribution
        ordinateMaxDistrib, maxDistrib = draw_histogram(412, "TOT", "ns", "Number of neutron peaks", TOT)
        maxDistribTOT = ordinateMaxDistrib
        # marker NEUTRON_TOT_MIN, TOT_PILE_UP_START and TOT_PILE_UP_LIMIT_REACHED
        plt.plot([GBL.NEUTRON_TOT_MIN, GBL.NEUTRON_TOT_MIN], [0, maxDistrib], color='k', linewidth=2.0)
        plt.annotate('NEUTRON_TOT_MIN', xy=(GBL.NEUTRON_TOT_MIN, maxDistrib/2.0), xytext=(GBL.NEUTRON_TOT_MIN, maxDistrib/2.0), fontsize=8)
        plt.plot([GBL.TOT_PILE_UP_START, GBL.TOT_PILE_UP_START], [0, maxDistrib], color='k', linewidth=2.0)
        plt.annotate('TOT_PILE_UP_START', xy=(GBL.TOT_PILE_UP_START, maxDistrib/2.0), xytext=(GBL.TOT_PILE_UP_START, maxDistrib/2.0), fontsize=8)
        plt.plot([GBL.TOT_PILE_UP_LIMIT_REACHED, GBL.TOT_PILE_UP_LIMIT_REACHED], [0, maxDistrib], color='k', linewidth=2.0)
        plt.annotate('TOT_PILE_UP_LIMIT_REACHED', xy=(GBL.TOT_PILE_UP_LIMIT_REACHED, maxDistrib/2.0), xytext=(GBL.TOT_PILE_UP_LIMIT_REACHED, maxDistrib/2.0), fontsize=8)
        # marker MPV
        # plt.plot([GBL.NEUTRON_TOT_MPV, GBL.NEUTRON_TOT_MPV], [0, maxDistrib], color='r', linewidth=3.0, label='actual TOT_MPV')
        # plt.plot([maxDistribTOT, maxDistribTOT], [0, maxDistrib], color='b', linewidth=3.0, label='suggested MPV (with histogram)')
        # plt.plot([TOTneutronPeakAverage, TOTneutronPeakAverage], [0, maxDistrib], color='orange', linewidth=3.0, label='suggested MPV (with neutron peak average)')
        # plt.plot([TOTestimation_MPV, TOTestimation_MPV], [0, maxDistrib], color='g', linewidth=3.0, label='suggested MPV (with peak counting)')
        plt.legend(loc='best')

        # neutrons amplitude distribution
        ordinateMaxDistrib, maxDistrib = draw_histogram(413, "Amplitude", "mV", "Number of neutron peaks",amplitude)
        maxDistribAmplitude = ordinateMaxDistrib
        # marker
        plt.plot([GBL.NEUTRON_AMPLITUDE_MIN, GBL.NEUTRON_AMPLITUDE_MIN], [0, maxDistrib], color='k', linewidth=2.0)
        plt.annotate('Amplitude min for a neutron', xy=(GBL.NEUTRON_AMPLITUDE_MIN, maxDistrib/2.0), xytext=(GBL.NEUTRON_AMPLITUDE_MIN, maxDistrib/2.0), fontsize=8)
        # marker MPV
        # plt.plot([GBL.NEUTRON_AMPLITUDE_MPV, GBL.NEUTRON_AMPLITUDE_MPV], [0, maxDistrib], color='r', linewidth=3.0, label='actual amp_MPV')
        # plt.plot([maxDistribAmplitude, maxDistribAmplitude], [0, maxDistrib], color='b', linewidth=3.0, label='suggested MPV (with histogram)')
        # plt.plot([amplitudeNeutronPeakAverage, amplitudeNeutronPeakAverage], [0, maxDistrib], color='orange', linewidth=3.0, label='suggested MPV (with neutron peak average)')
        # plt.plot([ampEstimation_MPV, ampEstimation_MPV], [0, maxDistrib], color='g', linewidth=3.0, label='suggested MPV (with peak counting)')
        plt.legend(loc='best')

        # neutrons rise time distribution
        ordinateMaxDistrib, maxDistrib = draw_histogram(414, "Rise time", "ns", "Number of neutron peaks", riseTime)
        maxDistribRiseTime = ordinateMaxDistrib
        # marker MPV
        # plt.plot([GBL.NEUTRON_RISE_TIME_MPV, GBL.NEUTRON_RISE_TIME_MPV], [0, maxDistrib], color='r', linewidth=3.0, label='actual riseTime_MPV')
        # plt.plot([maxDistribRiseTime, maxDistribRiseTime], [0, maxDistrib], color='b', linewidth=3.0, label='suggested MPV (with histogram)')
        # plt.plot([riseTimeNeutronPeakAverage, riseTimeNeutronPeakAverage], [0, maxDistrib], color='orange', linewidth=3.0, label='suggested MPV (with neutron peak average)')
        # plt.plot([riseTimeEstimation_MPV, riseTimeEstimation_MPV], [0, maxDistrib], color='g', linewidth=3.0, label='suggested MPV (with peak counting)')
        plt.legend(loc='best')
        
        # adjust subplot position
        functions.adjust_subplot_position()
        
        print "Distribution computed with:", neutronPeakCounter, "neuton peaks"
        print "MPV suggested (not adviced):"
        print "    - charge\t",     round(maxDistribCharge,2),      "Q"
        print "    - TOT\t",        round(maxDistribTOT,2),         "ns"
        print "    - amplitude\t",  round(maxDistribAmplitude,2),   "mV"
        print "    - rise time\t",  round(maxDistribRiseTime,2),    "ns"
    else:
        print "histogram skipped"
        maxDistribCharge    = 0
        maxDistribTOT       = 0
        maxDistribAmplitude = 0
        maxDistribRiseTime  = 0

    duration = time.time()-startFunction
    print "..end neutron peak distribution", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"
    return maxDistribCharge, maxDistribTOT, maxDistribAmplitude, maxDistribRiseTime, duration


def derivative(rawtada, sampleShift):
    """ derivate the raw data vector. sampleShift is the number of samples between the samples subtraction"""

    startFunction = time.time()
    print "start derivation", functions.elapsedTime(startFunction, GBL.START_TIME)

    # create a vector shiffted in time
    part1           = rawtada[0] * np.ones(sampleShift)            # use first sample of rawdata as padding
    part2           = rawtada[0 : (len(rawtada) - sampleShift)]    # ignore last points
    rawDataShifted  = np.concatenate((part1, part2), 0)            # concatenate 2 parts

    # do subtraction beetween rawtada and rawDataShifted
    rawDataDerivatived = rawtada - rawDataShifted

    # shift test purpose
    # for i in xrange(1, 100, 10):
    #     shift               = i
    #     rawDataShifted        = np.concatenate((rawtada[0:shift], rawtada[0:len(rawtada)-shift]),0)
    #     rawDataDerivatived    = rawtada - rawDataShifted
    #     plt.plot(rawDataDerivatived, label="Derivative, shift = "+ str(shift), marker='o', markersize=2)

    # plot result
    # plt.plot(rawDataDerivatived, label="Derivative, time shift= "+ str(sampleShift) + " samples", marker='o', markersize=2)
    # plt.plot(rawtada,           label="rawtada",                                                 marker='o', markersize=2)
    # plt.xlabel("samples")
    # plt.ylabel("mV")
    # plt.legend(loc="best")
    # plt.show()

    duration = time.time()-startFunction
    print "..end derivation", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"

    return rawDataDerivatived, duration


