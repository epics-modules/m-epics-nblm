import time                             # elasped time
import numpy as np                      # vector/matrix operations
import matplotlib.pyplot as plt         # plots

class canvas:
    """ canvas class"""
    def __init__(self, title="", fontSize=14, layout="tigh_layout"):
        self.title      = title
        self.fontSize   = fontSize
        self.layout     = layout

class axis:
    """ plot axis class"""
    def __init__(self, autoScale=True, lowLimit=0, highLimit=100, visibleAxe=True, label=""):
        self.autoScale  = autoScale
        self.lowLimit   = lowLimit
        self.highLimit  = highLimit
        self.visibleAxe = visibleAxe
        self.label      = label

class xyData(object):
    """ xy data class"""
    def __init__(self,  yData           = [],
                        xData           = [],
                        label           = "",
                        colorSelect     = False,
                        color           = "black",
                        lineWidth       = 1,
                        lineStyle       = "-",
                        marker          = ".",
                        markerSize      = 2,
                        plot            = True,
                        step            = False,
                        stepOption      = "pre",    # pre or mid or post
                        annotate        = False,
                        annotateText    = "",
                        annotateX       = 0,
                        annotateY       = 0):
        self.yData          = yData
        self.xData          = xData
        self.label          = label 
        self.colorSelect    = colorSelect
        self.color          = color 
        self.lineWidth      = lineWidth 
        self.lineStyle      = lineStyle
        self.marker         = marker 
        self.markerSize     = markerSize 
        self.plot           = plot
        self.step           = step
        self.stepOption     = stepOption 
        self.annotate       = annotate
        self.annotateText   = annotateText
        self.annotateX      = annotateX
        self.annotateY      = annotateY

class tableData(object):
    """ matrix data class"""
    def __init__(self,  title           = "Table title",
                        matrix          = [[]],
                        tableFontSize   = 12,
                        collabel        = [[]],
                        rowLabels       = [[]],
                        cellColors      = [[]],
                        legendPerso     = False,
                        legendHandles   = []):
        self.matrix         = matrix
        self.tableFontSize  = tableFontSize
        self.collabel       = collabel
        self.rowLabels      = rowLabels
        self.cellColors     = cellColors
        self.legendPerso    = legendPerso
        self.legendHandles  = legendHandles

class subplot:
    """ subplot class"""
    def __init__(self, title="", fontSize=12, xAxis=axis(), yAxis=axis(), data=xyData()):
        self.title      = title
        self.fontSize   = fontSize
        self.xAxis      = xAxis
        self.yAxis      = yAxis
        self.data       = data

def generic_plot(canvas, plotList):
    """ generic plot fonction """

    # figure title
    fig = plt.figure()
    fig.canvas.set_window_title(canvas.title) 
    fig.suptitle(canvas.title, fontsize=canvas.fontSize)

    # process every subplot
    nbOfSubplot = len(plotList)
    for subplot in range(nbOfSubplot):

        # subplot selection
        ax = plt.subplot(nbOfSubplot, 1, subplot+1) # +1 because 0 is forbidden by plt.subplot()

        # subplot title
        title   = plotList[subplot].title
        fontsize= plotList[subplot].fontSize
        plt.title(title, fontsize=fontsize)

        # check data format
        if not(type(plotList[subplot].data) is list):
            exit("Forgot to put data in a list: subplot(data=[myData])")

        ### XY data ###


        # XY data or matrix (table)
        if type(plotList[subplot].data[0]) is xyData:
            ### XY data ###
            for j in range(len(plotList[subplot].data)):
                # list of plot to display in one subplot
                x           = plotList[subplot].data[j].xData
                y           = plotList[subplot].data[j].yData
                label       = plotList[subplot].data[j].label
                colorSelect = plotList[subplot].data[j].colorSelect
                color       = plotList[subplot].data[j].color
                lineWidth   = plotList[subplot].data[j].lineWidth
                lineStyle   = plotList[subplot].data[j].lineStyle
                marker      = plotList[subplot].data[j].marker
                markerSize  = plotList[subplot].data[j].markerSize

                # https://matplotlib.org/api/_as_gen/matplotlib.pyplot.plot.html

                # no color selected
                if colorSelect:
                    plt.plot(x, y, label=label, linewidth=lineWidth, lineStyle=lineStyle, marker=marker, markersize=markerSize, color=color,)
                else:
                    plt.plot(x, y, label=label, linewidth=lineWidth, lineStyle=lineStyle, marker=marker, markersize=markerSize)
            
            ### axis
            ## x axis
            # limits
            if not plotList[subplot].xAxis.autoScale:
                plt.xlim( (plotList[subplot].xAxis.lowLimit, plotList[subplot].xAxis.highLimit) )
            # visible
            cur_axes = plt.gca()
            cur_axes.axes.get_xaxis().set_visible(plotList[subplot].xAxis.visibleAxe)
            # labels
            plt.xlabel(plotList[subplot].xAxis.label)
            ## y axis
            if not plotList[subplot].yAxis.autoScale:
                plt.xlim( (plotList[subplot].yAxis.lowLimit, plotList[subplot].yAxis.highLimit) )
            # visible
            cur_axes = plt.gca()
            cur_axes.axes.get_yaxis().set_visible(plotList[subplot].yAxis.visibleAxe)
            # labels
            plt.ylabel(plotList[subplot].yAxis.label)

            # legend for the subplot
            plt.legend(loc="best")

            # plt.axis('tight')
            # plt.axis('off')
            # plt.tight_layout()

        elif type(plotList[subplot].data[0]) is tableData:
            ### matrix data ###
            for j in range(len(plotList[subplot].data)):
                # list of tables to display in one subplot
                # table
                the_table = plt.table(  cellText    = plotList[subplot].data[j].matrix,
                                        colLabels   = plotList[subplot].data[j].collabel,
                                        rowLabels   = plotList[subplot].data[j].rowLabels,
                                        cellColours = plotList[subplot].data[j].cellColors,
                                        cellLoc     = 'center',
                                        loc         = 'center')
                # font size
                the_table.auto_set_font_size(False)
                the_table.set_fontsize(plotList[subplot].data[j].tableFontSize)
                plt.axis('tight')
                plt.axis('off')
                # plt.tight_layout()
                #  move tile (because of col labels)
                ttl = ax.title
                ttl.set_position([0.5, 1.15])
                # legend
                if plotList[subplot].data[j].legendPerso:
                    # custom legend asked
                    plt.legend(handles=plotList[subplot].data[j].legendHandles , loc='best')
                else:
                    plt.legend(loc='best') 
                    
        else:
            exit("FAILED: given type error")

if __name__ == "__main__":

    # canvas args
    myCanvas  = canvas(title="canvas test", fontSize=15)

    # subplot 1
    # x and y axis 
    xAxis = axis(label="ns")
    yAxis = axis(label="mV")
    # data
    tabSize = 10
    yData   = np.random.rand(tabSize)
    xData   = np.arange(tabSize)
    myData  = xyData(xData=xData, yData=yData,    label="random data")
    myData2 = xyData(xData=xData, yData=2*yData,  label="random data * 2")
    subplot1 = subplot(title="subplot title", fontSize="12", xAxis=xAxis, yAxis=yAxis, data=[myData, myData2])

    # subplot 2
    # x and y axis 
    xAxis = axis(label="ns")
    yAxis = axis(label="mV", autoScale=False, visibleAxe=False)
    # data
    tabSize = 100
    yData   = np.random.rand(tabSize)
    xData   = np.arange(tabSize)
    myData  = xyData(xData=xData, yData=yData,    label="random data", colorSelect=True)
    subplot2 = subplot(title="subplot title 2", fontSize="16", xAxis=xAxis, yAxis=yAxis, data=[myData])

    # subplot 3
    # x and y axis 
    xAxis = axis()
    yAxis = axis()
    # data
    col         = 3
    line        = 8
    matrix      = np.random.rand(line, col)
    collabel    = ["my col label " + str(i) for i in range(col)] 
    rowlabel    = ["my row label " + str(i) for i in range(line)]
    cellColors  = [["green"                 for i in range(col)] for j in range(line)]
    myData      = tableData(matrix=matrix, collabel=collabel, rowLabels=rowlabel, cellColors=cellColors)
    subplot3    = subplot(title="subplot title 3", fontSize="12", xAxis=xAxis, yAxis=yAxis, data=[myData])

    # plot
    generic_plot(myCanvas, [subplot1, subplot2, subplot3])

    # show plots
    plt.show()
