# to do:
#  - dataIn: 
#       - read from binary file (Thomas and Laura inputs)
#       - generate data from a pattern, then overlap in % or random
#       - chunk file
#       - add RF noise (RFQ)
#       - add sparks noise
#       - generate fast data
# - to implement
#       - spark detection
#       - MPV and pedestal stability

import time                     # chrono (elapsed time)   
import matplotlib.pyplot as plt # plot 

# my modules
from nblmlib import functions           # my functions for nBLM simulation
from nblmlib import dataIn              # input file for simulation
from nblmlib import dataOut             # save simulation results
from nblmlib import neutronDetection    # pre-processing and detects neutrons
from nblmlib import analysis            # pedestal calculation,  MPV calculation, sparks and saturation detection, stability ...                            
from nblmlib import plotUtils           # plot raw data and specific events

def simu_fpga_algo():

    # simulation duration
    startSimulation = time.time()

    # print config
    functions.display_configuration()

    # check simulation paths
    functions.check_simulation_paths()

    # get data
    rawData, rawDataMask, NB_OF_SAMPLES_ACQUIRED, getDataDuration = dataIn.get_data()
    nbOfIntegration1us = functions.get_number_of_integration(NB_OF_SAMPLES_ACQUIRED)

    # ADC saturation
    saturation1us, saturation, positiveSaturation, negativeSaturation, saturationDectectionDuration = analysis.saturation_detection(rawData, NB_OF_SAMPLES_ACQUIRED, nbOfIntegration1us)

    # pre-processing
    verbose = False
    rawDataCorrected, preProcessingDuration = neutronDetection.pre_processing(rawData, NB_OF_SAMPLES_ACQUIRED, verbose)

    # derivative 
    rawDataCorrectedDerivated, derivativeDuration = analysis.derivative(rawDataCorrected, sampleShift=1)
    # spark detection
    sparkDetected1us, sparkDetected, sparkDetectionDuration = analysis.sparks_detection(rawDataCorrectedDerivated, NB_OF_SAMPLES_ACQUIRED, nbOfIntegration1us)

    # event detection
    verbose = True
    neutronList, badEventList, eventDetectionDuration = neutronDetection.event_detection(rawDataCorrected, NB_OF_SAMPLES_ACQUIRED, verbose)

    # pedestal calculation
    pedestalAdvised, pedestalCalculationDuration = analysis.pedestal_calculation(rawData, NB_OF_SAMPLES_ACQUIRED, neutronList, badEventList)

    # neutron counters
    # combination of neutron shape no pile-up and charge during pile-up
    # (for faster python process, evnts are only neutrons shapes)
    neutronCounter1us, neutronCounterPeak1us, neutronCounterPileUp1us, nbOfPileUp1us, neutronCounter1usDuration = neutronDetection.neutron_counter_1us(neutronList, nbOfIntegration1us)
    # charge only, TOT only....
    neutronCountingCharge1us, neutronCountingTOT1us, neutronCountingAmp1us, neutronCountingRiseTime1us, neutronCounterCheapDuration = neutronDetection.neutron_counter_cheap(neutronList, nbOfIntegration1us)

    # MPV calculation
    # estimation using peak counting
    chargeEstimation_MPV, TOTestimation_MPV, ampEstimation_MPV, riseTimeEstimation_MPV, MPVestimationDuration = analysis.MPV_estimation(neutronList)
    # average neutron shape (! WARNING: very slow function !)
    chargeNeutronPeakAverage, TOTneutronPeakAverage, amplitudeNeutronPeakAverage, riseTimeNeutronPeakAverage, neutronPeakAverageDuration = analysis.neutron_peak_average(
        rawDataCorrected,
        NB_OF_SAMPLES_ACQUIRED,
        neutronList)
    # neutron distribution
    chargeDistributionMPV, TOTDistributionMPV, ampDistributionMPV, riseTimeDistributionMPV, neutronPeakDistributionDuration = analysis.neutron_peak_distribution(
        neutronList, 
        chargeNeutronPeakAverage,
        TOTneutronPeakAverage,
        amplitudeNeutronPeakAverage,
        riseTimeNeutronPeakAverage,
        chargeEstimation_MPV,
        TOTestimation_MPV,
        ampEstimation_MPV,
        riseTimeEstimation_MPV)

    # collecting data for EPICS -> useless
    # TBI

    # pedestal stability
    # TBI

    # MPV stability
    # TBI

    # resetIntegration1us (plot purpose only)
    resetIntegration1us, resetintegrationDuration = functions.get_reset_integration(NB_OF_SAMPLES_ACQUIRED)

    # save data of nblm detection in a log file
    nblmLogsDuration = dataOut.nblm_logs(   nbOfIntegration1us, 
                                            rawDataCorrected, 
                                            NB_OF_SAMPLES_ACQUIRED, 
                                            neutronList, 
                                            neutronCounter1us, 
                                            neutronCounterPeak1us, 
                                            neutronCounterPileUp1us, 
                                            neutronCountingCharge1us, 
                                            neutronCountingTOT1us, 
                                            neutronCountingAmp1us, 
                                            neutronCountingRiseTime1us, 
                                            badEventList)

    # time report: how much time for each function ?
    functions.time_report(  getDataDuration,
                            saturationDectectionDuration, 
                            preProcessingDuration, 
                            derivativeDuration, 
                            sparkDetectionDuration, 
                            eventDetectionDuration, 
                            pedestalCalculationDuration, 
                            neutronCounter1usDuration,
                            neutronCounterCheapDuration,
                            MPVestimationDuration,
                            neutronPeakAverageDuration,
                            neutronPeakDistributionDuration,
                            resetintegrationDuration,
                            nblmLogsDuration)

    print "*** simulation completed in (%f s) ***" %(time.time()-startSimulation)
    print "    - ", NB_OF_SAMPLES_ACQUIRED, "samples ->", functions.sample_to_time(NB_OF_SAMPLES_ACQUIRED)/1000000.0, "ms"
    print "    - number of events:", len(badEventList) + len(neutronList) , "events"
    print "        - number of bad events:\t\t",    len(badEventList) , "bad events"
    print "        - number of neutron events:\t",  len(neutronList) ,  "neutron events\n"

    # plot beam pulses
    plotUtils.overlapingBeamPulses(rawDataCorrected)

    # plotting full window using nblm log file
    plotUtils.plot_nblm_logs()

    # plotting data full window using script data
    plotUtils.plotting_full_window(  rawDataCorrected,
                                    rawDataCorrectedDerivated, 
                                    NB_OF_SAMPLES_ACQUIRED, 
                                    neutronList, 
                                    neutronCounter1us, 
                                    neutronCounterPeak1us, 
                                    neutronCounterPileUp1us, 
                                    neutronCountingCharge1us, 
                                    neutronCountingTOT1us, 
                                    neutronCountingAmp1us, 
                                    neutronCountingRiseTime1us, 
                                    badEventList, 
                                    resetIntegration1us,
                                    sparkDetected,
                                    saturation,
                                    positiveSaturation,
                                    negativeSaturation)

    # plotting scope/debug 10us window with trigger (ex, badEvent, neutron, pile up, sparks ...)
    plotUtils.scope_debug(  rawDataCorrected, 
                            rawDataCorrectedDerivated, 
                            rawDataMask, 
                            NB_OF_SAMPLES_ACQUIRED, 
                            neutronList, 
                            neutronCounter1us, 
                            neutronCounterPeak1us, 
                            neutronCounterPileUp1us, 
                            neutronCountingCharge1us, 
                            neutronCountingTOT1us, 
                            neutronCountingAmp1us, 
                            neutronCountingRiseTime1us, 
                            badEventList, 
                            resetIntegration1us,
                            sparkDetected,
                            saturation,
                            positiveSaturation,
                            negativeSaturation)

    # plot
    plt.show() # useful if 0 triggger (don't plot only scope but also other figures)

if __name__ == "__main__":
    simu_fpga_algo()
