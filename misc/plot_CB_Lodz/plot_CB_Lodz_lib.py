import os                       # check if directory/file exists
import sys                      # shell args 
import matplotlib.pyplot as plt # plots
import numpy as np              # vector operations
import subprocess               # execute bash command: reboot IFC1410 in case of DDR problem
import time                     # elapsed time

# my modules
# m_epics_PATH = "/home/vnadot/mnt/devspace/m-epics-nblm/misc/simulation/"
# m_epics_PATH = "/home/vnadot/dev/m-epics-nblm/misc/simulation/"
m_epics_PATH = "/home/iocuser/devspace/m-epics-nblm/misc/simulation/"
sys.path.insert(0, m_epics_PATH)
import config as GBL            # global variables
from nblmlib import functions   # my functions for nBLM simulation
from nblmlib import dataOut     # save simulation results
from nblmlib import dataIn      # load input file

NUMBER_OF_16BIT_WORD_PER_LINE   = 8 # 128bits / 16bits
IP_IFC1410_JEFF                 = "192.168.1.88"

def plot(rawData, label, xunit, yunit, ylimN, ylimP):
    """ create a figure with input data. A "plt.show()" must be done later"""
    timeAcq = np.linspace(0, 4*len(rawData), len(rawData))
    plt.figure()
    plt.plot(timeAcq, rawData, label=label)
    plt.ylabel(yunit)
    plt.xlabel(xunit)
    plt.ylim( (ylimN, ylimP) )
    plt.legend(loc="best")

def amplitudeUnsignedtToamplitudemV(amplitudeInt):
    """ convert 16bits unsigned to mV:
            - 2^16-1    => 500 mV
            - 2^15      => 0 mV
            - 0         => -500 mV
    """
    amplitude_mV = (amplitudeInt - pow(2, 15)) * 500.0 / pow(2, 15)
    return amplitude_mV

def amplitudemvToAmplitudeUnsigned(amplitudemV):
    """ converts mV to unsigned 16 bits"""
    amplitude_int = (amplitudemV * pow(2, 15) / 500.0) + pow(2, 15)
    return int(amplitude_int)

def getPathFile():
    # path given in args
    if len(sys.argv) > 1:
        # at least one aregument given in input
        pathFile = sys.argv[1]
        print "Path input file:", pathFile
        if os.path.isfile(pathFile) == False:
            exit("Failed: file given in arg doesn't exist")
    else:
        # no arg given in the shell, use defaut location
        pathFile = "/home/iocuser/devspace/m-epics-nblm/misc/plot_CB_Lodz/data_Lodz/output.txt"     # CEA computer
        print "[W] Defaut path input file used:", pathFile
        # pathFile = "/home/vnadot/development/m-epics-nblm/misc/plot_CB_Lodz/data_Lodz/output.txt"  # my computer
        if os.path.isfile(pathFile) == False:
            exit("Failed: file doesn't exist")
    return pathFile

def reboot_IFC1410(ipAddress):
    """ reboot the IFC1410 """
    print "reboot IFC1410 ..."
    cmd = ["ssh", "root@"+ipAddress, "reboot"]
    subprocess.check_output(cmd)

def txt2npy(pathFile, nbOfLines, nbOfRawData, fileNameNumpy):

    # time elapsed
    startConversionNumpy    = time.time()

    # check larger request for number of raw data
    if nbOfRawData > nbOfLines:
        nbOfRawData = nbOfLines

    # cerate numpy array
    rawData = np.arange(nbOfRawData, dtype=float)

    # count line in the file
    lineCounter = 0

    # data loading parameter
    printStatus = nbOfRawData/10 # samples. every 10% of loaded data, i will print status

    # save data to numpy array
    with open(pathFile) as rawDataFile :    # open file
        for line in rawDataFile :           # for each line in the file
            
            # print data loading status every 'printStatus' samples
            if lineCounter % printStatus == 0:
                print lineCounter, "over", nbOfRawData, "\t=>", round(lineCounter/float(nbOfRawData)*100) ,"% after", functions.elapsedTime(startConversionNumpy, GBL.START_TIME)

            # extract data from line
            data = line.split() # data[0]: sample, data[1]: data
            # 16bits to mV
            rawData[lineCounter] = amplitudeUnsignedtToamplitudemV(int(data[1]))
            # print int(data[1]), "\t->", amplitudeUnsignedtToamplitudemV(int(data[1]))

            if lineCounter == nbOfRawData-1:
                print "data have been collected in", time.time() - startConversionNumpy, "sec"

                print "Save data to .npy file ..."
                startSavingFile = time.time()
                dataOut.saveBinaryData(fileNameNumpy, rawData)
                print "... data have been saved in", time.time() - startSavingFile, "sec"

                # stop reading the file
                break

            # line counter
            lineCounter = lineCounter + 1

    return rawData, nbOfRawData

def loadBinaryDataFile(fileNameNumpy):
    return dataIn.loadBinaryData(fileNameNumpy)
