# ------------------------------------------------------------------------------
# -- Company      :   CEA Saclay                                              --
# -- Laboratory   :   IRFU / DIS / LDISC                                      --
# --                                                                          --
# -- Project      :   nBLM, CEA test bench                                    --
# --                                                                          --
# -- Author       :   Victor Nadot - victor.nadot@cea.fr                      --
# --                                                                          --
# -- -----------------------------------------------------------------------  --

import matplotlib.pyplot as plt
import numpy as np
import time                             # chrono (elapsed time)
from matplotlib.widgets import Button   # quit button when plotting

# do not change
PATH_SCRIPT_XPM     = "/export/nfsroots/ifc1410-ess-20170314/home/root/10_TscMon_Scripts/myScripts/"

# which PC is plotting ?
PATH_M_EPICS_NBLM   = "/home/iocuser/devspace/m-epics-nblm/"	# CT, less DDR (if big file, may slow down CT)
# PATH_M_EPICS_NBLM   = "/home/vnadot/mnt/devspace/m-epics-nblm/" # vnadot more DDR memory

import sys
sys.path.insert(0, PATH_M_EPICS_NBLM + "misc/simulation")
import config as GBL                    # global variables

# my modules
from nblmlib import functions           # my functions for nBLM simulation
from nblmlib import dataIn              # input file for simulation
from nblmlib import dataOut             # save simulation results
from nblmlib import neutronDetection    # pre-processing and detects neutrons
from nblmlib import analysis            # pedestal calculation, counts neutons, MPV calculation, sparks and saturation detection, stability ...                            
from nblmlib import plotUtils           # plot raw data and specific events


# global variables
rawDataVector_CHA = np.zeros(0, dtype=np.float)
rawDataVector_CHB = np.zeros(0, dtype=np.float)

# quit and save button
class Index(object):
    def quit(self, event):
        print "Stopped by 'quit' button"
        exit()
    
    def saveData(self, event):
        global rawDataVector_CHB
        date        = time.strftime("%Y_%m_%d_%H_%M_%S")
        fileName    = "data/dataSaved_" + date + "_CHB.npy"
        dataOut.saveBinaryData(fileName, rawDataVector_CHB)
        print len(rawDataVector_CHB), "samples(", len(rawDataVector_CHB)*4, "ns)  saved in", fileName
                             

## conversion
def amplitudeReal2Int (amplitudeReal):
    """ Translate user rawData ([-500;500] mV) into 16 bit signed (C2) word (ADC3111) """
    # check limits
    if amplitudeReal > GBL.SATURATION:
        amplitudeReal = GBL.SATURATION
    if amplitudeReal < - GBL.SATURATION:
        amplitudeReal = - GBL.SATURATION

    if amplitudeReal >= 0:
        amplitudeInt = int(amplitudeReal * (pow(2,15)-1) / GBL.SATURATION)
    else:
        amplitudeInt = int(amplitudeReal * (pow(2,15)) / GBL.SATURATION) + pow(2,16)

    return amplitudeInt & 0xFFFF

def chargeReal2Int (chargeReal):
    """ Translate user rawData Q ([-500*250;500*250] mV) into 23 bit signed (C2) word """
    # check limits
    # if chargeReal > GBL.SATURATION*250:
    #     chargeReal = GBL.SATURATION*250
    # if chargeReal < - GBL.SATURATION*250:
    #     chargeReal = - GBL.SATURATION*250
    # if chargeReal >= 0:
    #     chargeInt = int(chargeReal * (pow(2,15)-1) / GBL.SATURATION)
    # else:
    #     chargeInt = int(chargeReal * (pow(2,15)-1) / GBL.SATURATION) + pow(2,32)

    chargeInt = chargeReal * (pow(2,16)-1)/1000

    # MAY BE A BUG: should return a signed or unsigned ?
    return chargeInt & 0xFFFFFFFF

def amplitudeInt2Real (amplituteInt):
    """ Translate 16 bit signed (C2) word (ADC3111) into a user rawData ([-500;500] mV)"""
    # check limits
    if amplituteInt > (pow(2,15)-1):
        amplituteInt = (pow(2,15)-1)
        print "something is strange !"
    if amplituteInt < - (pow(2,15)):
        amplituteInt = - (pow(2,15))
        print "something is strange :"

    if amplituteInt >= 0:
        amplitudeReal = (amplituteInt * GBL.SATURATION) / float(pow(2,15)-1)
    else:
        # amplitudeReal = ( (amplituteInt-pow(2,16)) * GBL.SATURATION) / float(pow(2,15)-1)
        amplitudeReal = (amplituteInt * GBL.SATURATION) / float(pow(2,15)-1)
    return amplitudeReal

def chargeInt2Real(chargeInt):
    """ Translate an integer charge word into user rawData Q """
    # if chargeInt >= 0:
    #     chargeReal = (chargeInt - pow(2,24))*GBL.SATURATION / float((pow(2,15)-1))
    #     # chargeInt = int(chargeReal * (pow(2,15)-1) / GBL.SATURATION)
    # else:
    #     chargeReal = chargeInt*GBL.SATURATION / float((pow(2,15)-1))
    #     # chargeInt = int(chargeReal * (pow(2,15)-1) / GBL.SATURATION) + pow(2,24)

    chargeReal = chargeInt * (1000.0/(pow(2,16)-1))

    return chargeReal

def toSigned16(n):
    """ convert a 16 bits unsigned word into a 16 bits wod signed """
    if type(n) == str:
        # n is in exa, string
        n = int(n,16) & 0xffff
    elif type(n) == int or type(n) == np.int16 or type(n) == np.uint64:
        # n is integer (unsigned 16 bits)
        n = n & 0xffff
    return n | (-(n & 0x8000))

def toSigned32(n):
    """ convert a 32 bits unsigned word into a 32 bits wod signed """
    if type(n) == str:
        # n is in exa, string
        n = int(n,16) & 0xFFFFFFFF
    elif type(n) == int or type(n) == np.int16 or type(n) == np.uint64:
        # n is integer (unsigned 16 bits)
        n = n & 0xFFFFFFFF
    return n | (-(n & 0x80000000))

def toSigned64(n):
    """ convert a 64 bits unsigned word into a 64 bits wod signed """
    if type(n) == str:
        # n is in exa, string
        n = int(n,16) & 0xFFFFFFFFFFFFFFFF
    elif type(n) == int or type(n) == np.int16 or type(n) == np.uint64:
        # n is integer (unsigned 16 bits)
        n = n & 0xFFFFFFFFFFFFFFFF
    return n | (-(n & 0x8000000000000000))
## conversion


## word/bit management
def swap(myString):
    """ swap even strings (size doesn't matter)"""

    # save string
    tmp = myString

    # check even
    if len(myString) % 2 != 0:
        exit("FAILED: len(myString) needs to be even to be swapped")

    # swap
    myString = []
    for i in range(len(tmp)):
        myString.append(tmp[len(tmp)-1 - i])

    # write in myStringOut
    myStringOut = ""
    for i in range(len(myString)):
        myStringOut = myStringOut + myString[i]

    return myStringOut

def fillWith0(data):
    """ (quick made, not generic), return the input number in hexa to a formateed 6 length caracter string"""
    # if len(data) == 6:
    #     return data
    # elif len(data) == 5:
    #     return data[0] + data[1] + '0' + data[2] + data[3] + data[4]
    # elif len(data) == 4:
    #     return data[0] + data[1] + '0' + '0' + data[2] + data[3]
    # elif len(data) == 3:
    #     return data[0] + data[1] + '0' + '0' + '0'+ data[2]
    # else:
    #     print data, data[2:len(data)]
    #     print '0' + 'x' + '0' + '0' + '0'+ '0 VS', "0x" + str('{:04X}'.format( int(data[2:len(data)],16) ) )
    #     return '0' + 'x' + '0' + '0' + '0'+ '0'
    return "0x" + str('{:04X}'.format( int(data[2:len(data)],16) ) )

def get_bitVector(bit, TMEM_data, lineNumber, vector):
    # TMEM_data[lineNumber] = ['a6', '0d', '00', '00', 'e1', 'f0', '08', '00'] # debug value
    # print "\nTMEM_data[lineNumber] MSB to LSB:", TMEM_data[lineNumber]
    # print "bit", bit
    shift       = bit % 8
    byteSelect  = 7 - bit/8
    vector[lineNumber]    = ((int(TMEM_data[lineNumber][byteSelect],16) ) >> shift ) & 0x1
    # print "select octet:", byteSelect
    # print "data hex:", TMEM_data[lineNumber][byteSelect], "int:", int(TMEM_data[lineNumber][byteSelect],16)
    # print "shit", ((int(TMEM_data[lineNumber][byteSelect],16) ) >> shift )
    # print "result with mask", vector[lineNumber]
    ## word/bit management

def generateTscMonScriptToFillFileReader ():
    """ genererate a tscmon script (.xpm) to fill file reader DPRAM (4_11)"""

    # 4_11 start address
    FILE_READER_ADDRESS = "0x1B0000"

    # FILE selection
    GBL.RAW_DATA_FILE = "generatedData.txt"

    # get data
    rawData, rawDataMask, NB_OF_SAMPLES_ACQUIRED = dataIn.get_data()
    # limit to file reader size
    FILE_READER_NB_MAX_16BITS_WORDS = 4096*4 # DPRAM: 64bits*4096, 64bits=>4 words of 16 bits
    if NB_OF_SAMPLES_ACQUIRED > FILE_READER_NB_MAX_16BITS_WORDS:
        NB_OF_SAMPLES_ACQUIRED = FILE_READER_NB_MAX_16BITS_WORDS
        rawData     = rawData[0:FILE_READER_NB_MAX_16BITS_WORDS]
        rawDataMask = rawDataMask[0:FILE_READER_NB_MAX_16BITS_WORDS]
        print "Warning: signal has been cutted, length > 4096*4"
    print "NB_OF_SAMPLES_ACQUIRED:", NB_OF_SAMPLES_ACQUIRED
    plt.step([i for i in range(NB_OF_SAMPLES_ACQUIRED)], rawData, label='data given to file reader')
    # plt.show()

    file = open(PATH_SCRIPT_XPM + "file_reader_fill.xpm", "w")
    end = int(NB_OF_SAMPLES_ACQUIRED/4)*4
    for i in range(0,end,4):
        # writes 2 words of 32

        # real to int
        data = [j for j in range(4)]
        data[0] = amplitudeReal2Int(rawData[i])
        data[1] = amplitudeReal2Int(rawData[i+1])
        data[2] = amplitudeReal2Int(rawData[i+2])
        data[3] = amplitudeReal2Int(rawData[i+3])

        # int to hexa
        data[0] = hex(data[0])
        data[1] = hex(data[1])
        data[2] = hex(data[2])
        data[3] = hex(data[3])

        # print  hex(amplitudeReal2Int(rawData[i])), len( hex(amplitudeReal2Int(rawData[i])))
        if len( hex(amplitudeReal2Int(rawData[i])) ) > 6:
            print hex(amplitudeReal2Int(rawData[i])), rawData[i], "mV\n"
            exit()
        # format: length of 6 : 0XAAAA
        data[0] = fillWith0(data[0])
        data[1] = fillWith0(data[1])
        data[2] = fillWith0(data[2])
        data[3] = fillWith0(data[3])

        # debug
        # data[0] = "0x0123"
        # data[1] = "0xDEAD"
        # data[2] = "0x4567"
        # data[3] = "0xFACE"

        # concatenation and swap
        myData0 = data[1][4:6] + data[1][2:4] + data[0][4:6] + data[0][2:4]
        myData1 = data[3][4:6] + data[3][2:4] + data[2][4:6] + data[2][2:4]

        # write script
        # word 1 (LSB)
        addressTMEM = int(FILE_READER_ADDRESS,16) + int(2*i)
        file.write("pu1.w " + hex(addressTMEM) + " " + myData1 + "\n")
        # word 2 (MSB)
        addressTMEM = int(FILE_READER_ADDRESS,16) + int(2*i+4)
        file.write("pu1.w " + hex(addressTMEM) + " " + myData0 + "\n")
    
    file.write("$exit\n")
    file.close

def generateNeutronDetectionConfiguration(chargeMPV, pedestal, eventDetectionThreshold, neutronAmpMin, nbOfDataToComputeMPV, nbOfDataToComputePedestal, neutronTOTmin, TOTpileUpstart, TOTpileUpLimitReached):
    """ NOT IMPLMENTED YET generate the configuration for the neutron detection """

    ADDR_CHARGE_MPV                     = "1A0004"
    ADDR_PEDESTAL                       = "1A0000"
    ADDR_EVENT_DETECTION_TH             = "1A000C"
    ADDR_NEUTRON_AMP_MIN                = "1A0008"
    ADDR_NB_OF_DATA_TO_COMPUTE_MPV      = "1A0014"
    ADDR_NB_OF_DATA_TO_COMPUTE_PEDESTAL = "1A0010"
    ADDR_TOT_MIN_NEUTRON                = "1A001C"
    ADDR_TOT_PILE_UP_START              = "1A0018"
    ADDR_TOT_LIMIT                      = "1A0024"

    # chargeMPV
    dataFormatted = '{:08X}'.format(chargeReal2Int(chargeMPV))
    # swap
    dataFormattedTMEM = dataFormatted[6:8] + dataFormatted[4:6] + dataFormatted[2:4] + dataFormatted[0:2]
    chargeMPVstring = "pu1.w " + ADDR_CHARGE_MPV + " " + dataFormattedTMEM + "   # chargeMPV = 0x" + dataFormatted + " = " + str(chargeMPV) + " Q\n"

    # pedestal
    dataFormatted = '{:08X}'.format(amplitudeReal2Int(pedestal))
    # swap
    dataFormattedTMEM = dataFormatted[6:8] + dataFormatted[4:6] + dataFormatted[2:4] + dataFormatted[0:2]
    pedestalString = "pu1.w " + ADDR_PEDESTAL + " " + dataFormattedTMEM + "   # pedestal = 0x" + dataFormatted + " = " + str(pedestal) + " mV\n"

    # eventDetectionThreshold
    dataFormatted = '{:08X}'.format(amplitudeReal2Int(eventDetectionThreshold))
    # swap
    dataFormattedTMEM = dataFormatted[6:8] + dataFormatted[4:6] + dataFormatted[2:4] + dataFormatted[0:2]
    eventDetectionThresholdString = "pu1.w " + ADDR_EVENT_DETECTION_TH + " " + dataFormattedTMEM + "   # eventDetectionThreshold = 0x" + dataFormatted + " = " + str(eventDetectionThreshold) + " mV\n"

    # neutronAmpMin
    dataFormatted = '{:08X}'.format(amplitudeReal2Int(neutronAmpMin))
    # swap
    dataFormattedTMEM = dataFormatted[6:8] + dataFormatted[4:6] + dataFormatted[2:4] + dataFormatted[0:2]
    neutronAmpMinString = "pu1.w " + ADDR_NEUTRON_AMP_MIN + " " + dataFormattedTMEM + "   # neutronAmpMin = 0x" + dataFormatted + " = " + str(neutronAmpMin) + " mV\n"

    # nbOfDataToComputeMPV
    dataFormatted = '{:08X}'.format(nbOfDataToComputeMPV)
    # swap
    dataFormattedTMEM = dataFormatted[6:8] + dataFormatted[4:6] + dataFormatted[2:4] + dataFormatted[0:2]
    nbOfDataToComputeMPVstring = "pu1.w " + ADDR_NB_OF_DATA_TO_COMPUTE_MPV + " " + dataFormattedTMEM + "   # nbOfDataToComputeMPV = 0x" + dataFormatted + " = " + str(nbOfDataToComputeMPV) + "\n"
    
    # nbOfDataToComputePedestal
    dataFormatted = '{:08X}'.format(nbOfDataToComputePedestal)
    # swap
    dataFormattedTMEM = dataFormatted[6:8] + dataFormatted[4:6] + dataFormatted[2:4] + dataFormatted[0:2]
    nbOfDataToComputePedestalString = "pu1.w " + ADDR_NB_OF_DATA_TO_COMPUTE_PEDESTAL + " " + dataFormattedTMEM + "   # nbOfDataToComputePedestal = 0x" + dataFormatted + " = " + str(nbOfDataToComputePedestal) + "\n"
    
    # neutronTOTmin
    dataFormatted = '{:08X}'.format(neutronTOTmin)
    # swap
    dataFormattedTMEM = dataFormatted[6:8] + dataFormatted[4:6] + dataFormatted[2:4] + dataFormatted[0:2]
    neutronTOTminString = "pu1.w " + ADDR_TOT_MIN_NEUTRON + " " + dataFormattedTMEM + "   # neutronTOTmin = 0x" + dataFormatted + " = " + str(neutronTOTmin) + " samples\n"

    # TOTpileUpstart
    dataFormatted = '{:08X}'.format(TOTpileUpstart)
    # swap
    dataFormattedTMEM = dataFormatted[6:8] + dataFormatted[4:6] + dataFormatted[2:4] + dataFormatted[0:2]
    TOTpileUpstartString = "pu1.w " + ADDR_TOT_PILE_UP_START + " " + dataFormattedTMEM + "   # TOTpileUpstart = 0x" + dataFormatted + " = " + str(TOTpileUpstart) + " samples\n"

    # TOTpileUpLimitReached
    dataFormatted = '{:08X}'.format(TOTpileUpLimitReached)
    # swap
    dataFormattedTMEM = dataFormatted[6:8] + dataFormatted[4:6] + dataFormatted[2:4] + dataFormatted[0:2]
    TOTpileUpLimitReachedString = "pu1.w " + ADDR_TOT_LIMIT + " " + dataFormattedTMEM + "   # TOTpileUpLimitReached = 0x" + dataFormatted + " = " + str(TOTpileUpLimitReached) + " samples\n"

    file = open(PATH_SCRIPT_XPM + "/neutronDetectionConfigurationGenerated_set.xpm", "w")
    file.write("# **** This script has been generated from read_FPGA_data_acquisitionChainTester.py ****\n\n")
    file.write("# set chargeMPV and pedestal\n")
    file.write(chargeMPVstring)
    file.write(pedestalString)
    
    file.write("\n# set eventDetectionThreshold and neutronAmpMin\n")
    file.write(eventDetectionThresholdString)
    file.write(neutronAmpMinString)

    file.write("\n# set nbOfDataToComputeMPV and nbOfDataToCompute\n")
    file.write(nbOfDataToComputeMPVstring)
    file.write(nbOfDataToComputePedestalString)

    file.write("\n# set neutronTOTmin and TOTpileUpstart\n")
    file.write(neutronTOTminString)
    file.write(TOTpileUpstartString)

    file.write("\n# set TOTpileUpLimitReached\n")
    file.write(TOTpileUpLimitReachedString)
    
    file.write("\n# quit TscMon\n")
    file.write("$exit\n")
    file.close

    return 0

def generateScopeConfiguration(scopeTriggerSelection, scopeTriggerDelay, scopeTriggerValue):
    """ generate the configuration for the scope """

    ADDR_TRIGGER_SELECTION  = "1C0004"
    ADDR_TRIGGER_DELAY      = "1C0000"
    ADDR_TRIGGER_VALUE      = "1C000C"
    # ADDR_TSPARE           = "1C0008"

    # set triggerSelection and triggerDelay
    # pu1.w 1C0004 01000000   # triggerSelection  = x"00000001"
    # pu1.w 1C0000 00000000   # triggerDelay      = x"00000000"
    # # set triggerValue
    # # pu1.w 1C0008 00000000   # triggerValue    = x"00000000"
    # # pu1.w 1C000C 00000000
    # pu1.w 1C000C 00000000   # triggerValue    = x"00000000"
    # pu1.w 1C0008 00000000

    # triggerSelection
    dataFormatted = '{:08X}'.format(scopeTriggerSelection)
    # swap
    dataFormatted = dataFormatted[6:8] + dataFormatted[4:6] + dataFormatted[2:4] + dataFormatted[0:2]
    scopeTriggerSelectionString = "pu1.w " + ADDR_TRIGGER_SELECTION + " " + dataFormatted + "   # triggerSelection = 0x" + dataFormatted + "\n"

    # triggerDelay
    dataFormatted = '{:08X}'.format(scopeTriggerDelay)
    # swap
    dataFormatted = dataFormatted[6:8] + dataFormatted[4:6] + dataFormatted[2:4] + dataFormatted[0:2]
    scopeTriggerDelayString = "pu1.w " + ADDR_TRIGGER_DELAY + " " + dataFormatted + "   # triggerDelay = 0x" + dataFormatted + "\n"

    # triggerValue
    dataFormatted = '{:08X}'.format(scopeTriggerValue)
    # swap
    dataFormatted = dataFormatted[6:8] + dataFormatted[4:6] + dataFormatted[2:4] + dataFormatted[0:2]
    scopeTriggerValueString = "pu1.w " + ADDR_TRIGGER_VALUE + " " + dataFormatted + "   # triggerValue = 0x" + dataFormatted + "\n"

    file = open(PATH_SCRIPT_XPM + "scopeConfigurationGenerated_set.xpm", "w")
    file.write("# **** This script has been generated from read_FPGA_data_acquisitionChainTester.py ****\n\n")
    file.write("# set triggerSelection and triggerDelay\n")
    file.write(scopeTriggerSelectionString)
    file.write(scopeTriggerDelayString)

    file.write("\n# set triggerValue\n")
    file.write(scopeTriggerValueString)

    file.write("\n# quit TscMon\n")
    file.write("$exit\n")
    file.close

    return 0

def readTCSRregister(fileName, wordLine=6):
    """ read a TCSR word from a file at a specific line"""

    # open file and get data
    TCSRword    = 0
    lineCounter = 0
    with open(fileName) as rawDataFile :
        for line in rawDataFile : # for each line in the file
            if lineCounter == wordLine:
                charStart   = 11
                charStop    = 19
                # print line, line[charStart : charStop], int(line[charStart : charStop], base=16)
                TCSRword    = int(line[charStart : charStop], base=16)
            else:
                pass # no data on the line

            # new line counted
            lineCounter = lineCounter + 1

    return TCSRword

def shapping64bitsMemory(fileName, nbOf64BitWords):
    """ read result of dux.w command and put data in 2Dmaxtrix with swaping:
        ex (only one line here):
        input(hexa): 2 words of 64 bits ont he same line => 0x00100000 : 01234567 89abcdef 02468ace 13579bdf  ................
        output: 2 words of 64 bits (decimal) , MSB..LSB
            [   ['ef', 'cd', 'ab', '89', '67', '45', '23', '01'] 
                ['df', '9b', '57', '13', 'ce', '8a', '46', '02'] ]

    """
    # elapsed time
    startFunction = time.time()
    print "start shapping64bitsMemory() ..."

    # file name
    print "file used:", fileName
    # number of line in file
    num_lines = functions.getNumberOfLines(fileName)

    # number of data lines
    lineStart = 5
    lineStop  = min( lineStart+(nbOf64BitWords/2)*2, (num_lines-1) ) # / 2 * 2 is to get a even number (2 words per line), # -1 because last line is not a data line
    if lineStop <= lineStart:
        print "FAILED: please check your data file", fileName
        exit()
    
	# number of Bytes per line
    numberOfBytePerLine = 128/8 # 128 bits / 8bits
    
    # number of raw data
    nbOfRawData = 2 * (lineStop - lineStart) # 2 ADC word per line


    # create output matrix
    TMEM_data = np.zeros( (nbOfRawData, 8), dtype=int)

    # check number of 64 bits words
    if nbOf64BitWords > len(TMEM_data):
        print "[W]: your are asking more 64 bits word than there are in the file: asked =", nbOf64BitWords, "; in the file =", len(TMEM_data)

    lineCounter = 0
    with open(fileName) as rawDataFile :
        for line in rawDataFile : # for each line in the file
            # debug
            # line = "0x00100000 : 01234567 89abcdef 02468ace 13579bdf  ................ "
            if lineCounter >= lineStop:
                # finished
                print "... end shapping64bitsMemory():", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"
                return TMEM_data    
            elif lineCounter < lineStart:
                pass # no data line
            else:
                # check file format
                if len(line) != 67:
                    print "line number:", lineCounter
                    print "This is the wrong line:", line[0:len(line)-1] # to avoid \n
                    print "len(line) should be 67, not ", len(line)
                    print "[W]: line skipped. Line has not the right format. Sould be '0x00110000 : 44443333 22221111 788d0700 00000000  DD33""..x.......'"

                else:
                    # split in 16bits word                                                                                                                                       
                    # #0x00100000 : bfffbfff bfffbfff bfffbfff bfffbfff  ................
                    lineSplittedInByte = [ 
                                            ## first word of 64 bits
                                            (line[13:15]), (line[15:17]), (line[17:19]), (line[19:21]),  
                                            # line[21] is " " (space character)  
                                            (line[22:24]), (line[24:26]), (line[26:28]), (line[28:30]),  
                                            # line[30] is " " (space character)        
                                            ## second word of 64 bits
                                            (line[31:33]), (line[33:35]), (line[35:37]), (line[37:39]),  
                                            # line[39] is " " (space character)
                                            (line[40:42]), (line[42:44]), (line[44:46]), (line[46:48])  
                                        ]
                    # print "lineSplittedInByte", lineSplittedInByte
                    # print "2 words of 64bits (LSB..MSB and LSB..MSB, split in byte:):\n", lineSplittedInByte

                    # store words in maxtrix from MSB to LSB
                    # reversed fct(does not return a list) reverse a list: list(reversed([ 4, 8, 9 ]) ) =[9, 8, 4]
                    # TMEM_data.append(list(reversed(lineSplittedInByte[0                           : len(lineSplittedInByte)/2 ])))
                    # TMEM_data.append(list(reversed(lineSplittedInByte[len(lineSplittedInByte)/2   : len(lineSplittedInByte)   ])))
                    for j in range(numberOfBytePerLine):
                        if j < numberOfBytePerLine/2:
                            # 64 bit word number 1
                            row     = (lineCounter-lineStart)*2
                            column  = (numberOfBytePerLine/2-1) - j
                            TMEM_data[row][column] = int(lineSplittedInByte[j], 16)
                        else:
                            # even
                            # 64 bit word number 2
                            row     = ((lineCounter-lineStart)*2)+1
                            column  = (numberOfBytePerLine/2-1) - (j - (numberOfBytePerLine/2))
                            TMEM_data[row][column] = int(lineSplittedInByte[j], 16)
                                
                    # print lineSplittedInByte
                    # print [hex(TMEM_data[0][i]) for i in range(8)]
                    # print [hex(TMEM_data[1][i]) for i in range(8)]
                    # exit()

            lineCounter = lineCounter + 1

    # elasped time
    print "... end shapping64bitsMemory():", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"
    return TMEM_data

def readPulseProcessing (fileName, nbOf64BitWords):
    print "****** Pulse processing ***********"

    # fetch TMEM_data of file reader: list of 64bit words from LSB to MSB
    TMEM_data = shapping64bitsMemory(fileName, nbOf64BitWords)

    # matrix size
    nbOfRow = len(TMEM_data)
    nbOfColumn = len(TMEM_data[0])

    # matrix 64xn to vector 16x4n: 8 bytes to 2bytes
    offset = 0

    # nBOf1us
    index = 0 + offset
    nBOf1us = ( (int(TMEM_data[index][4],16)<<(3*8)) +
                (int(TMEM_data[index][5],16)<<(2*8)) +
                (int(TMEM_data[index][6],16)<<(1*8)) + 
                (int(TMEM_data[index][7],16)<<(0*8)) )
    value = "0x" + TMEM_data[index][7] + TMEM_data[index][6] + TMEM_data[index][5] + TMEM_data[index][4]
    print "Time since last CPU read\t:",nBOf1us, " us\t", value

    # neutroncharge
    index       = 1 + offset
    myString    = "Neutron charge:"
    unit        = "Q"
    pulseProcessingDecodeCharge(TMEM_data, myString, index ,unit , nBOf1us)

    # neutronCounter
    index       = 5 + offset
    myString    = "Neutron counter:"
    unit        = "neutrons"
    pulseProcessingDecodeNeutronCounter(TMEM_data, myString, index ,unit , nBOf1us)

    # neutronPeakCounter
    index       = 9 + offset
    myString    = "Neutron peak counter:"
    unit        = "peak(s)"
    pulseProcessingDecode(TMEM_data, myString, index ,unit , nBOf1us)

    # neutronPileUpCounter
    index       = 13 + offset
    myString    = "Neutron pile-up counter:"
    unit        = "pile-up"
    pulseProcessingDecode(TMEM_data, myString, index ,unit , nBOf1us)

    # nbTOTlimitReached
    index       = 17 + offset
    myString    = "Nb of TOT limit reached:"
    unit        = ""
    pulseProcessingDecode(TMEM_data, myString, index ,unit , nBOf1us)

    # nbSaturation
    index       = 21 + offset
    myString    = "Number of saturation(s):"
    unit        = "saturation(s)"
    pulseProcessingDecode(TMEM_data, myString, index ,unit , nBOf1us)

    # nbPositiveSaturation
    index       = 25 + offset
    myString    = "Number of negative saturation(s):"
    unit        = "saturation(s)"
    pulseProcessingDecode(TMEM_data, myString, index ,unit , nBOf1us)

    # nbNegativeSaturation
    index       = 29 + offset
    myString    = "Number of positive saturation(s):"
    unit        = "saturation(s)"
    pulseProcessingDecode(TMEM_data, myString, index ,unit , nBOf1us)

    # nbSparksDetected
    index       = 33 + offset
    myString    = "Neutron of sparks detected:"
    unit        = "spark(s)"
    pulseProcessingDecode(TMEM_data, myString, index ,unit , nBOf1us)
            

    print "****** Pulse processing ***********"

    return 0

def pulseProcessingDecode(TMEM_data, myString, index ,unit , nBOf1us):

    print myString

    # min: 1st word of 64 bits
    min = ( (int(TMEM_data[index+0][4],16)<<(3*8)) +
            (int(TMEM_data[index+0][5],16)<<(2*8)) +
            (int(TMEM_data[index+0][6],16)<<(1*8)) + 
            (int(TMEM_data[index+0][7],16)<<(0*8)) )
    value = "0x" + TMEM_data[index+0][7] + TMEM_data[index+0][6] + TMEM_data[index+0][5] + TMEM_data[index+0][4]
    print "\t - min :", min, unit, "\t", value

    # max: 2nd word of 64 bits
    max = ( (int(TMEM_data[index+1][4],16)<<(3*8)) +
            (int(TMEM_data[index+1][5],16)<<(2*8)) +
            (int(TMEM_data[index+1][6],16)<<(1*8)) + 
            (int(TMEM_data[index+1][7],16)<<(0*8)) )
    value = "0x" + TMEM_data[index+1][7] + TMEM_data[index+1][6] + TMEM_data[index+1][5] + TMEM_data[index+1][4]
    print "\t - max :", max, unit, "\t", value

    # sum: 3th word of 64 bits
    sum = ( (int(TMEM_data[index+2][0],16)<<(7*8)) +
            (int(TMEM_data[index+2][1],16)<<(6*8)) +
            (int(TMEM_data[index+2][2],16)<<(5*8)) +
            (int(TMEM_data[index+2][3],16)<<(4*8)) +
            (int(TMEM_data[index+2][4],16)<<(3*8)) +
            (int(TMEM_data[index+2][5],16)<<(2*8)) +
            (int(TMEM_data[index+2][6],16)<<(1*8)) + 
            (int(TMEM_data[index+2][7],16)<<(0*8)) )
    value = "0x" + TMEM_data[index+2][7] + TMEM_data[index+2][6] + TMEM_data[index+2][5] + TMEM_data[index+2][4] + TMEM_data[index+2][3] + TMEM_data[index+2][2] + TMEM_data[index+2][1] + TMEM_data[index+2][0]
    print "\t - sum :", sum, unit, "\t", value

    # variance: 4th word of 64 bits
    variance = (    (int(TMEM_data[index+3][0],16)<<(7*8)) +
                    (int(TMEM_data[index+3][1],16)<<(6*8)) +
                    (int(TMEM_data[index+3][2],16)<<(5*8)) +
                    (int(TMEM_data[index+3][3],16)<<(4*8)) +
                    (int(TMEM_data[index+3][4],16)<<(3*8)) +
                    (int(TMEM_data[index+3][5],16)<<(2*8)) +
                    (int(TMEM_data[index+3][6],16)<<(1*8)) + 
                    (int(TMEM_data[index+3][7],16)<<(0*8)) )
    value = "0x" + TMEM_data[index+3][7] + TMEM_data[index+3][6] + TMEM_data[index+3][5] + TMEM_data[index+3][4] + TMEM_data[index+3][3] + TMEM_data[index+3][2] + TMEM_data[index+3][1] + TMEM_data[index+3][0]
    print "\t - variance (NOT IMPLMENTED) :", variance, "\t", value

    # average
    print "\t\t -> Average between 2 CPU read =", float(sum)/nBOf1us, "/ us"

def pulseProcessingDecodeCharge(TMEM_data, myString, index ,unit , nBOf1us):

    print myString

    # min: 1st word of 64 bits
    min = chargeInt2Real(toSigned32(    ((int(TMEM_data[index+0][4],16)<<(3*8)) +
                                        (int(TMEM_data[index+0][5],16)<<(2*8)) +
                                        (int(TMEM_data[index+0][6],16)<<(1*8)) + 
                                        (int(TMEM_data[index+0][7],16)<<(0*8)) )))
    temp = ((int(TMEM_data[index+0][4],16)<<(3*8)) + (int(TMEM_data[index+0][5],16)<<(2*8)) + (int(TMEM_data[index+0][6],16)<<(1*8)) + (int(TMEM_data[index+0][7],16)<<(0*8)) )                  
    value = "0x" + TMEM_data[index+0][7] + TMEM_data[index+0][6] + TMEM_data[index+0][5] + TMEM_data[index+0][4]
    print "\t - min :", min, unit, "\t", value

    # max: 2nd word of 64 bits
    max = chargeInt2Real(toSigned32(    ((int(TMEM_data[index+1][4],16)<<(3*8)) +
                                        (int(TMEM_data[index+1][5],16)<<(2*8)) +
                                        (int(TMEM_data[index+1][6],16)<<(1*8)) + 
                                        (int(TMEM_data[index+1][7],16)<<(0*8)) )))
    value = "0x" + TMEM_data[index+1][7] + TMEM_data[index+1][6] + TMEM_data[index+1][5] + TMEM_data[index+1][4]
    print "\t - max :", max, unit, "\t", value

    # sum: 3th word of 64 bits
    sum = chargeInt2Real(toSigned64(    ((int(TMEM_data[index+2][0],16)<<(7*8)) +
                                        (int(TMEM_data[index+2][1],16)<<(6*8)) +
                                        (int(TMEM_data[index+2][2],16)<<(5*8)) +
                                        (int(TMEM_data[index+2][3],16)<<(4*8)) +
                                        (int(TMEM_data[index+2][4],16)<<(3*8)) +
                                        (int(TMEM_data[index+2][5],16)<<(2*8)) +
                                        (int(TMEM_data[index+2][6],16)<<(1*8)) + 
                                        (int(TMEM_data[index+2][7],16)<<(0*8)) )))
    value = "0x" + TMEM_data[index+2][7] + TMEM_data[index+2][6] + TMEM_data[index+2][5] + TMEM_data[index+2][4] + TMEM_data[index+2][3] + TMEM_data[index+2][2] + TMEM_data[index+2][1] + TMEM_data[index+2][0]
    print "\t - sum :", sum, unit, "\t", value
    # temp = ((int(TMEM_data[index+2][0],16)<<(7*8)) +
    #         (int(TMEM_data[index+2][1],16)<<(6*8)) +
    #         (int(TMEM_data[index+2][2],16)<<(5*8)) +
    #         (int(TMEM_data[index+2][3],16)<<(4*8)) +
    #         (int(TMEM_data[index+2][4],16)<<(3*8)) +
    #         (int(TMEM_data[index+2][5],16)<<(2*8)) +
    #         (int(TMEM_data[index+2][6],16)<<(1*8)) + 
    #         (int(TMEM_data[index+2][7],16)<<(0*8)) )
    # print "temp", temp
    # print "(toSigned64(temp)", (toSigned64(temp))
    # print "chargeInt2Real(toSigned64(temp)", chargeInt2Real(toSigned64(temp))

    # variance: 4th word of 64 bits
    variance = chargeInt2Real(toSigned64(   ((int(TMEM_data[index+3][0],16)<<(7*8)) +
                                            (int(TMEM_data[index+3][1],16)<<(6*8)) +
                                            (int(TMEM_data[index+3][2],16)<<(5*8)) +
                                            (int(TMEM_data[index+3][3],16)<<(4*8)) +
                                            (int(TMEM_data[index+3][4],16)<<(3*8)) +
                                            (int(TMEM_data[index+3][5],16)<<(2*8)) +
                                            (int(TMEM_data[index+3][6],16)<<(1*8)) + 
                                            (int(TMEM_data[index+3][7],16)<<(0*8)) )))
    value = "0x" + TMEM_data[index+3][7] + TMEM_data[index+3][6] + TMEM_data[index+3][5] + TMEM_data[index+3][4] + TMEM_data[index+3][3] + TMEM_data[index+3][2] + TMEM_data[index+3][1] + TMEM_data[index+3][0]
    print "\t - variance (NOT IMPLMENTED) :", variance, "\t", value

    # average
    print "\t\t -> Average between 2 CPU read =", float(sum)/nBOf1us, "/ us"

def pulseProcessingDecodeNeutronCounter(TMEM_data, myString, index ,unit , nBOf1us):

    # neutronCounter1us : (31 downto 6): quotient, (5 downto 0) : frac
    FRAC_LENGTH = 6 # 5 downto 0

    print myString

    # min: 1st word of 64 bits
    min = ( (int(TMEM_data[index+0][4],16)<<(3*8)) +
            (int(TMEM_data[index+0][5],16)<<(2*8)) +
            (int(TMEM_data[index+0][6],16)<<(1*8)) + 
            (int(TMEM_data[index+0][7],16)<<(0*8)) )
    quotient    = int(min) >> FRAC_LENGTH # 31 downto 6
    fracStep    = 1.0/pow(2,FRAC_LENGTH) # 0.015625 neutron
    fracMask    = pow(2,FRAC_LENGTH)-1 # 0x3F
    frac        = (int(min) & fracMask) * fracStep
    min = quotient + frac
    value = "0x" + TMEM_data[index+0][7] + TMEM_data[index+0][6] + TMEM_data[index+0][5] + TMEM_data[index+0][4]
    print "\t - min :", min, unit, "\t", value

    # max: 2nd word of 64 bits
    max = ( (int(TMEM_data[index+1][4],16)<<(3*8)) +
            (int(TMEM_data[index+1][5],16)<<(2*8)) +
            (int(TMEM_data[index+1][6],16)<<(1*8)) + 
            (int(TMEM_data[index+1][7],16)<<(0*8)) )
    quotient    = int(max) >> FRAC_LENGTH # 31 downto 6
    fracStep    = 1.0/pow(2,FRAC_LENGTH) # 0.015625 neutron
    fracMask    = pow(2,FRAC_LENGTH)-1 # 0x3F
    frac        = (int(max) & fracMask) * fracStep
    max = quotient + frac
    value = "0x" + TMEM_data[index+1][7] + TMEM_data[index+1][6] + TMEM_data[index+1][5] + TMEM_data[index+1][4]
    print "\t - max :", max, unit, "\t", value

    # sum: 3th word of 64 bits
    sum = ( (int(TMEM_data[index+2][0],16)<<(7*8)) +
            (int(TMEM_data[index+2][1],16)<<(6*8)) +
            (int(TMEM_data[index+2][2],16)<<(5*8)) +
            (int(TMEM_data[index+2][3],16)<<(4*8)) +
            (int(TMEM_data[index+2][4],16)<<(3*8)) +
            (int(TMEM_data[index+2][5],16)<<(2*8)) +
            (int(TMEM_data[index+2][6],16)<<(1*8)) + 
            (int(TMEM_data[index+2][7],16)<<(0*8)) )
    quotient    = int(sum) >> FRAC_LENGTH #  63 downto 6
    fracStep    = 1.0/pow(2,FRAC_LENGTH) # 0.015625 neutron
    fracMask    = pow(2,FRAC_LENGTH)-1 # 0x3F
    frac        = (int(sum) & fracMask) * fracStep
    sum = quotient + frac
    value = "0x" + TMEM_data[index+2][7] + TMEM_data[index+2][6] + TMEM_data[index+2][5] + TMEM_data[index+2][4] + TMEM_data[index+2][3] + TMEM_data[index+2][2] + TMEM_data[index+2][1] + TMEM_data[index+2][0]
    print "\t - sum :", sum, unit, "\t", value

    # variance: 4th word of 64 bits
    variance = (    (int(TMEM_data[index+3][0],16)<<(7*8)) +
                    (int(TMEM_data[index+3][1],16)<<(6*8)) +
                    (int(TMEM_data[index+3][2],16)<<(5*8)) +
                    (int(TMEM_data[index+3][3],16)<<(4*8)) +
                    (int(TMEM_data[index+3][4],16)<<(3*8)) +
                    (int(TMEM_data[index+3][5],16)<<(2*8)) +
                    (int(TMEM_data[index+3][6],16)<<(1*8)) + 
                    (int(TMEM_data[index+3][7],16)<<(0*8)) )
    quotient    = int(variance) >> FRAC_LENGTH #  63 downto 6
    fracStep    = 1.0/pow(2,FRAC_LENGTH) # 0.015625 neutron
    fracMask    = pow(2,FRAC_LENGTH)-1 # 0x3F
    frac        = (int(variance) & fracMask) * fracStep
    variance = quotient + frac
    value = "0x" + TMEM_data[index+3][7] + TMEM_data[index+3][6] + TMEM_data[index+3][5] + TMEM_data[index+3][4] + TMEM_data[index+3][3] + TMEM_data[index+3][2] + TMEM_data[index+3][1] + TMEM_data[index+3][0]
    print "\t - variance (NOT IMPLMENTED) :", variance, "\t", value

    # average
    print "\t\t -> Average between 2 CPU read =", float(sum)/nBOf1us, "/ us"

def readNeutronDetectionConfiguration (fileName, nbOf64BitWords, channel):
    print "****** " + channel + " neutron detection configuration ***********"

    # fetch TMEM_data of file reader: list of 64bit words from LSB to MSB
    TMEM_data = shapping64bitsMemory(fileName, nbOf64BitWords)

    # matrix size
    nbOfRow = len(TMEM_data)
    nbOfColumn = len(TMEM_data[0])

    # matrix 64xn to vector 16x4n: 8 bytes to 2bytes
    for i in range(nbOfRow):

        if i == 0:
            # charge MPV 32 bits / 23 bits ?
            chargeMPV = (   (int(TMEM_data[0][1],16)<<(3*8)) + 
                            (int(TMEM_data[i][1],16)<<(2*8)) + 
                            (int(TMEM_data[i][2],16)<<(1*8)) + 
                            (int(TMEM_data[i][3],16)<<(0*8)) )
            value = "0x" + TMEM_data[i][1] + TMEM_data[i][2] + TMEM_data[i][3]
            print "Charge MPV\t\t\t:", round(chargeInt2Real(toSigned32(chargeMPV)), 2), "Q\t", value 

            # pedestal 16 bits
            pedestal = (    (int(TMEM_data[i][6],16)<<(1*8)) + 
                            (int(TMEM_data[i][7],16)<<(0*8)) )
            value = "0x" + TMEM_data[i][6] + TMEM_data[i][7]
            print "Pedestal\t\t\t:", round(amplitudeInt2Real(toSigned16(pedestal)), 2), "mV\t", value 

        if i == 1:
            # event detection th 16 bits
            eventDetectionThreshold = ( (int(TMEM_data[i][2],16)<<(1*8)) + 
                                        (int(TMEM_data[i][3],16)<<(0*8)) )
            value = "0x" + TMEM_data[i][2] + TMEM_data[i][3]
            print "Event detection thresold\t:", round(amplitudeInt2Real(toSigned16(eventDetectionThreshold)), 2), "mV\t", value 

            # neutron amplitude min 16 bits
            neutronAmpMin = (   (int(TMEM_data[i][6],16)<<(1*8)) + 
                                (int(TMEM_data[i][7],16)<<(0*8)) )
            value = "0x" + TMEM_data[i][6] + TMEM_data[i][7]
            print "Neutron amplitude minimum\t:", round(amplitudeInt2Real(toSigned16(neutronAmpMin)), 2), "mV\t", value 

        if i == 2:
            # nb data to compute MPV 16 bits
            nbOfDataToComputeMPV = (    (int(TMEM_data[i][2],16)<<(2*8)) + 
                                        (int(TMEM_data[i][3],16)<<(1*8)) )
            value = "0x" + TMEM_data[i][2] + TMEM_data[i][3]
            print "Number of data to compute MPV\t:", nbOfDataToComputeMPV, "\t\t", value 

            # nb data to compute pedestal 16 bits
            nbOfWindowsToComputePedestal = (   (int(TMEM_data[i][6],16)<<(1*8)) + 
                                (int(TMEM_data[i][7],16)<<(0*8)) )
            value = "0x" + TMEM_data[i][6] + TMEM_data[i][7]
            print "Nb of data to compute pedestal\t:", nbOfWindowsToComputePedestal, "\t\t", value 

        if i == 3:
            # neutronTOTmin 8 bits
            neutronTOTmin = int(TMEM_data[i][3],16)<<(0*8)
            value = "0x" + TMEM_data[i][3]
            print "Neutron TOT min\t\t\t:", neutronTOTmin, "samples\t", value 

            # pile up start 8 bits
            TOTpileUpStart = int(TMEM_data[i][7],16)<<(0*8)
            value = "0x" + TMEM_data[i][7]
            print "TOT pile up start\t\t:", TOTpileUpStart, "samples\t", value 

        if i == 4:
            # pile up limit
            TOTpileUpStart = int(TMEM_data[i][3],16)<<(0*8)
            value = "0x" + TMEM_data[i][3]
            print "TOT pile up limit\t\t:", TOTpileUpStart, "samples\t", value 

    print "****** " + channel + " neutron detection configuration ***********"
    return amplitudeInt2Real(toSigned16(eventDetectionThreshold)), amplitudeInt2Real(toSigned16(neutronAmpMin))

def getScopeTriggerSelection(TMEM_data, i_word64):
    # scopeTriggerSelection : 5 bits
    scopeTriggerSelection = (int(TMEM_data[i_word64][3],16) << (0*8)) & pow(2,5-1)-1 # 0x1F
    value = "0x" + TMEM_data[i_word64][0] + TMEM_data[i_word64][1] + TMEM_data[i_word64][2] + TMEM_data[i_word64][3]
    return scopeTriggerSelection, value

def getScopePostTrig(TMEM_data, i_word64):
    # scopePostTrig 16 bits
    scopePostTrig = (   (int(TMEM_data[i_word64][6],16)<<(1*8)) + 
                        (int(TMEM_data[i_word64][7],16)<<(0*8)) ) & pow(2,16)-1 # 0xffff
    value = "0x" + TMEM_data[i_word64][4] + TMEM_data[i_word64][5] + TMEM_data[i_word64][6] + TMEM_data[i_word64][7]
    return scopePostTrig, value

def getScopeTriggerValue(TMEM_data, i_word64):
    # scopeTriggerValue 16 bits
    scopeTriggerValue = (   (int(TMEM_data[i_word64][2],16)<<(1*8)) + 
                            (int(TMEM_data[i_word64][3],16)<<(0*8)) )
    value = "0x" + TMEM_data[i_word64][0] + TMEM_data[i_word64][1] + TMEM_data[i_word64][2] + TMEM_data[i_word64][3]
    return scopeTriggerValue, value

def readScopeConfiguration (fileName, nbOf64BitWords):
    print "****** scope configuration ***********"

    # fetch TMEM_data of file reader: list of 64bit words from LSB to MSB
    TMEM_data = shapping64bitsMemory(fileName, nbOf64BitWords)

    # matrix size
    nbOfRow = len(TMEM_data)
    nbOfColumn = len(TMEM_data[0])

    # matrix 64xn to vector 16x4n: 8 bytes to 2bytes
    for i in range(nbOfRow):

        # scope chFR
        if i == 0:
            print "# scope chFR"
            scopeChFRTriggerSelection, value = getScopeTriggerSelection(TMEM_data, i)
            print "trigger selection\t:", GBL.SCOPE_TRIGGER[scopeChFRTriggerSelection], "\t", value

            scopeChFRPostTrig, value = getScopePostTrig(TMEM_data, i)
            print "post trig\t\t:", scopeChFRPostTrig, "samples\t\t", value

        if i == 1:
            scopeChFRTriggerValue, value = getScopeTriggerValue(TMEM_data, i)
            print "trigger value\t\t:", scopeChFRTriggerValue, "mV\t\t\t", value 

        # scope 1
        if i == 4:
            print "# scope 1"
            scope1TriggerSelection, value = getScopeTriggerSelection(TMEM_data, i)
            print "trigger selection\t:", GBL.SCOPE_TRIGGER[scope1TriggerSelection], "\t", value

            scope1PostTrig, value = getScopePostTrig(TMEM_data, i)
            print "post trig\t\t:", scope1PostTrig, "samples\t\t", value

        if i == 5:
            scope1TriggerValue, value = getScopeTriggerValue(TMEM_data, i)
            print "trigger value\t\t:", scope1TriggerValue, "mV\t\t\t", value 

        # scope 2
        if i == 8:
            print "# scope 2"
            scope2TriggerSelection, value = getScopeTriggerSelection(TMEM_data, i)
            print "trigger selection\t:", GBL.SCOPE_TRIGGER[scope2TriggerSelection], "\t", value

            scope2PostTrig, value = getScopePostTrig(TMEM_data, i)
            print "post trig\t\t:", scope2PostTrig, "samples\t\t", value

        if i == 9:
            scope2TriggerValue, value = getScopeTriggerValue(TMEM_data, i)
            print "trigger value\t\t:", scope2TriggerValue, "mV\t\t\t", value 


    print "****** scope configuration ***********"
    return [scopeChFRTriggerSelection, scopeChFRPostTrig, scopeChFRTriggerValue], [scope1TriggerSelection, scope1PostTrig, scope1TriggerValue], [scope2TriggerSelection, scope2PostTrig, scope2TriggerValue]

def plotTMEMfileReader(fileName, nbOf64BitWords):
    # fetch TMEM_data of file reader: list of 64bit words from MSB to LSB
    TMEM_data = shapping64bitsMemory("data/dataFileReader.txt", nbOf64BitWords)
    # print TMEM_data

    # matrix size
    nbOfRow = len(TMEM_data)
    nbOfColumn = len(TMEM_data[0])
    
    # output vector creation
    rawData = [i for i in range(nbOfRow*4)] # 4 ADC word in a 64 bit word

    # matrix 64xn to vector 16x4n: 8 bytes to 2bytes
    for i in range(nbOfRow):
        # ADC data 0
        rawData[4*i + 0] = ((int(TMEM_data[i][0],16)<<(1*8)) + 
                            (int(TMEM_data[i][1],16)<<(0*8)) )
        # ADC data 1
        rawData[4*i + 1] = ((int(TMEM_data[i][2],16)<<(1*8)) + 
                            (int(TMEM_data[i][3],16)<<(0*8)) )
        # ADC data 2
        rawData[4*i + 2] = ((int(TMEM_data[i][4],16)<<(1*8)) + 
                            (int(TMEM_data[i][5],16)<<(0*8)) )
        # ADC data 3
        rawData[4*i + 3] = ((int(TMEM_data[i][6],16)<<(1*8)) + 
                            (int(TMEM_data[i][7],16)<<(0*8)) )                   

    # convertion in mV
    for i in range(len(rawData)):
        rawData[i] = amplitudeInt2Real(toSigned16(rawData[i]))

    # plot vector
    plt.step([i for i in range(len(rawData))], rawData, label='data read back from file reader')
    plt.title("file reader data")
    plt.xlabel("samples")
    plt.ylabel("mV")
    plt.legend()
    plt.show()

def scope(directoryName, nbOf64BitWords250Mhz, nbOf64BitWords1Mhz, eventDetectionThreshold, neutronAmpMin, listDPRAMmselect, scopeName, scopeAddStart, scopeConfig):
    """ read from file and plot scope data, TMEM5 5_00 to 5_04"""

    # 250 MHZ data
    fig = plt.figure()
    fig.suptitle(scopeName + " results 250Mhz" , fontsize=14)
    ax = plotScope_DPRAM0   (directoryName + "scope_5_" + listDPRAMmselect[0] + ".txt", nbOf64BitWords250Mhz, scopeName, scopeAddStart, scopeConfig)
    plotScope_DPRAM1        (directoryName + "scope_5_" + listDPRAMmselect[1] + ".txt", nbOf64BitWords250Mhz, ax)
    plotScope_DPRAM4        (directoryName + "scope_5_" + listDPRAMmselect[4] + ".txt", nbOf64BitWords250Mhz, ax)

    # legend
    plt.subplot(411)
    # plot amplitude threshold
    plt.plot([eventDetectionThreshold for i in range(nbOf64BitWords250Mhz)],    label='eventDetectionThreshold')
    plt.plot([neutronAmpMin for i in range(nbOf64BitWords250Mhz)],              label='neutronAmpMin')
    plt.xlabel("samples")
    plt.ylabel("mV")
    plt.legend()
    plt.subplot(412, sharex=ax)
    plt.xlabel("samples")
    plt.ylabel("Q")
    plt.legend()
    plt.subplot(413, sharex=ax)
    plt.xlabel("samples")
    plt.ylabel("bit")
    plt.legend()
    plt.subplot(414, sharex=ax)
    plt.xlabel("samples")
    plt.ylabel("samples")
    plt.legend()

    # 1 MHZ data
    fig2 = plt.figure()
    fig2.suptitle(scopeName + " results 1Mhz" , fontsize=14)
    ax2 = plotScope_DPRAM2  (directoryName + "scope_5_" + listDPRAMmselect[2] + ".txt", nbOf64BitWords1Mhz,   scopeName, scopeAddStart)
    plotScope_DPRAM3        (directoryName + "scope_5_" + listDPRAMmselect[3] + ".txt", nbOf64BitWords1Mhz,   ax2)
    plt.subplot(411)
    plt.xlabel("samples")
    plt.legend()
    plt.subplot(412, sharex=ax2)
    plt.xlabel("samples")
    plt.legend()
    plt.subplot(413, sharex=ax2)
    plt.xlabel("samples")
    plt.legend()
    plt.subplot(414, sharex=ax2)
    plt.xlabel("samples")
    plt.legend()

    plt.show()

def plotScope_DPRAM0(fileName, nbOf64BitWords, scopeName, scopeAddStart, scopeConfig):
    # fetch TMEM_data of file reader: list of 64bit words from MSB to LSB
    TMEM_data = shapping64bitsMemory(fileName, nbOf64BitWords)

    # matrix size
    nbOfRow = len(TMEM_data)
    nbOfColumn = len(TMEM_data[0])
    
    # output vector creation
    chargeVector            = np.linspace(0, len(rawData)-1, num=len(rawData), dtype=int) # 1 charge  word in a 64 bit word
    rawDataVector           = np.linspace(0, len(rawData)-1, num=len(rawData), dtype=int) # 1 rawdata word in a 64 bit word
    rawDataVectorCorrected  = np.linspace(0, len(rawData)-1, num=len(rawData), dtype=int) # 1 rawdata corrected word in a 64 bit word

    # matrix 64xn to vector 16x4n: 8 bytes to 2bytes
    for i in range(nbOfRow):
        # charge
        chargeVector[i] = ( (int(TMEM_data[i][0],16)<<(3*8)) + 
                            (int(TMEM_data[i][1],16)<<(2*8)) + 
                            (int(TMEM_data[i][2],16)<<(1*8)) + 
                            (int(TMEM_data[i][3],16)<<(0*8)) )

        # raw data
        rawDataVector[i] = (    (int(TMEM_data[i][4],16)<<(1*8)) + 
                                (int(TMEM_data[i][5],16)<<(0*8)) )

        # raw data corrected
        rawDataVectorCorrected[i] = (   (int(TMEM_data[i][6],16)<<(1*8)) + 
                                        (int(TMEM_data[i][7],16)<<(0*8)) )

    # convertion in Q
    for i in range(nbOfRow):
        chargeVector[i] = chargeInt2Real(toSigned32(chargeVector[i]))

    # convertion in mV
    for i in range(nbOfRow):
        rawDataVector[i] = amplitudeInt2Real(toSigned16(rawDataVector[i]))
        rawDataVectorCorrected[i] = amplitudeInt2Real(toSigned16(rawDataVectorCorrected[i]))

    # plot vector
    xAxis = np.linspace(0, len(rawData)-1, num=len(rawData), dtype=int)
    ax = plt.subplot(411)
    plt.step(xAxis, rawDataVector,          label='scope: rawDataVector (DPRAM0)')
    plt.step(xAxis, rawDataVectorCorrected, label='scope: rawDataVectorCorrected (DPRAM0)')
    # plot scope address start
    x    = scopeAddStart
    yMax = max(rawDataVector)
    yMin = min(rawDataVector)
    plt.plot([x,x],[yMin,yMax], color='black')
    plt.annotate("Trig (in circular buffer)", xy=(x, (yMax+yMin)/2), xytext=(x, (yMax+yMin)/2), fontsize=8, verticalalignment='top')
    # post trig
    postTrig = scopeConfig[1] # scopeConfig[1]: post trig
    if postTrig != 0:
        xMin = scopeAddStart
        xMax = scopeAddStart + postTrig
        y = max(rawDataVector)
        plt.plot([xMin,xMax],[y,y], color='red')
        plt.annotate("Post trig", xy=(xMin, y), xytext=(xMin, y), fontsize=8, verticalalignment='top', color='red')
    plt.subplot(412, sharex=ax)
    plt.step(xAxis, chargeVector, label='scope: charge (DPRAM0)')

    return ax


def plotScope_DPRAM1(fileName, nbOf64BitWords, ax):
    # fetch TMEM_data of file reader: list of 64bit words from MSB to LSB
    TMEM_data = shapping64bitsMemory(fileName, nbOf64BitWords)

    # matrix size
    nbOfRow = len(TMEM_data)
    nbOfColumn = len(TMEM_data[0])
    
    # output vector creation
    event                   = np.linspace(0, len(rawData)-1, num=len(rawData), dtype=int) # 1 word in a 64 bit word
    endOfIntegration        = np.linspace(0, len(rawData)-1, num=len(rawData), dtype=int) # 1 word in a 64 bit word
    TOTlimitReached         = np.linspace(0, len(rawData)-1, num=len(rawData), dtype=int) # 1 word in a 64 bit word
    pileUp                  = np.linspace(0, len(rawData)-1, num=len(rawData), dtype=int) # 1 word in a 64 bit word
    AmplitudeNotValidated   = np.linspace(0, len(rawData)-1, num=len(rawData), dtype=int) # 1 word in a 64 bit word
    TOTnotValidated         = np.linspace(0, len(rawData)-1, num=len(rawData), dtype=int) # 1 word in a 64 bit word
    isNeutron               = np.linspace(0, len(rawData)-1, num=len(rawData), dtype=int) # 1 word in a 64 bit word
    riseTime                = np.linspace(0, len(rawData)-1, num=len(rawData), dtype=int) # 1 word in a 64 bit word
    TOT                     = np.linspace(0, len(rawData)-1, num=len(rawData), dtype=int) # 1 word in a 64 bit word
    amplitude               = np.linspace(0, len(rawData)-1, num=len(rawData), dtype=int) # 1 word in a 64 bit word


    # matrix 64xn to vector
    for i in range(nbOfRow):
        # event
        bit = 14
        get_bitVector(bit, TMEM_data, i, event)
        # endOfIntegration
        bit = 13
        get_bitVector(bit, TMEM_data, i, endOfIntegration)
        # TOTlimitReached
        bit = 12
        get_bitVector(bit, TMEM_data, i, TOTlimitReached)
        # pileUp
        bit = 11
        get_bitVector(bit, TMEM_data, i, pileUp)
        # AmplitudeNotValidated
        bit = 10
        get_bitVector(bit, TMEM_data, i, AmplitudeNotValidated)
        # TOTnotValidated
        bit = 9
        get_bitVector(bit, TMEM_data, i, TOTnotValidated)
        # isNeutron
        bit = 8
        get_bitVector(bit, TMEM_data, i, isNeutron)

        # risetime
        riseTime[i] =  int(TMEM_data[i][4],16)

        # TOT
        TOT[i] =  int(TMEM_data[i][5],16)

        # amplitude
        amplitude[i] = ((int(TMEM_data[i][0],16)<<(1*8)) + 
                        (int(TMEM_data[i][1],16)<<(0*8)) )

    # convertion in mV
    for i in range(len(amplitude)):
        amplitude[i] = amplitudeInt2Real(toSigned16(amplitude[i]))

    # plot vectors
    xAxis = np.linspace(0, len(rawData)-1, num=len(rawData), dtype=int)
    plt.subplot(411)
    plt.step(xAxis, amplitude, label='scope: amplitude (DPRAM1)')
    plt.subplot(413, sharex=ax)
    plt.step(xAxis, plotUtils.offset_list(event,                    0),   label='scope: events (DPRAM1)')
    plt.step(xAxis, plotUtils.offset_list(endOfIntegration,         -2),  label='scope: end of integration (DPRAM1)')
    plt.step(xAxis, plotUtils.offset_list(TOTlimitReached,          -4),  label='scope: TOTlimitReached (DPRAM1)')
    plt.step(xAxis, plotUtils.offset_list(pileUp,                   -6),  label='scope: pileUp (DPRAM1)')
    plt.step(xAxis, plotUtils.offset_list(TOTnotValidated,          -8),  label='scope: TOTnotValidated (DPRAM1)')
    plt.step(xAxis, plotUtils.offset_list(AmplitudeNotValidated,    -10), label='scope: AmplitudeNotValidated (DPRAM1)')
    plt.step(xAxis, plotUtils.offset_list(isNeutron,                -12), label='scope: isNeutron (DPRAM1)')
    # To hide the Y-axis of a plot
    cur_axes = plt.gca()
    cur_axes.axes.get_yaxis().set_visible(False)
    plt.subplot(414, sharex=ax)
    plt.step(xAxis, riseTime, label='scope: rise time (DPRAM1)')
    plt.step(xAxis, TOT, label='scope: TOT (DPRAM1)')
    
    return 0

def plotScope_DPRAM2(fileName, nbOf64BitWords, scopeName, scopeAddStart):

    # fetch TMEM_data of file reader: list of 64bit words from MSB to LSB
    TMEM_data = shapping64bitsMemory(fileName, nbOf64BitWords)

    # matrix size
    nbOfRow = len(TMEM_data)
    nbOfColumn = len(TMEM_data[0])
    
    # output vector creation
    neutronCounter1us       = np.zeros(nbOfRow) # 1 worf of 8-bits in a 64 bit word
    neutronCounterPileUp1us = np.zeros(nbOfRow) # 1 worf of 8-bits in a 64 bit word


    # matrix 64xn to vector 16x4n: 8 bytes to 2bytes
    for i in range(nbOfRow):

        # neutronCounter1us
        neutronCounter1us[i] = ((int(TMEM_data[i][0],16)<<(3*8)) +
                                (int(TMEM_data[i][1],16)<<(2*8)) +
                                (int(TMEM_data[i][2],16)<<(1*8)) + 
                                (int(TMEM_data[i][3],16)<<(0*8)) )
        # neutronCounter1us : (31 downto 6): quotient, (5 downto 0) : frac
        FRAC_LENGTH = 6 # 5 downto 0
        quotient    = int(neutronCounter1us[i]) >> FRAC_LENGTH # 31 downto 6
        fracStep    = 1.0/pow(2,FRAC_LENGTH) # 0.015625 neutron
        fracMask    = pow(2,FRAC_LENGTH)-1 # 0x3F
        frac        = (int(neutronCounter1us[i]) & fracMask) * fracStep
        neutronCounter1us[i] = quotient + frac


        # neutronCounterPileUp1us
        neutronCounterPileUp1us[i] = (  (int(TMEM_data[i][4],16)<<(3*8)) +
                                        (int(TMEM_data[i][5],16)<<(2*8)) +
                                        (int(TMEM_data[i][6],16)<<(1*8)) + 
                                        (int(TMEM_data[i][7],16)<<(0*8)) )
        # neutronCounterPileUp1us : (31 downto 6): quotient, (5 downto 0) : frac
        FRAC_LENGTH = 6 # 5 downto 0
        quotient    = int(neutronCounterPileUp1us[i]) >> FRAC_LENGTH # 31 downto 6
        fracStep    = 1.0/pow(2,FRAC_LENGTH) # 0.015625 neutron
        fracMask    = pow(2,FRAC_LENGTH)-1 # 0x3F
        frac        = (int(neutronCounterPileUp1us[i]) & fracMask) * fracStep
        neutronCounterPileUp1us[i] = quotient + frac

    # plot vector
    xAxis = np.linspace(0, len(rawData)-1, num=len(rawData), dtype=int)
    ax = plt.subplot(411)
    plt.step(xAxis, neutronCounter1us,          label='scope: neutronCounter1us (DPRAM2)')
    plt.step(xAxis, neutronCounterPileUp1us,    label='scope: neutronCounterPileUp1us (DPRAM2)')

    return ax

def plotScope_DPRAM3(fileName, nbOf64BitWords, ax):
    # fetch TMEM_data of file reader: list of 64bit words from MSB to LSB
    TMEM_data = shapping64bitsMemory(fileName, nbOf64BitWords)

    # matrix size
    nbOfRow = len(TMEM_data)
    nbOfColumn = len(TMEM_data[0])
    
    # output vector creation
    nbOfPileUp1us           = np.zeros(nbOfRow) # 1 worf of 8-bits in a 64 bit word
    peakNeutronCounter1us   = np.zeros(nbOfRow) # 1 worf of 8-bits in a 64 bit word
    nbTOTlimitReached1us    = np.zeros(nbOfRow) # 1 worf of 8-bits in a 64 bit word
    nbSaturation1us         = np.zeros(nbOfRow) # 1 worf of 8-bits in a 64 bit word
    nbPositiveSaturation1us = np.zeros(nbOfRow) # 1 worf of 8-bits in a 64 bit word
    nbNegativeSaturation1us = np.zeros(nbOfRow) # 1 worf of 8-bits in a 64 bit word    
    nbSparksDetected1us     = np.zeros(nbOfRow) # 1 worf of 8-bits in a 64 bit word    

    # matrix 64xn to vector 16x4n: 8 bytes to 2bytes
    for i in range(nbOfRow):

        # nbOfPileUp1us
        nbOfPileUp1us[i]            = int(TMEM_data[i][0],16)
        # peakNeutronCounter1us
        peakNeutronCounter1us[i]    = int(TMEM_data[i][1],16)
        # nbTOTlimitReached1us
        nbTOTlimitReached1us[i]     = int(TMEM_data[i][2],16)
        # nbSaturation1us
        nbSaturation1us[i]          = int(TMEM_data[i][3],16)
        # nbPositiveSaturation1us
        nbPositiveSaturation1us[i]  = int(TMEM_data[i][4],16)
        # nbNegativeSaturation1us
        nbNegativeSaturation1us[i]  = int(TMEM_data[i][5],16)
        # nbSparksDetected1us
        nbSparksDetected1us[i]      = int(TMEM_data[i][6],16)

    # plot vector
    xAxis = np.linspace(0, len(rawData)-1, num=len(rawData), dtype=int)
    plt.subplot(411)
    plt.step(xAxis, peakNeutronCounter1us,      label='scope: peakNeutronCounter1us (DPRAM3)')
    plt.subplot(412, sharex=ax)
    plt.step(xAxis, nbOfPileUp1us,              label='scope: nbOfPileUp1us (DPRAM3)')
    plt.subplot(413, sharex=ax)
    plt.step(xAxis, nbSaturation1us,            label='scope: nbSaturation1us (DPRAM3)')
    plt.step(xAxis, nbPositiveSaturation1us,    label='scope: nbPositiveSaturation1us (DPRAM3)')
    plt.step(xAxis, nbNegativeSaturation1us,    label='scope: nbNegativeSaturation1us (DPRAM3)')
    plt.subplot(414, sharex=ax)
    plt.step(xAxis, nbTOTlimitReached1us,       label='scope: nbTOTlimitReached1us (DPRAM3)')
    plt.step(xAxis, nbSparksDetected1us,        label='scope: nbSparksDetected1us (DPRAM3)')

    return ax

def plotScope_DPRAM4(fileName, nbOf64BitWords, ax):

    # fetch TMEM_data of file reader: list of 64bit words from MSB to LSB
    TMEM_data = shapping64bitsMemory(fileName, nbOf64BitWords)

    # matrix size
    nbOfRow = len(TMEM_data)
    nbOfColumn = len(TMEM_data[0])
    
    # output vector creation
    chargeMPVcalculated     = np.linspace(0, len(rawData)-1, num=len(rawData), dtype=int) # 1 word in a 64 bit word
    pedestalCalculated      = np.linspace(0, len(rawData)-1, num=len(rawData), dtype=int) # 1 word in a 64 bit word
    chargeMPVstability      = np.linspace(0, len(rawData)-1, num=len(rawData), dtype=int) # 1 word in a 64 bit word
    TOT_MPVstaibility       = np.linspace(0, len(rawData)-1, num=len(rawData), dtype=int) # 1 word in a 64 bit word
    pedestalStability       = np.linspace(0, len(rawData)-1, num=len(rawData), dtype=int) # 1 word in a 64 bit word

    # matrix 64xn to vector 16x4n: 8 bytes to 2bytes
    for i in range(nbOfRow):
        # charge calculated
        chargeMPVcalculated[i] = (  (int(TMEM_data[i][0],16)<<(3*8)) + 
                                    (int(TMEM_data[i][1],16)<<(2*8)) + 
                                    (int(TMEM_data[i][2],16)<<(1*8)) + 
                                    (int(TMEM_data[i][3],16)<<(0*8)) )
        # pedestalCalculated
        pedestalCalculated[i] = (   (int(TMEM_data[i][4],16)<<(1*8)) + 
                                    (int(TMEM_data[i][5],16)<<(0*8)) )
        # chargeMPVstability
        bit = 10
        get_bitVector(bit, TMEM_data, i, chargeMPVstability)

        # TOT_MPVstaibility
        bit = 9
        get_bitVector(bit, TMEM_data, i, TOT_MPVstaibility)

        # pedestalStability
        bit = 8
        get_bitVector(bit, TMEM_data, i, pedestalStability)

    # convertion in Q
    for i in range(nbOfRow):
        chargeMPVcalculated[i] = chargeInt2Real(toSigned32(chargeMPVcalculated[i]))

    # convertion in mV
    for i in range(nbOfRow):
        pedestalCalculated[i] = amplitudeInt2Real(toSigned16(pedestalCalculated[i]))

    # plot vector
    xAxis = np.linspace(0, len(rawData)-1, num=len(rawData), dtype=int)
    plt.subplot(411)
    plt.step(xAxis, pedestalCalculated, label='scope: calculated pedestal (DPRAM4)')
    plt.subplot(412, sharex=ax)
    plt.step(xAxis, chargeMPVcalculated, label='scope: calculated charge MPV (DPRAM4)')
    plt.subplot(413, sharex=ax)
    plt.step(xAxis, plotUtils.offset_list(chargeMPVstability,   -14), label='scope: charge MPV stability (DPRAM4)')
    plt.step(xAxis, plotUtils.offset_list(TOT_MPVstaibility,    -16), label='scope: TOT_MPV stability (DPRAM4)')
    plt.step(xAxis, plotUtils.offset_list(pedestalStability,    -18), label='scope: pedestal stability (DPRAM4)')
    # To hide the Y-axis of a plot
    cur_axes = plt.gca()
    cur_axes.axes.get_yaxis().set_visible(False)

    return ax

def amplitudeUnsignedtToamplitudemV(amplitudeInt):
    """ convert 16bits unsigned to mV:
            - 2^16-1    => 500 mV
            - 2^15      => 0 mV
            - 0         => -500 mV
    """
    amplitude_mV = (amplitudeInt - pow(2, 15)) * 500.0 / pow(2, 15)
    return amplitude_mV

def plot_smem1(fileName, nbOf64BitWords):

    # fetch SMEM_data of file reader: list of 64bit words from MSB to LSB
    print "start shapping64bitsMemory() ..."
    start = time.time()
    SMEM_data = shapping64bitsMemory(fileName, nbOf64BitWords)
    print "... end shapping64bitsMemory():", round(time.time() - start, 3), "sec"
    
    start = time.time()
    # matrix size
    nbOfRow     = len(SMEM_data)
    nbOfColumn 	= len(SMEM_data[0])
    
    # output vector creation
    rawDataVector0          = np.zeros(nbOfRow) # 1 rawdata word in a 64 bit word
    rawDataVector1          = np.zeros(nbOfRow) # 1 rawdata word in a 64 bit word
    rawDataVector2          = np.zeros(nbOfRow) # 1 rawdata word in a 64 bit word
    rawDataVector3          = np.zeros(nbOfRow) # 1 rawdata word in a 64 bit word

    # matrix 64xn to vector 16x4n: 8 bytes to 2bytes
	# array[:, 0] => all lines, column 0
    # raw data [MSB] : ADC_CH_B[k]
    # print SMEM_data[0][0], SMEM_data[0][0]<<8
    # exit()
    rawDataVector3 = (   (SMEM_data[:, 0]<<(1*8)) + 
                         (SMEM_data[:, 1]<<(0*8)) )

    # raw data : ADC_CH_B[k]
    rawDataVector2 = (   (SMEM_data[:, 2]<<(1*8)) + 
                         (SMEM_data[:, 3]<<(0*8)) )

    # raw data ADC_CH_A[k+1]
    rawDataVector1 = (   (SMEM_data[:, 4]<<(1*8)) + 
                         (SMEM_data[:, 5]<<(0*8)) )

    # raw data [LSB] : ADC_CH_A[k]
    rawDataVector0 = (   (SMEM_data[:, 6]<<(1*8)) + 
                         (SMEM_data[:, 7]<<(0*8)) )

    # convertion in mV
    # unsigned 16 to conversion in mV
    rawDataVector0 = amplitudeUnsignedtToamplitudemV(rawDataVector0)
    rawDataVector1 = amplitudeUnsignedtToamplitudemV(rawDataVector1)
    rawDataVector2 = amplitudeUnsignedtToamplitudemV(rawDataVector2)
    rawDataVector3 = amplitudeUnsignedtToamplitudemV(rawDataVector3)
    # C2 to mV
    # rawDataVector0 = amplitudeInt2Real(toSigned16(rawDataVector0))
    # rawDataVector1 = amplitudeInt2Real(toSigned16(rawDataVector1))
    # rawDataVector2 = amplitudeInt2Real(toSigned16(rawDataVector2))
    # rawDataVector3 = amplitudeInt2Real(toSigned16(rawDataVector3))
    print "8 bytes to ADC word + conversion to mV:", round(time.time() - start, 3), "sec"    
    
        
    # subplot with 4 ch
    print "plotting ..."
    xAxis = [functions.sample_to_time(i)*2 for i in range(nbOfRow)] # *2 because 2ADC word in one row
    fig = plt.figure()
    fig.suptitle("Smem data, per ADC word" , fontsize=14)
    ax = plt.subplot(411)
    plt.step(xAxis, rawDataVector0, label='(15..00) ADC_CH_A[k]', marker=".", markersize=2)
    plt.xlabel("ns")
    plt.ylabel("mV")
    plt.legend()
    ax = plt.subplot(412, sharex=ax, sharey=ax)
    plt.step(xAxis, rawDataVector1, label='(31..16) ADC_CH_A[k+1]', marker=".", markersize=2)
    plt.xlabel("ns")
    plt.ylabel("mV")
    plt.legend()
    ax = plt.subplot(413, sharex=ax, sharey=ax)
    plt.step(xAxis, rawDataVector2, label='(47..32) ADC_CH_B[k]', marker=".", markersize=2)
    plt.xlabel("ns")
    plt.ylabel("mV")
    plt.legend()
    ax = plt.subplot(414, sharex=ax, sharey=ax)
    plt.step(xAxis, rawDataVector3, label='(63..48) ADC_CH_B[k+1]', marker=".", markersize=2)
    plt.xlabel("ns")
    plt.ylabel("mV")
    plt.legend()

    # plot with channels concatenated
    global rawDataVector_CHA
    global rawDataVector_CHB
    rawDataVector_CHA = np.zeros(2*nbOfRow, dtype=np.float) # 2 ADC word per line
    rawDataVector_CHB = np.zeros(2*nbOfRow, dtype=np.float)
    for i in range(2*nbOfRow):
        index = (i/2)
        if i%2 == 0:
            rawDataVector_CHA[i] = rawDataVector0[index]
            rawDataVector_CHB[i] = rawDataVector2[index]
        elif i%2 == 1:
            rawDataVector_CHA[i] = rawDataVector1[index]
            rawDataVector_CHB[i] = rawDataVector3[index]
        else:
            exit("FAILED: not possible")
    fig = plt.figure()
    fig.suptitle("Smem data, channel A and B (ADC ch0 and ch1)" , fontsize=14)
    xAxis = [functions.sample_to_time(i) for i in range(2*nbOfRow)] # 2ADC word per row
    ax = plt.subplot(211)
    plt.plot(xAxis, rawDataVector_CHA, label="Channel A", marker=".", markersize=2)
    plt.xlabel("ns")
    plt.ylabel("mV")
    plt.legend()
    ax = plt.subplot(212, sharex=ax, sharey=ax)
    plt.plot(xAxis, rawDataVector_CHB, label="Channel B", marker=".", markersize=2)
    plt.xlabel("ns")
    plt.ylabel("mV")
    plt.legend()

    # save raw data button
    callback2   = Index()
    axnext      = plt.axes([0.9, 0.25, 0.091, 0.07]) # left, bottom, width, height
    bSave       = Button(axnext, 'Save data')
    bSave.on_clicked(callback2.saveData)

    plt.show()

    return ax

def getAddressStart(fileName):
    TCSR_register = readTCSRregister(fileName)

    # address start CHFR
    scopeAddStartChFR = TCSR_register & 0x0000FFFF

    # address start ch0-ch7
    scopeAddStartCh0_7 = (TCSR_register >> 16) & 0x0000FFFF
    # print "TCSR_register", hex(TCSR_register)
    # print "(TCSR_register >> 16):", hex( (TCSR_register >> 16) & 0x0000FFFF)

    return scopeAddStartChFR, scopeAddStartCh0_7


def main():

    # read DOD
    # read smem1
    nbOf64BitWords = pow(2, 30) # 2^30 to be sure to plot all the file
    plot_smem1(PATH_M_EPICS_NBLM + "misc/data/smem1.txt", nbOf64BitWords)
    # read smem2
    # ...
    exit()

    # generate neutrons, gererate script to send them to File Reader (4_11)
    userAnswer = raw_input("Do you want generate neutron signal ? ('yes' or 'y' vs <whatever> for no) (overwritten existing file if it exists)")  # Python 2
    if userAnswer == "yes" or userAnswer == "y" or userAnswer == "Y" or userAnswer == "YES" or userAnswer == "1" :
        # nb of neutrons to generate
        nbOfNeutrons = 1
        # gaussian distribution parameters
        amplitudeGaussian   = GBL.NEUTRON_AMPLITUDE_MPV # mV
        sigmaGaussian       = 20 # mV. Note that his parameter could be overwritten by the script to generate only positive neutron
        # space between neutrons
        meanSamplesBetween2Neutrons = 500 # samples
        variationBetween2Neutrons   = 100 # samples. position variation amplitude
        # pedestal level
        pedestal = GBL.PEDESTAL_LEVEL # mV
        # noise amplitude
        noiseAmplitude = 2 # mV
        # add sparks at the end
        sparks = False

        # generate data
        rawDataGenerated, rawDataGeneratedMask = dataIn.generate_neutron_signal(    nbOfNeutrons,
                                                                                    amplitudeGaussian,
                                                                                    sigmaGaussian,
                                                                                    meanSamplesBetween2Neutrons,
                                                                                    variationBetween2Neutrons,
                                                                                    pedestal,
                                                                                    noiseAmplitude,
                                                                                    sparks)
        # store genreated signals in 2 files using nBLM data structure
        # signal
        fileName = GBL.PATH_RAW_DATA_FILES + GBL.GENERATED_FILE_NAME + ".txt"
        dataOut.list_rawdata_to_file_rawdata(fileName, rawDataGenerated)
        # mask
        fileName =  GBL.PATH_RAW_DATA_FILES + GBL.GENERATED_FILE_NAME + "Mask.txt"
        dataOut.list_rawdata_to_file_rawdata(fileName, rawDataGeneratedMask)

        # from data generated, generate TscMon script to fill file reader
        generateTscMonScriptToFillFileReader()

    else:
        print "Neutron signal generation skipped"

    # generate neutronDetection configuration
    chargeMPV                   = GBL.NEUTRON_CHARGE_MPV
    pedestal                    = GBL.PEDESTAL_LEVEL
    eventDetectionThreshold     = GBL.EVENT_DETECTION_THRESHOLD 
    neutronAmpMin               = GBL.NEUTRON_AMPLITUDE_MIN
    nbOfDataToComputeMPV        = 1000
    nbOfDataToComputePedestal   = 1000
    neutronTOTmin               = GBL.NEUTRON_TOT_MIN   
    TOTpileUpstart              = GBL.TOT_PILE_UP_START
    TOTpileUpLimitReached       = GBL.TOT_PILE_UP_LIMIT_REACHED
    generateNeutronDetectionConfiguration(chargeMPV, pedestal, eventDetectionThreshold, neutronAmpMin, nbOfDataToComputeMPV, nbOfDataToComputePedestal, neutronTOTmin, TOTpileUpstart, TOTpileUpLimitReached)

    # generate scope configuration
    scopeTriggerSelection   = GBL.SCOPE_TRIGGER_SELECTION
    scopeTriggerDelay       = 0 # not implmented yet 
    scopeTriggerValue       = GBL.SCOPE_TRIGGER_VALUE
    generateScopeConfiguration(scopeTriggerSelection, scopeTriggerDelay, scopeTriggerValue)

    # read back file reader
    userAnswer = raw_input("Do you want check File Reader data ? ('yes' or 'y' vs <whatever> for no)")  # Python 2
    if userAnswer == "yes" or userAnswer == "y" or userAnswer == "Y" or userAnswer == "YES" or userAnswer == "1" :
        generateTscMonScriptToFillFileReader()
        nbOf64BitWords = 4096
        plotTMEMfileReader("data/dataFileReader.txt", nbOf64BitWords)

    # read back neutron configuration
    nbOf64BitWords  = 5
    channel         = "chFR"
    eventDetectionThresholdChFR, neutronAmpMinChFR = readNeutronDetectionConfiguration("data/neutronDetectionConfigurationChFR_read.txt", nbOf64BitWords, channel)
    channel         = "ch0"
    eventDetectionThresholdCh0, neutronAmpMinCh0 = readNeutronDetectionConfiguration("data/neutronDetectionConfigurationCh0_read.txt", nbOf64BitWords, channel)
    channel         = "ch1"
    eventDetectionThresholdCh1, neutronAmpMinCh1 = readNeutronDetectionConfiguration("data/neutronDetectionConfigurationCh1_read.txt", nbOf64BitWords, channel)

    # read back scope configuration
    nbOf64BitWords = 64
    scopeChFRconfig, scope1config, scope2config = readScopeConfiguration ("data/scopeConfiguration_read.txt", nbOf64BitWords)

    # read pulse processing
    nbOf64BitWords = 64
    readPulseProcessing ("data/pulseProcessing.txt", nbOf64BitWords)

    # read scopes
    nbOf64BitWords250Mhz    = 5000
    nbOf64BitWords1Mhz      = 21
    # get addess start in scope buffer
    scopeAddStartChFR, scopeAddStartCh0_7  = getAddressStart("data/scopeAddStart.txt")
    # scope File reader, DPRAMs 5_00 to 5_04 
    listDPRAMmselect        = ["00", "01", "02", "03", "04"]
    scopeName               = "Scope chFR"
    scope("data/", nbOf64BitWords250Mhz, nbOf64BitWords1Mhz, eventDetectionThresholdChFR, neutronAmpMinChFR, listDPRAMmselect, scopeName, scopeAddStartChFR, scopeChFRconfig) 
    # scope 1, DPRAMs 5_05 to 5_09
    scopeName               = "Scope 1"
    listDPRAMmselect        = ["05", "06", "07", "08", "09"]
    scope("data/", nbOf64BitWords250Mhz, nbOf64BitWords1Mhz, eventDetectionThresholdCh0, neutronAmpMinCh0, listDPRAMmselect, scopeName, scopeAddStartCh0_7, scope1config) 
    # scope 2, DPRAMs 5_10 to 5_14
    scopeName               = "Scope 2"
    listDPRAMmselect        = ["10", "11", "12", "13", "14"]
    scope("data/", nbOf64BitWords250Mhz, nbOf64BitWords1Mhz, eventDetectionThresholdCh1, neutronAmpMinCh1, listDPRAMmselect, scopeName, scopeAddStartCh0_7, scope2config) 

    # read DOD
    # read smem1
    nbOf64BitWords = 100000
    plot_smem1("data/smem1.txt", nbOf64BitWords)
    # read smem2
    # ...


if __name__ == "__main__":
    main()
