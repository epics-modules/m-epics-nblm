#!/bin/bash

### script to execute on MTCA CT ###

# IFC1410 IP target
# 99
IP_IFC1410=192.168.1.99
homeIFC140=/export/nfsroots/ifc1410-ess-20170314/home/root/
tscmonCmd=/home/root/bin/tscmon
# 88
IP_IFC1410=192.168.1.88
homeIFC140=/export/nfsroots/ifc1410-ess-20180927/home/root/
tscmonCmd=/home/root/tsc_driver/src/TscMon/TscMon
echo "IP IFC1410: ${IP_IFC1410}, path home: ${homeIFC140}, tscMon: ${tscmonCmd}"

# IP_VNADOT=132.166.34.179	# at CEA (extra)
# path to scripts
# PATH_raw_data_recorder=/home/vnadot/IOxOS
# PATH_xuser_nblm=/home/vnadot/IOxOS

IP_VNADOT=132.166.14.99	    # at CEA (extra)
# path to scripts
PATH_raw_data_recorder=/home/vnadot/dev
PATH_xuser_nblm=/home/vnadot/dev

# IP_VNADOT=192.168.1.x		# at CERN (local network)
# path to scripts
# PATH_raw_data_recorder=/...
# PATH_xuser_nblm=/...



# DDR calibration
echo 
echo Launch Tscmon to check DDR calibration ...
# tscmon log
tscMonLog=TscMonLog.txt
# reboot duration
timeToReboot=35 # sec
# launch tscmon then exit
ssh root@${IP_IFC1410} "${tscmonCmd} @exit.xpm" > ${tscMonLog}
# analyse ${tscMonLog}
if [ -s ${tscMonLog} ]
then
    # TscMon ok
    echo ... tscmon OK    
else
    # if empty, means tscmon error
    echo ... tscmon KO, rebooting IFC1410 ${IP_IFC1410} ...
    echo 
    # reboot
    ssh root@${IP_IFC1410} reboot
    # wait that the IFC1410 has reboot
    echo "IFC1410 reboot on going, wait for ${timeToReboot} sec ..."
    sleep ${timeToReboot}
    echo "please relauch script"
    exit
fi

# load configs on IFC1410
echo "copy configs on IFC1410 ..."
# ifc1410_scope_smem_fmc1_ch01_a0.xpm
scp -r vnadot@${IP_VNADOT}:${PATH_raw_data_recorder}/rawdata_recorder/scripts/ ${homeIFC140}
# s10_TscMon_Scripts
scp -r vnadot@${IP_VNADOT}:${PATH_xuser_nblm}/xuser_nblm/10_TscMon_Scripts/ ${homeIFC140}

# trig DDR
echo
echo "trig scope ..."
ssh root@${IP_IFC1410} "cd ~/scripts; ${tscmonCmd} @ifc1410_scope_smem_fmc1_ch01_a0.xpm"

# read DDR and put it on a file CT (and then try to copy but fails to do it)
echo
echo "read SMEM + copy file to IOCUSER. (you're going to have an error whil copying files (Permission denied, please try again.), ignore it ..."
STARTTIME=$(date +%s)
ssh root@${IP_IFC1410} "cd /home/root/10_TscMon_Scripts/myScripts; ./read_all.sh"
ENDTIME=$(date +%s)
echo "It takes $(($ENDTIME - $STARTTIME)) seconds to complete this task..."

# get files
echo
echo "copy file to IOCUSER. (no error this time) ..."
STARTTIME=$(date +%s)
cp ${homeIFC140}/dataTMEM/* ~/devspace/m-epics-nblm/misc/data/ # ~/devspace/m-epics-nblm/misc/data/ is mounted on vnadot
ENDTIME=$(date +%s)
echo "It takes $(($ENDTIME - $STARTTIME)) seconds to complete this task..."

# if you want to change which PC is plotting data, you should change path in read_FPGA_data_acquisitionChainTester.py
# plot DDR on vnadot computer (CT less DDR memory than my computer)
# echo
# echo "plot results ..."
# ssh -X vnadot@${IP_VNADOT} "cd ~/mnt/devspace/m-epics-nblm/misc; python read_FPGA_data_acquisitionChainTester.py"

# plot DDR on IOCUSER
echo
echo "plot results ..."
cd /home/iocuser/devspace/m-epics-nblm/misc
python read_FPGA_data_acquisitionChainTester.py

