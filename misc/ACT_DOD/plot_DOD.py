# ------------------------------------------------------------------------------
# -- Company      :   CEA Saclay                                              --
# -- Laboratory   :   IRFU / DIS / LDISC                                      --
# --                                                                          --
# -- Project      :   nBLM, CEA test bench                                    --
# --                                                                          --
# -- Author       :   Victor Nadot - victor.nadot@cea.fr                      --
# --                                                                          --
# -- -----------------------------------------------------------------------  --

import matplotlib.pyplot as plt
import numpy as np
import time                             # chrono (elapsed time)
from matplotlib.widgets import Button   # quit button when plotting

# which PC is plotting ?
# PATH_M_EPICS_NBLM   = "/home/vnadot/dev/m-epics-nblm/"
PATH_M_EPICS_NBLM   = "/home/iocuser/devspace/m-epics-nblm/"	# CT, less DDR (if big file, may slow down CT)
# PATH_M_EPICS_NBLM   = "/home/vnadot/mnt/devspace/m-epics-nblm/" # vnadot more DDR memory

import sys
sys.path.insert(0, PATH_M_EPICS_NBLM + "misc/simulation")
import config as GBL                    # global variables

# my modules
from nblmlib import functions           # my functions for nBLM simulation
from nblmlib import dataIn              # input file for simulation
from nblmlib import dataOut             # save simulation results
from nblmlib import neutronDetection    # pre-processing and detects neutrons
from nblmlib import analysis            # pedestal calculation, counts neutons, MPV calculation, sparks and saturation detection, stability ...                            
from nblmlib import plotUtils           # plot raw data and specific events
from nblmlib import genericPlot         # generic plot

# import ACT fonctions
import sys
sys.path.insert(0, PATH_M_EPICS_NBLM + "misc/")
import read_FPGA_data_acquisitionChainTester as ACT

# type of data stored in smem channel (32bits)
DISABLE                     = 0
RAW_DATA                    = 1
TRIGGER                     = 2
NEUTRON_COUNTER             = 3
TRIGGER_AND_NEUTRON_COUNTER = 4
DATA1MHZ                    = 5

class smemChannel(object):
    """ smem object. need 2 smemChannel(32bits) per smem(64bits)"""
    def __init__(self,  rawData         = [],
                        trigger         = [],
                        neutronCounter  = [],
                        data1Mhz        = [],
                        preTriggerSize  = 0):
        self.rawData        = rawData
        self.trigger        = trigger
        self.neutronCounter = neutronCounter 
        self.data1Mhz       = data1Mhz
        self.preTriggerSize = preTriggerSize


def decode_rawData(SMEM_data_channel, lastSampleCaptured, preTriggerSize):
    start = time.time()
    # matrix size
    nbOfRow     = len(SMEM_data_channel)
    nbOfColumn 	= len(SMEM_data_channel[0])
    
    # output vector creation
    rawDataVector0          = np.zeros(nbOfRow) # 1 rawdata word in a 32 bit word
    rawDataVector1          = np.zeros(nbOfRow) # 1 rawdata word in a 32 bit word

    # matrix 32xn to vector 16x2n: 4 bytes to 2bytes
	# array[:, 0] => all lines, column 0
    
    # raw data : ADC_CH_B[k+1]
    rawDataVector1 = (   (SMEM_data_channel[:, 0]<<(1*8)) + 
                         (SMEM_data_channel[:, 1]<<(0*8)) )

    # raw data : ADC_CH_B[k]
    rawDataVector0 = (   (SMEM_data_channel[:, 2]<<(1*8)) + 
                         (SMEM_data_channel[:, 3]<<(0*8)) )

    # convertion in mV
    # unsigned 16 to conversion in mV
    for i in range(nbOfRow):
        rawDataVector0[i] = ACT.amplitudeInt2Real(ACT.toSigned16(rawDataVector0[i]))
        rawDataVector1[i] = ACT.amplitudeInt2Real(ACT.toSigned16(rawDataVector1[i]))
    print "4 bytes to ADC word + conversion to mV:", round(time.time() - start, 3), "sec" 

    # concatenate data
    rawDataVector_CHx = np.zeros(2*nbOfRow, dtype=np.float) # 2 ADC word per line
    for i in range(2*nbOfRow):
        index = (i/2)
        if i%2 == 0:
            rawDataVector_CHx[i] = rawDataVector0[index]
        elif i%2 == 1:
            rawDataVector_CHx[i] = rawDataVector1[index]
        else:
            exit("FAILED: not possible")

    # re ajust pre trigger
    firstPart_start = lastSampleCaptured + 6 # offset of 6 strange but working
    firstPart_stop  = preTriggerSize
    secondPart_start= 0
    secondPart_stop = firstPart_start
    # print "firstPart_start", firstPart_start
    # print "firstPart_stop",  firstPart_stop
    # print "secondPart_start", secondPart_start
    # print "secondPart_stop", secondPart_stop
    rawDataVector_CHx = np.concatenate((rawDataVector_CHx[firstPart_start:firstPart_stop], rawDataVector_CHx[secondPart_start:secondPart_stop], rawDataVector_CHx[firstPart_stop:]))

    # put it into object
    chx = smemChannel(rawData=rawDataVector_CHx, preTriggerSize=preTriggerSize)
    return chx

def decode_smem_channel(SMEM_data_channel, smemDataType, lastSampleCaptured, preTriggerSize):
    """ decode smem channel (32bits) """

    # CHx: fisrt smem word (32bits)
    if smemDataType == DISABLE:
        # no decode
        chx = smemChannel() # empty object
    elif smemDataType == RAW_DATA:
        # decode 2 ADC word
        chx = decode_rawData(SMEM_data_channel, lastSampleCaptured, preTriggerSize)
    elif smemDataType == TRIGGER:
        # decode trigger in LSB
        chx = smemChannel() # empty object
    elif smemDataType == NEUTRON_COUNTER:
        # decode neutron counter
        chx = smemChannel() # empty object
    elif smemDataType == TRIGGER_AND_NEUTRON_COUNTER:
        # decode trigger + neutron counter
        chx = smemChannel() # empty object
    elif smemDataType == DATA1MHZ:
        # decode data 1MHz
        chx = smemChannel() # empty object
    else:
        # error
        exit("FAILED, wrong choice")
    return chx

def decode_smem(SMEM_data, typdeDataFirstWord, typdeDataSecondWord, preTriggerRatio, lastSampleCapturedAdd, acqSize):
    """ select which decode fonction should be used """

    ## manage pre trigger
    # acq size (in MB -> Bytes)
    acqSize = acqSize * 1024 * 1024 # in Bytes now
    print "Acquisition size:", acqSize/4, "samples,", acqSize/1000, "us"

    # ratio
    if preTriggerRatio<1 or preTriggerRatio>7:
        exit("Error ratio value: " + str(preTriggerRatio) + "/8" )
    print "Pre trigger ratio:", str(preTriggerRatio) + "/8"
    preTriggerSize = (acqSize/4)*preTriggerRatio/8 # samples
    print "Pre trigger size:", preTriggerSize, "samples", acqSize*preTriggerRatio/8/1000, "us"

    # last sample captured
    SMEM_OFFSET=0x200000 # offset SMEM
    nbByteADCword           = 2 # nb of bytes per ADC word
    nbADCchannel64bitWord   = 2 # 2 ADC channel in a 64bit word
    # print "lastSampleCaptured address", hex(lastSampleCapturedAdd)
    lastSampleCaptured      = (lastSampleCapturedAdd-SMEM_OFFSET)/nbByteADCword/nbADCchannel64bitWord # samples
    # print "Last trigger captured", lastSampleCaptured, "samples", lastSampleCaptured*4/1000, "us"

    # data selection
    CHAdata = SMEM_data[:, 4:9] # all rows, col 0 to 4 (first word of 32bits    => MSB)
    CHBdata = SMEM_data[:, 0:4] # all rows, col 4 to 9 (second word of 32bits   => LSB)

    # CHA
    cha = decode_smem_channel(CHAdata, typdeDataFirstWord, lastSampleCaptured, preTriggerSize)
    # CHB
    chb = decode_smem_channel(CHBdata, typdeDataSecondWord, lastSampleCaptured, preTriggerSize)

    return cha, chb

def plot_smem1(chaData, chaDisplay, chbData, chbDisplay):

    # canvas args
    myCanvas  = genericPlot.canvas(title="DOD data", fontSize=15)

    # x and y axis 
    xAxis = genericPlot.axis(label="us")
    yAxis = genericPlot.axis(label="mV")

    if chaDisplay:
        # subplot 1
        axisConversion  = 1000 # ns -> us
        # trigger
        triggerX    = chaData.preTriggerSize*4/axisConversion
        triggerYmin = min(chaData.rawData)
        triggerYmax = max(chaData.rawData)
        # data
        xData           = functions.sample_to_time(np.linspace(0, len(chaData.rawData)-1, len(chaData.rawData)))/axisConversion
        myData          = genericPlot.xyData(yData=chaData.rawData, xData=xData, label="CHA")
        myTrigger       = genericPlot.xyData(yData=[triggerYmin, triggerYmax], xData=[triggerX, triggerX], lineStyle=":", lineWidth=2, label="trigger")
        subplot1        = genericPlot.subplot(title="SMEM CHA (31..0)", fontSize="12", xAxis=xAxis, yAxis=yAxis, data=[myData, myTrigger])

        # 1 plot
        genericPlot.generic_plot(myCanvas, [subplot1])


    if chbDisplay:
        # subplot 2
        axisConversion  = 1000 # ns -> us
        # trigger
        triggerX    = chbData.preTriggerSize*4/axisConversion
        triggerYmin = min(chbData.rawData)
        triggerYmax = max(chbData.rawData)
        # data
        xData           = functions.sample_to_time(np.linspace(0, len(chbData.rawData)-1, len(chbData.rawData)))/axisConversion
        myData          = genericPlot.xyData(yData=chbData.rawData, xData=xData, label="CHB")
        myTrigger       = genericPlot.xyData(yData=[triggerYmin, triggerYmax], xData=[triggerX, triggerX], lineStyle=":", lineWidth=2, label="trigger")
        subplot2        = genericPlot.subplot(title="SMEM CHB (63..32)", fontSize="12", xAxis=xAxis, yAxis=yAxis, data=[myData, myTrigger])

        # 1 plot
        genericPlot.generic_plot(myCanvas, [subplot2])

    # show plots
    plt.show()

# raw data selection
nbOf64BitWords  = pow(2, 20) # 2^30 to be sure to plot all the file
rawDataFile     = PATH_M_EPICS_NBLM + "misc/data/smem1.txt"

## get info smem
# pre trigger size
# (05..07): Pre trigger (000: do not use, 001: 1/8 of pre trig, 010=2/8 of pre trig, ...)
preTriggerRatio         = ((ACT.readTCSRregister(PATH_M_EPICS_NBLM + "misc/data/smem1_info.txt", 6)) >> 5) & 0x7
# last sample captured
# scope_smem1_PRETR_ADD[29:3]
lastSampleCapturedAdd   = ((ACT.readTCSRregister(PATH_M_EPICS_NBLM + "misc/data/smem1_info.txt", 8)) >> 0) & 0x1FFFFFFF
# size ACQ (in MB)
# (00..07): SMEM size (00000010: 2MB, 11111111:256 MB)
acqSize                 = ((ACT.readTCSRregister(PATH_M_EPICS_NBLM + "misc/data/smem1_info.txt", 10))>> 0) & 0xFF

# read data
# fetch SMEM_data of file reader: list of 64bit words from MSB to LSB
SMEM_data = ACT.shapping64bitsMemory(fileName=rawDataFile, nbOf64BitWords=nbOf64BitWords)

# data formatting
cha, chb = decode_smem(SMEM_data=SMEM_data, typdeDataFirstWord=RAW_DATA, typdeDataSecondWord=DISABLE, preTriggerRatio=preTriggerRatio, lastSampleCapturedAdd=lastSampleCapturedAdd, acqSize=acqSize)

# plot 
plot_smem1(chaData=cha, chaDisplay=True, chbData=chb, chbDisplay=False)
