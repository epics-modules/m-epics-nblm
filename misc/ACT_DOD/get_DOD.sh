#!/bin/bash

# IP IFC140
IP_IFC1410=10.2.176.88
homeIFC140=/home/root

# check DDR calibration (TscMon)
tscMonLog=TscMonLog.txt
# tscmonCmd=${homeIFC140}/tsc_driver/src/TscMon/TscMon # .99
tscmonCmd=${homeIFC140}/bin/tscmon # .88
echo "tscmonCmd: ${tscmonCmd}"

# copy smem1 scripts from vnadot to iocuser
# echo "copy smem1 scripts from vnadot to iocuser"
# # IFC1410 99
# scp -r vnadot@132.166.14.99:/home/vnadot/dev/xuser_nblm/10_TscMon_Scripts/ /export/nfsroots/ifc1410-ess-20170314/home/root/
# # IFC1410 88
# scp -r vnadot@132.166.14.99:/home/vnadot/dev/xuser_nblm/10_TscMon_Scripts/ /export/nfsroots/ifc1410-ess-20180927/home/root/


# smem1 output file
DODinfoFile=/home/iocuser/devspace/m-epics-nblm/misc/data/smem1_info.txt
DODdataFile=/home/iocuser/devspace/m-epics-nblm/misc/data/smem1.txt
echo 
echo "DOD info file: ${DODinfoFile}"
echo "DOD data file: ${DODdataFile}"

# DDR calibration
echo 
echo Launch Tscmon to check DDR calibration ...
# launch tscmon then exit
echo "ssh root@${IP_IFC1410} "${tscmonCmd} @exit.xpm" > ${tscMonLog}"
ssh root@${IP_IFC1410} "${tscmonCmd} @exit.xpm" > ${tscMonLog}
# analyse ${tscMonLog}
if [ -s ${tscMonLog} ]
then
    # TscMon ok
    echo "... tscmon OK"
else
    # if empty, means tscmon error
    echo "... tscmon KO, please reboot IFC1410 ${IP_IFC1410} ..."
    exit 
fi

# configure and trig smem1
echo 
echo "DOD trig: ssh root@${IP_IFC1410} "${tscmonCmd} @${homeIFC140}/10_TscMon_Scripts/myScripts/smem1_trig_acq.xpm""
ssh root@${IP_IFC1410} "${tscmonCmd} @${homeIFC140}/10_TscMon_Scripts/myScripts/smem1_trig_acq.xpm"
sleep 1 # wait trig

# get smem info (pre trigger ratio, last sample captured acq size)
echo
echo "DOD info: ssh root@${IP_IFC1410} "${tscmonCmd} @${homeIFC140}/10_TscMon_Scripts/myScripts/smem1_info.xpm" > ${DODinfoFile}"
ssh root@${IP_IFC1410} "${tscmonCmd} @${homeIFC140}/10_TscMon_Scripts/myScripts/smem1_info.xpm" > ${DODinfoFile}

# read DOD data
echo
echo "DOD read: ssh root@${IP_IFC1410} "${tscmonCmd} @${homeIFC140}/10_TscMon_Scripts/myScripts/smem1_read.xpm" > ${DODdataFile}"
ssh root@${IP_IFC1410} "${tscmonCmd} @${homeIFC140}/10_TscMon_Scripts/myScripts/smem1_read.xpm" > ${DODdataFile}

# plot data
echo
echo "plot DOD data ..."
python plot_DOD.py
echo "... plot DOD data"
exit
