# compute mu and sigma (loaded and not) for all channels (semi-details)

from nblm_retrival_lib import *

## config ##
runList = range(1, 9, 1)
# run 0 is forbidden !
# last run as loaded (because of "[run+1] process) can't be process
# so if you have 6 runs, 0 to 6, the range should be 0 to 5
## config ##

def get_mu_sigma(chList, signalPV, dateStart, timeStart, dateStop, timeStop):
    """ get mu and sigma for a channel list """

    sigmaVector = []
    muVector    = []
    timeVector  = []

    for channel in chList:

        # PVs data to fetch on archiver
        pvToGet = "SY4527:" + BOARD_NUMBER + ":" + "{:03d}".format(channel) + signalPV

        # get data
        secs, vals = get_data_from_archiver(pvToGet, dateStart, timeStart, dateStop, timeStop)
        timeVector.append([secs, vals])

        # histo
        x, y = histogramCompute(vals, histoStep)

        # histo fit
        xFit, yFit, mu, sigma = histogramFitCompute(x, y)

        muVector.append(mu)
        sigmaVector.append(sigma)

    return timeVector, muVector, sigmaVector

def fct(signal, unit, signalPV, histoStep):
    # loaded
    sigmaVectorLoaded       = [ 0 for i in range(runList[-1]+1) ]
    muVectorLoaded          = [ 0 for i in range(runList[-1]+1) ]
    timeLoaded              = [ 0 for i in range(runList[-1]+1) ]
    #not loaded
    sigmaVectorNotLoaded    = [ 0 for i in range(runList[-1]+1) ]
    muVectorNotLoaded       = [ 0 for i in range(runList[-1]+1) ]
    timeNotLoaded           = [ 0 for i in range(runList[-1]+1) ]

    for run in runList:

        chList = range((run-1)*6, run*6, 1)  
        # run 0 -> not possible
        # run 1 -> ch0-6
        # run 2 -> ch6-12
        # ...

        # loaded
        loadedData      = get_mu_sigma(chList, signalPV, runs[run][DATE_START], runs[run][TIME_START], runs[run][DATE_STOP], runs[run][TIME_STOP])
        # save data
        timeLoaded          [run] = loadedData[0]
        muVectorLoaded      [run] = loadedData[1]
        sigmaVectorLoaded   [run] = loadedData[2]

        # not loaded
        runNotLoaded    = 9
        # run+1: you sure that run+1 is not loaded (but run+1 does not exist for last run)
        # run 0: none loaded but current offset
        # run 9: none loaded, no offset current
        notLoadedData   = get_mu_sigma(chList, signalPV, runs[runNotLoaded][DATE_START], runs[runNotLoaded][TIME_START], runs[runNotLoaded][DATE_STOP], runs[runNotLoaded][TIME_STOP])
        # save data
        timeNotLoaded           [run] = notLoadedData[0]
        muVectorNotLoaded       [run] = notLoadedData[1]
        sigmaVectorNotLoaded    [run] = notLoadedData[2]

    

    figTime = plt.figure()
    figTime.canvas.set_window_title(signal+" time") 
    # plot depending on time
    axTime = plt.subplot(2, 1, 1), plt.subplot(2, 1, 2)
    figTime.suptitle(signal + " variations")
    # Choose your xtick format string
    date_fmt = '%d-%m-%y %H:%M'
    # Use a DateFormatter to set the data to the correct format.
    date_formatter = mdate.DateFormatter(date_fmt)
    # Sets the tick labels diagonal so they fit easier.
    figTime.autofmt_xdate()

    for run in runList:

        for i in range(len(timeLoaded[run])):
            # channel number
            channel = ((run-1)*6) + i

            # loaded
            plt.subplot(2, 1, 1)
            secs = mdate.epoch2num(timeLoaded[run][i][0]) # date convesrion
            plt.title("HV channel loaded")
            plt.plot_date(secs, timeLoaded[run][i][1], '-', label="loaded, ch "+str(channel))
            axTime[0].xaxis.set_major_formatter(date_formatter)

            # not loaded
            plt.subplot(2, 1, 2)
            secs = mdate.epoch2num(timeNotLoaded[run][i][0]) # date convesrion
            plt.title("HV channel not loaded")
            plt.plot_date(secs, timeNotLoaded[run][i][1], '-', label="not loaded, ch "+str(channel))
            axTime[1].xaxis.set_major_formatter(date_formatter)

    # set ticks visible, if using sharex = True. Not needed otherwise
    for tick in axTime[0].get_xticklabels():
        tick.set_visible(True)

    # unit and axis*
    plt.subplot(2, 1, 1)
    plt.ylabel(unit)
    plt.legend()
    plt.subplot(2, 1, 2)
    plt.ylabel(unit)
    plt.legend()


    fig = plt.figure()
    fig.canvas.set_window_title(signal+" simgma and mu") 
    # mu sigma depending on channels
    ax = []
    ax.append(plt.subplot(2, 1, 1))
    ax.append(plt.subplot(2, 1, 2, sharex=ax[0]))
    fig.suptitle(signal + ": ($\mu$, $\sigma$) = fct(channels)")
    for run in runList:

        # xaxis
        chListString = []
        for i in range(len(muVectorLoaded[run])):
            # channel number
            channel = ((run-1)*6) + i
            chListString.append("ch"+str(channel))
        # to overlap to detect pattern because we use the same detector each 6 HV channels
        # chListString = [i for i in range(6)]

        # plot
        plt.subplot(2, 1, 1)
        plt.plot(chListString, muVectorLoaded[run],         linestyle='None', marker=".", markersize=10, color="b")
        plt.plot(chListString, muVectorNotLoaded[run],      linestyle='None', marker="x", markersize=10, color="tab:orange")

        plt.subplot(2, 1, 2)
        plt.plot(chListString, sigmaVectorLoaded[run],      linestyle='None', marker=".", markersize=10, color="b")
        plt.plot(chListString, sigmaVectorNotLoaded[run],   linestyle='None', marker="x", markersize=10, color="tab:orange")
    
    ## custom legend
    # mu
    plt.subplot(2, 1, 1)
    plt.xticks(rotation='vertical')
    plt.ylabel(unit)
    plt.title("$\mu$ - gaussian mean")
    plt.grid(True)
    patchMuLoaded       = mpatches.Patch(linewidth="1.0", color='b',            hatch="o", label="$\mu$ loaded")
    patchMuNotLoadedd   = mpatches.Patch(linewidth="1.0", color="tab:orange",   hatch="x", label="$\mu$ not loaded")
    legendHandles       = [patchMuLoaded, patchMuNotLoadedd]
    plt.legend(handles=legendHandles)
    # sigma
    plt.subplot(2, 1, 2)
    plt.ylabel(unit)
    plt.title("$\sigma$ - gaussian variations")
    plt.grid(True)
    plt.xticks(rotation='vertical')
    patchSigmaLoaded        = mpatches.Patch(linewidth="1.0", color='b',            hatch="o", label="$\sigma$ loaded")
    patchSigmaNotLoadedd    = mpatches.Patch(linewidth="1.0", color="tab:orange",   hatch="x", label="$\sigma$ not loaded")
    legendHandles           = [patchSigmaLoaded, patchSigmaNotLoadedd]
    plt.legend(handles=legendHandles)


# voltage
signal      = "Voltage"
unit        = "Volts"
signalPV    = ":VMon"
histoStep   = histoStepVoltage
fct(signal, unit, signalPV, histoStep)


# current
signal      = "Current"
unit        = "$\mu$A"  # uA
signalPV    = ":IMon"
histoStep   = histoStepCurrent
fct(signal, unit, signalPV, histoStep)


# show plots
plt.show()

