## compute one run or one channel (details)
# alarm infos also

from nblm_retrival_lib import *

def get_run_list(runList, nbOfRuns):
    if runList == "all":
        runList = range(nbOfRuns)
    else:
        runList = runList
    return runList

if __name__ == "__main__":

    ### config ###

    ## a run is one configuration of HV. For example, run 0 is when detectors were connected to HV ch 0 to 6 ##
    # analysis of several channels for one run
    COMPUTE_PER_RUN         = True
    COMPUTE_PER_RUN_RUN_NB  = 10                 # run number to compute
    chList                  = [13, 22, 24, 27, 32, 44]   # range(0,20,1)
    # true : no plot, figures are saved on the computer
    # false: figure are ploted
    onePlotPerFigure = False

    # save figure on the computer
    # if onePlotPerFigure==True, image will be saved anyway
    savePlot    = False


    # analysis of several runs (loaded and not)
    COMPUTE_PER_CHANNEL                 = False
    COMPUTE_PER_CHANNEL_CHANNEL_NUMBER  = 2     # channel number to compute
    runList                             = [9] # [0, 3] # range(0, 2, 1)

    # file to save archiver data
    myFile = PATH_SCRIPT_ARCHIVER + "HV_data.dat"

    # histo parameters
    histoEnable         = True # you should put it to False if you have big variation of data. Otherwise it would take a lot of time to process histo for nothing.
    
    ### config ###

    # get data structure
    HV_data = get_data_structure()

    # time (for file name)
    timestr = time.strftime("%Y%m%d-%H%M%S")

    # check config
    if COMPUTE_PER_RUN == COMPUTE_PER_CHANNEL:
        exit("FAILED: you need to choose between analysis per run or per channel but you can't do both at the same time")
    elif COMPUTE_PER_RUN:
        runList = range(COMPUTE_PER_RUN_RUN_NB, COMPUTE_PER_RUN_RUN_NB+1, 1)
        chList  = chList
        myTitle = "Analysis per run. run: " + str(COMPUTE_PER_RUN_RUN_NB)
        print myTitle
    elif COMPUTE_PER_CHANNEL:
        # runList check
        runList = get_run_list(runList, len(HV_data[0]))
        chList  = range(COMPUTE_PER_CHANNEL_CHANNEL_NUMBER, COMPUTE_PER_CHANNEL_CHANNEL_NUMBER+1, 1)
        myTitle = "Analysis per channel. channel: " + "{:03d}".format(COMPUTE_PER_CHANNEL_CHANNEL_NUMBER)
        print myTitle
    else:
        exit("FAILED: wrong config. check COMPUTE_PER_RUN and COMPUTE_PER_CHANNEL")


    # ckeck if local file exist
    if os.path.isfile(myFile):
        # local file exist
        # what do want to do ?
        print "There is a local file with archiver data."
        print "You can also use data from archiver but it's taking more time. Neverthless, you are sure that your data ar up to date"
        # temp
        getDataFromArchiver = raw_input("Do you want to use the local file (recommanded)? \ny or n: ")
        # getDataFromArchiver = "n"
        # temp
        if getDataFromArchiver == "y" :
            # use local file to get data
            getDataFromArchiver = False
        elif getDataFromArchiver == "n":
            # use archiver to get data
            getDataFromArchiver = True
        else:
            exit("Wrong choice, try again")
    else:
        # no local file
        # use local file to get data 
        print "no local data file. Have to use archiver."
        getDataFromArchiver = True

    
    if getDataFromArchiver:

        ## get data from archiver
        # for concerned channels
        for channel in chList:

            # PVs data to fetch on archiver
            pvToGetVoltage = "SY4527:" + BOARD_NUMBER + ":" + "{:03d}".format(channel) + ":VMon"
            pvToGetCurrent = "SY4527:" + BOARD_NUMBER + ":" + "{:03d}".format(channel) + ":IMon"
            pvToGetStatus  = "SY4527:" + BOARD_NUMBER + ":" + "{:03d}".format(channel) + ":Status"

            # get all channel runs 
            for run in runList:

                # voltage
                HV_data[channel][run].voltage.x, HV_data[channel][run].voltage.y = get_data_from_archiver(  
                                                pvToGet     = pvToGetVoltage,
                                                dateStart   = HV_data[channel][run].dateStart,    
                                                timeStart   = HV_data[channel][run].timeStart,
                                                dateStop    = HV_data[channel][run].dateStop,
                                                timeStop    = HV_data[channel][run].timeStop)  

                # current
                HV_data[channel][run].current.x, HV_data[channel][run].current.y = get_data_from_archiver(  
                                                pvToGet     = pvToGetCurrent,
                                                dateStart   = HV_data[channel][run].dateStart,    
                                                timeStart   = HV_data[channel][run].timeStart,
                                                dateStop    = HV_data[channel][run].dateStop,
                                                timeStop    = HV_data[channel][run].timeStop) 

                # status    
                HV_data[channel][run].status.x, HV_data[channel][run].status.y = get_data_from_archiver(  
                                                pvToGet     = pvToGetStatus,
                                                dateStart   = HV_data[channel][run].dateStart,    
                                                timeStart   = HV_data[channel][run].timeStart,
                                                dateStop    = HV_data[channel][run].dateStop,
                                                timeStop    = HV_data[channel][run].timeStop) 
   
        print "data get from archiver." 

        ## save data
        save_data(HV_data, myFile)
        print "data archiver saved. Next time you will not need to get data from archiver.", myFile

    else:
        ## load data
        HV_data = load_data(myFile)
        print "data loaded from local file", myFile
        pass

    # display database
    diplayDataBase(HV_data)


    ## computing data

    # for concerned channels
    for channel in chList:
        print "channel", channel

        # for concerned runs
        for run in runList:
            print "  run:", run

            ## voltage
            if histoEnable:
                # histo
                x, y = histogramCompute(HV_data[channel][run].voltage.y, histoStepVoltage)
                HV_data[channel][run].voltage.histoX = x
                HV_data[channel][run].voltage.histoY = y    

                # histo fit
                x, y, mu, sigma = histogramFitCompute(HV_data[channel][run].voltage.histoX, HV_data[channel][run].voltage.histoY)
                HV_data[channel][run].voltage.fitX       = x
                HV_data[channel][run].voltage.fitY       = y 
                HV_data[channel][run].voltage.fitMu      = mu
                HV_data[channel][run].voltage.fitSigma   = sigma                
            
                # current
                x, y = histogramCompute(HV_data[channel][run].current.y, histoStepCurrent)
                HV_data[channel][run].current.histoX = x
                HV_data[channel][run].current.histoY = y    

                # histo fit
                x, y, mu, sigma = histogramFitCompute(HV_data[channel][run].current.histoX, HV_data[channel][run].current.histoY)
                HV_data[channel][run].current.fitX       = x
                HV_data[channel][run].current.fitY       = y 
                HV_data[channel][run].current.fitMu      = mu
                HV_data[channel][run].current.fitSigma   = sigma      


    if COMPUTE_PER_RUN:

        ## status
        OVERCURRENT     = 3 # bit 3
        OVERVOLTAGE     = 4 # bit 4
        UNDERVOLTAGE    = 5 # bit 5
        xStatus             = [[] for channel in range(NB_OF_CHANNELS)]   # vector
        overCurrentStatus   = [[] for channel in range(NB_OF_CHANNELS)]   # vector
        overVoltageStatus   = [[] for channel in range(NB_OF_CHANNELS)]   # vector
        underVoltageStatus  = [[] for channel in range(NB_OF_CHANNELS)]   # vector
        for channel in chList:
            # date convesrion
            xStatus[channel] = mdate.epoch2num( HV_data[channel][COMPUTE_PER_RUN_RUN_NB].status.x)
            # bits oprations
            overCurrentStatus[channel]   = np.bitwise_and( np.right_shift( np.array(HV_data[channel][COMPUTE_PER_RUN_RUN_NB].status.y, dtype=int), OVERCURRENT), 0x1)
            overVoltageStatus[channel]   = np.bitwise_and( np.right_shift( np.array(HV_data[channel][COMPUTE_PER_RUN_RUN_NB].status.y, dtype=int), OVERVOLTAGE), 0x1)
            underVoltageStatus[channel]  = np.bitwise_and( np.right_shift( np.array(HV_data[channel][COMPUTE_PER_RUN_RUN_NB].status.y, dtype=int), UNDERVOLTAGE), 0x1)
    
        ## voltage data
        unit        = "Volts"
        signal      = "voltage"

        x           = [[] for channel in range(NB_OF_CHANNELS)]   # vector
        y           = [[] for channel in range(NB_OF_CHANNELS)]
        histoX      = [[] for channel in range(NB_OF_CHANNELS)]
        histoY      = [[] for channel in range(NB_OF_CHANNELS)]
        fitX        = [[] for channel in range(NB_OF_CHANNELS)]
        fitY        = [[] for channel in range(NB_OF_CHANNELS)]
        fitMu       = [0 for channel in range(NB_OF_CHANNELS)]    # scalar
        fitSigma    = [0 for channel in range(NB_OF_CHANNELS)]
        for channel in chList:
            x[channel]          = HV_data[channel][COMPUTE_PER_RUN_RUN_NB].voltage.x
            y[channel]          = HV_data[channel][COMPUTE_PER_RUN_RUN_NB].voltage.y
            histoX[channel]     = HV_data[channel][COMPUTE_PER_RUN_RUN_NB].voltage.histoX
            histoY[channel]     = HV_data[channel][COMPUTE_PER_RUN_RUN_NB].voltage.histoY
            fitX[channel]       = HV_data[channel][COMPUTE_PER_RUN_RUN_NB].voltage.fitX
            fitY[channel]       = HV_data[channel][COMPUTE_PER_RUN_RUN_NB].voltage.fitY
            fitMu[channel]      = HV_data[channel][COMPUTE_PER_RUN_RUN_NB].voltage.fitMu
            fitSigma[channel]   = HV_data[channel][COMPUTE_PER_RUN_RUN_NB].voltage.fitSigma

        # display the run
        display_run( chList, myTitle, signal, unit, histoEnable, x, y, histoX, histoY, fitX, fitY, fitMu, fitSigma, onePlotPerFigure, savePlot, timestr, xStatus, overCurrentStatus, overVoltageStatus, underVoltageStatus)


        ## current data
        unit        = "uA"
        signal      = "current"

        x           = [[] for channel in range(NB_OF_CHANNELS)]   # vector
        y           = [[] for channel in range(NB_OF_CHANNELS)]
        histoX      = [[] for channel in range(NB_OF_CHANNELS)]
        histoY      = [[] for channel in range(NB_OF_CHANNELS)]
        fitX        = [[] for channel in range(NB_OF_CHANNELS)]
        fitY        = [[] for channel in range(NB_OF_CHANNELS)]
        fitMu       = [0 for channel in range(NB_OF_CHANNELS)]    # scalar
        fitSigma    = [0 for channel in range(NB_OF_CHANNELS)]
        for channel in chList:
            x[channel]          = HV_data[channel][COMPUTE_PER_RUN_RUN_NB].current.x
            y[channel]          = HV_data[channel][COMPUTE_PER_RUN_RUN_NB].current.y
            histoX[channel]     = HV_data[channel][COMPUTE_PER_RUN_RUN_NB].current.histoX
            histoY[channel]     = HV_data[channel][COMPUTE_PER_RUN_RUN_NB].current.histoY
            fitX[channel]       = HV_data[channel][COMPUTE_PER_RUN_RUN_NB].current.fitX
            fitY[channel]       = HV_data[channel][COMPUTE_PER_RUN_RUN_NB].current.fitY
            fitMu[channel]      = HV_data[channel][COMPUTE_PER_RUN_RUN_NB].current.fitMu
            fitSigma[channel]   = HV_data[channel][COMPUTE_PER_RUN_RUN_NB].current.fitSigma

        # display the run
        display_run( chList, myTitle, signal, unit, histoEnable, x, y, histoX, histoY, fitX, fitY, fitMu, fitSigma, onePlotPerFigure, savePlot, timestr, xStatus, overCurrentStatus, overVoltageStatus, underVoltageStatus)





    if COMPUTE_PER_CHANNEL:

        # voltage
        unit = "Volts"

        # time
        fig, ax = plt.subplots()
        ax.set_title(myTitle)
        ax.set_ylabel(unit)
        date_fmt = '%d-%m-%y %H:%M:%S'
        date_formatter = mdate.DateFormatter(date_fmt)
        ax.xaxis.set_major_formatter(date_formatter)
        # Sets the tick labels diagonal so they fit easier.
        fig.autofmt_xdate()
        for run in runList:
            # data
            x = HV_data[COMPUTE_PER_CHANNEL_CHANNEL_NUMBER][run].voltage.x
            y = HV_data[COMPUTE_PER_CHANNEL_CHANNEL_NUMBER][run].voltage.y

            # date convesrion
            secs = mdate.epoch2num(x)

            # change color depending on loaded or not
            if HV_data[COMPUTE_PER_CHANNEL_CHANNEL_NUMBER][run].loaded:
                # loaded
                plt.plot_date(secs, y, '-', label="run " + str(run) + " (loaded)", color="red")
            else:
                # not loaded
                plt.plot_date(secs, y, '-', label="run " + str(run) + " (not loaded)", color="black")
        plt.legend()

        # histo and fit
        figHisto, axHisto = plt.subplots()
        axHisto.set_title(myTitle)
        axHisto.set_xlabel(unit)
        figHistoFit, axHistoFit = plt.subplots()
        axHistoFit.set_title(myTitle)
        axHistoFit.set_xlabel(unit)
        for run in runList:
            # data histo
            x = HV_data[COMPUTE_PER_CHANNEL_CHANNEL_NUMBER][run].voltage.histoX
            y = HV_data[COMPUTE_PER_CHANNEL_CHANNEL_NUMBER][run].voltage.histoY
            # dataFit
            xFit = HV_data[COMPUTE_PER_CHANNEL_CHANNEL_NUMBER][run].voltage.fitX
            yFit = HV_data[COMPUTE_PER_CHANNEL_CHANNEL_NUMBER][run].voltage.fitY
            mu   = HV_data[COMPUTE_PER_CHANNEL_CHANNEL_NUMBER][run].voltage.fitMu
            sigma= HV_data[COMPUTE_PER_CHANNEL_CHANNEL_NUMBER][run].voltage.fitSigma

            fitAmplitude = max(yFit)

            # contruct fit annotation
            annotation = "run " + str(run) + ", $\mu$:" + str(round(mu, 3)) + unit + ", $\sigma$:" + str(round(sigma, 3)) + unit

            # change color depending on loaded or not
            if HV_data[COMPUTE_PER_CHANNEL_CHANNEL_NUMBER][run].loaded:
                # loaded
                axHisto.step(x, y, '-', label="run " + str(run) + " (loaded)", color="red")
                axHisto.plot(xFit, yFit, ':', color="red")
                axHisto.annotate(annotation, xy=(mu, fitAmplitude), xytext=(mu, fitAmplitude), fontsize=8, verticalalignment='bottom', color="red")

                # fit
                axHistoFit.plot(xFit, yFit, ':', label="fit run " + str(run) + " (loaded)", color="red")
                axHistoFit.annotate(annotation, xy=(mu, fitAmplitude), xytext=(mu, fitAmplitude), fontsize=8, verticalalignment='bottom', color="red")
            else:
                # not loaded
                axHisto.step(x, y, '-', label="run " + str(run) + " (not loaded)", color="black")
                axHisto.plot(xFit, yFit, ':', color="black")
                axHisto.annotate(annotation, xy=(mu, fitAmplitude), xytext=(mu, fitAmplitude), fontsize=8, verticalalignment='bottom', color="black")

                # fit
                axHistoFit.plot(xFit, yFit, ':', label="fit run " + str(run) + " (loaded)", color="black")
                axHistoFit.annotate(annotation, xy=(mu, fitAmplitude), xytext=(mu, fitAmplitude), fontsize=8, verticalalignment='bottom', color="black")
        plt.legend()



        # voltage
        unit = "uA"

        # time
        figCurrent, axCurrent = plt.subplots()
        axCurrent.set_title(myTitle)
        axCurrent.set_ylabel(unit)
        date_fmt = '%d-%m-%y %H:%M:%S'
        date_formatter = mdate.DateFormatter(date_fmt)
        axCurrent.xaxis.set_major_formatter(date_formatter)
        # Sets the tick labels diagonal so they fit easier.
        figCurrent.autofmt_xdate()
        for run in runList:
            # data
            x = HV_data[COMPUTE_PER_CHANNEL_CHANNEL_NUMBER][run].current.x
            y = HV_data[COMPUTE_PER_CHANNEL_CHANNEL_NUMBER][run].current.y

            # date convesrion
            secs = mdate.epoch2num(x)

            # change color depending on loaded or not
            if HV_data[COMPUTE_PER_CHANNEL_CHANNEL_NUMBER][run].loaded:
                # loaded
                plt.plot_date(secs, y, '-', label="run " + str(run) + " (loaded)", color="red")
            else:
                # not loaded
                plt.plot_date(secs, y, '-', label="run " + str(run) + " (not loaded)", color="black")
        plt.legend()

        # histo and fit
        figHistoCurrent, axHistoCurrent = plt.subplots()
        axHistoCurrent.set_title(myTitle)
        axHistoCurrent.set_xlabel(unit)
        figHistoFitCurrent, axHistoFitCurrent = plt.subplots()
        axHistoFitCurrent.set_title(myTitle)
        axHistoFitCurrent.set_xlabel(unit)
        for run in runList:
            # data histo
            x = HV_data[COMPUTE_PER_CHANNEL_CHANNEL_NUMBER][run].current.histoX
            y = HV_data[COMPUTE_PER_CHANNEL_CHANNEL_NUMBER][run].current.histoY
            # dataFit
            xFit = HV_data[COMPUTE_PER_CHANNEL_CHANNEL_NUMBER][run].current.fitX
            yFit = HV_data[COMPUTE_PER_CHANNEL_CHANNEL_NUMBER][run].current.fitY
            mu   = HV_data[COMPUTE_PER_CHANNEL_CHANNEL_NUMBER][run].current.fitMu
            sigma= HV_data[COMPUTE_PER_CHANNEL_CHANNEL_NUMBER][run].current.fitSigma

            fitAmplitude = max(yFit)

            # contruct fit annotation
            annotation = "run " + str(run) + ", $\mu$:" + str(round(mu, 3)) + unit + ", $\sigma$:" + str(round(sigma, 3)) + unit

            # change color depending on loaded or not
            if HV_data[COMPUTE_PER_CHANNEL_CHANNEL_NUMBER][run].loaded:
                # loaded
                axHistoCurrent.step(x, y, '-', label="run " + str(run) + " (loaded)", color="red")
                axHistoCurrent.plot(xFit, yFit, ':', color="red")
                axHistoCurrent.annotate(annotation, xy=(mu, fitAmplitude), xytext=(mu, fitAmplitude), fontsize=8, verticalalignment='bottom', color="red")

                # fit
                axHistoFitCurrent.plot(xFit, yFit, ':', label="fit run " + str(run) + " (loaded)", color="red")
                axHistoFitCurrent.annotate(annotation, xy=(mu, fitAmplitude), xytext=(mu, fitAmplitude), fontsize=8, verticalalignment='bottom', color="red")
            else:
                # not loaded
                axHistoCurrent.step(x, y, '-', label="run " + str(run) + " (not loaded)", color="black")
                axHistoCurrent.plot(xFit, yFit, ':', color="black")
                axHistoCurrent.annotate(annotation, xy=(mu, fitAmplitude), xytext=(mu, fitAmplitude), fontsize=8, verticalalignment='bottom', color="black")

                # fit
                axHistoFitCurrent.plot(xFit, yFit, ':', label="fit run " + str(run) + " (loaded)", color="black")
                axHistoFitCurrent.annotate(annotation, xy=(mu, fitAmplitude), xytext=(mu, fitAmplitude), fontsize=8, verticalalignment='bottom', color="black")
        plt.legend()



    # show plots
    if not onePlotPerFigure:
        plt.show()   


