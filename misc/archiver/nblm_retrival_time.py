## plot channels, runs and warning you want over time (overview)

import datetime

from nblm_retrival_lib import *

## config ##
chList      = [ [13, 22, 24, 27, 32, 44],
                # range(00, 10, 1),
                # range(10, 20, 1),
                # range(20, 30, 1),
                # range(30, 40, 1),
                # range(40, 48, 1)
]          

runList     = [11] # [1,2,3,4,6,7,8] # range(0, 9, 1)
## config ##


for subplot in range(len(chList)):
    # nb of subplots
    nbOfPlots = len(chList[subplot])

    figVoltage, axVoltage = plt.subplots(nbOfPlots, 1, sharex=True, sharey=True)
    figVoltage.suptitle("Voltage variations")
    # Choose your xtick format string
    date_fmt = '%d-%m-%y %H:%M:%S'
    # Use a DateFormatter to set the data to the correct format.
    date_formatter = mdate.DateFormatter(date_fmt)
    # Sets the tick labels diagonal so they fit easier.
    figVoltage.autofmt_xdate()

    figCurrent, axCurrent = plt.subplots(nbOfPlots, 1, sharex=True, sharey=True)
    figCurrent.suptitle("Current variations")
    # Choose your xtick format string
    date_fmt = '%d-%m-%y %H:%M:%S'
    # Use a DateFormatter to set the data to the correct format.
    date_formatter = mdate.DateFormatter(date_fmt)
    # Sets the tick labels diagonal so they fit easier.
    figCurrent.autofmt_xdate()

    figStatus, axStatus = plt.subplots(nbOfPlots, 1, sharex=True, sharey=True)
    figStatus.suptitle("HV Status")
    # Choose your xtick format string
    date_fmt = '%d-%m-%y %H:%M:%S'
    # Use a DateFormatter to set the data to the correct format.
    date_formatter = mdate.DateFormatter(date_fmt)
    # Sets the tick labels diagonal so they fit easier.
    figStatus.autofmt_xdate()

    figNbOfMeasure, axNbOfMeasure = plt.subplots(2, 1)
    figNbOfMeasure.suptitle("Number of measurements per run (current and voltage)")

    channelVoltageMeasurement = [ [] for channel in range(len(chList[subplot]))]
    channelCurrentMeasurement = [ [] for channel in range(len(chList[subplot]))]
    subplotCounter = 0
    for channel in chList[subplot]:

        # custom legend
        patchLoaded     = mpatches.Patch(linewidth="1.0", color='r',  label="ch"+str(channel) + " (loaded)")
        patchNotLoaded  = mpatches.Patch(linewidth="1.0", color='k',  label="ch"+str(channel) + " (not loaded)")
        legendHandles   = [patchLoaded, patchNotLoaded]

        # PVs data to fetch on archiver
        pvToGetStatus   = "SY4527:" + BOARD_NUMBER + ":" + "{:03d}".format(channel) + ":Status"
        pvToGetVoltage  = "SY4527:" + BOARD_NUMBER + ":" + "{:03d}".format(channel) + ":VMon"
        pvToGetCurrent  = "SY4527:" + BOARD_NUMBER + ":" + "{:03d}".format(channel) + ":IMon"

        # status
        for run in runList:
            secs, vals = get_data_from_archiver(pvToGetStatus, runs[run][DATE_START], runs[run][TIME_START], runs[run][DATE_STOP], runs[run][TIME_STOP])
            # conversion date
            secs = mdate.epoch2num(secs)
            # trace color
            myColors = [    "xkcd:purple",     
                            "xkcd:green",      
                            "xkcd:blue",       
                            "xkcd:pink",       
                            "xkcd:brown",      
                            "xkcd:red",        
                            "xkcd:light blue", 
                            "xkcd:teal",       
                            "xkcd:orange",     
                            "xkcd:light green",
                            "xkcd:magenta",    
                            "xkcd:yellow",     
                            "xkcd:sky blue",   
                            "xkcd:grey",       
                            "xkcd:lime green",
                            "xkcd:light purple"]

            bitMeaning  = [ "on/off",     
                            "ramp up",      
                            "ramp down",       
                            "over current",       
                            "over voltage",      
                            "under voltage",        
                            "external trip", 
                            "vmax reached",       
                            "external disable",     
                            "internal trip",
                            "calibration error",    
                            "unplugged",     
                            "reserved",   
                            "over voltage protection",       
                            "power fail",
                            "temperature error"]

            # plot
            # 16 bits words
            for bitToShift in range(16):
                valsBit = np.bitwise_and( np.right_shift( np.array( vals, dtype=int), bitToShift), 0x1)
                nbOfSameValue = sum(valsBit == valsBit[0])
                if nbOfSameValue != len(valsBit): # len(valsBit) > 1: # np.sum(valsBit) != 0:
                    axStatus[subplotCounter].plot_date(secs, valsBit, '-', color=myColors[bitToShift], label="bit" + str(bitToShift) +" "+ bitMeaning[bitToShift], drawstyle="steps-post")
            axStatus[subplotCounter].xaxis.set_major_formatter(date_formatter)

        # legend
        axStatus[subplotCounter].legend()

        # y axis
        axStatus[subplotCounter].set_title("ch"+str(channel))
        axStatus[subplotCounter].yaxis.set_ticks(range(2))
        axStatus[subplotCounter].yaxis.set_ticklabels(["off", "on"])

        # voltage
        for run in runList:
            secs, vals = get_data_from_archiver(pvToGetVoltage, runs[run][DATE_START], runs[run][TIME_START], runs[run][DATE_STOP], runs[run][TIME_STOP])
            # conversion date
            secs = mdate.epoch2num(secs)
            # trace color
            if run == 0:
                # not loaded: none channel loaded for run 0
                label = ""
                color = "k" # black
            elif ((run-1)*6 <= channel) and (run*6 > channel):
                # loaded
                label = "ch "+str(channel) + " (loaded)"
                color = "r"
            else:
                # not loaded
                label = ""
                color = "k" # black
            # plot
            axVoltage[subplotCounter].plot_date(secs, vals, '-', color=color)
            axVoltage[subplotCounter].set_ylabel("Volts")
            axVoltage[subplotCounter].xaxis.set_major_formatter(date_formatter)

            # save number of measurements
            channelVoltageMeasurement[subplotCounter].append(len(vals))

        # legend
        axVoltage[subplotCounter].legend(handles=legendHandles)


        # current
        for run in runList:
            secs, vals = get_data_from_archiver(pvToGetCurrent, runs[run][DATE_START], runs[run][TIME_START], runs[run][DATE_STOP], runs[run][TIME_STOP])
            # conversion date
            secs = mdate.epoch2num(secs)
            # trace color
            if run == 0:
                # not loaded: none channel loaded for run 0
                color = "k" # black
            elif ((run-1)*6 <= channel) and (run*6 > channel):
                # loaded
                color = "r"
            else:
                # not loaded
                color = "k" # black
            # plot
            axCurrent[subplotCounter].plot_date(secs, vals, '-', label=label, color=color)
            axCurrent[subplotCounter].set_ylabel("$\mu$A")
            axCurrent[subplotCounter].xaxis.set_major_formatter(date_formatter)
            axCurrent[subplotCounter].legend()

            # save number of measurements
            channelCurrentMeasurement[subplotCounter].append(len(vals))

        # legend
        axCurrent[subplotCounter].legend(handles=legendHandles)

        # increase counter
        subplotCounter = subplotCounter + 1



    ### plot number of measurements
    ## voltage
    # set ticks visible, if using sharex = True. Not needed otherwise
    for tick in axNbOfMeasure[0].get_xticklabels():
        tick.set_visible(True)
    # x axis display
    chListString = []
    for run in runList:
        # run number
        chListString.append("run"+str(run))
    # plot
    for channel in range(len(channelVoltageMeasurement)):
        axNbOfMeasure[0].plot(chListString, channelVoltageMeasurement[channel], marker="o", markersize=5, label="ch"+str(chList[subplot][channel]))
    axNbOfMeasure[0].set_title("voltage (VMON)")
    axNbOfMeasure[0].xaxis.set_tick_params(rotation=90)
    axNbOfMeasure[0].set_ylabel("number of measurements")
    axNbOfMeasure[0].legend()

    ## current
    # set ticks visible, if using sharex = True. Not needed otherwise
    for tick in axNbOfMeasure[1].get_xticklabels():
        tick.set_visible(True)
    # x axis display
    chListString = []
    for run in runList:
        # run number
        chListString.append("run"+str(run))
    # plot
    for channel in range(len(channelCurrentMeasurement)):
        axNbOfMeasure[1].plot(chListString, channelCurrentMeasurement[channel], marker="o", markersize=5, label="ch"+str(chList[subplot][channel]))
    axNbOfMeasure[1].set_title("current (IMON)")
    axNbOfMeasure[1].xaxis.set_tick_params(rotation=90)
    axNbOfMeasure[1].set_ylabel("number of measurements")
    axNbOfMeasure[1].legend()



plt.show()