import numpy as np                  # vector operation         
import urllib                       # to contruct url
import json                         # json
import time                         # timer
import matplotlib.pyplot as plt     # plots
import matplotlib.dates as mdate    # convert date for matplotlib
import matplotlib.patches as mpatches   # legend
import os                           # create directory
import pickle                       # to save and load data

from scipy.optimize import curve_fit    # fit
from scipy import asarray as ar,exp     # fit
from scipy import signal                # fit

# vnadot (laptop)
PATH_M_EPICS_NBLM       = "/home/vnadot/mnt/devspace/"
PATH_SCRIPT_ARCHIVER    = PATH_M_EPICS_NBLM + "m-epics-nblm/misc/archiver/"

# iocuser (MTCA crate)
# PATH_M_EPICS_NBLM       = "/home/iocuser/devspace/"
# PATH_SCRIPT_ARCHIVER    = PATH_M_EPICS_NBLM + "m-epics-nblm/misc/archiver/"

# temp
# PATH_M_EPICS_NBLM       = "/home/vnadot/"
# PATH_SCRIPT_ARCHIVER    = PATH_M_EPICS_NBLM + "archiver/"
# temp

# archiver parameters
ADRESS_ARCHIVER = "10.2.176.35"
PORT_ARCHIVER   = "17668"

# constants
NOT_LOADED      = 0
LOADED          = 1
NB_OF_CHANNELS  = 48
BOARD_NUMBER    = "02"  # 02: HV board number, 01: LV board channel

DATE_START  = 0
TIME_START  = 1
DATE_STOP   = 2
TIME_STOP   = 3

# histo parameters
histoStepVoltage    = 0.010  # Volts
histoStepCurrent    = 0.002  # uA

#           date start      time start  date stop       time stop
runs =  [   ["2019-01-11",  "18:00:00", "2019-01-14",   "08:00:00"],    # run 0 : none loaded
            ["2019-01-14",  "12:00:00", "2019-01-15",   "10:00:00"],    # run 1 : 00-06 loaded
            ["2019-01-15",  "12:00:00", "2019-01-16",   "10:00:00"],    # run 2 : 06-12 loaded
            ["2019-01-16",  "11:00:00", "2019-01-17",   "10:00:00"],    # run 3 : 12-18 loaded
            ["2019-01-17",  "11:00:00", "2019-01-18",   "10:00:00"],    # run 4 : 18-24 loaded
            ["2019-01-18",  "11:00:00", "2019-01-21",   "10:00:00"],    # run 5 : 24-30 loaded
            ["2019-01-21",  "11:00:00", "2019-01-22",   "10:00:00"],    # run 6 : 30-36 loaded
            ["2019-01-22",  "11:10:00", "2019-01-23",   "10:00:00"],    # run 7 : 36-42 loaded
            ["2019-01-23",  "11:00:00", "2019-01-24",   "10:00:00"],    # run 8 : 42-48 loaded
            ["2019-01-24",  "10:15:00", "2019-01-25",   "10:00:00"],    # run 9 : none loaded
            ["2019-01-25",  "16:10:00", "2019-02-02",   "10:00:00"],    # run 10 : 13 22 24 27 32 44 loaded => worst channels
            ["2019-02-22",  "10:20:00", "2019-02-22",   "23:00:00"],    # run 11 : 13 22 24 27 32 44 loaded => worst channels
        ]


class C_channelData(object):
    """ channel data """
    def __init__(self,  x           = [],
                        y           = [],
                        histoX      = [],
                        histoY      = [],
                        fitX        = [],
                        fitY        = [],
                        fitMu       = 0,
                        fitSigma    = 0):
        self.x          = x
        self.y          = y
        self.histoX     = histoX
        self.histoY     = histoY
        self.fitX       = fitX
        self.fitY       = fitY
        self.fitMu      = fitMu
        self.fitSigma   = fitSigma
    
class C_channel(object):
    """ database class"""
    def __init__(self,  loaded      = False,
                        dateStart   = "",
                        timeStart   = "",
                        dateStop    = "",
                        timeStop    = "",
                        voltage     = C_channelData(),
                        current     = C_channelData(),
                        status      = C_channelData()):
        self.loaded     = loaded            # detector connected to HV channel
        self.dateStart  = dateStart         # date start run
        self.timeStart  = timeStart
        self.dateStop   = dateStop
        self.timeStop   = timeStop
        self.voltage    = voltage
        self.current    = current
        self.status     = status

def get_data_structure():
     # data structure creation
        HV_data = [[] for ch in range(NB_OF_CHANNELS)]
        # HV_data[0]        |[0].   |loaded
        #                   |       |dateStart
        #                   |       |timeStart
        #                   |       |dateStop
        #                   |       |timeStop
        #                   |       |voltage    |.x
        #                   |       |           |.y
        #                   |       |           |.histoX
        #                   |       |           |.histoY
        #                   |       |           |.fitX
        #                   |       |           |.fitY
        #                   |       |           |.fitMu
        #                   |       |           |.fitSigma
        #                   |       |           |
        #                   |       |current    |x
        #                   |       |           |...
        #                   |       |           |
        #                   |       |status     |x
        #                   |       |           |...
        #                   |
        #                   |[1].   |loaded
        #                   |       |dateStart 
        #                   |       | ...
        #
        # HV_data[1]        |[0].   |loaded
        #                   |       |dateStart
        #                   |       |...
        # ....

        ## set time and date to database

        
        ## run 0
        # add not loaded to channel 0 to 47
        for channel in range(NB_OF_CHANNELS):
            HV_data[channel].append( C_channel(     loaded      = False,
                                                    dateStart   = runs[0][DATE_START],
                                                    timeStart   = runs[0][TIME_START],
                                                    dateStop    = runs[0][DATE_STOP],
                                                    timeStop    = runs[0][TIME_STOP],
                                                    voltage     = C_channelData(),
                                                    current     = C_channelData(),
                                                    status      = C_channelData()))
        for run in range(1, len(runs), 1):
            ## run 1 to 9
            # 0 to 6 loaded
            for channel in range(NB_OF_CHANNELS):
                if (channel>= (run-1)*6) and (channel < run*6):
                    # add loaded to channel 0 to 6
                    HV_data[channel].append( C_channel( loaded      = True,
                                                        dateStart   = runs[run][DATE_START],
                                                        timeStart   = runs[run][TIME_START],
                                                        dateStop    = runs[run][DATE_STOP],
                                                        timeStop    = runs[run][TIME_STOP],
                                                        voltage     = C_channelData(),
                                                        current     = C_channelData(),
                                                        status      = C_channelData()))
                else:
                    # add not loaded to others
                    HV_data[channel].append( C_channel( loaded      = False,
                                                        dateStart   = runs[run][DATE_START],
                                                        timeStart   = runs[run][TIME_START],
                                                        dateStop    = runs[run][DATE_STOP],
                                                        timeStop    = runs[run][TIME_STOP],
                                                        voltage     = C_channelData(),
                                                        current     = C_channelData(),
                                                        status      = C_channelData()))

        return HV_data

def load_data(myFile):
    try:
        with open(myFile) as f:
            x = pickle.load(f)
    except:
        x = []
    return x

def save_data(data, myFile):
    with open(myFile, "wb") as f:
        pickle.dump(data, f)

def gaus(x, a, x0, sigma):
    """ gaussian function """
    return a*np.exp(-(x-x0)**2/(2*sigma**2))

def get_date_and_time_formatted(myDate, myTime):
    """ get formatted date/time for EPICS archiver request"""
    # date
    # UTC+2 could be different to localtime()
    # time winter / summer +1 ?
    is_dst = time.daylight and time.localtime().tm_isdst > 0
    utc_offset = - (time.altzone if is_dst else time.timezone)
    utc_offset = utc_offset/3600 # secs to hours
    if utc_offset < 0:
        TIME_DIFFERENCE_WITH_UTC = '000-0' + str(utc_offset) + ':00'
    elif utc_offset > 0:
        TIME_DIFFERENCE_WITH_UTC = '000+0' + str(utc_offset) + ':00'
    else:
        TIME_DIFFERENCE_WITH_UTC = '000+00:00'
    # data in ISO 8601: yyyy-MM-dd"T"HH:mm:ss.SSSZZ
    dateAndTime = myDate + "T" + myTime + "." + TIME_DIFFERENCE_WITH_UTC

    return dateAndTime, utc_offset

def get_data_from_archiver(pvToGet, dateStart, timeStart, dateStop, timeStop):
    """ get data from a specific PV"""

    # time elapsed
    startFunction = time.time()

    # pv name
    print "PV name:", pvToGet

    # date/time start and stop
    myDateTimeStart, utc_offset = get_date_and_time_formatted(dateStart, timeStart)
    myDateTimeStop, utc_offset  = get_date_and_time_formatted(dateStop, timeStop)
    print "date start:", myDateTimeStart
    print "date stop:", myDateTimeStop

    # url construction
    url = "http://" + ADRESS_ARCHIVER + ":" + PORT_ARCHIVER + "/retrieval/data/getData.json?pv=" + urllib.quote_plus(pvToGet) + "&from=" + urllib.quote_plus(myDateTimeStart) + "&to=" +  urllib.quote_plus(myDateTimeStop)
    print url

    # request to archiver
    # print "start requesting data to archiver..", time.time()-startFunction, "sec"
    req = urllib.urlopen(url)
    data = json.load(req)
    secs = [x['secs'] for x in data[0]['data']]
    vals = [x['val'] for x in data[0]['data']]
    # duration = time.time()-startFunction
    # print "..end requesting data to archiver", time.time()-startFunction, "sec", "\n"

    # UTC offset shit
    for i in range(len(secs)):
        secs[i] = secs[i] + utc_offset*60*60 

    return secs, vals  
        

def histogramCompute(vals, histoStep):
    """ compute histogram """
    # print "start histogram computing..", time.time()-startFunction, "sec"

    # binning
    sequenceBinning     = np.arange(min(vals), max(vals), histoStep)
    
    # compute histo
    myFigure = plt.figure()
    histY, histX, Null  = plt.hist(vals, histtype='step', bins=sequenceBinning) # , density=True)
    # doing my own normalisation
    histY = histY / np.sum(histY)
    # print "sum histo:", sum(histY)
    plt.close(myFigure)
    
    # histX has one element more than histY
    x = []
    for i in range(len(histX)-1):
        x.append((histX[i] + histX[i+1])/2.0)

    # print "..end histogram computing",  time.time()-startFunction, "sec", "\n"
    return x, histY

def histogramFitCompute(x, y):
    """ compute histogram fit"""
    # print "start histogram fit computing..", time.time()-startFunction, "sec"

    ## gaussian fit
    # initial guess for the parameters
    amplitude   = max(y)
    # x0          = (max(histX)+min(histX))/2.0 # window center
    x0          = x[y.argmax()]
    sigma       = (max(x)-min(x))/6.0       # rough approximation case of big amplitude peaks
    p0          = [amplitude, x0, sigma]

    try:
        # gaussian fit
        # p0: parameters initial guess
        # popt: parameter calculated
        popt, pcov = curve_fit(gaus, x, y ,p0)
        gaussianCalculated  = gaus(x,*popt)

        # gaussian results
        mu      = popt[1]
        sigma   = popt[2] 

        # change sign of sigma if negative
        # strange, should bot happen
        if sigma < 0:
            sigma = - sigma
            print "[w] sigma < 0. Sign changed."

        return x, gaussianCalculated, mu, sigma
    
    except:
        print "WARNING: failed to compute gaussian fit,"


    # print "..end histogram fit computing",  time.time()-startFunction, "sec", "\n"
    return np.zeros(len(x)), np.zeros(len(y)), 0 , 0

def diplayDataBase(myDatabase):
    """ display database """

    # get number of runs
    nbOfRuns        = len(myDatabase[0])
    stringNbOfRuns  = ""
    for runNumber in range(nbOfRuns):
        stringNbOfRuns = stringNbOfRuns + "run " + str(runNumber) + "\t"


    print "channel\tloaded\tnot loaded\t" + stringNbOfRuns
    for channel in range(len(myDatabase)):
        myString = ""
        myString = myString + str(channel) + "\t" 

        # init
        nbLoaded    = 0
        nbNotLoaded = 0
        mySubString = ""
        for run in range(len(myDatabase[channel])):
            # count number of loader and not loaded
            if myDatabase[channel][run].loaded == True:
                nbLoaded        = nbLoaded + 1
            else:
                nbNotLoaded     = nbNotLoaded + 1

            # sub-string
            mySubString = mySubString + str(myDatabase[channel][run].loaded) + "\t"

        myString = myString + str(nbLoaded) + "\t" + str(nbNotLoaded) + "\t\t" + mySubString
        print myString




def display_run(chList, myTitle, signal, unit, histoEnable, x, y, histoX, histoY, fitX, fitY, fitMu, fitSigma, onePlotPerFigure, savePlot, timestr, xStatus, overCurrentStatus, overVoltageStatus, underVoltageStatus):
    """ display a run """

    # init
    firstTime = True

    # create directory to save figures
    pathToSaveFigure = ""
    if (savePlot == True and onePlotPerFigure == False) or onePlotPerFigure == True:
        pathToSaveFigure = PATH_SCRIPT_ARCHIVER + timestr + "_" + signal + "/"
        os.mkdir(pathToSaveFigure)
        print "path to save data:", pathToSaveFigure

    # for concerned channels
    for channel in chList:

        if firstTime == True or onePlotPerFigure:
            # create figure for time plot
            fig, (ax, ax2) = plt.subplots(2, 1, sharex=True)
            fig.suptitle(myTitle)
            ax.set_ylabel(unit)
            ### time axis format
            # Choose your xtick format string
            date_fmt = '%d-%m-%y %H:%M:%S'
            # Use a DateFormatter to set the data to the correct format.
            date_formatter = mdate.DateFormatter(date_fmt)
            ax.xaxis.set_major_formatter(date_formatter)
            ax2.xaxis.set_major_formatter(date_formatter)
            # Sets the tick labels diagonal so they fit easier.
            fig.autofmt_xdate()
            
            if histoEnable:
                # create figure for histo plot
                figHisto, axHisto = plt.subplots()
                plt.title(myTitle)
                plt.xlabel(unit)

                # create figure for histo fit plot
                figHistoFit, axHistoFit = plt.subplots()
                plt.title(myTitle)
                plt.xlabel(unit)

            # not first time anymore
            firstTime = False

        # time elapsed
        startFunction = time.time()
            
        ## display
        # date convesrion
        secs = mdate.epoch2num(x[channel])
        # board number
        myLabel = "{:03d}".format(channel) # ch number
        # plot time
        my2DLine = ax.plot_date(secs, y[channel], '-', label=myLabel)
        colorLine = my2DLine[-1].get_color()

        # status
        if np.sum(overCurrentStatus[channel]) != 0:
            ax2.plot_date(xStatus[channel], overCurrentStatus[channel], '-', label=myLabel+" over current", drawstyle="steps-post", color=colorLine)
        if np.sum(overVoltageStatus[channel]) != 0:
            ax2.plot_date(xStatus[channel], overVoltageStatus[channel], '-', label=myLabel+" over voltage", drawstyle="steps-post", color=colorLine)
        if np.sum(underVoltageStatus[channel]) != 0:
            ax2.plot_date(xStatus[channel], underVoltageStatus[channel], '-', label=myLabel+" under voltage", drawstyle="steps-post", color=colorLine)


        # save figure
        if onePlotPerFigure:
            # image name (time)
            imageName = timestr + "_" + BOARD_NUMBER +":"+ myLabel +":"+ signal + "_time"
            # save image plot time 
            fig.savefig(pathToSaveFigure + imageName, bbox_inches='tight')
            print "new image saved (time):", imageName

            
        if histoEnable:
            # draw histogram
            axHisto.step(histoX[channel], histoY[channel], label=myLabel, color=colorLine)

            # contruct fit annotation
            annotation = myLabel + ", $\mu$:" + str(round(fitMu[channel], 3)) + unit + ", $\sigma$:" + str(round(fitSigma[channel], 3)) + unit

            # max fit value
            fitAmplitude = max(fitY[channel])

            # plot with histo + fit
            axHisto.plot(fitX[channel], fitY[channel],':', color=colorLine)
            axHisto.annotate(annotation, xy=(fitMu[channel], fitAmplitude), xytext=(fitMu[channel], fitAmplitude), fontsize=8, verticalalignment='bottom', color=colorLine)

            # plot with only fits
            axHistoFit.plot(fitX[channel], fitY[channel],':', label= myLabel + " fit", color=colorLine)
            axHistoFit.annotate(annotation, xy=(fitMu[channel], fitAmplitude), xytext=(fitMu[channel], fitAmplitude), fontsize=8, verticalalignment='bottom', color=colorLine)
            # print "x", x0Calculated
            # print "y", amplitudeCalculated

        
        if onePlotPerFigure:
            # legend
            ax.legend()
            ax2.legend()
            
            if histoEnable:
                axHisto.legend()
                axHistoFit.legend()

                # image name (histo)
                imageName = timestr + "_" + BOARD_NUMBER +":"+ myLabel +":"+ signal + "_histo"
                # save image plot histo 
                figHisto.savefig(pathToSaveFigure + imageName, bbox_inches='tight')
                print "new image saved (histo):", imageName

                # image name (histo fit)
                imageName = timestr + "_" + BOARD_NUMBER +":"+ myLabel +":"+ signal + "_histoFit"
                # save image plot histo fit
                figHistoFit.savefig(pathToSaveFigure + imageName, bbox_inches='tight')
                print "new image saved (histo fit):", imageName

    if not onePlotPerFigure:
        # legend
        ax.legend()
        ax2.legend()
        if histoEnable:
            axHisto.legend()
            axHistoFit.legend()

        if savePlot:
            ## save figure
            # image name (time)
            imageName = timestr + "_" + BOARD_NUMBER +":"+ myLabel +":"+ signal + "_time"
            # save image plot time 
            fig.savefig(pathToSaveFigure + imageName, bbox_inches='tight')
            print "new image saved (time):", imageName
            if histoEnable:
                # image name (histo)
                imageName = timestr + "_" + BOARD_NUMBER +":"+ myLabel +":"+ signal + "_histo"
                # save image plot histo 
                figHisto.savefig(pathToSaveFigure + imageName, bbox_inches='tight')
                print "new image saved (histo):", imageName

                # image name (histo fit)
                imageName = timestr + "_" + BOARD_NUMBER +":"+ myLabel +":"+ signal + "_histoFit"
                # save image plot histo fit
                figHistoFit.savefig(pathToSaveFigure + imageName, bbox_inches='tight')
                print "new image saved (histo fit):", imageName



