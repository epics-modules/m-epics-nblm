import time                             # elasped time
import numpy as np                      # vector/matrix operations
import matplotlib.pyplot as plt         # plots
import matplotlib.patches as mpatches   # legend
import subprocess                       # execute bash command

# to do
# - connect real PV

# simu
import random
# real
import epics

# import config
from robustness_test_config import *

from non_regression_test_define import *  # pv name and defines

# robustness test states
ROBUSTNESS_TEST_IDLE    = 0
ROBUSTNESS_TEST_PASSED  = 1
ROBUSTNESS_TEST_FAILED  = 2
ROBUSTNESS_TEST_RUNNING = 3

# stop states
STOP_IDLE       = 0
STOP_RUNNING    = 1
STOP_DONE       = 2

# start states
START_IDLE      = 0
START_RUNNING   = 1

def myCaputCustomed (pv, value, pvReadBack):
    """ execute a secured caput. In case of error, push error to status PV before exiting the script"""
    status = caput(pv, value, pvReadBack)
    # status:
    # 0: 
    # 1: ok
    # 2: pv connection failed
    # 3: time out caput
    if status == STATUS_OK:
        # caput went ok
        return True
    else:
        # error
        # end of the test
        caput(PV_robustnessStop,    STOP_DONE,                  PV_robustnessStop)
        caput(PV_robustnessState,   ROBUSTNESS_TEST_RUNNING,    PV_robustnessState )
        if status == STATUS_PV_CONNECTION_FAILED:
            exit("PV connection failed: " + str(pv))
        elif status == STATUS_TIME_OUT:
            exit("Time out caput: " + str(pv))

        return False # useless

def myCagetCustomed (pv):
    """ secured caget """
    result, status = caget(pv)

    if status != STATUS_OK:
        # end of the test
        caput(PV_robustnessStop,    STOP_DONE,              PV_robustnessStop)
        caput(PV_robustnessState,   ROBUSTNESS_TEST_IDLE,   PV_robustnessState)
        exit("PV connection failed or time out caget: " + str(pv))

    return result

def init():
    """ init variables and PVs """
    timeLeft    = 0 # not calculated yet
    nbOfPassed  = 0
    if SIMU_MODE:
        # simulation mode
        pass # do nothing more
    else:
        # real mode
        # push init to PVs
        myCaputCustomed(PV_robustnessTimeLeft,          "being calculated...",              PV_robustnessTimeLeft ) 
        myCaputCustomed(PV_robustnessTimeElapsed,       "being calculated...",              PV_robustnessTimeElapsed ) 
        myCaputCustomed(PV_robustnessNbOfPassed,        nbOfPassed,                         PV_robustnessNbOfPassed)
        myCaputCustomed(PV_robustnessActualIteration,   0,                                  PV_robustnessActualIteration)
        myCaputCustomed(PV_robustnessState,             ROBUSTNESS_TEST_RUNNING,            PV_robustnessState)
        myCaputCustomed(PV_robustnessStop,              STOP_IDLE,                          PV_robustnessStop)

    return timeLeft, nbOfPassed

### display ###
def display_selected_mode():
    if SIMU_MODE:
        print "[i] Simulation mode selected"
    else:
        print "[i] Real mode (with hardware in the loop) selected"

def display_test_config(testNumber, nbOfExecution):
    """ display the test config"""
    print "\n[i] test number", testNumber+1, "/", nbOfExecution
    # push data to PV
    if not SIMU_MODE:
        myCaputCustomed(PV_robustnessActualIteration, testNumber+1, PV_robustnessActualIteration)

def display_matrix(matrixresults, nbLine, nbColumn):
    # header
    myString = "\ntest n" + "\t"
    for i in range(nbColumn):
        myString = myString + str(i) + "\t"
    print myString

    # plot matrix
    for line in range(nbLine):
        myString = str(line) + ".\t"
        for column in range(nbColumn):
            myString =  myString + str(matrixresults[line][column]) + "\t"
        print myString

def plot_matrix(matrix, testDuration, nbOfExecution):
    """ plot matrix using matplotlib"""

    ### figure
    # canvas args
    myCanvas  = genericPlot.canvas(title="ACT robustness test based on non-regression tests", fontSize=12)
    
    ## subplot 1
    # plot matrix
    title = "results"
    # x and y axis 
    xAxis = genericPlot.axis()
    yAxis = genericPlot.axis()
    # custom legend
    patchIdle       = mpatches.Patch(color='grey',  label='IDLE')
    patchPassed     = mpatches.Patch(color='green', label='PASSED')
    patchFailed     = mpatches.Patch(color='red',   label='FAILED')
    patchTimeOut    = mpatches.Patch(color='orange',label='TIME OUT')
    legendHandles   = [patchIdle, patchPassed, patchFailed, patchTimeOut ]
    # column labels
    collabel = []
    for i in range(nbOfExecution):
        collabel.append("test\n" + str(i+1))
    # cell colors
    cellColors = [["" for j in range(nbOfExecution)] for i in range(NUMBER_OF_NON_REGRESSION_TESTS)]
    for i in range(NUMBER_OF_NON_REGRESSION_TESTS):
        for j in range(nbOfExecution):
            if matrix[i][j] == IDLE:
                cellColors[i][j] = "grey"
            elif matrix[i][j] == PASSED:
                cellColors[i][j] = "green"
            elif matrix[i][j] == FAILED:
                cellColors[i][j] = "red"
            elif matrix[i][j] == TIME_OUT:
                cellColors[i][j] = "orange"
            else:
                exit("FAILED: unkown value")
    # data
    myData      = genericPlot.tableData(matrix=matrix, collabel=collabel, rowLabels=unitTest, cellColors=cellColors, legendPerso=True, legendHandles=legendHandles)
    # subplot
    subplot1    = genericPlot.subplot(title=title, fontSize=10, xAxis=xAxis, yAxis=yAxis, data=[myData])

    ## subplot 1
    # testDuration
    title = "test durations"
    # mean value
    testDurationMean = np.mean(testDuration) * np.ones(nbOfExecution)
    # subplot 1
    # x and y axis 
    xAxis = genericPlot.axis(label="non-regression test number")
    yAxis = genericPlot.axis(label="sec")
    # data
    tabSize = nbOfExecution
    xData   = np.linspace(1, tabSize, tabSize) # 1 because test from 1 to tabSize (not 0)
    myData  = genericPlot.xyData(xData=xData, yData=testDuration,     label="execution time of every non-regression test")
    myData2 = genericPlot.xyData(xData=xData, yData=testDurationMean, label="average execution time")
    subplot2= genericPlot.subplot(title=title, fontSize=10, xAxis=xAxis, yAxis=yAxis, data=[myData, myData2])

    # plot it !
    genericPlot.generic_plot(myCanvas, [subplot1, subplot2])

    # show
    plt.show()
### display ###

def get_config():
    """ get robustness config: nbOfExecution and generateInputFile"""
    if SIMU_MODE:
        # simulation mode
        nbOfExecution       = NB_OF_EXECUTION
        generateInputFile   = GENERATE_INPUT_FILE_EVERY_EXECUTION
    else:
        # real mode
        nbOfExecution       = int(myCagetCustomed(PV_robustnessNbOfExecution))
        generateInputFile   = bool(myCagetCustomed(PV_robustnessGenerateFile))
    print "[i] robusteness test config:"
    print "      - nb of execution:", nbOfExecution
    print "      - generate input file every non regesssion test:", generateInputFile
    return nbOfExecution, generateInputFile

def check_stop(nonRegressionTestResult):
    """ check if a stop has been asked by user"""
    if SIMU_MODE:
        # simulation mode
        stop            = myRandom(PROBA_STOP_USER, 100)
        stopOnFailed    = STOP_ON_FAILED
    else:
        # real mode
        stopOnFailed    = myCagetCustomed(PV_robustnessStopOnFailed)
        # check if stop is asked by user
        if myCagetCustomed(PV_robustnessStop) == STOP_RUNNING:
            stop = True
            # put stop status in PV
            myCaputCustomed(PV_robustnessStop, STOP_DONE, PV_robustnessStop)
        else:
            stop = False
    
    if stop:
        print "\n[W] test aborted by user"

    if stopOnFailed and not nonRegressionTestResult:
        # stop on failed ofption + test failed
        stop = True
        print "\n[W] test aborted by because non-regression test failed"
    
    return stop

def generate_input_files(generateInputFile):
    """ generate input files for FPGA"""

    if  generateInputFile == True:
        # if asked by user generate files
        cmd = "python " + PATH_M_EPICS_NBM + "misc/non_regression_test/generateFilesForNonRegressionTetsts.py"
        # print "executed shell command:", cmd
        subprocess.call(cmd, shell=True)
        print "[i] input file generation done"
    else:
        print "[i] input file generation skipped"


def non_regression_test():
    """ start non regression test """
    if SIMU_MODE:
        # simulation mode
        pass    # in simulation mode, test result is done in  save_test()
    else:
        cmd = "python " + PATH_M_EPICS_NBM + "misc/non_regression_test/non_regression_test.py"
        print "executed shell command:", cmd
        subprocess.call(cmd, shell=True)

def save_test(nbOfPassed, nbOfExecution):
    """ save non regression test results """

    # test result here only in simulation mode
    result = np.zeros(NUMBER_OF_NON_REGRESSION_TESTS, dtype=int)

    # init
    nonRegressionTest = True

    if SIMU_MODE:
        # simulation mode

        for test in range(NUMBER_OF_NON_REGRESSION_TESTS):
            myResult = myRandom(PROBA_NON_REGRESSION_TEST_SUCCES, 100)
            if myResult:
                # test succes
                result[test] = PASSED
            else:
                # arbitrary proba
                # proba FAILED:     1/2
                # proba TIME_OUT:   1/2
                myResult = myRandom(50, 100)
                if myResult:
                    result[test] = FAILED
                else:
                    result[test] = TIME_OUT
                # non-regression test failed
                nonRegressionTest = False

            # IDLE state
            if test == 0:
                # don't do first test (test in development)
                result[test] = IDLE

        # if test PASSED
        if nonRegressionTest:
            # actual nb of iteration passed
            nbOfPassed = nbOfPassed + 1

    else:
        # real mode

        # test 0-9: neutron detection test
        # test 10: MPV test
        # test 11: syncho test
        result[0]   = myCagetCustomed ( PV_unitTest11State )
        result[1]   = myCagetCustomed ( PV_unitTest10State )
        result[2]   = myCagetCustomed ( PV_unitTest0State  )
        result[3]   = myCagetCustomed ( PV_unitTest1State  )
        result[4]   = myCagetCustomed ( PV_unitTest2State  )
        result[5]   = myCagetCustomed ( PV_unitTest3State  )
        result[6]   = myCagetCustomed ( PV_unitTest4State  )
        result[7]   = myCagetCustomed ( PV_unitTest5State  )
        result[8]   = myCagetCustomed ( PV_unitTest6State  )
        result[9]   = myCagetCustomed ( PV_unitTest7State  )
        result[10]  = myCagetCustomed ( PV_unitTest8State  )
        result[11]  = myCagetCustomed ( PV_unitTest9State  )

        # nb of non-regression PASSED
        for i in range(NUMBER_OF_NON_REGRESSION_TESTS):
            if result[i] == FAILED or result[i] == TIME_OUT:
                nonRegressionTest = False
                break # quit for loop

        if nonRegressionTest:
            # actual nb of iteration passed
            nbOfPassed = nbOfPassed + 1
            # +1 iteration passed
            myCaputCustomed(PV_robustnessNbOfPassed, nbOfPassed , PV_robustnessNbOfPassed)

    print "[i] nb of non-regression test PASSED", nbOfPassed, "/", nbOfExecution
    return nonRegressionTest, result , nbOfPassed


### fonctions which are not depending on SIMU_MODE ###
def get_time():
    """ get actual time """
    return time.time()

def update_execution_time_estimation(startScript, startTest,  testNumber, nbOfExecution):
    """ update execution time estimation """
    # time.sleep(0.1) # debug
    now                 = time.time()
    iterationDuration   = now - startTest
    timeLeft            = (nbOfExecution - (testNumber+1)) * iterationDuration
    timeElasped         = now - startScript
    print "[i] iteration duration:",    functions.elapsedTime(iterationDuration, 1),    "sec"   # 0: START_TIME, 1:DURATION
    print "[i] time elasped:",          functions.elapsedTime(timeElasped, 1),          "sec"   # 0: START_TIME, 1:DURATION
    print "[i] time left:",             functions.elapsedTime(timeLeft, 1),             "sec"   # 0: START_TIME, 1:DURATION

    # push to PVs
    if not SIMU_MODE:
        myCaputCustomed(PV_robustnessTimeLeft,      functions.elapsedTime(timeLeft, 1),     PV_robustnessTimeLeft )    # 0: START_TIME, 1:DURATION
        myCaputCustomed(PV_robustnessTimeElapsed,   functions.elapsedTime(timeElasped, 1),  PV_robustnessTimeElapsed ) # 0: START_TIME, 1:DURATION
    return timeLeft, iterationDuration

def myRandom(numerator, denominator):
    """ return true 'numerator' times over 'denominator'"""
    rand    = random.random()
    result  = rand < (numerator / float(denominator))
    # print rand, "<", numerator/float(denominator), "=>", result
    return result

def get_test_result(matrixResult, nbOfExecution, startScript):
    """ gives robustness test result"""
    # init
    result      = PASSED
    nbOfPassed  = 0
    nbOfIdle    = 0
    nbOfFailed  = 0

    # stat non regression test
    numberOfPossibleStates = 4  # 4: IDLE PASSE FAILED TIME OUT
    unitTestMatrix      = np.zeros((NUMBER_OF_NON_REGRESSION_TESTS, numberOfPossibleStates), dtype=int)
    for i in range(NUMBER_OF_NON_REGRESSION_TESTS):
        unitTestMatrix[i][0] = np.sum(matrixResult[i,:] == IDLE)
        unitTestMatrix[i][1] = np.sum(matrixResult[i,:] == PASSED)
        unitTestMatrix[i][2] = np.sum(matrixResult[i,:] == FAILED)
        unitTestMatrix[i][3] = np.sum(matrixResult[i,:] == TIME_OUT)
        # prompt
        # print "non regression test", i+1, ":", unitTest[i], ":"
        # print "    - IDLE:\t",      unitTestMatrix[i][0]
        # print "    - PASSED:\t",    unitTestMatrix[i][1]
        # print "    - FAILED:\t",    unitTestMatrix[i][2]
        # print "    - TIMOUT:\t",    unitTestMatrix[i][3]
    
    ### plot matrix ###
    myCanvas  = genericPlot.canvas(title="Results per unit test", fontSize=15)
    # subplot 
    title = "Results per unit test"
    # x and y axis 
    xAxis = genericPlot.axis()
    yAxis = genericPlot.axis()
    # column labels
    collabel = ["IDLE", "PASSED", "FAILED", "TIME OUT"]
    # cell colors
    cellColors = [["white" for j in range(numberOfPossibleStates)] for i in range(NUMBER_OF_NON_REGRESSION_TESTS)]
    for line in range(NUMBER_OF_NON_REGRESSION_TESTS):
        for col in range(numberOfPossibleStates):
            # col IDLE
            if col == IDLE:
                # no color
                pass
            elif col == PASSED:
                # not failed neither time out
                if unitTestMatrix[line][FAILED] == 0 and  unitTestMatrix[line][TIME_OUT] == 0:
                    cellColors[line][col] = "green"
            elif col == FAILED:
                if unitTestMatrix[line][col] > 0:
                    # at least on failed
                    cellColors[line][col] = "red"
            elif col == TIME_OUT:
                if unitTestMatrix[line][col] > 0:
                    # at least one time out
                    cellColors[line][col] = "orange"
            else:
                exit("FAILED: unkown value")
    # data
    myData      = genericPlot.tableData(matrix=unitTestMatrix, collabel=collabel, rowLabels=unitTest, cellColors=cellColors, legendPerso=False)
    # subplot
    subplot1    = genericPlot.subplot(title=title, xAxis=xAxis, yAxis=yAxis, data=[myData])
    # plot it !
    genericPlot.generic_plot(myCanvas, [subplot1])

    # nb of passed, failes timeout, idle
    for line in range (NUMBER_OF_NON_REGRESSION_TESTS):
        for col in range(nbOfExecution):
            if matrixResult[line, col] == PASSED:
                # PASSED
                nbOfPassed  = nbOfPassed + 1
            elif matrixResult[line, col] == IDLE:
                # IDLE: test not done
                nbOfIdle    = nbOfIdle + 1
            elif matrixResult[line, col] == FAILED or matrixResult[line, col] == TIME_OUT:
                # test failed or time_out
                nbOfFailed  = nbOfFailed + 1
            else:
                # error
                exit("unknow parameter. Value should be in range[" + str(IDLE) + "; " + str(TIME_OUT) + "] but value is " + str(matrixResult[line, col]))

    # Robustness test duration
    print "\n[i] Robustness test duration", functions.elapsedTime(startScript, 0), "sec"   # 0: START_TIME, 1:DURATION

    if nbOfFailed > 0:
        # failed
        print "\n[w] FAILED: robustness test"
        return ROBUSTNESS_TEST_FAILED
    else:
        # passed
        print "\n[i] PASSED: robustness test"
        print "Robustness test duration", functions.elapsedTime(startScript, 0), "sec"   # 0: START_TIME, 1:DURATION
        return ROBUSTNESS_TEST_PASSED

def set_test_state(state):
    """ set robustness test state """
    if SIMU_MODE:
        # simulation mode
        pass    # nothing to do in simu mode
    else:
        # real mode
        myCaputCustomed(PV_robustnessState, state , PV_robustnessState)
### fonctions which are not depending on SIMU_MODE ###