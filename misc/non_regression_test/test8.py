from non_regression_test_define import *

def test8():
    myTest = t_test(    
                        # general information
                        number                      = 8,
                        name                        = "positive saturation test",
                        description                 = "1 positive saturation, pedestal=0mV, amp=500mV, duration=4ns",
                        file                        = "saturationPositive_p0mV_a500mV_d4.txt",
                        # neutron detection config
                        chargeMPV                   = -1733,    # Q
                        pedestal                    = 0,        # mV
                        eventDetectionThreshold     = -3,       # mV
                        neutronAmplitudeMin         = -10,      # mV
                        neutronTOTmin               = 40,       # s
                        TOTpileUpStart              = 300,      # s
                        TOTpileUpLimit              = 1000,      # s        
                        # scope 
                        scopeTriggerPost            = 10000, # event in the center of the window
                        scopeTriggerValue           = 1,    # need to be != from 0
                        scopeTriggerListSelect      = 0,    # list 1 selected
                        scopeTriggerList1           = 15,   # positive saturation
                        scopeTriggerList2           = 0,    # whatever
                        # reference: what the output of pulse processing should be
                        neutronCounterRef           = 0,    # neutrons
                        neutronChargeRef            = 0, # Q
                        badEventChargeRef           = 0,    # Q
                        nbOfPeaksRef                = 0,    
                        nbOfPileUpRef               = 0,    
                        nbOfTOTlimiReachedRef       = 0,    
                        nbOfSaturationsRef          = 508,  # saturations 
                        nbOfPositiveSaturationsRef  = 508,  # saturations
                        nbOfNegativeSaturationRef   = 0,
                        badEventPeak                = 0,
                        badEventPileUp              = 0,
                        badEventTOTlimiReached      = 0,
                        # error authorised in %
                        neutronCounterError             = 0, # %
                        neutronChargeError              = 1, # %
                        badEventChargeError             = 0, # %
                        nbOfPeaksError                  = 0, # %
                        nbOfPileUpError                 = 0, # %
                        nbOfTOTlimiReachedError         = 0, # %
                        nbOfSaturationsError            = 0, # %
                        nbOfPositiveSaturationsError    = 0, # %
                        nbOfNegativeSaturationError     = 0, # %
                        badEventPeakError               = 0, # %
                        badEventPileUpError             = 0, # %
                        badEventTOTlimiReachedError     = 0, # %
                    )
    return myTest