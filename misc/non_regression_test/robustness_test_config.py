import sys

# m-epics-nblm path
PATH_M_EPICS_NBM    = "/home/iocuser/devspace/m-epics-nblm/"
# PATH_M_EPICS_NBM    = "/home/vnadot/development/m-epics-nblm/"

# import nblm functions
sys.path.insert(0, PATH_M_EPICS_NBM + "misc/simulation")
from nblmlib import functions   # my functions for nBLM simulation
from nblmlib import genericPlot # generic plot
# import nblm functions
sys.path.insert(0, PATH_M_EPICS_NBM + "misc")
from epicsFunctions import *     # secure caget and caput (blocant function with read back)

# select mode (simu or whith real hardware)
SIMU_MODE = False

# constant
NUMBER_OF_NON_REGRESSION_TESTS      = 12 # pulse processing sync(1), MPV(1), neutron detection (10)

# parameter for real and simu mode

# simu parameters
NB_OF_EXECUTION                     = 120
PROBA_NON_REGRESSION_TEST_SUCCES    = 90    # %
PROBA_STOP_USER                     = 1     # %
GENERATE_INPUT_FILE_EVERY_EXECUTION = False
STOP_ON_FAILED                      = False

# real mode parameters
# defines PV MACROs
PREFIX          = "IFC1410_nBLM"   
DEVICE_TUNING   = "PBI-nBLM"        
CHANNEL         = "CH0"             
### PVs
# neutron detection configuration WRITE
# user input only
PV_robustnessNbOfExecution  = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "RobustnessNbOfExecution")
PV_robustnessGenerateFile   = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "RobustnessGenerateFile")
PV_robustnessStopOnFailed   = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "RobustnessStopOnFailed")
# user input with modification in script
PV_robustnessStop           = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "RobustnessStop")
# user read only
PV_robustnessNbOfPassed     = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "RobustnessNbOfPassed")
PV_robustnessActualIteration= connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "RobustnessActualIteration")
PV_robustnessState          = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "RobustnessState")
PV_robustnessTimeLeft       = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "RobustnessTimeLeft")
PV_robustnessTimeElapsed    = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":" + "RobustnessTimeElapsed")

# unit test labels
unitTest = [   "Pulse processing\nsync",
                "MPV",
                "neutron peak",
                "neutron pile-up",
                "neutron TOT\nlimit reached",
                "bad event peak",
                "bad event\npile-up",
                "bad event TOT\nlimit reached",
                "+ pedestal",
                "- pedestal",
                "+ saturation",
                "- saturation"
            ]