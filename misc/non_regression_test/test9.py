from non_regression_test_define import *

def test9():
    myTest = t_test(    
                        # general information
                        number                      = 9,
                        name                        = "negative staturation test",
                        description                 = "1 negative saturation, pedestal=0mV, amp=-500mV, duration=4ns",
                        file                        = "saturationNegative_p0mV_a-500mV_d4.txt",
                        # neutron detection config
                        chargeMPV                   = -1733,    # Q
                        pedestal                    = 0,        # mV
                        eventDetectionThreshold     = -3,       # mV
                        neutronAmplitudeMin         = -10,      # mV
                        neutronTOTmin               = 40,       # s
                        TOTpileUpStart              = 300,      # s
                        TOTpileUpLimit              = 1000,      # s        
                        # scope 
                        scopeTriggerPost            = 10000, # event in the center of the window
                        scopeTriggerValue           = 1,    # need to be != from 0
                        scopeTriggerListSelect      = 1,    # list 2 selected
                        scopeTriggerList1           = 0,    # on negative saturation
                        scopeTriggerList2           = 0,    # whatever
                        # reference: what the output of pulse processing should be
                        neutronCounterRef           = 144,     # neutrons
                        neutronChargeRef            = -250000, # Q
                        badEventChargeRef           = -4000,   # Q
                        nbOfPeaksRef                = 0,    
                        nbOfPileUpRef               = 2,    
                        nbOfTOTlimiReachedRef       = 2,    
                        nbOfSaturationsRef          = 508,  # saturations
                        nbOfPositiveSaturationsRef  = 0,
                        nbOfNegativeSaturationRef   = 508,  # saturations
                        badEventPeak                = 8,
                        badEventPileUp              = 0,
                        badEventTOTlimiReached      = 0,
                        # error authorised in %
                        neutronCounterError             = 1, # %
                        neutronChargeError              = 1, # %
                        badEventChargeError             = 1, # %
                        nbOfPeaksError                  = 0, # %
                        nbOfPileUpError                 = 0, # %
                        nbOfTOTlimiReachedError         = 0, # %
                        nbOfSaturationsError            = 0, # %
                        nbOfPositiveSaturationsError    = 0, # %
                        nbOfNegativeSaturationError     = 0, # %
                        badEventPeakError               = 0, # %
                        badEventPileUpError             = 0, # %
                        badEventTOTlimiReachedError     = 0, # %
                    )
    return myTest