import epics    # caget caput
import time     # time elapsed
import sys

from non_regression_test_define import *  # pv name and defines

# secure caget and caput (blocant functions with read back)
sys.path.insert(0, PATH_M_EPICS_NBM + "misc")
from epicsFunctions import *

# import nblm modules
simulationPath      = PATH_M_EPICS_NBM + "misc/simulation/"
sys.path.insert(0, simulationPath)
import config as GBL            # nblm global variables
from nblmlib import functions   # nblm fcts

def myCaputCustom (pv, value, pvReadBack):
    """ execute a secured caput. In case of error, push error to status PV before exiting the script"""
    status = caput(pv, value, pvReadBack)
    # status:
    # 0: 
    # 1: ok
    # 2: pv connection failed
    # 3: time out caput
    if status == STATUS_OK:
        # caput went ok
        return True
    else:
        # error
        # end of the test
        myCaput(PV_unitTestPVconnectionFailed,  1, PV_unitTestPVconnectionFailed)
        myCaput(PV_unitTestRunning,             0, PV_unitTestRunning)
        if status == STATUS_PV_CONNECTION_FAILED:
            exit("PV connection failed: " + str(pv))
        elif status == STATUS_TIME_OUT:
            exit("Time out caput: " + str(pv))

        return False # useless

def myCagetCustom (pv):
    """ secured caget """
    result, status = caget(pv)

    if status != STATUS_OK:
        # end of the test
        myCaput(PV_unitTestPVconnectionFailed,  1, PV_unitTestPVconnectionFailed)
        myCaput(PV_unitTestRunning,             0, PV_unitTestRunning)
        exit("PV connection failed or time out caget: " + str(pv))

    return result
       

def myAssert(referenceValue, pv, error):
    """ check if value is in the range of the reference (pv) """
    # data from pulse processing from FPGA
    valueReceived = myCagetCustom(pv) 

    # min and max accepted value depending on reference and authorised error
    authorisedError = referenceValue * error/100.0
    a = referenceValue - authorisedError
    b = referenceValue + authorisedError
    # a and b could be either + or -.
    minRange = min(a,b)
    maxRange = max(a,b)

    # precision
    PRINT_PRECISION = 1
    endString = "\t" + str(round(minRange,PRINT_PRECISION)) + "\t" + str(round(valueReceived,PRINT_PRECISION)) + "\t" + str(round(maxRange,PRINT_PRECISION)) + "\t" + str(error) + "\t" + pv.pvname
    if (valueReceived >= minRange) and (valueReceived <= maxRange):
        # valueReceived is in the range of the reference
        print bcolors.OKGREEN + "PASSED" + bcolors.ENDC + endString
        return True
    else:
        # valueReceived is NOT in the range of the reference
        print bcolors.FAIL + "FAILED" + bcolors.ENDC + endString
        return False

def scopeReady(startFunction):
    """ check scope is ready. If not, force it to be ready (trig on "EPICS_DEMAND") """
    if myCagetCustom(PV_armScope) == SCOPE_RUNNING:
        print "Force the scope to trig"
        # scope one time (and not multi trig)
        myCaputCustom(PV_scopeReadContinuous,     0,    PV_scopeReadContinuous)     # one scope
        # force to trig on "EPICS demand"
        myCaputCustom(PV_scopeTriggerListSelect,  0,    PV_scopeTriggerListSelect)  # list 0
        myCaputCustom(PV_scopeTriggerList1,       1,    PV_scopeTriggerList1)       # EPICS demand
        # waiting for "on" state (needed because of CSS button)
        while (myCagetCustom(PV_armScope) != SCOPE_ON) and ((time.time()-startFunction) <= TIMEOUT_TEST): # waiting for "on" state
            pass
    if (time.time()-startFunction) > TIMEOUT_TEST:
        myCaputCustom(PV_unitTestRunning, 0,      PV_unitTestRunning) # test finished
        exit("? time out ?")
        return False
    else:
        return True

def synchroTest():
    """ perform synchro test: check the counter1us max delta between channels.
        return:
            0: IDLE
            1: PASSED
            2. FAILED """
    # time elapsed
    startFunction = time.time()
    print "start pulse processing synchonisation test", functions.elapsedTime(startFunction, GBL.START_TIME)
    print "[TEST IN DEVELOPMENT]"

    # coutinuous mode (not one shot) => pulse processing is running
    myCaputCustom(PV_fileReadermode,  1, PV_fileReadermode)

    # config trigger pulse processing and value
    myCaputCustom(PV_timingSource,    0,    PV_timingSource)    # soft trigger
    myCaputCustom(PV_timingFrequency, 14,   PV_timingFrequency) # 14Hz

    # tempo (time to pulse processing to run)
    time.sleep(0.1) # 100ms > 71ms

    # Pulse Processing integration pulse duration
    integration_chFR    = myCagetCustom(PV_counter1usCHFR)
    integration_ch1     = myCagetCustom(PV_counter1usCH0)
    integration_ch2     = myCagetCustom(PV_counter1usCH1)
    integration_ch3     = myCagetCustom(PV_counter1usCH2)
    integration_ch4     = myCagetCustom(PV_counter1usCH3)
    integration_ch5     = myCagetCustom(PV_counter1usCH4)
    integration_ch6     = myCagetCustom(PV_counter1usCH5)
    integration_ch7     = myCagetCustom(PV_counter1usCH6)
    integration_ch8     = myCagetCustom(PV_counter1usCH7)
    # display PP integration
    print "chFR",   integration_chFR,   "us"
    print "ch1",    integration_ch1,    "us"
    print "ch2",    integration_ch2,    "us"
    print "ch3",    integration_ch3,    "us"
    print "ch4",    integration_ch4,    "us"
    print "ch5",    integration_ch5,    "us"
    print "ch6",    integration_ch6,    "us"
    print "ch7",    integration_ch7,    "us"
    print "ch8",    integration_ch8,    "us"

    # min max counter 1us
    minCounter = min(   integration_chFR,
                        integration_ch1,
                        integration_ch2,
                        integration_ch3,
                        integration_ch4,
                        integration_ch5,
                        integration_ch6,
                        integration_ch7,
                        integration_ch8)
    maxCounter = max(   integration_chFR,
                        integration_ch1,
                        integration_ch2,
                        integration_ch3,
                        integration_ch4,
                        integration_ch5,
                        integration_ch6,
                        integration_ch7,
                        integration_ch8)
    print "Min:", minCounter, "us, max:", maxCounter

    # end of the test
    endOfTest = "..end pulse processing synchonisation test", functions.elapsedTime(startFunction, GBL.START_TIME)

    MAX_DELTA = 50 # us
    # display results in CSS
    if (time.time()-startFunction) >= TIMEOUT_TEST:
        # time out
        print bcolors.WARNING + "Time out !" + bcolors.ENDC + "\n" + endOfTest
        return TIME_OUT
    if (maxCounter-minCounter) > MAX_DELTA:
        # synchro fail
        print bcolors.FAIL + "FAILED" + bcolors.ENDC + "\t" + str(maxCounter) + "us - " + str(minCounter) + "us = " + str(maxCounter-minCounter) + "us > " + str(MAX_DELTA) + "us"
        print endOfTest
        return FAILED
    else:
        print bcolors.OKGREEN + "PASSED" + bcolors.ENDC + "\t" + str(maxCounter) + "us - " + str(minCounter) + "us = " + str(maxCounter-minCounter) + "us > " + str(MAX_DELTA) + "us"
        print endOfTest
        return SUCCES


def do_test(myTest):
    """ perform the test of one file
        return:
            0: IDLE
            1: PASSED
            2. FAILED """
    # time elapsed
    startFunction = time.time()
    print "start test %d: %s.. (%f s)" %(myTest.number, myTest.name, time.time()-startFunction)
    print " - file:", myTest.file
    print " - file characteristics:", myTest.description

    # check the scope is not running (from previous config)
    scopeReady(startFunction)

    # scope one time
    myCaputCustom(PV_scopeReadContinuous, 0, PV_scopeReadContinuous)            # one shot scope

    # FR mode, select file
    myCaputCustom(PV_UnitTestSelection, myTest.number, PV_UnitTestSelection)    # select file and fill file reader with it
    time.sleep(1) # debug/test: diver takes time to read, add padding and load file in FPGA 

    # config scope
    myCaputCustom(PV_scopeTriggerPost,        myTest.scopeTriggerPost,        PV_scopeTriggerPost_RB)
    myCaputCustom(PV_scopeTriggerValue,       myTest.scopeTriggerValue,       PV_scopeTriggerValue_RB) 
    myCaputCustom(PV_scopeTriggerListSelect,  myTest.scopeTriggerListSelect,  PV_scopeTriggerListSelect) 
    if myTest.scopeTriggerListSelect == 0:
        # list 1
        myCaputCustom(PV_scopeTriggerList1,       myTest.scopeTriggerList1,       PV_scopeTriggerList1) 
    else:
        # list 2
        myCaputCustom(PV_scopeTriggerList2,       myTest.scopeTriggerList2,       PV_scopeTriggerList2) 

    # neutron detection PV config
    myCaputCustom(PV_neutronChargeMPV,      myTest.chargeMPV,               PV_neutronChargeMPV_RB)
    myCaputCustom(PV_pedestalLevel,         myTest.pedestal,                PV_pedestalLevel_RB)
    myCaputCustom(PV_eventDetectionTh,      myTest.eventDetectionThreshold, PV_eventDetectionTh_RB)
    myCaputCustom(PV_neutronAmplitudeMin,   myTest.neutronAmplitudeMin,     PV_neutronAmplitudeMin_RB)
    myCaputCustom(PV_neutronTOTmin,         myTest.neutronTOTmin,           PV_neutronTOTmin_RB)
    myCaputCustom(PV_TOTpileUpStart,        myTest.TOTpileUpStart,          PV_TOTpileUpStart_RB)
    myCaputCustom(PV_TOTpileUpLimit,        myTest.TOTpileUpLimit,          PV_TOTpileUpLimit_RB)

    # continuous mode: the goal is to have the file read several times before one shot test => same pedestal and config
    # myCaputCustom(PV_fileReadermode,  1, PV_fileReadermode)
    # time.sleep(0.5) # tempo needed: time to read least onetime the file

    # FR: on shot mode (read time one time)
    myCaputCustom(PV_fileReadermode,  0, PV_fileReadermode)

    # play file and arm scope
    myCaputCustom(PV_armScope, SCOPE_RUNNING, PV_armScope) # arm the scope ("RUNNING")
    # diver is doing a neutron detection reset (chFR only)) at this time
    # wait for SCOPE_RUNNING state
    while myCagetCustom(PV_armScope) != SCOPE_RUNNING:
        pass

    # waiting for "on" state (needed because of CSS button)
    while (myCagetCustom(PV_armScope) != SCOPE_ON) and ((time.time()-startFunction) <= TIMEOUT_TEST): # waiting for "on" state
        pass

    # check timeout
    if (time.time()-startFunction) > TIMEOUT_TEST:
        exit("!!!!! time out !!!!")

    # tempo needed ? (driver takes time to push data into PP PVs)
    time.sleep(0.5)

    # get results from FPGA
    print "\tMin\tValue\tMax\t% error" # print for enjoyable read
    a = myAssert(myTest.neutronCounterRef,          PV_neutronCounter,          myTest.neutronCounterError)
    b = myAssert(myTest.neutronChargeRef,           PV_neutronCharge,           myTest.neutronChargeError)
    c = myAssert(myTest.badEventChargeRef,          PV_badEventCharge,          myTest.badEventChargeError)
    d = myAssert(myTest.nbOfPeaksRef,               PV_nbOfPeaks,               myTest.nbOfPeaksError)
    e = myAssert(myTest.nbOfPileUpRef,              PV_nbOfPileUp,              myTest.nbOfPileUpError)
    f = myAssert(myTest.nbOfTOTlimiReachedRef,      PV_nbOfTOTlimiReached,      myTest.nbOfTOTlimiReachedError)
    g = myAssert(myTest.nbOfSaturationsRef,         PV_nbOfSaturations,         myTest.nbOfSaturationsError)
    h = myAssert(myTest.nbOfPositiveSaturationsRef, PV_nbOfPositiveSaturations, myTest.nbOfPositiveSaturationsError)
    i = myAssert(myTest.nbOfNegativeSaturationRef,  PV_nbOfNegativeSaturation,  myTest.nbOfNegativeSaturationError)
    j = myAssert(myTest.badEventPeak,               PV_badEventPeak,            myTest.badEventPeakError)
    k = myAssert(myTest.badEventPileUp,             PV_badEventPileUp,          myTest.badEventPileUpError)
    l = myAssert(myTest.badEventTOTlimiReached,     PV_badEventTOTlimiReached,  myTest.badEventTOTlimiReachedError)

    # end of the test
    endOfTest = "..end of the test %d: %s (%f s)\n" %(myTest.number, myTest.name, time.time()-startFunction)

    # display results in CSS
    if (time.time()-startFunction) >= TIMEOUT_TEST:
        # time out
        print bcolors.WARNING + "Time out !" + bcolors.ENDC + "\n" + endOfTest
        return TIME_OUT
    elif (a and b and c and d and e and f and g and h and i and j and k and l) == False:
        # at least one test failed
        print endOfTest
        return FAILED
    else:
        # all tests was passed
        print endOfTest
        return PASSED


def getPVconfig():
    """ save neutron detection config and scope config for restoring purpose"""
    print "Config saved before non-regression test"
    config = [
        # neutron detection config
        myCagetCustom(PV_neutronChargeMPV),
        myCagetCustom(PV_pedestalLevel),
        myCagetCustom(PV_eventDetectionTh),
        myCagetCustom(PV_neutronAmplitudeMin),
        myCagetCustom(PV_neutronTOTmin),
        myCagetCustom(PV_TOTpileUpStart),
        myCagetCustom(PV_TOTpileUpLimit),
        # scope config
        myCagetCustom(PV_scopeReadContinuous),
        myCagetCustom(PV_fileReadermode),
        myCagetCustom(PV_scopeTriggerPost),
        myCagetCustom(PV_scopeTriggerValue),
        myCagetCustom(PV_scopeTriggerPost_RB),
        myCagetCustom(PV_scopeTriggerValue_RB),
        myCagetCustom(PV_scopeTriggerListSelect),
        myCagetCustom(PV_scopeTriggerList1),
        myCagetCustom(PV_scopeTriggerList2),
        myCagetCustom(PV_UnitTestSelection)
    ]
    return config

def restoreConfig(config):
    """ restore neutron detection and scope config"""
    print "Config restored after non-regression test"
    # restore neutron detection config
    myCaputCustom(PV_neutronChargeMPV,    config[0],  PV_neutronChargeMPV_RB)
    myCaputCustom(PV_pedestalLevel,       config[1],  PV_pedestalLevel_RB)
    myCaputCustom(PV_eventDetectionTh,    config[2],  PV_eventDetectionTh_RB)
    myCaputCustom(PV_neutronAmplitudeMin, config[3],  PV_neutronAmplitudeMin_RB)
    myCaputCustom(PV_neutronTOTmin,       config[4],  PV_neutronTOTmin_RB)
    myCaputCustom(PV_TOTpileUpStart,      config[5],  PV_TOTpileUpStart_RB)
    myCaputCustom(PV_TOTpileUpLimit,      config[6],  PV_TOTpileUpLimit_RB)
    # restore scope config
    myCaputCustom(PV_scopeReadContinuous,     config[7],  PV_scopeReadContinuous)
    myCaputCustom(PV_fileReadermode,          config[8],  PV_fileReadermode)
    myCaputCustom(PV_scopeTriggerPost,        config[9],  PV_scopeTriggerPost)
    myCaputCustom(PV_scopeTriggerValue,       config[10], PV_scopeTriggerValue)
    myCaputCustom(PV_scopeTriggerPost_RB,     config[11], PV_scopeTriggerPost_RB)
    myCaputCustom(PV_scopeTriggerValue_RB,    config[12], PV_scopeTriggerValue_RB)
    myCaputCustom(PV_scopeTriggerListSelect,  config[13], PV_scopeTriggerListSelect)
    myCaputCustom(PV_scopeTriggerList1,       config[14], PV_scopeTriggerList1)
    myCaputCustom(PV_scopeTriggerList2,       config[15], PV_scopeTriggerList2)
    myCaputCustom(PV_UnitTestSelection,       config[16], PV_UnitTestSelection)


def calculatedDataTest():
    """ test calculated data: compare python and FPGA result (charge MPV + TOT MPV + pedestal) """
    startFunction = time.time()
    print "start calculated data test (MPVs + pedestal)", functions.elapsedTime(startFunction, GBL.START_TIME)

    # check the scope is not running (from previous config)
    scopeReady(startFunction)

    # scope one time
    myCaputCustom(PV_scopeReadContinuous,     0,      PV_scopeReadContinuous)     # one scope (not multi scope)

    # FR mode, select file and one shot mode
    myCaputCustom(PV_UnitTestSelection, 6, PV_UnitTestSelection)  # select file and fill file reader: neutron peak with positive pedestal (pedestalPositive_p250mV_a-100mV_d100.txt)
    time.sleep(1) # debug/test: time to read, add padding and load file in FPGA 

    # config scope
    myCaputCustom(PV_scopeTriggerPost,        10000,  PV_scopeTriggerPost_RB)     # trig on the center of the window
    myCaputCustom(PV_scopeTriggerValue,       0,      PV_scopeTriggerValue_RB)    # whatever
    myCaputCustom(PV_scopeTriggerListSelect,  0,      PV_scopeTriggerListSelect)  # list 1
    myCaputCustom(PV_scopeTriggerList1,       4,      PV_scopeTriggerList1)       # on neutron peak

    # configure neutron detection
    myCaputCustom(PV_neutronChargeMPV,    0,      PV_neutronChargeMPV_RB) # wathever
    myCaputCustom(PV_pedestalLevel,       250,    PV_pedestalLevel_RB)
    myCaputCustom(PV_eventDetectionTh,    -3,     PV_eventDetectionTh_RB)
    myCaputCustom(PV_neutronAmplitudeMin, -10,    PV_neutronAmplitudeMin_RB)
    myCaputCustom(PV_neutronTOTmin,       40,     PV_neutronTOTmin_RB)
    myCaputCustom(PV_TOTpileUpStart,      300,    PV_TOTpileUpStart_RB)
    myCaputCustom(PV_TOTpileUpLimit,      1000,   PV_TOTpileUpLimit_RB)

    # configure MPV et pedestal calculation
    myCaputCustom(PV_nbOfDataToComputeMPV         , 1,    PV_nbOfDataToComputeMPV)        # only one neutron peak
    myCaputCustom(PV_nbOfDataToComputePedestal    , 20,   PV_nbOfDataToComputePedestal)

    # continuous mode: the goal is to have the file read several times before one shot test => same pedestal and config
    # myCaputCustom(PV_fileReadermode,  1, PV_fileReadermode)

    # tempo needed ? (time to read least onetime the file)
    # time.sleep(0.1)

    # on shot mode
    myCaputCustom(PV_fileReadermode,  0, PV_fileReadermode)

    # play file and arm scope
    myCaputCustom(PV_armScope, 7, PV_armScope) # arm the scope ("RUNNING")
    # FR + one shot: THIS COMMAND TRIGGER A RESET ON FPGA (MANDATORY for reset flip flop (containing event...))
    # waiting for "on" state (needed because of CSS button)
    while (myCagetCustom(PV_armScope) != SCOPE_ON) and ((time.time()-startFunction) <= TIMEOUT_TEST): # waiting for "on" state
        pass

    # check timeout
    if (time.time()-startFunction) > TIMEOUT_TEST:
        exit("!!!!! time out !!!!")

    # tempo needed ? (driver takes time to push data into PVs)
    time.sleep(0.5)

    ## check result
    ERROR_CHARGE_MPV    = 1 # %
    ERROR_TOT_MPV       = 1 # %
    ERROR_PEDESTAL      = 1 # % 

    # reference
    chargeMPVref    = -1485 # Q
    TOT_MPVref      = 128   # ns
    pedestalRef     = 250   # mV

    print "\tMin\tValue\tMax\t% error" # print for enjoyable read
    # check charge MPV
    a = myAssert(chargeMPVref,  PV_MPVchargeAdviced,    ERROR_CHARGE_MPV)
    # check TOT MPV
    b = myAssert(TOT_MPVref,    PV_MPV_TOTadviced,      ERROR_TOT_MPV)
    # check pedestal MPV
    c = myAssert(pedestalRef,   PV_pedestalAdviced,     ERROR_PEDESTAL)

    print "..end of the calculated data test (MPVs + pedestal)", functions.elapsedTime(startFunction, GBL.START_TIME), "\n"

    if (a and b and c) == True:
        return PASSED
    elif (time.time()-startFunction) > TIMEOUT_TEST:
        return TIME_OUT
    else:
        return FAILED

def processResult(result, pvTestResult, inititalConfig):
    """ manage user interface (leds), may restore the config depending on the user config and if the test fails"""
    # result
    myCaputCustom(pvTestResult, result, pvTestResult)

    # in case of failed or time out
    if myCagetCustom(PV_unitTestStopWhenFailed) == 1 and (result == FAILED or result == TIME_OUT):
        # restore config if asked by user
        if myCagetCustom(PV_unitTestRestoreConfig) == 1:
            restoreConfig(inititalConfig)

        # end of test, display in CSS
        myCaputCustom(PV_unitTestRunning, 0,      PV_unitTestRunning) # test finished
        exit()


