from non_regression_test_define import *

def test0():
    myTest = t_test(    
                        # general information
                        number                      = 0,
                        name                        = "neutron peak test",
                        description                 = "1 neutron peak, pedestal=0mV, amp=-100mV, duration=100ns",
                        file                        = "neutronPeak_p0mV_a-100mV_d100.txt",
                        # neutron detection config
                        chargeMPV                   = -1485,    # Q
                        pedestal                    = 0,        # mV
                        eventDetectionThreshold     = -3,       # mV
                        neutronAmplitudeMin         = -10,      # mV
                        neutronTOTmin               = 40,       # s
                        TOTpileUpStart              = 300,      # s
                        TOTpileUpLimit              = 1000,      # s        
                        # scope 
                        scopeTriggerPost            = 10000, # event in the center of the window
                        scopeTriggerValue           = 0,    # whatever
                        scopeTriggerListSelect      = 0,    # list 1 selected
                        scopeTriggerList1           = 4,    # on neutron peak
                        scopeTriggerList2           = 0,    # whatever
                        # reference: what the output of pulse processing should be
                        neutronCounterRef           = 1,    # neutrons
                        neutronChargeRef            = -1492,# Q
                        badEventChargeRef           = 0,    # Q
                        nbOfPeaksRef                = 1,    
                        nbOfPileUpRef               = 0,    
                        nbOfTOTlimiReachedRef       = 0,    
                        nbOfSaturationsRef          = 0, 
                        nbOfPositiveSaturationsRef  = 0,
                        nbOfNegativeSaturationRef   = 0,
                        badEventPeak                = 0,
                        badEventPileUp              = 0,
                        badEventTOTlimiReached      = 0,
                        # error authorised in %
                        neutronCounterError             = 0, # %
                        neutronChargeError              = 1, # %
                        badEventChargeError             = 0, # %
                        nbOfPeaksError                  = 0, # %
                        nbOfPileUpError                 = 0, # %
                        nbOfTOTlimiReachedError         = 0, # %
                        nbOfSaturationsError            = 0, # %
                        nbOfPositiveSaturationsError    = 0, # %
                        nbOfNegativeSaturationError     = 0, # %
                        badEventPeakError               = 0, # %
                        badEventPileUpError             = 0, # %
                        badEventTOTlimiReachedError     = 0, # %
                    )
    return myTest