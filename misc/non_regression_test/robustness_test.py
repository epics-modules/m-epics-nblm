# the goal of this script is to test the robustness of FPGA neutron detection based on non-regression test.
import time

# import robutness lib
from robustness_test_lib import *

# robustness test: running state
set_test_state(ROBUSTNESS_TEST_RUNNING)

# print selected mode (simu or real)
display_selected_mode()

# init
timeLeft, nbOfPassed = init()

# get config
nbOfExecution, generateInputFile = get_config()

# matrix results
matrixresults   = np.zeros((NUMBER_OF_NON_REGRESSION_TESTS, nbOfExecution), dtype=int)

# duration of every test
testDuration = np.zeros(nbOfExecution, dtype=float)

# generate input files (for FPGA) to be sure they exist
generate_input_files(True)

# get time
startScript = get_time()

# start tests
for testNumber in range(nbOfExecution):

    # iteration duation
    startTest = time.time()

    # display non regression test config
    display_test_config(testNumber, nbOfExecution)

    # generate input files (for FPGA)
    generate_input_files(generateInputFile)

    # start non regression test
    non_regression_test()

    # display and save results in matrix
    result, iterationResults, nbOfPassed    = save_test(nbOfPassed, nbOfExecution)
    matrixresults[:, testNumber]            = iterationResults

    # update estimation of script duration
    timeLeft, testDuration[testNumber] = update_execution_time_estimation(startScript, startTest, testNumber, nbOfExecution)

    # check if user asked for a stop or a failed (configurable)
    stop = check_stop(result)
    if stop:
        break

# robustness test result
testResult = get_test_result(matrixresults, nbOfExecution, startScript)
set_test_state(testResult)

# display robustness test results
# display_matrix(matrixresults, NUMBER_OF_NON_REGRESSION_TESTS, nbOfExecution)
plot_matrix(matrixresults, testDuration, nbOfExecution)
