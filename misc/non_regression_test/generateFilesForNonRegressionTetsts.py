import numpy as np              # vector operation
import matplotlib.pyplot as plt # plot
import os                       # check if directory/file exists
import sys                      # import python module with path
import subprocess               # execute bash command (copy files to IFC1410)

# path m-epics-nblm
# PATH_M_EPICS_NBLM   = "/home/vnadot/mnt/devspace/m-epics-nblm/" # vnadot 
PATH_M_EPICS_NBLM   = "/home/iocuser/devspace/m-epics-nblm/"    # MTCA CT, iocuser
# PATH_M_EPICS_NBLM   = "/home/vnadot/development/m-epics-nblm/" # vnadot 

# simulation configuration
# sys.path.insert(0, "/home/vnadot/development/m-epics-nblm/misc/simulation")
sys.path.insert(0, PATH_M_EPICS_NBLM + "misc/simulation")
import config as GBL # global variables

# simulation configuration
import config as GBL                    # global variables

# my modules
from nblmlib import functions           # my functions for nBLM simulation
from nblmlib import dataIn              # input file for simulation
from nblmlib import dataOut             # save simulation results
from nblmlib import neutronDetection    # pre-processing and detects neutrons
from nblmlib import analysis            # pedestal calculation,  MPV calculation, sparks and saturation detection, stability ...                            
from nblmlib import plotUtils           # plot raw data and specific events


def fillDPRAM(inputSignal, value):
    """ at value at the end of the signal to fill DPRAM file reader"""
    lenInput = len(inputSignal)
    sizeDPRAM = 4*4096 # 4*4096 = DPRAM size for file reader
    if lenInput < sizeDPRAM:
        outputSignal = np.concatenate((inputSignal, value*np.ones(sizeDPRAM-lenInput)), 0)
    else:
        print "[WARNING]: padding impossible because input signal is larger than DPRAM"
    # plt.plot(outputSignal)
    # plt.show()
    return outputSignal

def generate_files_for_non_regression_tests():
    #### neutron peak ####
    # output file name
    GBL.GENERATED_FILE_NAME = "neutronPeak_p0mV_a-100mV_d100"
    fileName                = GBL.PATH_RAW_DATA_FILES + GBL.GENERATED_FILE_NAME + ".txt"
    rawData, rawDataMask = dataIn.generate_neutron_signal(  nbOfNeutrons                = 1,    # 1 neutron peak
                                                            amplitudeGaussian           = -100, # mV
                                                            sigmaGaussian               = 0,    # mV. Note that his parameter could be overwritten by the script to generate only positive neutron 
                                                            meanSamplesBetween2Neutrons = 300,  # samples   
                                                            variationBetween2Neutrons   = 10,   # samples. position variation amplitude
                                                            pedestal                    = 0,    # mV
                                                            noiseAmplitude              = 0,    # mV !!! If you put some noise on the signal, every time you generate the signal, its is different. The noise can create bad events. I recommand to NOT use noise for non regression tests !!!
                                                            sparks                      = False)
    # add padding at the end (to fill file reader) (to fill file reader)
    rawData = fillDPRAM(inputSignal = rawData, value = 0)
    dataOut.list_rawdata_to_file_rawdata(fileName, rawData)

    #### neutron pile-up ####
    # output file name
    GBL.GENERATED_FILE_NAME = "neutronPileUp_p0mV_a-100mV_d500"
    fileName                = GBL.PATH_RAW_DATA_FILES + GBL.GENERATED_FILE_NAME + ".txt"
    rawData, rawDataMask = dataIn.generate_neutron_signal(  nbOfNeutrons                = 6,    # 6 neutron peaks
                                                            amplitudeGaussian           = -100, # mV
                                                            sigmaGaussian               = 0,    # mV. Note that his parameter could be overwritten by the script to generate only positive neutron 
                                                            meanSamplesBetween2Neutrons = 20,   # samples => pile-up   
                                                            variationBetween2Neutrons   = 10,   # samples. position variation amplitude
                                                            pedestal                    = 0,    # mV
                                                            noiseAmplitude              = 0,    # mV !!! If you put some noise on the signal, every time you generate the signal, its is different. The noise can create bad events. I recommand to NOT use noise for non regression tests !!!
                                                            sparks                      = False)
    # add padding at the end (to fill file reader) (to fill file reader)
    rawData = fillDPRAM(inputSignal = rawData, value = 0)
    dataOut.list_rawdata_to_file_rawdata(fileName, rawData)

    #### neutron TOT limit reached ####
    # output file name
    GBL.GENERATED_FILE_NAME = "neutronTOTlimitReached_p0mV_a-100mV_d2400"
    fileName                = GBL.PATH_RAW_DATA_FILES + GBL.GENERATED_FILE_NAME + ".txt"
    rawData, rawDataMask = dataIn.generate_neutron_signal(  nbOfNeutrons                = 30,   # 30 neutron peaks
                                                            amplitudeGaussian           = -100, # mV
                                                            sigmaGaussian               = 0,    # mV. Note that his parameter could be overwritten by the script to generate only positive neutron 
                                                            meanSamplesBetween2Neutrons = 20,   # samples => pile-up / TOT limit reached  
                                                            variationBetween2Neutrons   = 10,   # samples. position variation amplitude
                                                            pedestal                    = 0,    # mV
                                                            noiseAmplitude              = 0,    # mV !!! If you put some noise on the signal, every time you generate the signal, its is different. The noise can create bad events. I recommand to NOT use noise for non regression tests !!!
                                                            sparks                      = False)
    # add padding at the end (to fill file reader) (to fill file reader)
    rawData = fillDPRAM(inputSignal = rawData, value = 0)
    dataOut.list_rawdata_to_file_rawdata(fileName, rawData)



    #### bad event peak ####
    # output file name
    GBL.GENERATED_FILE_NAME = "badEventPeak_p0mV_a-5mV_d80"
    fileName                = GBL.PATH_RAW_DATA_FILES + GBL.GENERATED_FILE_NAME + ".txt"
    rawData, rawDataMask = dataIn.generate_neutron_signal(  nbOfNeutrons                = 1,    # 1 neutron peak
                                                            amplitudeGaussian           = -5,   # mV
                                                            sigmaGaussian               = 0,    # mV. Note that his parameter could be overwritten by the script to generate only positive neutron 
                                                            meanSamplesBetween2Neutrons = 15,  # samples   
                                                            variationBetween2Neutrons   = 0,   # samples. position variation amplitude
                                                            pedestal                    = 0,    # mV
                                                            noiseAmplitude              = 0,    # mV !!! If you put some noise on the signal, every time you generate the signal, its is different. The noise can create bad events. I recommand to NOT use noise for non regression tests !!!
                                                            sparks                      = False)
    # add padding at the end (to fill file reader) (to fill file reader)
    rawData = fillDPRAM(inputSignal = rawData, value = 0)
    dataOut.list_rawdata_to_file_rawdata(fileName, rawData)

    #### bad event pile-up ####
    # output file name
    GBL.GENERATED_FILE_NAME = "badEventPile-up_p0mV_a-5mV_d500"
    fileName                = GBL.PATH_RAW_DATA_FILES + GBL.GENERATED_FILE_NAME + ".txt"
    rawData, rawDataMask = dataIn.generate_neutron_signal(  nbOfNeutrons                = 6,    # 6 neutron peaks
                                                            amplitudeGaussian           = -5,   # mV
                                                            sigmaGaussian               = 0,    # mV. Note that his parameter could be overwritten by the script to generate only positive neutron 
                                                            meanSamplesBetween2Neutrons = 15,   # samples => pile-up   
                                                            variationBetween2Neutrons   = 0,   # samples. position variation amplitude
                                                            pedestal                    = 0,    # mV
                                                            noiseAmplitude              = 0,    # mV !!! If you put some noise on the signal, every time you generate the signal, its is different. The noise can create bad events. I recommand to NOT use noise for non regression tests !!!
                                                            sparks                      = False)
    # add padding at the end (to fill file reader) (to fill file reader)
    rawData = fillDPRAM(inputSignal = rawData, value = 0)
    dataOut.list_rawdata_to_file_rawdata(fileName, rawData)

    #### bad event TOT limit reached ####
    # output file name
    GBL.GENERATED_FILE_NAME = "badEventTOTlimitReached_p0mV_a-5mV_d2400"
    fileName                = GBL.PATH_RAW_DATA_FILES + GBL.GENERATED_FILE_NAME + ".txt"
    rawData, rawDataMask = dataIn.generate_neutron_signal(  nbOfNeutrons                = 30,   # 30 neutron peaks
                                                            amplitudeGaussian           = -5,   # mV
                                                            sigmaGaussian               = 0,    # mV. Note that his parameter could be overwritten by the script to generate only positive neutron 
                                                            meanSamplesBetween2Neutrons = 15,   # samples => pile-up / TOT limit reached  
                                                            variationBetween2Neutrons   = 0,   # samples. position variation amplitude
                                                            pedestal                    = 0,    # mV
                                                            noiseAmplitude              = 0,    # mV !!! If you put some noise on the signal, every time you generate the signal, its is different. The noise can create bad events. I recommand to NOT use noise for non regression tests !!!
                                                            sparks                      = False)
    # add padding at the end (to fill file reader) (to fill file reader)
    rawData = fillDPRAM(inputSignal = rawData, value = 0)
    dataOut.list_rawdata_to_file_rawdata(fileName, rawData)



    ### pedestal positive ###
    # output file name
    GBL.GENERATED_FILE_NAME = "pedestalPositive_p250mV_a-100mV_d100"
    fileName                = GBL.PATH_RAW_DATA_FILES + GBL.GENERATED_FILE_NAME + ".txt"
    rawData, rawDataMask = dataIn.generate_neutron_signal(  nbOfNeutrons                = 1,    # 1 neutron peak
                                                            amplitudeGaussian           = -100, # mV
                                                            sigmaGaussian               = 0,    # mV. Note that his parameter could be overwritten by the script to generate only positive neutron 
                                                            meanSamplesBetween2Neutrons = 300,  # samples   
                                                            variationBetween2Neutrons   = 10,   # samples. position variation amplitude
                                                            pedestal                    = 250,  # mV
                                                            noiseAmplitude              = 0,    # mV !!! If you put some noise on the signal, every time you generate the signal, its is different. The noise can create bad events. I recommand to NOT use noise for non regression tests !!!
                                                            sparks                      = False)
    # add padding at the end (to fill file reader) (to fill file reader)
    rawData = fillDPRAM(inputSignal = rawData, value = 250)
    dataOut.list_rawdata_to_file_rawdata(fileName, rawData)

    ### pedestal negative ###
    # output file name
    GBL.GENERATED_FILE_NAME = "pedestalNegative_p-250mV_a-100mV_d100"
    fileName                = GBL.PATH_RAW_DATA_FILES + GBL.GENERATED_FILE_NAME + ".txt"
    rawData, rawDataMask = dataIn.generate_neutron_signal(  nbOfNeutrons                = 1,    # 1 neutron peak
                                                            amplitudeGaussian           = -100, # mV
                                                            sigmaGaussian               = 0,    # mV. Note that his parameter could be overwritten by the script to generate only positive neutron 
                                                            meanSamplesBetween2Neutrons = 300,  # samples   
                                                            variationBetween2Neutrons   = 10,   # samples. position variation amplitude
                                                            pedestal                    = -250, # mV
                                                            noiseAmplitude              = 0,    # mV !!! If you put some noise on the signal, every time you generate the signal, its is different. The noise can create bad events. I recommand to NOT use noise for non regression tests !!!
                                                            sparks                      = False)
    # add padding at the end (to fill file reader) (to fill file reader)
    rawData = fillDPRAM(inputSignal = rawData, value = -250)
    dataOut.list_rawdata_to_file_rawdata(fileName, rawData)



    ### positive saturation ###
    pedestal = 0 # mV
    positiveSaturation = np.zeros(1000)
    # one saturation first 1us
    positiveSaturation[50] = GBL.SATURATION
    # several saturation in second 1us
    for i in xrange(20, 90, 10):
        positiveSaturation[250 + i] = GBL.SATURATION
    # continuous saturation
    for i in range(500):
        positiveSaturation[2*250 + i] = GBL.SATURATION
    fileName = GBL.PATH_RAW_DATA_FILES + "saturationPositive_p0mV_a500mV_d4" + ".txt"
    # add padding at the end (to fill file reader)
    rawData = fillDPRAM(inputSignal = positiveSaturation, value = pedestal)
    dataOut.list_rawdata_to_file_rawdata(fileName, rawData)
    # plt.plot(positiveSaturation)

    ### negative saturation ###
    pedestal = 0 # mV
    negativeSaturation = np.zeros(1000)
    # one saturation first 1us
    negativeSaturation[50] = - GBL.SATURATION
    # several saturation in second 1us
    for i in xrange(20, 90, 10):
        negativeSaturation[250 + i] = - GBL.SATURATION
    # continuous saturation
    for i in range(500):
        negativeSaturation[2*250 + i] = - GBL.SATURATION
    fileName = GBL.PATH_RAW_DATA_FILES + "saturationNegative_p0mV_a-500mV_d4" + ".txt"
    # add padding at the end (to fill file reader)
    rawData = fillDPRAM(inputSignal = negativeSaturation, value = pedestal)
    dataOut.list_rawdata_to_file_rawdata(fileName, rawData)
    # plt.plot(negativeSaturation)
    # plt.show()

def generate_nblm_all_cases():
    """ generate in one file all the possible cases (peak, neutrons, pile-up, saturation ...)"""
    # concatenates all files
    for i in xrange(0, len(fileList), 2):
        # select input file
        GBL.RAW_DATA_FILE = PATH_M_EPICS_NBLM + "misc/simulation/rawDataFiles/" + fileList[i] + ".txt"

        # check if file has been generated
        myFile = GBL.RAW_DATA_FILE
        if os.path.isfile(myFile) == False:
            exit("FAILED: wrong location, check the file exists. File path: " + str(myFile) + " .If the file doesn't exist, generate it by running 'm-epics-nblm/misc/non_regression_test/generateFilesForNonRegressionTetsts.py'")
        
        # get data from file
        rawData, rawDataMask, NB_OF_SAMPLES_ACQUIRED, duration = dataIn.get_data()

        # dataIn.get_data() add padding before and after signal to avoid bug in scope
        paddingSize_get_data = functions.get_padding_size()

        # remove padding (used to fill file reader but not usefull here)
        paddingFile = fileList[i+1] # fileList[i+1] = duration of signal without padding
        rawData = rawData[paddingSize_get_data: paddingFile + paddingSize_get_data]
        if i == 0:
            # first raw data (no concatenation)
            myRawData = rawData
        else:
            # concatenates with previous raw data
            myRawData = np.concatenate((myRawData, np.array(rawData)), 0)

    # add padding at the end (to fit file reader)
    lenMyRawData = len(myRawData)
    sizeDPRAM = 4*4096 # 4*4096 = DPRAM size for file reader
    if lenMyRawData < sizeDPRAM:
        myRawData = np.concatenate((myRawData, np.zeros(sizeDPRAM-lenMyRawData)), 0)

    # save all signals in one file
    GBL.GENERATED_FILE_NAME = "allEvents"
    fileName                = GBL.PATH_RAW_DATA_FILES + GBL.GENERATED_FILE_NAME + ".txt"
    dataOut.list_rawdata_to_file_rawdata(fileName, myRawData)

    # plot results
    plt.plot(myRawData, label="raw data generated")
    plt.title("All kind of possible shapes")
    plt.xlabel("samples")
    plt.ylabel("mV")
    plt.legend(loc='best')
    # plt.show()

# use all data
GBL.USE_DATA_UNDER_EVENT_THRESHOLD  = True

# several files: neutron peak, neutron pile, up, bad event, saturation...
generate_files_for_non_regression_tests()


# all files to concatenate
fileList =[
            "neutronPeak_p0mV_a-100mV_d100",                300, # d100 in ns, 300 in samples
            "neutronPileUp_p0mV_a-100mV_d500",              600,
            "neutronTOTlimitReached_p0mV_a-100mV_d2400",    1500,
            "badEventPeak_p0mV_a-5mV_d80",                  300,
            "badEventPile-up_p0mV_a-5mV_d500",              600,
            "badEventTOTlimitReached_p0mV_a-5mV_d2400",     1500,
            # "pedestalPositive_p250mV_a-100mV_d100",         300,
            # "pedestalNegative_p-250mV_a-100mV_d100",        300,
            "saturationPositive_p0mV_a500mV_d4",            2000,
            "saturationNegative_p0mV_a-500mV_d4",           2000
]
# one file with all cases concatenated
generate_nblm_all_cases()


# copy file to IFC1410
folderSource        = GBL.PATH_RAW_DATA_FILES
folderDestination   = "/export/nfsroots/ifc1410-ess-20170314/home/root/ifc1410_nBLM_proto/data/"
print "\ncopy data file to IFC1410, from", folderSource, "to", folderDestination
cmd = "\cp " + folderSource + "* " + folderDestination #  \cp: overwrite if the file exist
print "executed shell command:", cmd
subprocess.call(cmd, shell=True)
