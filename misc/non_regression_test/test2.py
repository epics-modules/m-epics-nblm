from non_regression_test_define import *

def test2():
    myTest = t_test(    
                        # general information
                        number                      = 2,
                        name                        = "neutron TOT limit reached test",
                        description                 = "2 TOT limit reached, pedestal=0mV, amp=-100mV, duration=2200ns",
                        file                        = "neutronTOTlimitReached_p0mV_a-100mV_d2400.txt",
                        # neutron detection config
                        chargeMPV                   = -1447,    # Q
                        pedestal                    = 0,        # mV
                        eventDetectionThreshold     = -3,       # mV
                        neutronAmplitudeMin         = -10,      # mV
                        neutronTOTmin               = 40,       # s
                        TOTpileUpStart              = 300,      # s
                        TOTpileUpLimit              = 1000,      # s        
                        # scope 
                        scopeTriggerPost            = 10000, # event in the center of the window
                        scopeTriggerValue           = 0,    # whatever
                        scopeTriggerListSelect      = 0,    # list 1 selected
                        scopeTriggerList1           = 6,    # on neutron TOT limit reached
                        scopeTriggerList2           = 0,    # whatever
                        # reference: what the output of pulse processing should be
                        neutronCounterRef           = 31,    # neutrons
                        neutronChargeRef            = -45000, # Q
                        badEventChargeRef           = 0,# Q
                        nbOfPeaksRef                = 0,    
                        nbOfPileUpRef               = 3,    
                        nbOfTOTlimiReachedRef       = 2,    
                        nbOfSaturationsRef          = 0, 
                        nbOfPositiveSaturationsRef  = 0,
                        nbOfNegativeSaturationRef   = 0,
                        badEventPeak                = 0,
                        badEventPileUp              = 0,
                        badEventTOTlimiReached      = 0,
                        # error authorised in %
                        neutronCounterError             = 1, # % 
                        neutronChargeError              = 1, # %
                        badEventChargeError             = 1, # %
                        nbOfPeaksError                  = 0, # %
                        nbOfPileUpError                 = 0, # %
                        nbOfTOTlimiReachedError         = 0, # %
                        nbOfSaturationsError            = 0, # %
                        nbOfPositiveSaturationsError    = 0, # %
                        nbOfNegativeSaturationError     = 0, # %
                        badEventPeakError               = 0, # %
                        badEventPileUpError             = 0, # %
                        badEventTOTlimiReachedError     = 0, # %
                    )
    return myTest