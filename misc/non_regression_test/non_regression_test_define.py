import epics        # CA
import collections  # tuple (t_test)

# m-epics-nblm path
PATH_M_EPICS_NBM    = "/home/iocuser/devspace/m-epics-nblm/"
# PATH_M_EPICS_NBM    = "/home/vnadot/development/m-epics-nblm/"

import sys
sys.path.insert(0, PATH_M_EPICS_NBM + "misc")
from epicsFunctions import * # secure caget and caput (blocant function with read back)

# defines PV MACROs
PREFIX          = "IFC1410_nBLM"    #
DEVICE          = "PROTO"           # Defaut value
DEVICE_TUNING   = "PBI-nBLM"        # May be overwritten
CHANNEL         = "CH0"             # 

# constant
TIMEOUT_TEST    = 10 # sec

### PVs
# neutron detection configuration WRITE
PV_neutronChargeMPV         = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFCHARGEMPV")
PV_pedestalLevel            = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFPEDESTAL")
PV_eventDetectionTh         = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFEVENTDETECTIONTHRESHOLD")
PV_neutronAmplitudeMin      = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFNEUTRONAMPLITUDEMIN")
PV_neutronTOTmin            = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFNEUTRONTOTMIN")
PV_TOTpileUpStart           = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFTOTPILEUPSTART")
PV_TOTpileUpLimit           = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFTOTPILEUPLIMIT")
# neutron detection Read back
PV_neutronChargeMPV_RB      = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFCHARGEMPV-RB")
PV_pedestalLevel_RB         = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFPEDESTAL-RB")
PV_eventDetectionTh_RB      = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFEVENTDETECTIONTHRESHOLD-RB")
PV_neutronAmplitudeMin_RB   = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFNEUTRONAMPLITUDEMIN-RB")
PV_neutronTOTmin_RB         = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFNEUTRONTOTMIN-RB")
PV_TOTpileUpStart_RB        = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFTOTPILEUPSTART-RB")
PV_TOTpileUpLimit_RB        = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFTOTPILEUPLIMIT-RB")

# pulse processing READ
PV_neutronCounter           = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "NEUTRONCOUNTER1usSUM")
PV_neutronCharge            = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "NEUTRONCHARGE1usSUM")
PV_badEventCharge           = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "BADEVENTCHARGE1usSUM")
PV_nbOfPeaks                = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "NUMBEROFPEAKSUM")
PV_nbOfPileUp               = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "NUMBEROFPILEUPSUM")
PV_nbOfTOTlimiReached       = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "NUMBEROFTOTLIMITSUM")
PV_nbOfSaturations          = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "NUMBEROFSATURATIONSUM")
PV_nbOfPositiveSaturations  = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "NUMBEROFPOSSATURATIONSUM")
PV_nbOfNegativeSaturation   = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "NUMBEROFNEGSATURATIONSUM")
PV_badEventPeak             = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "BADEVTPEAKCOUNTER1usSUM")
PV_badEventPileUp           = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "BADEVTPILEUPCOUNTER1usSUM")
PV_badEventTOTlimiReached   = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "NBOFBADEVENTTOTLIMITSUM")

# CSS display
PV_unitTest0State               = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":UnitTest0State")
PV_unitTest1State               = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":UnitTest1State")
PV_unitTest2State               = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":UnitTest2State")
PV_unitTest3State               = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":UnitTest3State")
PV_unitTest4State               = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":UnitTest4State")
PV_unitTest5State               = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":UnitTest5State")
PV_unitTest6State               = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":UnitTest6State")
PV_unitTest7State               = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":UnitTest7State")
PV_unitTest8State               = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":UnitTest8State")
PV_unitTest9State               = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":UnitTest9State")
PV_unitTest10State              = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":UnitTest10State")
PV_unitTest11State              = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":UnitTest11State")
PV_unitTestRunning              = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":UnitTestRunning")
PV_unitTestPVconnectionFailed   = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":UnitTestPVconnectionFailed")
PV_unitTestStopWhenFailed       = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":UnitTestStopWhenFailed")
PV_unitTestRestoreConfig        = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":UnitTestRestoreConfig")

# do the test
PV_unitTest0MakeTest        = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":UnitTest0MakeTest")
PV_unitTest1MakeTest        = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":UnitTest1MakeTest")
PV_unitTest2MakeTest        = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":UnitTest2MakeTest")
PV_unitTest3MakeTest        = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":UnitTest3MakeTest")
PV_unitTest4MakeTest        = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":UnitTest4MakeTest")
PV_unitTest5MakeTest        = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":UnitTest5MakeTest")
PV_unitTest6MakeTest        = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":UnitTest6MakeTest")
PV_unitTest7MakeTest        = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":UnitTest7MakeTest")
PV_unitTest8MakeTest        = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":UnitTest8MakeTest")
PV_unitTest9MakeTest        = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":UnitTest9MakeTest")
PV_unitTest10MakeTest       = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":UnitTest10MakeTest")
PV_unitTest11MakeTest       = connectPV(PREFIX + ':' + DEVICE_TUNING + '-' + CHANNEL + ":UnitTest11MakeTest")

# test input file selection
PV_UnitTestSelection        = connectPV(PREFIX + ':' + DEVICE + "-NBLM_" + "UNITTESTSELECTION")

# scope
PV_scopeReadContinuous      = connectPV(PREFIX + ':' + DEVICE + "-NBLM_" + "READSCOPECONTINOUSLYCH0FR")
PV_fileReadermode           = connectPV(PREFIX + ':' + DEVICE + "-NBLM_" + "READFILECONTINOUSLY")
PV_armScope                 = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + '-' + "STAT")
PV_scopeRunning             = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "ACQUISITIONSCOPERUNCH0FR-RB")
PV_scopeTriggerPost         = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "SCOPEPOSTTRIGGER")
PV_scopeTriggerPost_RB      = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "SCOPEPOSTTRIGGER-RB")
PV_scopeTriggerValue        = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "TRIGGERVALUESCOPE")
PV_scopeTriggerValue_RB     = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "TRIGGERVALUESCOPE-RB")
PV_scopeTriggerListSelect   = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "TRIGGERSELECTIONSCOPELIST")
PV_scopeTriggerList1        = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "TRIGGERSELECTIONSCOPE1")
PV_scopeTriggerList2        = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "TRIGGERSELECTIONSCOPE2")

# calculated data from FPGA
PV_MPVchargeAdviced         = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "MPVCHARGEADVICED")
PV_MPV_TOTadviced           = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "MPVTOTADVICED")
PV_pedestalAdviced          = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "PEDESTALADVICED")
# nb of data to compute
PV_nbOfDataToComputeMPV     = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFNBOFDATAFORMPV")
PV_nbOfDataToComputePedestal= connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFNBOFWINDOWSFORPEDESTAL")

# test of syncho
PV_counter1usCHFR           = connectPV(PREFIX + ':' + DEVICE + '-' + "CH0" + '-' + "NBLMCOUNTER1us")
PV_counter1usCH0            = connectPV(PREFIX + ':' + DEVICE + '-' + "CH1" + '-' + "NBLMCOUNTER1us")
PV_counter1usCH1            = connectPV(PREFIX + ':' + DEVICE + '-' + "CH2" + '-' + "NBLMCOUNTER1us")
PV_counter1usCH2            = connectPV(PREFIX + ':' + DEVICE + '-' + "CH3" + '-' + "NBLMCOUNTER1us")
PV_counter1usCH3            = connectPV(PREFIX + ':' + DEVICE + '-' + "CH4" + '-' + "NBLMCOUNTER1us")
PV_counter1usCH4            = connectPV(PREFIX + ':' + DEVICE + '-' + "CH5" + '-' + "NBLMCOUNTER1us")
PV_counter1usCH5            = connectPV(PREFIX + ':' + DEVICE + '-' + "CH6" + '-' + "NBLMCOUNTER1us")
PV_counter1usCH6            = connectPV(PREFIX + ':' + DEVICE + '-' + "CH7" + '-' + "NBLMCOUNTER1us")
PV_counter1usCH7            = connectPV(PREFIX + ':' + DEVICE + '-' + "CH8" + '-' + "NBLMCOUNTER1us")
PV_timingSource             = connectPV(PREFIX + ':' + DEVICE + '-' + "NBLM" + '_' + "TIMINGSRC")
PV_timingFrequency          = connectPV(PREFIX + ':' + DEVICE + '-' + "NBLM" + '_' + "SOFTFREQ")


# test structure
t_test = collections.namedtuple	('t_test', [                
                                                "number",                                                
                                                "name",
                                                "description",
                                                "file",
                                                "chargeMPV",
                                                "pedestal",
                                                "eventDetectionThreshold",
                                                "neutronAmplitudeMin",
                                                "neutronTOTmin",
                                                "TOTpileUpStart",
                                                "TOTpileUpLimit",
                                                "scopeTriggerPost",
                                                "scopeTriggerValue",
                                                "scopeTriggerListSelect",
                                                "scopeTriggerList1",
                                                "scopeTriggerList2",
                                                "neutronCounterRef",
                                                "neutronChargeRef",
                                                "badEventChargeRef",
                                                "nbOfPeaksRef",
                                                "nbOfPileUpRef",
                                                "nbOfTOTlimiReachedRef",
                                                "nbOfSaturationsRef",
                                                "nbOfPositiveSaturationsRef",
                                                "nbOfNegativeSaturationRef",
                                                "badEventPeak",
                                                "badEventPileUp",
                                                "badEventTOTlimiReached",
                                                "neutronCounterError",
                                                "neutronChargeError",
                                                "badEventChargeError",
                                                "nbOfPeaksError",
                                                "nbOfPileUpError",
                                                "nbOfTOTlimiReachedError",
                                                "nbOfSaturationsError",
                                                "nbOfPositiveSaturationsError",
                                                "nbOfNegativeSaturationError",
                                                "badEventPeakError",
                                                "badEventPileUpError",
                                                "badEventTOTlimiReachedError"
                                            ]
)

# color for print
class bcolors:
    HEADER      = '\033[95m'
    OKBLUE      = '\033[94m'
    OKGREEN     = '\033[92m'
    WARNING     = '\033[33m'
    FAIL        = '\033[91m'
    ENDC        = '\033[0m'
    BOLD        = '\033[1m'
    UNDERLINE   = '\033[4m'


# test states
IDLE    = 0
PASSED  = 1
FAILED  = 2
TIME_OUT= 3

# scope states
SCOPE_RUNNING   = 7
SCOPE_ON        = 4