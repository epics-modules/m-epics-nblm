from non_regression_test_define import *

def test3():
    myTest = t_test(    
                        # general information
                        number                      = 3,
                        name                        = "bad event peak test",
                        description                 = "1 bad event peak, pedestal=0mV, amp=-5mV, duration=100ns",
                        file                        = "badEventPeak_p0mV_a-5mV_d80.txt",
                        # neutron detection config
                        chargeMPV                   = 0,        # Q
                        pedestal                    = 0,        # mV
                        eventDetectionThreshold     = -3,       # mV
                        neutronAmplitudeMin         = -10,      # mV
                        neutronTOTmin               = 40,       # s
                        TOTpileUpStart              = 300,      # s
                        TOTpileUpLimit              = 1000,     # s        
                        # scope 
                        scopeTriggerPost            = 10000, # event in the center of the window
                        scopeTriggerValue           = 0,    # whatever
                        scopeTriggerListSelect      = 0,    # list 1 selected
                        scopeTriggerList1           = 10,   # on bad event peak
                        scopeTriggerList2           = 0,    # whatever
                        # reference: what the output of pulse processing should be
                        neutronCounterRef           = 0,    # neutrons
                        neutronChargeRef            = 0,    # Q
                        badEventChargeRef           = -51,    # Q
                        nbOfPeaksRef                = 0,    
                        nbOfPileUpRef               = 0,    
                        nbOfTOTlimiReachedRef       = 0,    
                        nbOfSaturationsRef          = 0, 
                        nbOfPositiveSaturationsRef  = 0,
                        nbOfNegativeSaturationRef   = 0,
                        badEventPeak                = 1,
                        badEventPileUp              = 0,
                        badEventTOTlimiReached      = 0,
                        # error authorised in %
                        neutronCounterError             = 0, # %
                        neutronChargeError              = 0, # %
                        badEventChargeError             = 10, # %
                        nbOfPeaksError                  = 0, # %
                        nbOfPileUpError                 = 0, # %
                        nbOfTOTlimiReachedError         = 0, # %
                        nbOfSaturationsError            = 0, # %
                        nbOfPositiveSaturationsError    = 0, # %
                        nbOfNegativeSaturationError     = 0, # %
                        badEventPeakError               = 0, # %
                        badEventPileUpError             = 0, # %
                        badEventTOTlimiReachedError     = 0, # %
                    )
    return myTest