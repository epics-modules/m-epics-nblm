# ------------------------------------------------------------------------------
# -- Company      :   CEA Saclay                                              --
# -- Laboratory   :   IRFU / DIS / LDISC                                      --
# --                                                                          --
# -- Project      :   nBLM, CEA test bench                                    --
# --                                                                          --
# -- Author       :   Victor Nadot - victor.nadot@cea.fr                      --
# --                                                                          --
# -- -----------------------------------------------------------------------  --
# -- Notes                                                                    --
# -- -----------------------------------------------------------------------  --
# --  - this script has to be launched from CSS                               --
# --  - it uses as much as possible RB to be sure the config is effective     --
# -- -----------------------------------------------------------------------  --

# all prints redirected to file
import sys
f = open('/home/iocuser/devspace/m-epics-nblm/misc/non_regression_test/non_regression_test_logs.txt', 'w')
# sys.stdout = f

from non_regression_test_lib import *
from test0 import *
from test1 import *
from test2 import *
from test3 import *
from test4 import *
from test5 import *
from test6 import *
from test7 import *
from test8 import *
from test9 import *

# sequence configuration
testSequence = [    test0(),
                    test1(),
                    test2(),
                    test3(),
                    test4(),
                    test5(),
                    test6(),
                    test7(),
                    test8(),
                    test9()]
# store result
pvResults = [   PV_unitTest0State,
                PV_unitTest1State,
                PV_unitTest2State,
                PV_unitTest3State,
                PV_unitTest4State,
                PV_unitTest5State,
                PV_unitTest6State,
                PV_unitTest7State,
                PV_unitTest8State,
                PV_unitTest9State]
numberOfTests = len(testSequence)
# enable test
enableTest = [  PV_unitTest0MakeTest, 
                PV_unitTest1MakeTest, 
                PV_unitTest2MakeTest, 
                PV_unitTest3MakeTest, 
                PV_unitTest4MakeTest, 
                PV_unitTest5MakeTest, 
                PV_unitTest6MakeTest, 
                PV_unitTest7MakeTest, 
                PV_unitTest8MakeTest, 
                PV_unitTest9MakeTest]

# init result (IDLE)
for i in range(numberOfTests):
    myCaputCustom(pvResults[i],   IDLE, pvResults[i]) 
myCaputCustom(PV_unitTest10State, IDLE, PV_unitTest10State)
myCaputCustom(PV_unitTest11State, IDLE, PV_unitTest11State)

# test is running, display in CSS
myCaputCustom(PV_unitTestRunning,             1,      PV_unitTestRunning)             # test running
myCaputCustom(PV_unitTestPVconnectionFailed,   0,      PV_unitTestPVconnectionFailed)   # no PV connection failed so far

# save config
inititalConfig = getPVconfig()

# pulse processing synchro test
if myCagetCustom(PV_unitTest11MakeTest) == 1: # test allowed
    result = synchroTest()
    processResult(result, PV_unitTest11State, inititalConfig)

# data calculated test (MPV + pedestal)
if myCagetCustom(PV_unitTest10MakeTest) == 1: # test allowed
    result = calculatedDataTest()
    processResult(result, PV_unitTest10State, inititalConfig)

# launch test on different shapes (neutrons, peaks, saturation, pedestal ...)
for i in range(numberOfTests):
    if myCagetCustom(enableTest[i]) == 1: # test enabled
        # test one file
        result = do_test(testSequence[i])

        # manage end of test (leds, user options ...)
        processResult(result, pvResults[i], inititalConfig)

# restore config if asked by user
if myCagetCustom(PV_unitTestRestoreConfig) == 1:
    restoreConfig(inititalConfig)

# end of test, display in CSS
myCaputCustom(PV_unitTestRunning, 0,      PV_unitTestRunning) # test finished

# close log file
f.close()
