import epics
import sys
import time


# get argument
print 'sys.argv: ', sys.argv
if len(sys.argv) > 1:
	# for i in xrange(1, len(sys.argv)):
		# print(sys.argv[i])

        button = epics.PV(sys.argv[1]) #pv name

        # trigger FPGA debug
        button.put('1')
        # make sure PV value is 1
        while(button.get() == 0):
                pass
        
        #wait 1sec
        time.sleep(1)

        # reset
        button.put('0')
        # make sure PV value is 0
        while(button.get() == 0):
                pass
else:
	print('No argument')
