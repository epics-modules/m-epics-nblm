## CEA module for nBLM

* nBLM analysis tool (python)

* GUIs: high/low voltage (gaz and neutron detection soon)

* EPICS database:

    * high and low voltage

    * (coming soon): gas

    * (coming soon): neutron detection (Lodz firmware)

* tools to plot Lodz raw data recorder in a fast way (only plotting events)
  
* EPICS driver for Lodz bitstream is in this [nblmapp/ module](https://bitbucket.org/europeanspallationsource/nblmapp/src/master/)

* ACT (Aquisition Chain Tester): neutron detection **prototype**

    * GUIs

    * bitsream

    * cmd to run IOC

    * non regression tests

    * robustness tests

    * tunning neutron detection

